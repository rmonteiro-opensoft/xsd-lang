package pt.opensoft.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import pt.opensoft.services.XpathGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXpathParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'/'", "','", "'return'", "'for'", "'$'", "'in'", "'some'", "'every'", "'satisfies'", "'if'", "'('", "')'", "'or'", "'and'", "'+'", "'-'", "'union'", "'|'", "'intersect'", "'except'", "'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'eq'", "'ne'", "'lt'", "'le'", "'gt'", "'ge'", "'is'", "'<<'", "'>>'", "'//'", "'not'", "'ns:'", "'sum'", "'count'", "'then'", "'else'", "'true()'", "'false()'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXpathParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXpathParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXpathParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXpath.g"; }



     	private XpathGrammarAccess grammarAccess;

        public InternalXpathParser(TokenStream input, XpathGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";
       	}

       	@Override
       	protected XpathGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDomainmodel"
    // InternalXpath.g:64:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // InternalXpath.g:64:52: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // InternalXpath.g:65:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalXpath.g:71:1: ruleDomainmodel returns [EObject current=null] : ( ( (lv_validationPath_0_0= ruleValidationPath ) ) otherlv_1= ';' ( (lv_elements_2_0= ruleXPath ) ) otherlv_3= ';' )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_validationPath_0_0 = null;

        EObject lv_elements_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:77:2: ( ( ( (lv_validationPath_0_0= ruleValidationPath ) ) otherlv_1= ';' ( (lv_elements_2_0= ruleXPath ) ) otherlv_3= ';' )* )
            // InternalXpath.g:78:2: ( ( (lv_validationPath_0_0= ruleValidationPath ) ) otherlv_1= ';' ( (lv_elements_2_0= ruleXPath ) ) otherlv_3= ';' )*
            {
            // InternalXpath.g:78:2: ( ( (lv_validationPath_0_0= ruleValidationPath ) ) otherlv_1= ';' ( (lv_elements_2_0= ruleXPath ) ) otherlv_3= ';' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXpath.g:79:3: ( (lv_validationPath_0_0= ruleValidationPath ) ) otherlv_1= ';' ( (lv_elements_2_0= ruleXPath ) ) otherlv_3= ';'
            	    {
            	    // InternalXpath.g:79:3: ( (lv_validationPath_0_0= ruleValidationPath ) )
            	    // InternalXpath.g:80:4: (lv_validationPath_0_0= ruleValidationPath )
            	    {
            	    // InternalXpath.g:80:4: (lv_validationPath_0_0= ruleValidationPath )
            	    // InternalXpath.g:81:5: lv_validationPath_0_0= ruleValidationPath
            	    {

            	    					newCompositeNode(grammarAccess.getDomainmodelAccess().getValidationPathValidationPathParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_validationPath_0_0=ruleValidationPath();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"validationPath",
            	    						lv_validationPath_0_0,
            	    						"pt.opensoft.Xpath.ValidationPath");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }

            	    otherlv_1=(Token)match(input,11,FOLLOW_4); 

            	    			newLeafNode(otherlv_1, grammarAccess.getDomainmodelAccess().getSemicolonKeyword_1());
            	    		
            	    // InternalXpath.g:102:3: ( (lv_elements_2_0= ruleXPath ) )
            	    // InternalXpath.g:103:4: (lv_elements_2_0= ruleXPath )
            	    {
            	    // InternalXpath.g:103:4: (lv_elements_2_0= ruleXPath )
            	    // InternalXpath.g:104:5: lv_elements_2_0= ruleXPath
            	    {

            	    					newCompositeNode(grammarAccess.getDomainmodelAccess().getElementsXPathParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_elements_2_0=ruleXPath();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_2_0,
            	    						"pt.opensoft.Xpath.XPath");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }

            	    otherlv_3=(Token)match(input,11,FOLLOW_5); 

            	    			newLeafNode(otherlv_3, grammarAccess.getDomainmodelAccess().getSemicolonKeyword_3());
            	    		

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleValidationPath"
    // InternalXpath.g:129:1: entryRuleValidationPath returns [EObject current=null] : iv_ruleValidationPath= ruleValidationPath EOF ;
    public final EObject entryRuleValidationPath() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValidationPath = null;


        try {
            // InternalXpath.g:129:55: (iv_ruleValidationPath= ruleValidationPath EOF )
            // InternalXpath.g:130:2: iv_ruleValidationPath= ruleValidationPath EOF
            {
             newCompositeNode(grammarAccess.getValidationPathRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValidationPath=ruleValidationPath();

            state._fsp--;

             current =iv_ruleValidationPath; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValidationPath"


    // $ANTLR start "ruleValidationPath"
    // InternalXpath.g:136:1: ruleValidationPath returns [EObject current=null] : ( ( (lv_path_0_0= RULE_ID ) ) (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )* ) ;
    public final EObject ruleValidationPath() throws RecognitionException {
        EObject current = null;

        Token lv_path_0_0=null;
        Token otherlv_1=null;
        Token lv_path_list_2_0=null;


        	enterRule();

        try {
            // InternalXpath.g:142:2: ( ( ( (lv_path_0_0= RULE_ID ) ) (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )* ) )
            // InternalXpath.g:143:2: ( ( (lv_path_0_0= RULE_ID ) ) (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )* )
            {
            // InternalXpath.g:143:2: ( ( (lv_path_0_0= RULE_ID ) ) (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )* )
            // InternalXpath.g:144:3: ( (lv_path_0_0= RULE_ID ) ) (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )*
            {
            // InternalXpath.g:144:3: ( (lv_path_0_0= RULE_ID ) )
            // InternalXpath.g:145:4: (lv_path_0_0= RULE_ID )
            {
            // InternalXpath.g:145:4: (lv_path_0_0= RULE_ID )
            // InternalXpath.g:146:5: lv_path_0_0= RULE_ID
            {
            lv_path_0_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_path_0_0, grammarAccess.getValidationPathAccess().getPathIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getValidationPathRule());
            					}
            					setWithLastConsumed(
            						current,
            						"path",
            						lv_path_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalXpath.g:162:3: (otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXpath.g:163:4: otherlv_1= '/' ( (lv_path_list_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,12,FOLLOW_7); 

            	    				newLeafNode(otherlv_1, grammarAccess.getValidationPathAccess().getSolidusKeyword_1_0());
            	    			
            	    // InternalXpath.g:167:4: ( (lv_path_list_2_0= RULE_ID ) )
            	    // InternalXpath.g:168:5: (lv_path_list_2_0= RULE_ID )
            	    {
            	    // InternalXpath.g:168:5: (lv_path_list_2_0= RULE_ID )
            	    // InternalXpath.g:169:6: lv_path_list_2_0= RULE_ID
            	    {
            	    lv_path_list_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            	    						newLeafNode(lv_path_list_2_0, grammarAccess.getValidationPathAccess().getPath_listIDTerminalRuleCall_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getValidationPathRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"path_list",
            	    							lv_path_list_2_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValidationPath"


    // $ANTLR start "entryRuleXPath"
    // InternalXpath.g:190:1: entryRuleXPath returns [EObject current=null] : iv_ruleXPath= ruleXPath EOF ;
    public final EObject entryRuleXPath() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXPath = null;


        try {
            // InternalXpath.g:190:46: (iv_ruleXPath= ruleXPath EOF )
            // InternalXpath.g:191:2: iv_ruleXPath= ruleXPath EOF
            {
             newCompositeNode(grammarAccess.getXPathRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXPath=ruleXPath();

            state._fsp--;

             current =iv_ruleXPath; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXPath"


    // $ANTLR start "ruleXPath"
    // InternalXpath.g:197:1: ruleXPath returns [EObject current=null] : ( (lv_expr_0_0= ruleExpr ) ) ;
    public final EObject ruleXPath() throws RecognitionException {
        EObject current = null;

        EObject lv_expr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:203:2: ( ( (lv_expr_0_0= ruleExpr ) ) )
            // InternalXpath.g:204:2: ( (lv_expr_0_0= ruleExpr ) )
            {
            // InternalXpath.g:204:2: ( (lv_expr_0_0= ruleExpr ) )
            // InternalXpath.g:205:3: (lv_expr_0_0= ruleExpr )
            {
            // InternalXpath.g:205:3: (lv_expr_0_0= ruleExpr )
            // InternalXpath.g:206:4: lv_expr_0_0= ruleExpr
            {

            				newCompositeNode(grammarAccess.getXPathAccess().getExprExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_expr_0_0=ruleExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getXPathRule());
            				}
            				set(
            					current,
            					"expr",
            					lv_expr_0_0,
            					"pt.opensoft.Xpath.Expr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXPath"


    // $ANTLR start "entryRuleExpr"
    // InternalXpath.g:226:1: entryRuleExpr returns [EObject current=null] : iv_ruleExpr= ruleExpr EOF ;
    public final EObject entryRuleExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpr = null;


        try {
            // InternalXpath.g:226:45: (iv_ruleExpr= ruleExpr EOF )
            // InternalXpath.g:227:2: iv_ruleExpr= ruleExpr EOF
            {
             newCompositeNode(grammarAccess.getExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpr=ruleExpr();

            state._fsp--;

             current =iv_ruleExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpr"


    // $ANTLR start "ruleExpr"
    // InternalXpath.g:233:1: ruleExpr returns [EObject current=null] : ( ( (lv_exprSingle_0_0= ruleExprSingle ) ) (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )* ) ;
    public final EObject ruleExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_exprSingle_0_0 = null;

        EObject lv_multi_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:239:2: ( ( ( (lv_exprSingle_0_0= ruleExprSingle ) ) (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )* ) )
            // InternalXpath.g:240:2: ( ( (lv_exprSingle_0_0= ruleExprSingle ) ) (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )* )
            {
            // InternalXpath.g:240:2: ( ( (lv_exprSingle_0_0= ruleExprSingle ) ) (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )* )
            // InternalXpath.g:241:3: ( (lv_exprSingle_0_0= ruleExprSingle ) ) (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )*
            {
            // InternalXpath.g:241:3: ( (lv_exprSingle_0_0= ruleExprSingle ) )
            // InternalXpath.g:242:4: (lv_exprSingle_0_0= ruleExprSingle )
            {
            // InternalXpath.g:242:4: (lv_exprSingle_0_0= ruleExprSingle )
            // InternalXpath.g:243:5: lv_exprSingle_0_0= ruleExprSingle
            {

            					newCompositeNode(grammarAccess.getExprAccess().getExprSingleExprSingleParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_8);
            lv_exprSingle_0_0=ruleExprSingle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExprRule());
            					}
            					set(
            						current,
            						"exprSingle",
            						lv_exprSingle_0_0,
            						"pt.opensoft.Xpath.ExprSingle");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:260:3: (otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==13) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXpath.g:261:4: otherlv_1= ',' ( (lv_multi_list_2_0= ruleExprSingle ) )
            	    {
            	    otherlv_1=(Token)match(input,13,FOLLOW_4); 

            	    				newLeafNode(otherlv_1, grammarAccess.getExprAccess().getCommaKeyword_1_0());
            	    			
            	    // InternalXpath.g:265:4: ( (lv_multi_list_2_0= ruleExprSingle ) )
            	    // InternalXpath.g:266:5: (lv_multi_list_2_0= ruleExprSingle )
            	    {
            	    // InternalXpath.g:266:5: (lv_multi_list_2_0= ruleExprSingle )
            	    // InternalXpath.g:267:6: lv_multi_list_2_0= ruleExprSingle
            	    {

            	    						newCompositeNode(grammarAccess.getExprAccess().getMulti_listExprSingleParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_multi_list_2_0=ruleExprSingle();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"multi_list",
            	    							lv_multi_list_2_0,
            	    							"pt.opensoft.Xpath.ExprSingle");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpr"


    // $ANTLR start "entryRuleExprSingle"
    // InternalXpath.g:289:1: entryRuleExprSingle returns [EObject current=null] : iv_ruleExprSingle= ruleExprSingle EOF ;
    public final EObject entryRuleExprSingle() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExprSingle = null;


        try {
            // InternalXpath.g:289:51: (iv_ruleExprSingle= ruleExprSingle EOF )
            // InternalXpath.g:290:2: iv_ruleExprSingle= ruleExprSingle EOF
            {
             newCompositeNode(grammarAccess.getExprSingleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExprSingle=ruleExprSingle();

            state._fsp--;

             current =iv_ruleExprSingle; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExprSingle"


    // $ANTLR start "ruleExprSingle"
    // InternalXpath.g:296:1: ruleExprSingle returns [EObject current=null] : ( ( (lv_forExpr_0_0= ruleForExpr ) ) | ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) ) | ( (lv_ifExpr_2_0= ruleIfExpr ) ) | ( (lv_orExpr_3_0= ruleOrExpr ) ) ) ;
    public final EObject ruleExprSingle() throws RecognitionException {
        EObject current = null;

        EObject lv_forExpr_0_0 = null;

        EObject lv_quantExpr_1_0 = null;

        EObject lv_ifExpr_2_0 = null;

        EObject lv_orExpr_3_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:302:2: ( ( ( (lv_forExpr_0_0= ruleForExpr ) ) | ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) ) | ( (lv_ifExpr_2_0= ruleIfExpr ) ) | ( (lv_orExpr_3_0= ruleOrExpr ) ) ) )
            // InternalXpath.g:303:2: ( ( (lv_forExpr_0_0= ruleForExpr ) ) | ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) ) | ( (lv_ifExpr_2_0= ruleIfExpr ) ) | ( (lv_orExpr_3_0= ruleOrExpr ) ) )
            {
            // InternalXpath.g:303:2: ( ( (lv_forExpr_0_0= ruleForExpr ) ) | ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) ) | ( (lv_ifExpr_2_0= ruleIfExpr ) ) | ( (lv_orExpr_3_0= ruleOrExpr ) ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt4=1;
                }
                break;
            case 18:
            case 19:
                {
                alt4=2;
                }
                break;
            case 21:
                {
                alt4=3;
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case 22:
            case 48:
            case 49:
            case 50:
            case 51:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalXpath.g:304:3: ( (lv_forExpr_0_0= ruleForExpr ) )
                    {
                    // InternalXpath.g:304:3: ( (lv_forExpr_0_0= ruleForExpr ) )
                    // InternalXpath.g:305:4: (lv_forExpr_0_0= ruleForExpr )
                    {
                    // InternalXpath.g:305:4: (lv_forExpr_0_0= ruleForExpr )
                    // InternalXpath.g:306:5: lv_forExpr_0_0= ruleForExpr
                    {

                    					newCompositeNode(grammarAccess.getExprSingleAccess().getForExprForExprParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_forExpr_0_0=ruleForExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExprSingleRule());
                    					}
                    					set(
                    						current,
                    						"forExpr",
                    						lv_forExpr_0_0,
                    						"pt.opensoft.Xpath.ForExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:324:3: ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) )
                    {
                    // InternalXpath.g:324:3: ( (lv_quantExpr_1_0= ruleQuantifiedExpr ) )
                    // InternalXpath.g:325:4: (lv_quantExpr_1_0= ruleQuantifiedExpr )
                    {
                    // InternalXpath.g:325:4: (lv_quantExpr_1_0= ruleQuantifiedExpr )
                    // InternalXpath.g:326:5: lv_quantExpr_1_0= ruleQuantifiedExpr
                    {

                    					newCompositeNode(grammarAccess.getExprSingleAccess().getQuantExprQuantifiedExprParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_quantExpr_1_0=ruleQuantifiedExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExprSingleRule());
                    					}
                    					set(
                    						current,
                    						"quantExpr",
                    						lv_quantExpr_1_0,
                    						"pt.opensoft.Xpath.QuantifiedExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:344:3: ( (lv_ifExpr_2_0= ruleIfExpr ) )
                    {
                    // InternalXpath.g:344:3: ( (lv_ifExpr_2_0= ruleIfExpr ) )
                    // InternalXpath.g:345:4: (lv_ifExpr_2_0= ruleIfExpr )
                    {
                    // InternalXpath.g:345:4: (lv_ifExpr_2_0= ruleIfExpr )
                    // InternalXpath.g:346:5: lv_ifExpr_2_0= ruleIfExpr
                    {

                    					newCompositeNode(grammarAccess.getExprSingleAccess().getIfExprIfExprParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_ifExpr_2_0=ruleIfExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExprSingleRule());
                    					}
                    					set(
                    						current,
                    						"ifExpr",
                    						lv_ifExpr_2_0,
                    						"pt.opensoft.Xpath.IfExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalXpath.g:364:3: ( (lv_orExpr_3_0= ruleOrExpr ) )
                    {
                    // InternalXpath.g:364:3: ( (lv_orExpr_3_0= ruleOrExpr ) )
                    // InternalXpath.g:365:4: (lv_orExpr_3_0= ruleOrExpr )
                    {
                    // InternalXpath.g:365:4: (lv_orExpr_3_0= ruleOrExpr )
                    // InternalXpath.g:366:5: lv_orExpr_3_0= ruleOrExpr
                    {

                    					newCompositeNode(grammarAccess.getExprSingleAccess().getOrExprOrExprParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_orExpr_3_0=ruleOrExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExprSingleRule());
                    					}
                    					set(
                    						current,
                    						"orExpr",
                    						lv_orExpr_3_0,
                    						"pt.opensoft.Xpath.OrExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExprSingle"


    // $ANTLR start "entryRuleForExpr"
    // InternalXpath.g:387:1: entryRuleForExpr returns [EObject current=null] : iv_ruleForExpr= ruleForExpr EOF ;
    public final EObject entryRuleForExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForExpr = null;


        try {
            // InternalXpath.g:387:48: (iv_ruleForExpr= ruleForExpr EOF )
            // InternalXpath.g:388:2: iv_ruleForExpr= ruleForExpr EOF
            {
             newCompositeNode(grammarAccess.getForExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForExpr=ruleForExpr();

            state._fsp--;

             current =iv_ruleForExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForExpr"


    // $ANTLR start "ruleForExpr"
    // InternalXpath.g:394:1: ruleForExpr returns [EObject current=null] : ( ( (lv_simpleForClause_0_0= ruleSimpleForClause ) ) otherlv_1= 'return' ( (lv_exprSingle_2_0= ruleExprSingle ) ) ) ;
    public final EObject ruleForExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_simpleForClause_0_0 = null;

        EObject lv_exprSingle_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:400:2: ( ( ( (lv_simpleForClause_0_0= ruleSimpleForClause ) ) otherlv_1= 'return' ( (lv_exprSingle_2_0= ruleExprSingle ) ) ) )
            // InternalXpath.g:401:2: ( ( (lv_simpleForClause_0_0= ruleSimpleForClause ) ) otherlv_1= 'return' ( (lv_exprSingle_2_0= ruleExprSingle ) ) )
            {
            // InternalXpath.g:401:2: ( ( (lv_simpleForClause_0_0= ruleSimpleForClause ) ) otherlv_1= 'return' ( (lv_exprSingle_2_0= ruleExprSingle ) ) )
            // InternalXpath.g:402:3: ( (lv_simpleForClause_0_0= ruleSimpleForClause ) ) otherlv_1= 'return' ( (lv_exprSingle_2_0= ruleExprSingle ) )
            {
            // InternalXpath.g:402:3: ( (lv_simpleForClause_0_0= ruleSimpleForClause ) )
            // InternalXpath.g:403:4: (lv_simpleForClause_0_0= ruleSimpleForClause )
            {
            // InternalXpath.g:403:4: (lv_simpleForClause_0_0= ruleSimpleForClause )
            // InternalXpath.g:404:5: lv_simpleForClause_0_0= ruleSimpleForClause
            {

            					newCompositeNode(grammarAccess.getForExprAccess().getSimpleForClauseSimpleForClauseParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_9);
            lv_simpleForClause_0_0=ruleSimpleForClause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForExprRule());
            					}
            					set(
            						current,
            						"simpleForClause",
            						lv_simpleForClause_0_0,
            						"pt.opensoft.Xpath.SimpleForClause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getForExprAccess().getReturnKeyword_1());
            		
            // InternalXpath.g:425:3: ( (lv_exprSingle_2_0= ruleExprSingle ) )
            // InternalXpath.g:426:4: (lv_exprSingle_2_0= ruleExprSingle )
            {
            // InternalXpath.g:426:4: (lv_exprSingle_2_0= ruleExprSingle )
            // InternalXpath.g:427:5: lv_exprSingle_2_0= ruleExprSingle
            {

            					newCompositeNode(grammarAccess.getForExprAccess().getExprSingleExprSingleParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_exprSingle_2_0=ruleExprSingle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForExprRule());
            					}
            					set(
            						current,
            						"exprSingle",
            						lv_exprSingle_2_0,
            						"pt.opensoft.Xpath.ExprSingle");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForExpr"


    // $ANTLR start "entryRuleSimpleForClause"
    // InternalXpath.g:448:1: entryRuleSimpleForClause returns [EObject current=null] : iv_ruleSimpleForClause= ruleSimpleForClause EOF ;
    public final EObject entryRuleSimpleForClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleForClause = null;


        try {
            // InternalXpath.g:448:56: (iv_ruleSimpleForClause= ruleSimpleForClause EOF )
            // InternalXpath.g:449:2: iv_ruleSimpleForClause= ruleSimpleForClause EOF
            {
             newCompositeNode(grammarAccess.getSimpleForClauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleForClause=ruleSimpleForClause();

            state._fsp--;

             current =iv_ruleSimpleForClause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleForClause"


    // $ANTLR start "ruleSimpleForClause"
    // InternalXpath.g:455:1: ruleSimpleForClause returns [EObject current=null] : (otherlv_0= 'for' otherlv_1= '$' ( (lv_varname_2_0= ruleVarName ) ) otherlv_3= 'in' ( (lv_exprSingle_4_0= ruleExprSingle ) ) (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )* ) ;
    public final EObject ruleSimpleForClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_varname_2_0 = null;

        EObject lv_exprSingle_4_0 = null;

        EObject lv_varname_list_7_0 = null;

        EObject lv_exprSingle_list_9_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:461:2: ( (otherlv_0= 'for' otherlv_1= '$' ( (lv_varname_2_0= ruleVarName ) ) otherlv_3= 'in' ( (lv_exprSingle_4_0= ruleExprSingle ) ) (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )* ) )
            // InternalXpath.g:462:2: (otherlv_0= 'for' otherlv_1= '$' ( (lv_varname_2_0= ruleVarName ) ) otherlv_3= 'in' ( (lv_exprSingle_4_0= ruleExprSingle ) ) (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )* )
            {
            // InternalXpath.g:462:2: (otherlv_0= 'for' otherlv_1= '$' ( (lv_varname_2_0= ruleVarName ) ) otherlv_3= 'in' ( (lv_exprSingle_4_0= ruleExprSingle ) ) (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )* )
            // InternalXpath.g:463:3: otherlv_0= 'for' otherlv_1= '$' ( (lv_varname_2_0= ruleVarName ) ) otherlv_3= 'in' ( (lv_exprSingle_4_0= ruleExprSingle ) ) (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )*
            {
            otherlv_0=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getSimpleForClauseAccess().getForKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_1());
            		
            // InternalXpath.g:471:3: ( (lv_varname_2_0= ruleVarName ) )
            // InternalXpath.g:472:4: (lv_varname_2_0= ruleVarName )
            {
            // InternalXpath.g:472:4: (lv_varname_2_0= ruleVarName )
            // InternalXpath.g:473:5: lv_varname_2_0= ruleVarName
            {

            					newCompositeNode(grammarAccess.getSimpleForClauseAccess().getVarnameVarNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_11);
            lv_varname_2_0=ruleVarName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleForClauseRule());
            					}
            					set(
            						current,
            						"varname",
            						lv_varname_2_0,
            						"pt.opensoft.Xpath.VarName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getSimpleForClauseAccess().getInKeyword_3());
            		
            // InternalXpath.g:494:3: ( (lv_exprSingle_4_0= ruleExprSingle ) )
            // InternalXpath.g:495:4: (lv_exprSingle_4_0= ruleExprSingle )
            {
            // InternalXpath.g:495:4: (lv_exprSingle_4_0= ruleExprSingle )
            // InternalXpath.g:496:5: lv_exprSingle_4_0= ruleExprSingle
            {

            					newCompositeNode(grammarAccess.getSimpleForClauseAccess().getExprSingleExprSingleParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_exprSingle_4_0=ruleExprSingle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleForClauseRule());
            					}
            					set(
            						current,
            						"exprSingle",
            						lv_exprSingle_4_0,
            						"pt.opensoft.Xpath.ExprSingle");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:513:3: (otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==13) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalXpath.g:514:4: otherlv_5= ',' otherlv_6= '$' ( (lv_varname_list_7_0= ruleVarName ) ) otherlv_8= 'in' ( (lv_exprSingle_list_9_0= ruleExprSingle ) )
            	    {
            	    otherlv_5=(Token)match(input,13,FOLLOW_10); 

            	    				newLeafNode(otherlv_5, grammarAccess.getSimpleForClauseAccess().getCommaKeyword_5_0());
            	    			
            	    otherlv_6=(Token)match(input,16,FOLLOW_7); 

            	    				newLeafNode(otherlv_6, grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_5_1());
            	    			
            	    // InternalXpath.g:522:4: ( (lv_varname_list_7_0= ruleVarName ) )
            	    // InternalXpath.g:523:5: (lv_varname_list_7_0= ruleVarName )
            	    {
            	    // InternalXpath.g:523:5: (lv_varname_list_7_0= ruleVarName )
            	    // InternalXpath.g:524:6: lv_varname_list_7_0= ruleVarName
            	    {

            	    						newCompositeNode(grammarAccess.getSimpleForClauseAccess().getVarname_listVarNameParserRuleCall_5_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_varname_list_7_0=ruleVarName();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSimpleForClauseRule());
            	    						}
            	    						add(
            	    							current,
            	    							"varname_list",
            	    							lv_varname_list_7_0,
            	    							"pt.opensoft.Xpath.VarName");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_8=(Token)match(input,17,FOLLOW_4); 

            	    				newLeafNode(otherlv_8, grammarAccess.getSimpleForClauseAccess().getInKeyword_5_3());
            	    			
            	    // InternalXpath.g:545:4: ( (lv_exprSingle_list_9_0= ruleExprSingle ) )
            	    // InternalXpath.g:546:5: (lv_exprSingle_list_9_0= ruleExprSingle )
            	    {
            	    // InternalXpath.g:546:5: (lv_exprSingle_list_9_0= ruleExprSingle )
            	    // InternalXpath.g:547:6: lv_exprSingle_list_9_0= ruleExprSingle
            	    {

            	    						newCompositeNode(grammarAccess.getSimpleForClauseAccess().getExprSingle_listExprSingleParserRuleCall_5_4_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_exprSingle_list_9_0=ruleExprSingle();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSimpleForClauseRule());
            	    						}
            	    						add(
            	    							current,
            	    							"exprSingle_list",
            	    							lv_exprSingle_list_9_0,
            	    							"pt.opensoft.Xpath.ExprSingle");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleForClause"


    // $ANTLR start "entryRuleQuantifiedExpr"
    // InternalXpath.g:569:1: entryRuleQuantifiedExpr returns [EObject current=null] : iv_ruleQuantifiedExpr= ruleQuantifiedExpr EOF ;
    public final EObject entryRuleQuantifiedExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifiedExpr = null;


        try {
            // InternalXpath.g:569:55: (iv_ruleQuantifiedExpr= ruleQuantifiedExpr EOF )
            // InternalXpath.g:570:2: iv_ruleQuantifiedExpr= ruleQuantifiedExpr EOF
            {
             newCompositeNode(grammarAccess.getQuantifiedExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuantifiedExpr=ruleQuantifiedExpr();

            state._fsp--;

             current =iv_ruleQuantifiedExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifiedExpr"


    // $ANTLR start "ruleQuantifiedExpr"
    // InternalXpath.g:576:1: ruleQuantifiedExpr returns [EObject current=null] : ( ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' ) otherlv_2= '$' ( (lv_element_3_0= ruleVarName ) ) otherlv_4= 'in' ( (lv_collection_5_0= ruleExprSingle ) ) (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )* otherlv_11= 'satisfies' ( (lv_condition_12_0= ruleExprSingle ) ) ) ;
    public final EObject ruleQuantifiedExpr() throws RecognitionException {
        EObject current = null;

        Token lv_quanti_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_element_3_0 = null;

        EObject lv_collection_5_0 = null;

        EObject lv_element_list_8_0 = null;

        EObject lv_collection_list_10_0 = null;

        EObject lv_condition_12_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:582:2: ( ( ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' ) otherlv_2= '$' ( (lv_element_3_0= ruleVarName ) ) otherlv_4= 'in' ( (lv_collection_5_0= ruleExprSingle ) ) (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )* otherlv_11= 'satisfies' ( (lv_condition_12_0= ruleExprSingle ) ) ) )
            // InternalXpath.g:583:2: ( ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' ) otherlv_2= '$' ( (lv_element_3_0= ruleVarName ) ) otherlv_4= 'in' ( (lv_collection_5_0= ruleExprSingle ) ) (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )* otherlv_11= 'satisfies' ( (lv_condition_12_0= ruleExprSingle ) ) )
            {
            // InternalXpath.g:583:2: ( ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' ) otherlv_2= '$' ( (lv_element_3_0= ruleVarName ) ) otherlv_4= 'in' ( (lv_collection_5_0= ruleExprSingle ) ) (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )* otherlv_11= 'satisfies' ( (lv_condition_12_0= ruleExprSingle ) ) )
            // InternalXpath.g:584:3: ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' ) otherlv_2= '$' ( (lv_element_3_0= ruleVarName ) ) otherlv_4= 'in' ( (lv_collection_5_0= ruleExprSingle ) ) (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )* otherlv_11= 'satisfies' ( (lv_condition_12_0= ruleExprSingle ) )
            {
            // InternalXpath.g:584:3: ( ( (lv_quanti_0_0= 'some' ) ) | otherlv_1= 'every' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            else if ( (LA6_0==19) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalXpath.g:585:4: ( (lv_quanti_0_0= 'some' ) )
                    {
                    // InternalXpath.g:585:4: ( (lv_quanti_0_0= 'some' ) )
                    // InternalXpath.g:586:5: (lv_quanti_0_0= 'some' )
                    {
                    // InternalXpath.g:586:5: (lv_quanti_0_0= 'some' )
                    // InternalXpath.g:587:6: lv_quanti_0_0= 'some'
                    {
                    lv_quanti_0_0=(Token)match(input,18,FOLLOW_10); 

                    						newLeafNode(lv_quanti_0_0, grammarAccess.getQuantifiedExprAccess().getQuantiSomeKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getQuantifiedExprRule());
                    						}
                    						setWithLastConsumed(current, "quanti", lv_quanti_0_0, "some");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:600:4: otherlv_1= 'every'
                    {
                    otherlv_1=(Token)match(input,19,FOLLOW_10); 

                    				newLeafNode(otherlv_1, grammarAccess.getQuantifiedExprAccess().getEveryKeyword_0_1());
                    			

                    }
                    break;

            }

            otherlv_2=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_1());
            		
            // InternalXpath.g:609:3: ( (lv_element_3_0= ruleVarName ) )
            // InternalXpath.g:610:4: (lv_element_3_0= ruleVarName )
            {
            // InternalXpath.g:610:4: (lv_element_3_0= ruleVarName )
            // InternalXpath.g:611:5: lv_element_3_0= ruleVarName
            {

            					newCompositeNode(grammarAccess.getQuantifiedExprAccess().getElementVarNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_11);
            lv_element_3_0=ruleVarName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExprRule());
            					}
            					set(
            						current,
            						"element",
            						lv_element_3_0,
            						"pt.opensoft.Xpath.VarName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getQuantifiedExprAccess().getInKeyword_3());
            		
            // InternalXpath.g:632:3: ( (lv_collection_5_0= ruleExprSingle ) )
            // InternalXpath.g:633:4: (lv_collection_5_0= ruleExprSingle )
            {
            // InternalXpath.g:633:4: (lv_collection_5_0= ruleExprSingle )
            // InternalXpath.g:634:5: lv_collection_5_0= ruleExprSingle
            {

            					newCompositeNode(grammarAccess.getQuantifiedExprAccess().getCollectionExprSingleParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_12);
            lv_collection_5_0=ruleExprSingle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExprRule());
            					}
            					set(
            						current,
            						"collection",
            						lv_collection_5_0,
            						"pt.opensoft.Xpath.ExprSingle");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:651:3: (otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==13) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXpath.g:652:4: otherlv_6= ',' otherlv_7= '$' ( (lv_element_list_8_0= ruleVarName ) ) otherlv_9= 'in' ( (lv_collection_list_10_0= ruleExprSingle ) )
            	    {
            	    otherlv_6=(Token)match(input,13,FOLLOW_10); 

            	    				newLeafNode(otherlv_6, grammarAccess.getQuantifiedExprAccess().getCommaKeyword_5_0());
            	    			
            	    otherlv_7=(Token)match(input,16,FOLLOW_7); 

            	    				newLeafNode(otherlv_7, grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_5_1());
            	    			
            	    // InternalXpath.g:660:4: ( (lv_element_list_8_0= ruleVarName ) )
            	    // InternalXpath.g:661:5: (lv_element_list_8_0= ruleVarName )
            	    {
            	    // InternalXpath.g:661:5: (lv_element_list_8_0= ruleVarName )
            	    // InternalXpath.g:662:6: lv_element_list_8_0= ruleVarName
            	    {

            	    						newCompositeNode(grammarAccess.getQuantifiedExprAccess().getElement_listVarNameParserRuleCall_5_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_element_list_8_0=ruleVarName();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getQuantifiedExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"element_list",
            	    							lv_element_list_8_0,
            	    							"pt.opensoft.Xpath.VarName");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_9=(Token)match(input,17,FOLLOW_4); 

            	    				newLeafNode(otherlv_9, grammarAccess.getQuantifiedExprAccess().getInKeyword_5_3());
            	    			
            	    // InternalXpath.g:683:4: ( (lv_collection_list_10_0= ruleExprSingle ) )
            	    // InternalXpath.g:684:5: (lv_collection_list_10_0= ruleExprSingle )
            	    {
            	    // InternalXpath.g:684:5: (lv_collection_list_10_0= ruleExprSingle )
            	    // InternalXpath.g:685:6: lv_collection_list_10_0= ruleExprSingle
            	    {

            	    						newCompositeNode(grammarAccess.getQuantifiedExprAccess().getCollection_listExprSingleParserRuleCall_5_4_0());
            	    					
            	    pushFollow(FOLLOW_12);
            	    lv_collection_list_10_0=ruleExprSingle();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getQuantifiedExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"collection_list",
            	    							lv_collection_list_10_0,
            	    							"pt.opensoft.Xpath.ExprSingle");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_11=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_11, grammarAccess.getQuantifiedExprAccess().getSatisfiesKeyword_6());
            		
            // InternalXpath.g:707:3: ( (lv_condition_12_0= ruleExprSingle ) )
            // InternalXpath.g:708:4: (lv_condition_12_0= ruleExprSingle )
            {
            // InternalXpath.g:708:4: (lv_condition_12_0= ruleExprSingle )
            // InternalXpath.g:709:5: lv_condition_12_0= ruleExprSingle
            {

            					newCompositeNode(grammarAccess.getQuantifiedExprAccess().getConditionExprSingleParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_2);
            lv_condition_12_0=ruleExprSingle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuantifiedExprRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_12_0,
            						"pt.opensoft.Xpath.ExprSingle");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifiedExpr"


    // $ANTLR start "entryRuleIfExpr"
    // InternalXpath.g:730:1: entryRuleIfExpr returns [EObject current=null] : iv_ruleIfExpr= ruleIfExpr EOF ;
    public final EObject entryRuleIfExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfExpr = null;


        try {
            // InternalXpath.g:730:47: (iv_ruleIfExpr= ruleIfExpr EOF )
            // InternalXpath.g:731:2: iv_ruleIfExpr= ruleIfExpr EOF
            {
             newCompositeNode(grammarAccess.getIfExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIfExpr=ruleIfExpr();

            state._fsp--;

             current =iv_ruleIfExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfExpr"


    // $ANTLR start "ruleIfExpr"
    // InternalXpath.g:737:1: ruleIfExpr returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_conditionExpr_2_0= ruleOrExpr ) ) otherlv_3= ')' ( (lv_thenElse_4_0= ruleThenElse ) ) ) ;
    public final EObject ruleIfExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpr_2_0 = null;

        EObject lv_thenElse_4_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:743:2: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_conditionExpr_2_0= ruleOrExpr ) ) otherlv_3= ')' ( (lv_thenElse_4_0= ruleThenElse ) ) ) )
            // InternalXpath.g:744:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_conditionExpr_2_0= ruleOrExpr ) ) otherlv_3= ')' ( (lv_thenElse_4_0= ruleThenElse ) ) )
            {
            // InternalXpath.g:744:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_conditionExpr_2_0= ruleOrExpr ) ) otherlv_3= ')' ( (lv_thenElse_4_0= ruleThenElse ) ) )
            // InternalXpath.g:745:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_conditionExpr_2_0= ruleOrExpr ) ) otherlv_3= ')' ( (lv_thenElse_4_0= ruleThenElse ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getIfExprAccess().getIfKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getIfExprAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXpath.g:753:3: ( (lv_conditionExpr_2_0= ruleOrExpr ) )
            // InternalXpath.g:754:4: (lv_conditionExpr_2_0= ruleOrExpr )
            {
            // InternalXpath.g:754:4: (lv_conditionExpr_2_0= ruleOrExpr )
            // InternalXpath.g:755:5: lv_conditionExpr_2_0= ruleOrExpr
            {

            					newCompositeNode(grammarAccess.getIfExprAccess().getConditionExprOrExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_14);
            lv_conditionExpr_2_0=ruleOrExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIfExprRule());
            					}
            					set(
            						current,
            						"conditionExpr",
            						lv_conditionExpr_2_0,
            						"pt.opensoft.Xpath.OrExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getIfExprAccess().getRightParenthesisKeyword_3());
            		
            // InternalXpath.g:776:3: ( (lv_thenElse_4_0= ruleThenElse ) )
            // InternalXpath.g:777:4: (lv_thenElse_4_0= ruleThenElse )
            {
            // InternalXpath.g:777:4: (lv_thenElse_4_0= ruleThenElse )
            // InternalXpath.g:778:5: lv_thenElse_4_0= ruleThenElse
            {

            					newCompositeNode(grammarAccess.getIfExprAccess().getThenElseThenElseParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_thenElse_4_0=ruleThenElse();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIfExprRule());
            					}
            					set(
            						current,
            						"thenElse",
            						lv_thenElse_4_0,
            						"pt.opensoft.Xpath.ThenElse");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfExpr"


    // $ANTLR start "entryRuleOrExpr"
    // InternalXpath.g:799:1: entryRuleOrExpr returns [EObject current=null] : iv_ruleOrExpr= ruleOrExpr EOF ;
    public final EObject entryRuleOrExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpr = null;


        try {
            // InternalXpath.g:799:47: (iv_ruleOrExpr= ruleOrExpr EOF )
            // InternalXpath.g:800:2: iv_ruleOrExpr= ruleOrExpr EOF
            {
             newCompositeNode(grammarAccess.getOrExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrExpr=ruleOrExpr();

            state._fsp--;

             current =iv_ruleOrExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpr"


    // $ANTLR start "ruleOrExpr"
    // InternalXpath.g:806:1: ruleOrExpr returns [EObject current=null] : ( (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? ) | ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' ) | ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* ) ) ;
    public final EObject ruleOrExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_operator_3_0=null;
        Token lv_operator_4_0=null;
        Token lv_operator_7_0=null;
        Token lv_operator_8_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_groupedCondition_1_0 = null;

        EObject lv_orExpr_5_0 = null;

        EObject lv_andExpr_6_0 = null;

        EObject lv_rightGroupedCondition_10_0 = null;

        EObject lv_andExpr_12_0 = null;

        EObject lv_orExpr_list_14_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:812:2: ( ( (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? ) | ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' ) | ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* ) ) )
            // InternalXpath.g:813:2: ( (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? ) | ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' ) | ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* ) )
            {
            // InternalXpath.g:813:2: ( (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? ) | ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' ) | ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* ) )
            int alt12=3;
            alt12 = dfa12.predict(input);
            switch (alt12) {
                case 1 :
                    // InternalXpath.g:814:3: (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? )
                    {
                    // InternalXpath.g:814:3: (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? )
                    // InternalXpath.g:815:4: otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )?
                    {
                    otherlv_0=(Token)match(input,22,FOLLOW_4); 

                    				newLeafNode(otherlv_0, grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_0_0());
                    			
                    // InternalXpath.g:819:4: ( (lv_groupedCondition_1_0= ruleOrExpr ) )
                    // InternalXpath.g:820:5: (lv_groupedCondition_1_0= ruleOrExpr )
                    {
                    // InternalXpath.g:820:5: (lv_groupedCondition_1_0= ruleOrExpr )
                    // InternalXpath.g:821:6: lv_groupedCondition_1_0= ruleOrExpr
                    {

                    						newCompositeNode(grammarAccess.getOrExprAccess().getGroupedConditionOrExprParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_groupedCondition_1_0=ruleOrExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrExprRule());
                    						}
                    						set(
                    							current,
                    							"groupedCondition",
                    							lv_groupedCondition_1_0,
                    							"pt.opensoft.Xpath.OrExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,23,FOLLOW_16); 

                    				newLeafNode(otherlv_2, grammarAccess.getOrExprAccess().getRightParenthesisKeyword_0_2());
                    			
                    // InternalXpath.g:842:4: ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( ((LA9_0>=24 && LA9_0<=25)) ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalXpath.g:843:5: ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) )
                            {
                            // InternalXpath.g:843:5: ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) )
                            int alt8=2;
                            int LA8_0 = input.LA(1);

                            if ( (LA8_0==24) ) {
                                alt8=1;
                            }
                            else if ( (LA8_0==25) ) {
                                alt8=2;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 8, 0, input);

                                throw nvae;
                            }
                            switch (alt8) {
                                case 1 :
                                    // InternalXpath.g:844:6: ( (lv_operator_3_0= 'or' ) )
                                    {
                                    // InternalXpath.g:844:6: ( (lv_operator_3_0= 'or' ) )
                                    // InternalXpath.g:845:7: (lv_operator_3_0= 'or' )
                                    {
                                    // InternalXpath.g:845:7: (lv_operator_3_0= 'or' )
                                    // InternalXpath.g:846:8: lv_operator_3_0= 'or'
                                    {
                                    lv_operator_3_0=(Token)match(input,24,FOLLOW_4); 

                                    								newLeafNode(lv_operator_3_0, grammarAccess.getOrExprAccess().getOperatorOrKeyword_0_3_0_0_0());
                                    							

                                    								if (current==null) {
                                    									current = createModelElement(grammarAccess.getOrExprRule());
                                    								}
                                    								setWithLastConsumed(current, "operator", lv_operator_3_0, "or");
                                    							

                                    }


                                    }


                                    }
                                    break;
                                case 2 :
                                    // InternalXpath.g:859:6: ( (lv_operator_4_0= 'and' ) )
                                    {
                                    // InternalXpath.g:859:6: ( (lv_operator_4_0= 'and' ) )
                                    // InternalXpath.g:860:7: (lv_operator_4_0= 'and' )
                                    {
                                    // InternalXpath.g:860:7: (lv_operator_4_0= 'and' )
                                    // InternalXpath.g:861:8: lv_operator_4_0= 'and'
                                    {
                                    lv_operator_4_0=(Token)match(input,25,FOLLOW_4); 

                                    								newLeafNode(lv_operator_4_0, grammarAccess.getOrExprAccess().getOperatorAndKeyword_0_3_0_1_0());
                                    							

                                    								if (current==null) {
                                    									current = createModelElement(grammarAccess.getOrExprRule());
                                    								}
                                    								setWithLastConsumed(current, "operator", lv_operator_4_0, "and");
                                    							

                                    }


                                    }


                                    }
                                    break;

                            }

                            // InternalXpath.g:874:5: ( (lv_orExpr_5_0= ruleOrExpr ) )
                            // InternalXpath.g:875:6: (lv_orExpr_5_0= ruleOrExpr )
                            {
                            // InternalXpath.g:875:6: (lv_orExpr_5_0= ruleOrExpr )
                            // InternalXpath.g:876:7: lv_orExpr_5_0= ruleOrExpr
                            {

                            							newCompositeNode(grammarAccess.getOrExprAccess().getOrExprOrExprParserRuleCall_0_3_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_orExpr_5_0=ruleOrExpr();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getOrExprRule());
                            							}
                            							set(
                            								current,
                            								"orExpr",
                            								lv_orExpr_5_0,
                            								"pt.opensoft.Xpath.OrExpr");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:896:3: ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' )
                    {
                    // InternalXpath.g:896:3: ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' )
                    // InternalXpath.g:897:4: ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')'
                    {
                    // InternalXpath.g:897:4: ( (lv_andExpr_6_0= ruleAndExpr ) )
                    // InternalXpath.g:898:5: (lv_andExpr_6_0= ruleAndExpr )
                    {
                    // InternalXpath.g:898:5: (lv_andExpr_6_0= ruleAndExpr )
                    // InternalXpath.g:899:6: lv_andExpr_6_0= ruleAndExpr
                    {

                    						newCompositeNode(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_1_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_andExpr_6_0=ruleAndExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrExprRule());
                    						}
                    						set(
                    							current,
                    							"andExpr",
                    							lv_andExpr_6_0,
                    							"pt.opensoft.Xpath.AndExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalXpath.g:916:4: ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) )
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==24) ) {
                        alt10=1;
                    }
                    else if ( (LA10_0==25) ) {
                        alt10=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 0, input);

                        throw nvae;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalXpath.g:917:5: ( (lv_operator_7_0= 'or' ) )
                            {
                            // InternalXpath.g:917:5: ( (lv_operator_7_0= 'or' ) )
                            // InternalXpath.g:918:6: (lv_operator_7_0= 'or' )
                            {
                            // InternalXpath.g:918:6: (lv_operator_7_0= 'or' )
                            // InternalXpath.g:919:7: lv_operator_7_0= 'or'
                            {
                            lv_operator_7_0=(Token)match(input,24,FOLLOW_13); 

                            							newLeafNode(lv_operator_7_0, grammarAccess.getOrExprAccess().getOperatorOrKeyword_1_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getOrExprRule());
                            							}
                            							setWithLastConsumed(current, "operator", lv_operator_7_0, "or");
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalXpath.g:932:5: ( (lv_operator_8_0= 'and' ) )
                            {
                            // InternalXpath.g:932:5: ( (lv_operator_8_0= 'and' ) )
                            // InternalXpath.g:933:6: (lv_operator_8_0= 'and' )
                            {
                            // InternalXpath.g:933:6: (lv_operator_8_0= 'and' )
                            // InternalXpath.g:934:7: lv_operator_8_0= 'and'
                            {
                            lv_operator_8_0=(Token)match(input,25,FOLLOW_13); 

                            							newLeafNode(lv_operator_8_0, grammarAccess.getOrExprAccess().getOperatorAndKeyword_1_1_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getOrExprRule());
                            							}
                            							setWithLastConsumed(current, "operator", lv_operator_8_0, "and");
                            						

                            }


                            }


                            }
                            break;

                    }

                    otherlv_9=(Token)match(input,22,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_1_2());
                    			
                    // InternalXpath.g:951:4: ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) )
                    // InternalXpath.g:952:5: (lv_rightGroupedCondition_10_0= ruleOrExpr )
                    {
                    // InternalXpath.g:952:5: (lv_rightGroupedCondition_10_0= ruleOrExpr )
                    // InternalXpath.g:953:6: lv_rightGroupedCondition_10_0= ruleOrExpr
                    {

                    						newCompositeNode(grammarAccess.getOrExprAccess().getRightGroupedConditionOrExprParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_rightGroupedCondition_10_0=ruleOrExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrExprRule());
                    						}
                    						set(
                    							current,
                    							"rightGroupedCondition",
                    							lv_rightGroupedCondition_10_0,
                    							"pt.opensoft.Xpath.OrExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_11=(Token)match(input,23,FOLLOW_2); 

                    				newLeafNode(otherlv_11, grammarAccess.getOrExprAccess().getRightParenthesisKeyword_1_4());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:976:3: ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* )
                    {
                    // InternalXpath.g:976:3: ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* )
                    // InternalXpath.g:977:4: ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )*
                    {
                    // InternalXpath.g:977:4: ( (lv_andExpr_12_0= ruleAndExpr ) )
                    // InternalXpath.g:978:5: (lv_andExpr_12_0= ruleAndExpr )
                    {
                    // InternalXpath.g:978:5: (lv_andExpr_12_0= ruleAndExpr )
                    // InternalXpath.g:979:6: lv_andExpr_12_0= ruleAndExpr
                    {

                    						newCompositeNode(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_andExpr_12_0=ruleAndExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrExprRule());
                    						}
                    						set(
                    							current,
                    							"andExpr",
                    							lv_andExpr_12_0,
                    							"pt.opensoft.Xpath.AndExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalXpath.g:996:4: (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==24) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalXpath.g:997:5: otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) )
                    	    {
                    	    otherlv_13=(Token)match(input,24,FOLLOW_19); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getOrExprAccess().getOrKeyword_2_1_0());
                    	    				
                    	    // InternalXpath.g:1001:5: ( (lv_orExpr_list_14_0= ruleAndExpr ) )
                    	    // InternalXpath.g:1002:6: (lv_orExpr_list_14_0= ruleAndExpr )
                    	    {
                    	    // InternalXpath.g:1002:6: (lv_orExpr_list_14_0= ruleAndExpr )
                    	    // InternalXpath.g:1003:7: lv_orExpr_list_14_0= ruleAndExpr
                    	    {

                    	    							newCompositeNode(grammarAccess.getOrExprAccess().getOrExpr_listAndExprParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_18);
                    	    lv_orExpr_list_14_0=ruleAndExpr();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOrExprRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"orExpr_list",
                    	    								lv_orExpr_list_14_0,
                    	    								"pt.opensoft.Xpath.AndExpr");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpr"


    // $ANTLR start "entryRuleAndExpr"
    // InternalXpath.g:1026:1: entryRuleAndExpr returns [EObject current=null] : iv_ruleAndExpr= ruleAndExpr EOF ;
    public final EObject entryRuleAndExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpr = null;


        try {
            // InternalXpath.g:1026:48: (iv_ruleAndExpr= ruleAndExpr EOF )
            // InternalXpath.g:1027:2: iv_ruleAndExpr= ruleAndExpr EOF
            {
             newCompositeNode(grammarAccess.getAndExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndExpr=ruleAndExpr();

            state._fsp--;

             current =iv_ruleAndExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpr"


    // $ANTLR start "ruleAndExpr"
    // InternalXpath.g:1033:1: ruleAndExpr returns [EObject current=null] : ( ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) ) (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )* ) ;
    public final EObject ruleAndExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_comparisonExpr_0_0 = null;

        EObject lv_andCondition_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1039:2: ( ( ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) ) (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )* ) )
            // InternalXpath.g:1040:2: ( ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) ) (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )* )
            {
            // InternalXpath.g:1040:2: ( ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) ) (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )* )
            // InternalXpath.g:1041:3: ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) ) (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )*
            {
            // InternalXpath.g:1041:3: ( (lv_comparisonExpr_0_0= ruleComparisonExpr ) )
            // InternalXpath.g:1042:4: (lv_comparisonExpr_0_0= ruleComparisonExpr )
            {
            // InternalXpath.g:1042:4: (lv_comparisonExpr_0_0= ruleComparisonExpr )
            // InternalXpath.g:1043:5: lv_comparisonExpr_0_0= ruleComparisonExpr
            {

            					newCompositeNode(grammarAccess.getAndExprAccess().getComparisonExprComparisonExprParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_20);
            lv_comparisonExpr_0_0=ruleComparisonExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAndExprRule());
            					}
            					set(
            						current,
            						"comparisonExpr",
            						lv_comparisonExpr_0_0,
            						"pt.opensoft.Xpath.ComparisonExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:1060:3: (otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==25) ) {
                    int LA13_2 = input.LA(2);

                    if ( ((LA13_2>=RULE_STRING && LA13_2<=RULE_INT)||(LA13_2>=48 && LA13_2<=51)) ) {
                        alt13=1;
                    }


                }


                switch (alt13) {
            	case 1 :
            	    // InternalXpath.g:1061:4: otherlv_1= 'and' ( (lv_andCondition_list_2_0= ruleComparisonExpr ) )
            	    {
            	    otherlv_1=(Token)match(input,25,FOLLOW_19); 

            	    				newLeafNode(otherlv_1, grammarAccess.getAndExprAccess().getAndKeyword_1_0());
            	    			
            	    // InternalXpath.g:1065:4: ( (lv_andCondition_list_2_0= ruleComparisonExpr ) )
            	    // InternalXpath.g:1066:5: (lv_andCondition_list_2_0= ruleComparisonExpr )
            	    {
            	    // InternalXpath.g:1066:5: (lv_andCondition_list_2_0= ruleComparisonExpr )
            	    // InternalXpath.g:1067:6: lv_andCondition_list_2_0= ruleComparisonExpr
            	    {

            	    						newCompositeNode(grammarAccess.getAndExprAccess().getAndCondition_listComparisonExprParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_20);
            	    lv_andCondition_list_2_0=ruleComparisonExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"andCondition_list",
            	    							lv_andCondition_list_2_0,
            	    							"pt.opensoft.Xpath.ComparisonExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpr"


    // $ANTLR start "entryRuleComparisonExpr"
    // InternalXpath.g:1089:1: entryRuleComparisonExpr returns [EObject current=null] : iv_ruleComparisonExpr= ruleComparisonExpr EOF ;
    public final EObject entryRuleComparisonExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparisonExpr = null;


        try {
            // InternalXpath.g:1089:55: (iv_ruleComparisonExpr= ruleComparisonExpr EOF )
            // InternalXpath.g:1090:2: iv_ruleComparisonExpr= ruleComparisonExpr EOF
            {
             newCompositeNode(grammarAccess.getComparisonExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparisonExpr=ruleComparisonExpr();

            state._fsp--;

             current =iv_ruleComparisonExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparisonExpr"


    // $ANTLR start "ruleComparisonExpr"
    // InternalXpath.g:1096:1: ruleComparisonExpr returns [EObject current=null] : ( ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? ) | ( (lv_notExpr_5_0= ruleNotExpr ) ) | ( (lv_function_6_0= ruleFunction ) ) ) ;
    public final EObject ruleComparisonExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_additiveExpr_0_0 = null;

        AntlrDatatypeRuleToken lv_valueCompOptional_1_0 = null;

        AntlrDatatypeRuleToken lv_generalCompOptional_2_0 = null;

        AntlrDatatypeRuleToken lv_nodeCompOptional_3_0 = null;

        EObject lv_additiveExprOptional_4_0 = null;

        EObject lv_notExpr_5_0 = null;

        EObject lv_function_6_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1102:2: ( ( ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? ) | ( (lv_notExpr_5_0= ruleNotExpr ) ) | ( (lv_function_6_0= ruleFunction ) ) ) )
            // InternalXpath.g:1103:2: ( ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? ) | ( (lv_notExpr_5_0= ruleNotExpr ) ) | ( (lv_function_6_0= ruleFunction ) ) )
            {
            // InternalXpath.g:1103:2: ( ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? ) | ( (lv_notExpr_5_0= ruleNotExpr ) ) | ( (lv_function_6_0= ruleFunction ) ) )
            int alt16=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
            case 49:
                {
                alt16=1;
                }
                break;
            case 48:
                {
                alt16=2;
                }
                break;
            case 50:
            case 51:
                {
                alt16=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalXpath.g:1104:3: ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? )
                    {
                    // InternalXpath.g:1104:3: ( ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )? )
                    // InternalXpath.g:1105:4: ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) ) ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )?
                    {
                    // InternalXpath.g:1105:4: ( (lv_additiveExpr_0_0= ruleAdditiveExpr ) )
                    // InternalXpath.g:1106:5: (lv_additiveExpr_0_0= ruleAdditiveExpr )
                    {
                    // InternalXpath.g:1106:5: (lv_additiveExpr_0_0= ruleAdditiveExpr )
                    // InternalXpath.g:1107:6: lv_additiveExpr_0_0= ruleAdditiveExpr
                    {

                    						newCompositeNode(grammarAccess.getComparisonExprAccess().getAdditiveExprAdditiveExprParserRuleCall_0_0_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_additiveExpr_0_0=ruleAdditiveExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                    						}
                    						set(
                    							current,
                    							"additiveExpr",
                    							lv_additiveExpr_0_0,
                    							"pt.opensoft.Xpath.AdditiveExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalXpath.g:1124:4: ( ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) ) )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( ((LA15_0>=32 && LA15_0<=46)) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // InternalXpath.g:1125:5: ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) ) ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) )
                            {
                            // InternalXpath.g:1125:5: ( ( (lv_valueCompOptional_1_0= ruleValueComp ) ) | ( (lv_generalCompOptional_2_0= ruleGeneralComp ) ) | ( (lv_nodeCompOptional_3_0= ruleNodeComp ) ) )
                            int alt14=3;
                            switch ( input.LA(1) ) {
                            case 38:
                            case 39:
                            case 40:
                            case 41:
                            case 42:
                            case 43:
                                {
                                alt14=1;
                                }
                                break;
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                            case 36:
                            case 37:
                                {
                                alt14=2;
                                }
                                break;
                            case 44:
                            case 45:
                            case 46:
                                {
                                alt14=3;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 14, 0, input);

                                throw nvae;
                            }

                            switch (alt14) {
                                case 1 :
                                    // InternalXpath.g:1126:6: ( (lv_valueCompOptional_1_0= ruleValueComp ) )
                                    {
                                    // InternalXpath.g:1126:6: ( (lv_valueCompOptional_1_0= ruleValueComp ) )
                                    // InternalXpath.g:1127:7: (lv_valueCompOptional_1_0= ruleValueComp )
                                    {
                                    // InternalXpath.g:1127:7: (lv_valueCompOptional_1_0= ruleValueComp )
                                    // InternalXpath.g:1128:8: lv_valueCompOptional_1_0= ruleValueComp
                                    {

                                    								newCompositeNode(grammarAccess.getComparisonExprAccess().getValueCompOptionalValueCompParserRuleCall_0_1_0_0_0());
                                    							
                                    pushFollow(FOLLOW_22);
                                    lv_valueCompOptional_1_0=ruleValueComp();

                                    state._fsp--;


                                    								if (current==null) {
                                    									current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                                    								}
                                    								set(
                                    									current,
                                    									"valueCompOptional",
                                    									lv_valueCompOptional_1_0,
                                    									"pt.opensoft.Xpath.ValueComp");
                                    								afterParserOrEnumRuleCall();
                                    							

                                    }


                                    }


                                    }
                                    break;
                                case 2 :
                                    // InternalXpath.g:1146:6: ( (lv_generalCompOptional_2_0= ruleGeneralComp ) )
                                    {
                                    // InternalXpath.g:1146:6: ( (lv_generalCompOptional_2_0= ruleGeneralComp ) )
                                    // InternalXpath.g:1147:7: (lv_generalCompOptional_2_0= ruleGeneralComp )
                                    {
                                    // InternalXpath.g:1147:7: (lv_generalCompOptional_2_0= ruleGeneralComp )
                                    // InternalXpath.g:1148:8: lv_generalCompOptional_2_0= ruleGeneralComp
                                    {

                                    								newCompositeNode(grammarAccess.getComparisonExprAccess().getGeneralCompOptionalGeneralCompParserRuleCall_0_1_0_1_0());
                                    							
                                    pushFollow(FOLLOW_22);
                                    lv_generalCompOptional_2_0=ruleGeneralComp();

                                    state._fsp--;


                                    								if (current==null) {
                                    									current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                                    								}
                                    								set(
                                    									current,
                                    									"generalCompOptional",
                                    									lv_generalCompOptional_2_0,
                                    									"pt.opensoft.Xpath.GeneralComp");
                                    								afterParserOrEnumRuleCall();
                                    							

                                    }


                                    }


                                    }
                                    break;
                                case 3 :
                                    // InternalXpath.g:1166:6: ( (lv_nodeCompOptional_3_0= ruleNodeComp ) )
                                    {
                                    // InternalXpath.g:1166:6: ( (lv_nodeCompOptional_3_0= ruleNodeComp ) )
                                    // InternalXpath.g:1167:7: (lv_nodeCompOptional_3_0= ruleNodeComp )
                                    {
                                    // InternalXpath.g:1167:7: (lv_nodeCompOptional_3_0= ruleNodeComp )
                                    // InternalXpath.g:1168:8: lv_nodeCompOptional_3_0= ruleNodeComp
                                    {

                                    								newCompositeNode(grammarAccess.getComparisonExprAccess().getNodeCompOptionalNodeCompParserRuleCall_0_1_0_2_0());
                                    							
                                    pushFollow(FOLLOW_22);
                                    lv_nodeCompOptional_3_0=ruleNodeComp();

                                    state._fsp--;


                                    								if (current==null) {
                                    									current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                                    								}
                                    								set(
                                    									current,
                                    									"nodeCompOptional",
                                    									lv_nodeCompOptional_3_0,
                                    									"pt.opensoft.Xpath.NodeComp");
                                    								afterParserOrEnumRuleCall();
                                    							

                                    }


                                    }


                                    }
                                    break;

                            }

                            // InternalXpath.g:1186:5: ( (lv_additiveExprOptional_4_0= ruleAdditiveExpr ) )
                            // InternalXpath.g:1187:6: (lv_additiveExprOptional_4_0= ruleAdditiveExpr )
                            {
                            // InternalXpath.g:1187:6: (lv_additiveExprOptional_4_0= ruleAdditiveExpr )
                            // InternalXpath.g:1188:7: lv_additiveExprOptional_4_0= ruleAdditiveExpr
                            {

                            							newCompositeNode(grammarAccess.getComparisonExprAccess().getAdditiveExprOptionalAdditiveExprParserRuleCall_0_1_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_additiveExprOptional_4_0=ruleAdditiveExpr();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                            							}
                            							set(
                            								current,
                            								"additiveExprOptional",
                            								lv_additiveExprOptional_4_0,
                            								"pt.opensoft.Xpath.AdditiveExpr");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1208:3: ( (lv_notExpr_5_0= ruleNotExpr ) )
                    {
                    // InternalXpath.g:1208:3: ( (lv_notExpr_5_0= ruleNotExpr ) )
                    // InternalXpath.g:1209:4: (lv_notExpr_5_0= ruleNotExpr )
                    {
                    // InternalXpath.g:1209:4: (lv_notExpr_5_0= ruleNotExpr )
                    // InternalXpath.g:1210:5: lv_notExpr_5_0= ruleNotExpr
                    {

                    					newCompositeNode(grammarAccess.getComparisonExprAccess().getNotExprNotExprParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_notExpr_5_0=ruleNotExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                    					}
                    					set(
                    						current,
                    						"notExpr",
                    						lv_notExpr_5_0,
                    						"pt.opensoft.Xpath.NotExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1228:3: ( (lv_function_6_0= ruleFunction ) )
                    {
                    // InternalXpath.g:1228:3: ( (lv_function_6_0= ruleFunction ) )
                    // InternalXpath.g:1229:4: (lv_function_6_0= ruleFunction )
                    {
                    // InternalXpath.g:1229:4: (lv_function_6_0= ruleFunction )
                    // InternalXpath.g:1230:5: lv_function_6_0= ruleFunction
                    {

                    					newCompositeNode(grammarAccess.getComparisonExprAccess().getFunctionFunctionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_function_6_0=ruleFunction();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getComparisonExprRule());
                    					}
                    					set(
                    						current,
                    						"function",
                    						lv_function_6_0,
                    						"pt.opensoft.Xpath.Function");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonExpr"


    // $ANTLR start "entryRuleAdditiveExpr"
    // InternalXpath.g:1251:1: entryRuleAdditiveExpr returns [EObject current=null] : iv_ruleAdditiveExpr= ruleAdditiveExpr EOF ;
    public final EObject entryRuleAdditiveExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditiveExpr = null;


        try {
            // InternalXpath.g:1251:53: (iv_ruleAdditiveExpr= ruleAdditiveExpr EOF )
            // InternalXpath.g:1252:2: iv_ruleAdditiveExpr= ruleAdditiveExpr EOF
            {
             newCompositeNode(grammarAccess.getAdditiveExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAdditiveExpr=ruleAdditiveExpr();

            state._fsp--;

             current =iv_ruleAdditiveExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditiveExpr"


    // $ANTLR start "ruleAdditiveExpr"
    // InternalXpath.g:1258:1: ruleAdditiveExpr returns [EObject current=null] : ( ( (lv_valueExpr_0_0= ruleValueExpr ) ) ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )* ) ;
    public final EObject ruleAdditiveExpr() throws RecognitionException {
        EObject current = null;

        Token lv_additive_list_1_1=null;
        Token lv_additive_list_1_2=null;
        EObject lv_valueExpr_0_0 = null;

        EObject lv_valueExpr_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1264:2: ( ( ( (lv_valueExpr_0_0= ruleValueExpr ) ) ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )* ) )
            // InternalXpath.g:1265:2: ( ( (lv_valueExpr_0_0= ruleValueExpr ) ) ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )* )
            {
            // InternalXpath.g:1265:2: ( ( (lv_valueExpr_0_0= ruleValueExpr ) ) ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )* )
            // InternalXpath.g:1266:3: ( (lv_valueExpr_0_0= ruleValueExpr ) ) ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )*
            {
            // InternalXpath.g:1266:3: ( (lv_valueExpr_0_0= ruleValueExpr ) )
            // InternalXpath.g:1267:4: (lv_valueExpr_0_0= ruleValueExpr )
            {
            // InternalXpath.g:1267:4: (lv_valueExpr_0_0= ruleValueExpr )
            // InternalXpath.g:1268:5: lv_valueExpr_0_0= ruleValueExpr
            {

            					newCompositeNode(grammarAccess.getAdditiveExprAccess().getValueExprValueExprParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_23);
            lv_valueExpr_0_0=ruleValueExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAdditiveExprRule());
            					}
            					set(
            						current,
            						"valueExpr",
            						lv_valueExpr_0_0,
            						"pt.opensoft.Xpath.ValueExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:1285:3: ( ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=26 && LA18_0<=27)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalXpath.g:1286:4: ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) ) ( (lv_valueExpr_list_2_0= ruleValueExpr ) )
            	    {
            	    // InternalXpath.g:1286:4: ( ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) ) )
            	    // InternalXpath.g:1287:5: ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) )
            	    {
            	    // InternalXpath.g:1287:5: ( (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' ) )
            	    // InternalXpath.g:1288:6: (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' )
            	    {
            	    // InternalXpath.g:1288:6: (lv_additive_list_1_1= '+' | lv_additive_list_1_2= '-' )
            	    int alt17=2;
            	    int LA17_0 = input.LA(1);

            	    if ( (LA17_0==26) ) {
            	        alt17=1;
            	    }
            	    else if ( (LA17_0==27) ) {
            	        alt17=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 17, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt17) {
            	        case 1 :
            	            // InternalXpath.g:1289:7: lv_additive_list_1_1= '+'
            	            {
            	            lv_additive_list_1_1=(Token)match(input,26,FOLLOW_22); 

            	            							newLeafNode(lv_additive_list_1_1, grammarAccess.getAdditiveExprAccess().getAdditive_listPlusSignKeyword_1_0_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getAdditiveExprRule());
            	            							}
            	            							addWithLastConsumed(current, "additive_list", lv_additive_list_1_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalXpath.g:1300:7: lv_additive_list_1_2= '-'
            	            {
            	            lv_additive_list_1_2=(Token)match(input,27,FOLLOW_22); 

            	            							newLeafNode(lv_additive_list_1_2, grammarAccess.getAdditiveExprAccess().getAdditive_listHyphenMinusKeyword_1_0_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getAdditiveExprRule());
            	            							}
            	            							addWithLastConsumed(current, "additive_list", lv_additive_list_1_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalXpath.g:1313:4: ( (lv_valueExpr_list_2_0= ruleValueExpr ) )
            	    // InternalXpath.g:1314:5: (lv_valueExpr_list_2_0= ruleValueExpr )
            	    {
            	    // InternalXpath.g:1314:5: (lv_valueExpr_list_2_0= ruleValueExpr )
            	    // InternalXpath.g:1315:6: lv_valueExpr_list_2_0= ruleValueExpr
            	    {

            	    						newCompositeNode(grammarAccess.getAdditiveExprAccess().getValueExpr_listValueExprParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_23);
            	    lv_valueExpr_list_2_0=ruleValueExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAdditiveExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"valueExpr_list",
            	    							lv_valueExpr_list_2_0,
            	    							"pt.opensoft.Xpath.ValueExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveExpr"


    // $ANTLR start "entryRuleUnionExpr"
    // InternalXpath.g:1337:1: entryRuleUnionExpr returns [EObject current=null] : iv_ruleUnionExpr= ruleUnionExpr EOF ;
    public final EObject entryRuleUnionExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnionExpr = null;


        try {
            // InternalXpath.g:1337:50: (iv_ruleUnionExpr= ruleUnionExpr EOF )
            // InternalXpath.g:1338:2: iv_ruleUnionExpr= ruleUnionExpr EOF
            {
             newCompositeNode(grammarAccess.getUnionExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnionExpr=ruleUnionExpr();

            state._fsp--;

             current =iv_ruleUnionExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnionExpr"


    // $ANTLR start "ruleUnionExpr"
    // InternalXpath.g:1344:1: ruleUnionExpr returns [EObject current=null] : ( ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) ) ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )* ) ;
    public final EObject ruleUnionExpr() throws RecognitionException {
        EObject current = null;

        Token lv_intercection_list_1_1=null;
        Token lv_intercection_list_1_2=null;
        EObject lv_intersectExceptExpr_0_0 = null;

        EObject lv_intersectExceptExpr_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1350:2: ( ( ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) ) ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )* ) )
            // InternalXpath.g:1351:2: ( ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) ) ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )* )
            {
            // InternalXpath.g:1351:2: ( ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) ) ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )* )
            // InternalXpath.g:1352:3: ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) ) ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )*
            {
            // InternalXpath.g:1352:3: ( (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr ) )
            // InternalXpath.g:1353:4: (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr )
            {
            // InternalXpath.g:1353:4: (lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr )
            // InternalXpath.g:1354:5: lv_intersectExceptExpr_0_0= ruleIntersectExceptExpr
            {

            					newCompositeNode(grammarAccess.getUnionExprAccess().getIntersectExceptExprIntersectExceptExprParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_24);
            lv_intersectExceptExpr_0_0=ruleIntersectExceptExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUnionExprRule());
            					}
            					set(
            						current,
            						"intersectExceptExpr",
            						lv_intersectExceptExpr_0_0,
            						"pt.opensoft.Xpath.IntersectExceptExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:1371:3: ( ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=28 && LA20_0<=29)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalXpath.g:1372:4: ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) ) ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) )
            	    {
            	    // InternalXpath.g:1372:4: ( ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) ) )
            	    // InternalXpath.g:1373:5: ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) )
            	    {
            	    // InternalXpath.g:1373:5: ( (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' ) )
            	    // InternalXpath.g:1374:6: (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' )
            	    {
            	    // InternalXpath.g:1374:6: (lv_intercection_list_1_1= 'union' | lv_intercection_list_1_2= '|' )
            	    int alt19=2;
            	    int LA19_0 = input.LA(1);

            	    if ( (LA19_0==28) ) {
            	        alt19=1;
            	    }
            	    else if ( (LA19_0==29) ) {
            	        alt19=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 19, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt19) {
            	        case 1 :
            	            // InternalXpath.g:1375:7: lv_intercection_list_1_1= 'union'
            	            {
            	            lv_intercection_list_1_1=(Token)match(input,28,FOLLOW_22); 

            	            							newLeafNode(lv_intercection_list_1_1, grammarAccess.getUnionExprAccess().getIntercection_listUnionKeyword_1_0_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getUnionExprRule());
            	            							}
            	            							addWithLastConsumed(current, "intercection_list", lv_intercection_list_1_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalXpath.g:1386:7: lv_intercection_list_1_2= '|'
            	            {
            	            lv_intercection_list_1_2=(Token)match(input,29,FOLLOW_22); 

            	            							newLeafNode(lv_intercection_list_1_2, grammarAccess.getUnionExprAccess().getIntercection_listVerticalLineKeyword_1_0_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getUnionExprRule());
            	            							}
            	            							addWithLastConsumed(current, "intercection_list", lv_intercection_list_1_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalXpath.g:1399:4: ( (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr ) )
            	    // InternalXpath.g:1400:5: (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr )
            	    {
            	    // InternalXpath.g:1400:5: (lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr )
            	    // InternalXpath.g:1401:6: lv_intersectExceptExpr_list_2_0= ruleIntersectExceptExpr
            	    {

            	    						newCompositeNode(grammarAccess.getUnionExprAccess().getIntersectExceptExpr_listIntersectExceptExprParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_24);
            	    lv_intersectExceptExpr_list_2_0=ruleIntersectExceptExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getUnionExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"intersectExceptExpr_list",
            	    							lv_intersectExceptExpr_list_2_0,
            	    							"pt.opensoft.Xpath.IntersectExceptExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnionExpr"


    // $ANTLR start "entryRuleIntersectExceptExpr"
    // InternalXpath.g:1423:1: entryRuleIntersectExceptExpr returns [EObject current=null] : iv_ruleIntersectExceptExpr= ruleIntersectExceptExpr EOF ;
    public final EObject entryRuleIntersectExceptExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntersectExceptExpr = null;


        try {
            // InternalXpath.g:1423:60: (iv_ruleIntersectExceptExpr= ruleIntersectExceptExpr EOF )
            // InternalXpath.g:1424:2: iv_ruleIntersectExceptExpr= ruleIntersectExceptExpr EOF
            {
             newCompositeNode(grammarAccess.getIntersectExceptExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntersectExceptExpr=ruleIntersectExceptExpr();

            state._fsp--;

             current =iv_ruleIntersectExceptExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntersectExceptExpr"


    // $ANTLR start "ruleIntersectExceptExpr"
    // InternalXpath.g:1430:1: ruleIntersectExceptExpr returns [EObject current=null] : ( ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) ) ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )* ) ;
    public final EObject ruleIntersectExceptExpr() throws RecognitionException {
        EObject current = null;

        Token lv_instance_list_1_1=null;
        Token lv_instance_list_1_2=null;
        EObject lv_instanceofExpr_0_0 = null;

        EObject lv_instanceofExpr_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1436:2: ( ( ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) ) ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )* ) )
            // InternalXpath.g:1437:2: ( ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) ) ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )* )
            {
            // InternalXpath.g:1437:2: ( ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) ) ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )* )
            // InternalXpath.g:1438:3: ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) ) ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )*
            {
            // InternalXpath.g:1438:3: ( (lv_instanceofExpr_0_0= ruleInstanceofExpr ) )
            // InternalXpath.g:1439:4: (lv_instanceofExpr_0_0= ruleInstanceofExpr )
            {
            // InternalXpath.g:1439:4: (lv_instanceofExpr_0_0= ruleInstanceofExpr )
            // InternalXpath.g:1440:5: lv_instanceofExpr_0_0= ruleInstanceofExpr
            {

            					newCompositeNode(grammarAccess.getIntersectExceptExprAccess().getInstanceofExprInstanceofExprParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_25);
            lv_instanceofExpr_0_0=ruleInstanceofExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIntersectExceptExprRule());
            					}
            					set(
            						current,
            						"instanceofExpr",
            						lv_instanceofExpr_0_0,
            						"pt.opensoft.Xpath.InstanceofExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:1457:3: ( ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=30 && LA22_0<=31)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalXpath.g:1458:4: ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) ) ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) )
            	    {
            	    // InternalXpath.g:1458:4: ( ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) ) )
            	    // InternalXpath.g:1459:5: ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) )
            	    {
            	    // InternalXpath.g:1459:5: ( (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' ) )
            	    // InternalXpath.g:1460:6: (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' )
            	    {
            	    // InternalXpath.g:1460:6: (lv_instance_list_1_1= 'intersect' | lv_instance_list_1_2= 'except' )
            	    int alt21=2;
            	    int LA21_0 = input.LA(1);

            	    if ( (LA21_0==30) ) {
            	        alt21=1;
            	    }
            	    else if ( (LA21_0==31) ) {
            	        alt21=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 21, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt21) {
            	        case 1 :
            	            // InternalXpath.g:1461:7: lv_instance_list_1_1= 'intersect'
            	            {
            	            lv_instance_list_1_1=(Token)match(input,30,FOLLOW_22); 

            	            							newLeafNode(lv_instance_list_1_1, grammarAccess.getIntersectExceptExprAccess().getInstance_listIntersectKeyword_1_0_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getIntersectExceptExprRule());
            	            							}
            	            							addWithLastConsumed(current, "instance_list", lv_instance_list_1_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalXpath.g:1472:7: lv_instance_list_1_2= 'except'
            	            {
            	            lv_instance_list_1_2=(Token)match(input,31,FOLLOW_22); 

            	            							newLeafNode(lv_instance_list_1_2, grammarAccess.getIntersectExceptExprAccess().getInstance_listExceptKeyword_1_0_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getIntersectExceptExprRule());
            	            							}
            	            							addWithLastConsumed(current, "instance_list", lv_instance_list_1_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalXpath.g:1485:4: ( (lv_instanceofExpr_list_2_0= ruleInstanceofExpr ) )
            	    // InternalXpath.g:1486:5: (lv_instanceofExpr_list_2_0= ruleInstanceofExpr )
            	    {
            	    // InternalXpath.g:1486:5: (lv_instanceofExpr_list_2_0= ruleInstanceofExpr )
            	    // InternalXpath.g:1487:6: lv_instanceofExpr_list_2_0= ruleInstanceofExpr
            	    {

            	    						newCompositeNode(grammarAccess.getIntersectExceptExprAccess().getInstanceofExpr_listInstanceofExprParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_25);
            	    lv_instanceofExpr_list_2_0=ruleInstanceofExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getIntersectExceptExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"instanceofExpr_list",
            	    							lv_instanceofExpr_list_2_0,
            	    							"pt.opensoft.Xpath.InstanceofExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntersectExceptExpr"


    // $ANTLR start "entryRuleInstanceofExpr"
    // InternalXpath.g:1509:1: entryRuleInstanceofExpr returns [EObject current=null] : iv_ruleInstanceofExpr= ruleInstanceofExpr EOF ;
    public final EObject entryRuleInstanceofExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstanceofExpr = null;


        try {
            // InternalXpath.g:1509:55: (iv_ruleInstanceofExpr= ruleInstanceofExpr EOF )
            // InternalXpath.g:1510:2: iv_ruleInstanceofExpr= ruleInstanceofExpr EOF
            {
             newCompositeNode(grammarAccess.getInstanceofExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstanceofExpr=ruleInstanceofExpr();

            state._fsp--;

             current =iv_ruleInstanceofExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstanceofExpr"


    // $ANTLR start "ruleInstanceofExpr"
    // InternalXpath.g:1516:1: ruleInstanceofExpr returns [EObject current=null] : ( (lv_treatExpr_0_0= ruleTreatExpr ) ) ;
    public final EObject ruleInstanceofExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_treatExpr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1522:2: ( ( (lv_treatExpr_0_0= ruleTreatExpr ) ) )
            // InternalXpath.g:1523:2: ( (lv_treatExpr_0_0= ruleTreatExpr ) )
            {
            // InternalXpath.g:1523:2: ( (lv_treatExpr_0_0= ruleTreatExpr ) )
            // InternalXpath.g:1524:3: (lv_treatExpr_0_0= ruleTreatExpr )
            {
            // InternalXpath.g:1524:3: (lv_treatExpr_0_0= ruleTreatExpr )
            // InternalXpath.g:1525:4: lv_treatExpr_0_0= ruleTreatExpr
            {

            				newCompositeNode(grammarAccess.getInstanceofExprAccess().getTreatExprTreatExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_treatExpr_0_0=ruleTreatExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getInstanceofExprRule());
            				}
            				set(
            					current,
            					"treatExpr",
            					lv_treatExpr_0_0,
            					"pt.opensoft.Xpath.TreatExpr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstanceofExpr"


    // $ANTLR start "entryRuleTreatExpr"
    // InternalXpath.g:1545:1: entryRuleTreatExpr returns [EObject current=null] : iv_ruleTreatExpr= ruleTreatExpr EOF ;
    public final EObject entryRuleTreatExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTreatExpr = null;


        try {
            // InternalXpath.g:1545:50: (iv_ruleTreatExpr= ruleTreatExpr EOF )
            // InternalXpath.g:1546:2: iv_ruleTreatExpr= ruleTreatExpr EOF
            {
             newCompositeNode(grammarAccess.getTreatExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTreatExpr=ruleTreatExpr();

            state._fsp--;

             current =iv_ruleTreatExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTreatExpr"


    // $ANTLR start "ruleTreatExpr"
    // InternalXpath.g:1552:1: ruleTreatExpr returns [EObject current=null] : ( (lv_castableExpr_0_0= ruleCastableExpr ) ) ;
    public final EObject ruleTreatExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_castableExpr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1558:2: ( ( (lv_castableExpr_0_0= ruleCastableExpr ) ) )
            // InternalXpath.g:1559:2: ( (lv_castableExpr_0_0= ruleCastableExpr ) )
            {
            // InternalXpath.g:1559:2: ( (lv_castableExpr_0_0= ruleCastableExpr ) )
            // InternalXpath.g:1560:3: (lv_castableExpr_0_0= ruleCastableExpr )
            {
            // InternalXpath.g:1560:3: (lv_castableExpr_0_0= ruleCastableExpr )
            // InternalXpath.g:1561:4: lv_castableExpr_0_0= ruleCastableExpr
            {

            				newCompositeNode(grammarAccess.getTreatExprAccess().getCastableExprCastableExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_castableExpr_0_0=ruleCastableExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getTreatExprRule());
            				}
            				set(
            					current,
            					"castableExpr",
            					lv_castableExpr_0_0,
            					"pt.opensoft.Xpath.CastableExpr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTreatExpr"


    // $ANTLR start "entryRuleCastableExpr"
    // InternalXpath.g:1581:1: entryRuleCastableExpr returns [EObject current=null] : iv_ruleCastableExpr= ruleCastableExpr EOF ;
    public final EObject entryRuleCastableExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCastableExpr = null;


        try {
            // InternalXpath.g:1581:53: (iv_ruleCastableExpr= ruleCastableExpr EOF )
            // InternalXpath.g:1582:2: iv_ruleCastableExpr= ruleCastableExpr EOF
            {
             newCompositeNode(grammarAccess.getCastableExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCastableExpr=ruleCastableExpr();

            state._fsp--;

             current =iv_ruleCastableExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCastableExpr"


    // $ANTLR start "ruleCastableExpr"
    // InternalXpath.g:1588:1: ruleCastableExpr returns [EObject current=null] : ( (lv_castExpr_0_0= ruleCastExpr ) ) ;
    public final EObject ruleCastableExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_castExpr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1594:2: ( ( (lv_castExpr_0_0= ruleCastExpr ) ) )
            // InternalXpath.g:1595:2: ( (lv_castExpr_0_0= ruleCastExpr ) )
            {
            // InternalXpath.g:1595:2: ( (lv_castExpr_0_0= ruleCastExpr ) )
            // InternalXpath.g:1596:3: (lv_castExpr_0_0= ruleCastExpr )
            {
            // InternalXpath.g:1596:3: (lv_castExpr_0_0= ruleCastExpr )
            // InternalXpath.g:1597:4: lv_castExpr_0_0= ruleCastExpr
            {

            				newCompositeNode(grammarAccess.getCastableExprAccess().getCastExprCastExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_castExpr_0_0=ruleCastExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getCastableExprRule());
            				}
            				set(
            					current,
            					"castExpr",
            					lv_castExpr_0_0,
            					"pt.opensoft.Xpath.CastExpr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCastableExpr"


    // $ANTLR start "entryRuleCastExpr"
    // InternalXpath.g:1617:1: entryRuleCastExpr returns [EObject current=null] : iv_ruleCastExpr= ruleCastExpr EOF ;
    public final EObject entryRuleCastExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCastExpr = null;


        try {
            // InternalXpath.g:1617:49: (iv_ruleCastExpr= ruleCastExpr EOF )
            // InternalXpath.g:1618:2: iv_ruleCastExpr= ruleCastExpr EOF
            {
             newCompositeNode(grammarAccess.getCastExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCastExpr=ruleCastExpr();

            state._fsp--;

             current =iv_ruleCastExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCastExpr"


    // $ANTLR start "ruleCastExpr"
    // InternalXpath.g:1624:1: ruleCastExpr returns [EObject current=null] : ( (lv_unaryExpr_0_0= ruleUnaryExpr ) ) ;
    public final EObject ruleCastExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_unaryExpr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1630:2: ( ( (lv_unaryExpr_0_0= ruleUnaryExpr ) ) )
            // InternalXpath.g:1631:2: ( (lv_unaryExpr_0_0= ruleUnaryExpr ) )
            {
            // InternalXpath.g:1631:2: ( (lv_unaryExpr_0_0= ruleUnaryExpr ) )
            // InternalXpath.g:1632:3: (lv_unaryExpr_0_0= ruleUnaryExpr )
            {
            // InternalXpath.g:1632:3: (lv_unaryExpr_0_0= ruleUnaryExpr )
            // InternalXpath.g:1633:4: lv_unaryExpr_0_0= ruleUnaryExpr
            {

            				newCompositeNode(grammarAccess.getCastExprAccess().getUnaryExprUnaryExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_unaryExpr_0_0=ruleUnaryExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getCastExprRule());
            				}
            				set(
            					current,
            					"unaryExpr",
            					lv_unaryExpr_0_0,
            					"pt.opensoft.Xpath.UnaryExpr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCastExpr"


    // $ANTLR start "entryRuleUnaryExpr"
    // InternalXpath.g:1653:1: entryRuleUnaryExpr returns [EObject current=null] : iv_ruleUnaryExpr= ruleUnaryExpr EOF ;
    public final EObject entryRuleUnaryExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpr = null;


        try {
            // InternalXpath.g:1653:50: (iv_ruleUnaryExpr= ruleUnaryExpr EOF )
            // InternalXpath.g:1654:2: iv_ruleUnaryExpr= ruleUnaryExpr EOF
            {
             newCompositeNode(grammarAccess.getUnaryExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnaryExpr=ruleUnaryExpr();

            state._fsp--;

             current =iv_ruleUnaryExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpr"


    // $ANTLR start "ruleUnaryExpr"
    // InternalXpath.g:1660:1: ruleUnaryExpr returns [EObject current=null] : ( (lv_valueExpr_0_0= ruleValueExpr ) ) ;
    public final EObject ruleUnaryExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_valueExpr_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1666:2: ( ( (lv_valueExpr_0_0= ruleValueExpr ) ) )
            // InternalXpath.g:1667:2: ( (lv_valueExpr_0_0= ruleValueExpr ) )
            {
            // InternalXpath.g:1667:2: ( (lv_valueExpr_0_0= ruleValueExpr ) )
            // InternalXpath.g:1668:3: (lv_valueExpr_0_0= ruleValueExpr )
            {
            // InternalXpath.g:1668:3: (lv_valueExpr_0_0= ruleValueExpr )
            // InternalXpath.g:1669:4: lv_valueExpr_0_0= ruleValueExpr
            {

            				newCompositeNode(grammarAccess.getUnaryExprAccess().getValueExprValueExprParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_valueExpr_0_0=ruleValueExpr();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getUnaryExprRule());
            				}
            				set(
            					current,
            					"valueExpr",
            					lv_valueExpr_0_0,
            					"pt.opensoft.Xpath.ValueExpr");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpr"


    // $ANTLR start "entryRuleValueExpr"
    // InternalXpath.g:1689:1: entryRuleValueExpr returns [EObject current=null] : iv_ruleValueExpr= ruleValueExpr EOF ;
    public final EObject entryRuleValueExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueExpr = null;


        try {
            // InternalXpath.g:1689:50: (iv_ruleValueExpr= ruleValueExpr EOF )
            // InternalXpath.g:1690:2: iv_ruleValueExpr= ruleValueExpr EOF
            {
             newCompositeNode(grammarAccess.getValueExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValueExpr=ruleValueExpr();

            state._fsp--;

             current =iv_ruleValueExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueExpr"


    // $ANTLR start "ruleValueExpr"
    // InternalXpath.g:1696:1: ruleValueExpr returns [EObject current=null] : ( ( (lv_pathExpr_0_0= rulePathExpr ) ) | ( (lv_stringliteral_1_0= ruleStringLiteral ) ) | ( (lv_numericLiteral_2_0= ruleNumericLiteral ) ) ) ;
    public final EObject ruleValueExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_pathExpr_0_0 = null;

        EObject lv_stringliteral_1_0 = null;

        EObject lv_numericLiteral_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1702:2: ( ( ( (lv_pathExpr_0_0= rulePathExpr ) ) | ( (lv_stringliteral_1_0= ruleStringLiteral ) ) | ( (lv_numericLiteral_2_0= ruleNumericLiteral ) ) ) )
            // InternalXpath.g:1703:2: ( ( (lv_pathExpr_0_0= rulePathExpr ) ) | ( (lv_stringliteral_1_0= ruleStringLiteral ) ) | ( (lv_numericLiteral_2_0= ruleNumericLiteral ) ) )
            {
            // InternalXpath.g:1703:2: ( ( (lv_pathExpr_0_0= rulePathExpr ) ) | ( (lv_stringliteral_1_0= ruleStringLiteral ) ) | ( (lv_numericLiteral_2_0= ruleNumericLiteral ) ) )
            int alt23=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt23=1;
                }
                break;
            case RULE_STRING:
                {
                alt23=2;
                }
                break;
            case RULE_INT:
                {
                alt23=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalXpath.g:1704:3: ( (lv_pathExpr_0_0= rulePathExpr ) )
                    {
                    // InternalXpath.g:1704:3: ( (lv_pathExpr_0_0= rulePathExpr ) )
                    // InternalXpath.g:1705:4: (lv_pathExpr_0_0= rulePathExpr )
                    {
                    // InternalXpath.g:1705:4: (lv_pathExpr_0_0= rulePathExpr )
                    // InternalXpath.g:1706:5: lv_pathExpr_0_0= rulePathExpr
                    {

                    					newCompositeNode(grammarAccess.getValueExprAccess().getPathExprPathExprParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_pathExpr_0_0=rulePathExpr();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getValueExprRule());
                    					}
                    					set(
                    						current,
                    						"pathExpr",
                    						lv_pathExpr_0_0,
                    						"pt.opensoft.Xpath.PathExpr");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1724:3: ( (lv_stringliteral_1_0= ruleStringLiteral ) )
                    {
                    // InternalXpath.g:1724:3: ( (lv_stringliteral_1_0= ruleStringLiteral ) )
                    // InternalXpath.g:1725:4: (lv_stringliteral_1_0= ruleStringLiteral )
                    {
                    // InternalXpath.g:1725:4: (lv_stringliteral_1_0= ruleStringLiteral )
                    // InternalXpath.g:1726:5: lv_stringliteral_1_0= ruleStringLiteral
                    {

                    					newCompositeNode(grammarAccess.getValueExprAccess().getStringliteralStringLiteralParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_stringliteral_1_0=ruleStringLiteral();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getValueExprRule());
                    					}
                    					set(
                    						current,
                    						"stringliteral",
                    						lv_stringliteral_1_0,
                    						"pt.opensoft.Xpath.StringLiteral");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1744:3: ( (lv_numericLiteral_2_0= ruleNumericLiteral ) )
                    {
                    // InternalXpath.g:1744:3: ( (lv_numericLiteral_2_0= ruleNumericLiteral ) )
                    // InternalXpath.g:1745:4: (lv_numericLiteral_2_0= ruleNumericLiteral )
                    {
                    // InternalXpath.g:1745:4: (lv_numericLiteral_2_0= ruleNumericLiteral )
                    // InternalXpath.g:1746:5: lv_numericLiteral_2_0= ruleNumericLiteral
                    {

                    					newCompositeNode(grammarAccess.getValueExprAccess().getNumericLiteralNumericLiteralParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_numericLiteral_2_0=ruleNumericLiteral();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getValueExprRule());
                    					}
                    					set(
                    						current,
                    						"numericLiteral",
                    						lv_numericLiteral_2_0,
                    						"pt.opensoft.Xpath.NumericLiteral");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueExpr"


    // $ANTLR start "entryRuleGeneralComp"
    // InternalXpath.g:1767:1: entryRuleGeneralComp returns [String current=null] : iv_ruleGeneralComp= ruleGeneralComp EOF ;
    public final String entryRuleGeneralComp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleGeneralComp = null;


        try {
            // InternalXpath.g:1767:51: (iv_ruleGeneralComp= ruleGeneralComp EOF )
            // InternalXpath.g:1768:2: iv_ruleGeneralComp= ruleGeneralComp EOF
            {
             newCompositeNode(grammarAccess.getGeneralCompRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGeneralComp=ruleGeneralComp();

            state._fsp--;

             current =iv_ruleGeneralComp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGeneralComp"


    // $ANTLR start "ruleGeneralComp"
    // InternalXpath.g:1774:1: ruleGeneralComp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '=' | kw= '!=' | kw= '<' | kw= '<=' | kw= '>' | kw= '>=' ) ;
    public final AntlrDatatypeRuleToken ruleGeneralComp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXpath.g:1780:2: ( (kw= '=' | kw= '!=' | kw= '<' | kw= '<=' | kw= '>' | kw= '>=' ) )
            // InternalXpath.g:1781:2: (kw= '=' | kw= '!=' | kw= '<' | kw= '<=' | kw= '>' | kw= '>=' )
            {
            // InternalXpath.g:1781:2: (kw= '=' | kw= '!=' | kw= '<' | kw= '<=' | kw= '>' | kw= '>=' )
            int alt24=6;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt24=1;
                }
                break;
            case 33:
                {
                alt24=2;
                }
                break;
            case 34:
                {
                alt24=3;
                }
                break;
            case 35:
                {
                alt24=4;
                }
                break;
            case 36:
                {
                alt24=5;
                }
                break;
            case 37:
                {
                alt24=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalXpath.g:1782:3: kw= '='
                    {
                    kw=(Token)match(input,32,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getEqualsSignKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalXpath.g:1788:3: kw= '!='
                    {
                    kw=(Token)match(input,33,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getExclamationMarkEqualsSignKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalXpath.g:1794:3: kw= '<'
                    {
                    kw=(Token)match(input,34,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getLessThanSignKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalXpath.g:1800:3: kw= '<='
                    {
                    kw=(Token)match(input,35,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getLessThanSignEqualsSignKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalXpath.g:1806:3: kw= '>'
                    {
                    kw=(Token)match(input,36,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getGreaterThanSignKeyword_4());
                    		

                    }
                    break;
                case 6 :
                    // InternalXpath.g:1812:3: kw= '>='
                    {
                    kw=(Token)match(input,37,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getGeneralCompAccess().getGreaterThanSignEqualsSignKeyword_5());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGeneralComp"


    // $ANTLR start "entryRuleValueComp"
    // InternalXpath.g:1821:1: entryRuleValueComp returns [String current=null] : iv_ruleValueComp= ruleValueComp EOF ;
    public final String entryRuleValueComp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleValueComp = null;


        try {
            // InternalXpath.g:1821:49: (iv_ruleValueComp= ruleValueComp EOF )
            // InternalXpath.g:1822:2: iv_ruleValueComp= ruleValueComp EOF
            {
             newCompositeNode(grammarAccess.getValueCompRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValueComp=ruleValueComp();

            state._fsp--;

             current =iv_ruleValueComp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueComp"


    // $ANTLR start "ruleValueComp"
    // InternalXpath.g:1828:1: ruleValueComp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'eq' | kw= 'ne' | kw= 'lt' | kw= 'le' | kw= 'gt' | kw= 'ge' ) ;
    public final AntlrDatatypeRuleToken ruleValueComp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXpath.g:1834:2: ( (kw= 'eq' | kw= 'ne' | kw= 'lt' | kw= 'le' | kw= 'gt' | kw= 'ge' ) )
            // InternalXpath.g:1835:2: (kw= 'eq' | kw= 'ne' | kw= 'lt' | kw= 'le' | kw= 'gt' | kw= 'ge' )
            {
            // InternalXpath.g:1835:2: (kw= 'eq' | kw= 'ne' | kw= 'lt' | kw= 'le' | kw= 'gt' | kw= 'ge' )
            int alt25=6;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt25=1;
                }
                break;
            case 39:
                {
                alt25=2;
                }
                break;
            case 40:
                {
                alt25=3;
                }
                break;
            case 41:
                {
                alt25=4;
                }
                break;
            case 42:
                {
                alt25=5;
                }
                break;
            case 43:
                {
                alt25=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // InternalXpath.g:1836:3: kw= 'eq'
                    {
                    kw=(Token)match(input,38,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getEqKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalXpath.g:1842:3: kw= 'ne'
                    {
                    kw=(Token)match(input,39,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getNeKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalXpath.g:1848:3: kw= 'lt'
                    {
                    kw=(Token)match(input,40,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getLtKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalXpath.g:1854:3: kw= 'le'
                    {
                    kw=(Token)match(input,41,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getLeKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalXpath.g:1860:3: kw= 'gt'
                    {
                    kw=(Token)match(input,42,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getGtKeyword_4());
                    		

                    }
                    break;
                case 6 :
                    // InternalXpath.g:1866:3: kw= 'ge'
                    {
                    kw=(Token)match(input,43,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getValueCompAccess().getGeKeyword_5());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueComp"


    // $ANTLR start "entryRuleNodeComp"
    // InternalXpath.g:1875:1: entryRuleNodeComp returns [String current=null] : iv_ruleNodeComp= ruleNodeComp EOF ;
    public final String entryRuleNodeComp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNodeComp = null;


        try {
            // InternalXpath.g:1875:48: (iv_ruleNodeComp= ruleNodeComp EOF )
            // InternalXpath.g:1876:2: iv_ruleNodeComp= ruleNodeComp EOF
            {
             newCompositeNode(grammarAccess.getNodeCompRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNodeComp=ruleNodeComp();

            state._fsp--;

             current =iv_ruleNodeComp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNodeComp"


    // $ANTLR start "ruleNodeComp"
    // InternalXpath.g:1882:1: ruleNodeComp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'is' | kw= '<<' | kw= '>>' ) ;
    public final AntlrDatatypeRuleToken ruleNodeComp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXpath.g:1888:2: ( (kw= 'is' | kw= '<<' | kw= '>>' ) )
            // InternalXpath.g:1889:2: (kw= 'is' | kw= '<<' | kw= '>>' )
            {
            // InternalXpath.g:1889:2: (kw= 'is' | kw= '<<' | kw= '>>' )
            int alt26=3;
            switch ( input.LA(1) ) {
            case 44:
                {
                alt26=1;
                }
                break;
            case 45:
                {
                alt26=2;
                }
                break;
            case 46:
                {
                alt26=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalXpath.g:1890:3: kw= 'is'
                    {
                    kw=(Token)match(input,44,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeCompAccess().getIsKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalXpath.g:1896:3: kw= '<<'
                    {
                    kw=(Token)match(input,45,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeCompAccess().getLessThanSignLessThanSignKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalXpath.g:1902:3: kw= '>>'
                    {
                    kw=(Token)match(input,46,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeCompAccess().getGreaterThanSignGreaterThanSignKeyword_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNodeComp"


    // $ANTLR start "entryRulePathExpr"
    // InternalXpath.g:1911:1: entryRulePathExpr returns [EObject current=null] : iv_rulePathExpr= rulePathExpr EOF ;
    public final EObject entryRulePathExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePathExpr = null;


        try {
            // InternalXpath.g:1911:49: (iv_rulePathExpr= rulePathExpr EOF )
            // InternalXpath.g:1912:2: iv_rulePathExpr= rulePathExpr EOF
            {
             newCompositeNode(grammarAccess.getPathExprRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePathExpr=rulePathExpr();

            state._fsp--;

             current =iv_rulePathExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePathExpr"


    // $ANTLR start "rulePathExpr"
    // InternalXpath.g:1918:1: rulePathExpr returns [EObject current=null] : ( ( (lv_element_0_0= ruleElement ) ) ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )* ) ;
    public final EObject rulePathExpr() throws RecognitionException {
        EObject current = null;

        Token lv_path_list_1_1=null;
        Token lv_path_list_1_2=null;
        EObject lv_element_0_0 = null;

        EObject lv_pathElement_list_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:1924:2: ( ( ( (lv_element_0_0= ruleElement ) ) ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )* ) )
            // InternalXpath.g:1925:2: ( ( (lv_element_0_0= ruleElement ) ) ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )* )
            {
            // InternalXpath.g:1925:2: ( ( (lv_element_0_0= ruleElement ) ) ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )* )
            // InternalXpath.g:1926:3: ( (lv_element_0_0= ruleElement ) ) ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )*
            {
            // InternalXpath.g:1926:3: ( (lv_element_0_0= ruleElement ) )
            // InternalXpath.g:1927:4: (lv_element_0_0= ruleElement )
            {
            // InternalXpath.g:1927:4: (lv_element_0_0= ruleElement )
            // InternalXpath.g:1928:5: lv_element_0_0= ruleElement
            {

            					newCompositeNode(grammarAccess.getPathExprAccess().getElementElementParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_26);
            lv_element_0_0=ruleElement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPathExprRule());
            					}
            					set(
            						current,
            						"element",
            						lv_element_0_0,
            						"pt.opensoft.Xpath.Element");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXpath.g:1945:3: ( ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==12||LA28_0==47) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalXpath.g:1946:4: ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) ) ( (lv_pathElement_list_2_0= ruleElement ) )
            	    {
            	    // InternalXpath.g:1946:4: ( ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) ) )
            	    // InternalXpath.g:1947:5: ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) )
            	    {
            	    // InternalXpath.g:1947:5: ( (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' ) )
            	    // InternalXpath.g:1948:6: (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' )
            	    {
            	    // InternalXpath.g:1948:6: (lv_path_list_1_1= '/' | lv_path_list_1_2= '//' )
            	    int alt27=2;
            	    int LA27_0 = input.LA(1);

            	    if ( (LA27_0==12) ) {
            	        alt27=1;
            	    }
            	    else if ( (LA27_0==47) ) {
            	        alt27=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 27, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt27) {
            	        case 1 :
            	            // InternalXpath.g:1949:7: lv_path_list_1_1= '/'
            	            {
            	            lv_path_list_1_1=(Token)match(input,12,FOLLOW_27); 

            	            							newLeafNode(lv_path_list_1_1, grammarAccess.getPathExprAccess().getPath_listSolidusKeyword_1_0_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getPathExprRule());
            	            							}
            	            							addWithLastConsumed(current, "path_list", lv_path_list_1_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalXpath.g:1960:7: lv_path_list_1_2= '//'
            	            {
            	            lv_path_list_1_2=(Token)match(input,47,FOLLOW_27); 

            	            							newLeafNode(lv_path_list_1_2, grammarAccess.getPathExprAccess().getPath_listSolidusSolidusKeyword_1_0_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getPathExprRule());
            	            							}
            	            							addWithLastConsumed(current, "path_list", lv_path_list_1_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalXpath.g:1973:4: ( (lv_pathElement_list_2_0= ruleElement ) )
            	    // InternalXpath.g:1974:5: (lv_pathElement_list_2_0= ruleElement )
            	    {
            	    // InternalXpath.g:1974:5: (lv_pathElement_list_2_0= ruleElement )
            	    // InternalXpath.g:1975:6: lv_pathElement_list_2_0= ruleElement
            	    {

            	    						newCompositeNode(grammarAccess.getPathExprAccess().getPathElement_listElementParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_26);
            	    lv_pathElement_list_2_0=ruleElement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPathExprRule());
            	    						}
            	    						add(
            	    							current,
            	    							"pathElement_list",
            	    							lv_pathElement_list_2_0,
            	    							"pt.opensoft.Xpath.Element");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePathExpr"


    // $ANTLR start "entryRuleStepExpr"
    // InternalXpath.g:1997:1: entryRuleStepExpr returns [EObject current=null] : iv_ruleStepExpr= ruleStepExpr EOF ;
    public final EObject entryRuleStepExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStepExpr = null;


        try {
            // InternalXpath.g:1997:49: (iv_ruleStepExpr= ruleStepExpr EOF )
            // InternalXpath.g:1998:2: iv_ruleStepExpr= ruleStepExpr EOF
            {
             newCompositeNode(grammarAccess.getStepExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepExpr=ruleStepExpr();

            state._fsp--;

             current =iv_ruleStepExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepExpr"


    // $ANTLR start "ruleStepExpr"
    // InternalXpath.g:2004:1: ruleStepExpr returns [EObject current=null] : ( (lv_element_0_0= ruleElement ) ) ;
    public final EObject ruleStepExpr() throws RecognitionException {
        EObject current = null;

        EObject lv_element_0_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2010:2: ( ( (lv_element_0_0= ruleElement ) ) )
            // InternalXpath.g:2011:2: ( (lv_element_0_0= ruleElement ) )
            {
            // InternalXpath.g:2011:2: ( (lv_element_0_0= ruleElement ) )
            // InternalXpath.g:2012:3: (lv_element_0_0= ruleElement )
            {
            // InternalXpath.g:2012:3: (lv_element_0_0= ruleElement )
            // InternalXpath.g:2013:4: lv_element_0_0= ruleElement
            {

            				newCompositeNode(grammarAccess.getStepExprAccess().getElementElementParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_element_0_0=ruleElement();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getStepExprRule());
            				}
            				set(
            					current,
            					"element",
            					lv_element_0_0,
            					"pt.opensoft.Xpath.Element");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepExpr"


    // $ANTLR start "entryRuleNotExpr"
    // InternalXpath.g:2033:1: entryRuleNotExpr returns [EObject current=null] : iv_ruleNotExpr= ruleNotExpr EOF ;
    public final EObject entryRuleNotExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotExpr = null;


        try {
            // InternalXpath.g:2033:48: (iv_ruleNotExpr= ruleNotExpr EOF )
            // InternalXpath.g:2034:2: iv_ruleNotExpr= ruleNotExpr EOF
            {
             newCompositeNode(grammarAccess.getNotExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotExpr=ruleNotExpr();

            state._fsp--;

             current =iv_ruleNotExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotExpr"


    // $ANTLR start "ruleNotExpr"
    // InternalXpath.g:2040:1: ruleNotExpr returns [EObject current=null] : (otherlv_0= 'not' otherlv_1= '(' ( (lv_pathExpr_2_0= rulePathExpr ) ) otherlv_3= ')' ) ;
    public final EObject ruleNotExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_pathExpr_2_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2046:2: ( (otherlv_0= 'not' otherlv_1= '(' ( (lv_pathExpr_2_0= rulePathExpr ) ) otherlv_3= ')' ) )
            // InternalXpath.g:2047:2: (otherlv_0= 'not' otherlv_1= '(' ( (lv_pathExpr_2_0= rulePathExpr ) ) otherlv_3= ')' )
            {
            // InternalXpath.g:2047:2: (otherlv_0= 'not' otherlv_1= '(' ( (lv_pathExpr_2_0= rulePathExpr ) ) otherlv_3= ')' )
            // InternalXpath.g:2048:3: otherlv_0= 'not' otherlv_1= '(' ( (lv_pathExpr_2_0= rulePathExpr ) ) otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,48,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getNotExprAccess().getNotKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_27); 

            			newLeafNode(otherlv_1, grammarAccess.getNotExprAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXpath.g:2056:3: ( (lv_pathExpr_2_0= rulePathExpr ) )
            // InternalXpath.g:2057:4: (lv_pathExpr_2_0= rulePathExpr )
            {
            // InternalXpath.g:2057:4: (lv_pathExpr_2_0= rulePathExpr )
            // InternalXpath.g:2058:5: lv_pathExpr_2_0= rulePathExpr
            {

            					newCompositeNode(grammarAccess.getNotExprAccess().getPathExprPathExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_14);
            lv_pathExpr_2_0=rulePathExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNotExprRule());
            					}
            					set(
            						current,
            						"pathExpr",
            						lv_pathExpr_2_0,
            						"pt.opensoft.Xpath.PathExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getNotExprAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotExpr"


    // $ANTLR start "entryRuleVarName"
    // InternalXpath.g:2083:1: entryRuleVarName returns [EObject current=null] : iv_ruleVarName= ruleVarName EOF ;
    public final EObject entryRuleVarName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarName = null;


        try {
            // InternalXpath.g:2083:48: (iv_ruleVarName= ruleVarName EOF )
            // InternalXpath.g:2084:2: iv_ruleVarName= ruleVarName EOF
            {
             newCompositeNode(grammarAccess.getVarNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVarName=ruleVarName();

            state._fsp--;

             current =iv_ruleVarName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarName"


    // $ANTLR start "ruleVarName"
    // InternalXpath.g:2090:1: ruleVarName returns [EObject current=null] : this_QName_0= ruleQName ;
    public final EObject ruleVarName() throws RecognitionException {
        EObject current = null;

        EObject this_QName_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2096:2: (this_QName_0= ruleQName )
            // InternalXpath.g:2097:2: this_QName_0= ruleQName
            {

            		newCompositeNode(grammarAccess.getVarNameAccess().getQNameParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_QName_0=ruleQName();

            state._fsp--;


            		current = this_QName_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarName"


    // $ANTLR start "entryRuleQName"
    // InternalXpath.g:2108:1: entryRuleQName returns [EObject current=null] : iv_ruleQName= ruleQName EOF ;
    public final EObject entryRuleQName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQName = null;


        try {
            // InternalXpath.g:2108:46: (iv_ruleQName= ruleQName EOF )
            // InternalXpath.g:2109:2: iv_ruleQName= ruleQName EOF
            {
             newCompositeNode(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQName=ruleQName();

            state._fsp--;

             current =iv_ruleQName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalXpath.g:2115:1: ruleQName returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleQName() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalXpath.g:2121:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalXpath.g:2122:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalXpath.g:2122:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalXpath.g:2123:3: (lv_name_0_0= RULE_ID )
            {
            // InternalXpath.g:2123:3: (lv_name_0_0= RULE_ID )
            // InternalXpath.g:2124:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getQNameAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getQNameRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleElement"
    // InternalXpath.g:2143:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // InternalXpath.g:2143:48: (iv_ruleElement= ruleElement EOF )
            // InternalXpath.g:2144:2: iv_ruleElement= ruleElement EOF
            {
             newCompositeNode(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElement=ruleElement();

            state._fsp--;

             current =iv_ruleElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalXpath.g:2150:1: ruleElement returns [EObject current=null] : (otherlv_0= 'ns:' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalXpath.g:2156:2: ( (otherlv_0= 'ns:' ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalXpath.g:2157:2: (otherlv_0= 'ns:' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalXpath.g:2157:2: (otherlv_0= 'ns:' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalXpath.g:2158:3: otherlv_0= 'ns:' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,49,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getElementAccess().getNsKeyword_0());
            		
            // InternalXpath.g:2162:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalXpath.g:2163:4: (lv_name_1_0= RULE_ID )
            {
            // InternalXpath.g:2163:4: (lv_name_1_0= RULE_ID )
            // InternalXpath.g:2164:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getElementAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getElementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleFunction"
    // InternalXpath.g:2184:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalXpath.g:2184:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalXpath.g:2185:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalXpath.g:2191:1: ruleFunction returns [EObject current=null] : ( ( (lv_sumFunction_0_0= ruleSumFunction ) ) | ( (lv_countFunction_1_0= ruleCountFunction ) ) ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        EObject lv_sumFunction_0_0 = null;

        EObject lv_countFunction_1_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2197:2: ( ( ( (lv_sumFunction_0_0= ruleSumFunction ) ) | ( (lv_countFunction_1_0= ruleCountFunction ) ) ) )
            // InternalXpath.g:2198:2: ( ( (lv_sumFunction_0_0= ruleSumFunction ) ) | ( (lv_countFunction_1_0= ruleCountFunction ) ) )
            {
            // InternalXpath.g:2198:2: ( ( (lv_sumFunction_0_0= ruleSumFunction ) ) | ( (lv_countFunction_1_0= ruleCountFunction ) ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==50) ) {
                alt29=1;
            }
            else if ( (LA29_0==51) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalXpath.g:2199:3: ( (lv_sumFunction_0_0= ruleSumFunction ) )
                    {
                    // InternalXpath.g:2199:3: ( (lv_sumFunction_0_0= ruleSumFunction ) )
                    // InternalXpath.g:2200:4: (lv_sumFunction_0_0= ruleSumFunction )
                    {
                    // InternalXpath.g:2200:4: (lv_sumFunction_0_0= ruleSumFunction )
                    // InternalXpath.g:2201:5: lv_sumFunction_0_0= ruleSumFunction
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getSumFunctionSumFunctionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_sumFunction_0_0=ruleSumFunction();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"sumFunction",
                    						lv_sumFunction_0_0,
                    						"pt.opensoft.Xpath.SumFunction");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:2219:3: ( (lv_countFunction_1_0= ruleCountFunction ) )
                    {
                    // InternalXpath.g:2219:3: ( (lv_countFunction_1_0= ruleCountFunction ) )
                    // InternalXpath.g:2220:4: (lv_countFunction_1_0= ruleCountFunction )
                    {
                    // InternalXpath.g:2220:4: (lv_countFunction_1_0= ruleCountFunction )
                    // InternalXpath.g:2221:5: lv_countFunction_1_0= ruleCountFunction
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getCountFunctionCountFunctionParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_countFunction_1_0=ruleCountFunction();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"countFunction",
                    						lv_countFunction_1_0,
                    						"pt.opensoft.Xpath.CountFunction");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleSumFunction"
    // InternalXpath.g:2242:1: entryRuleSumFunction returns [EObject current=null] : iv_ruleSumFunction= ruleSumFunction EOF ;
    public final EObject entryRuleSumFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSumFunction = null;


        try {
            // InternalXpath.g:2242:52: (iv_ruleSumFunction= ruleSumFunction EOF )
            // InternalXpath.g:2243:2: iv_ruleSumFunction= ruleSumFunction EOF
            {
             newCompositeNode(grammarAccess.getSumFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSumFunction=ruleSumFunction();

            state._fsp--;

             current =iv_ruleSumFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSumFunction"


    // $ANTLR start "ruleSumFunction"
    // InternalXpath.g:2249:1: ruleSumFunction returns [EObject current=null] : (otherlv_0= 'sum' otherlv_1= '(' ( (lv_sumElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) ) ;
    public final EObject ruleSumFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_sumElement_2_0 = null;

        EObject lv_resultElement_5_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2255:2: ( (otherlv_0= 'sum' otherlv_1= '(' ( (lv_sumElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) ) )
            // InternalXpath.g:2256:2: (otherlv_0= 'sum' otherlv_1= '(' ( (lv_sumElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) )
            {
            // InternalXpath.g:2256:2: (otherlv_0= 'sum' otherlv_1= '(' ( (lv_sumElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) )
            // InternalXpath.g:2257:3: otherlv_0= 'sum' otherlv_1= '(' ( (lv_sumElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) )
            {
            otherlv_0=(Token)match(input,50,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getSumFunctionAccess().getSumKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_27); 

            			newLeafNode(otherlv_1, grammarAccess.getSumFunctionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXpath.g:2265:3: ( (lv_sumElement_2_0= rulePathExpr ) )
            // InternalXpath.g:2266:4: (lv_sumElement_2_0= rulePathExpr )
            {
            // InternalXpath.g:2266:4: (lv_sumElement_2_0= rulePathExpr )
            // InternalXpath.g:2267:5: lv_sumElement_2_0= rulePathExpr
            {

            					newCompositeNode(grammarAccess.getSumFunctionAccess().getSumElementPathExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_14);
            lv_sumElement_2_0=rulePathExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSumFunctionRule());
            					}
            					set(
            						current,
            						"sumElement",
            						lv_sumElement_2_0,
            						"pt.opensoft.Xpath.PathExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_28); 

            			newLeafNode(otherlv_3, grammarAccess.getSumFunctionAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,32,FOLLOW_27); 

            			newLeafNode(otherlv_4, grammarAccess.getSumFunctionAccess().getEqualsSignKeyword_4());
            		
            // InternalXpath.g:2292:3: ( (lv_resultElement_5_0= rulePathExpr ) )
            // InternalXpath.g:2293:4: (lv_resultElement_5_0= rulePathExpr )
            {
            // InternalXpath.g:2293:4: (lv_resultElement_5_0= rulePathExpr )
            // InternalXpath.g:2294:5: lv_resultElement_5_0= rulePathExpr
            {

            					newCompositeNode(grammarAccess.getSumFunctionAccess().getResultElementPathExprParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_2);
            lv_resultElement_5_0=rulePathExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSumFunctionRule());
            					}
            					set(
            						current,
            						"resultElement",
            						lv_resultElement_5_0,
            						"pt.opensoft.Xpath.PathExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSumFunction"


    // $ANTLR start "entryRuleCountFunction"
    // InternalXpath.g:2315:1: entryRuleCountFunction returns [EObject current=null] : iv_ruleCountFunction= ruleCountFunction EOF ;
    public final EObject entryRuleCountFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCountFunction = null;


        try {
            // InternalXpath.g:2315:54: (iv_ruleCountFunction= ruleCountFunction EOF )
            // InternalXpath.g:2316:2: iv_ruleCountFunction= ruleCountFunction EOF
            {
             newCompositeNode(grammarAccess.getCountFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCountFunction=ruleCountFunction();

            state._fsp--;

             current =iv_ruleCountFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCountFunction"


    // $ANTLR start "ruleCountFunction"
    // InternalXpath.g:2322:1: ruleCountFunction returns [EObject current=null] : (otherlv_0= 'count' otherlv_1= '(' ( (lv_countElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) ) ;
    public final EObject ruleCountFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_countElement_2_0 = null;

        EObject lv_resultElement_5_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2328:2: ( (otherlv_0= 'count' otherlv_1= '(' ( (lv_countElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) ) )
            // InternalXpath.g:2329:2: (otherlv_0= 'count' otherlv_1= '(' ( (lv_countElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) )
            {
            // InternalXpath.g:2329:2: (otherlv_0= 'count' otherlv_1= '(' ( (lv_countElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) ) )
            // InternalXpath.g:2330:3: otherlv_0= 'count' otherlv_1= '(' ( (lv_countElement_2_0= rulePathExpr ) ) otherlv_3= ')' otherlv_4= '=' ( (lv_resultElement_5_0= rulePathExpr ) )
            {
            otherlv_0=(Token)match(input,51,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getCountFunctionAccess().getCountKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_27); 

            			newLeafNode(otherlv_1, grammarAccess.getCountFunctionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXpath.g:2338:3: ( (lv_countElement_2_0= rulePathExpr ) )
            // InternalXpath.g:2339:4: (lv_countElement_2_0= rulePathExpr )
            {
            // InternalXpath.g:2339:4: (lv_countElement_2_0= rulePathExpr )
            // InternalXpath.g:2340:5: lv_countElement_2_0= rulePathExpr
            {

            					newCompositeNode(grammarAccess.getCountFunctionAccess().getCountElementPathExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_14);
            lv_countElement_2_0=rulePathExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCountFunctionRule());
            					}
            					set(
            						current,
            						"countElement",
            						lv_countElement_2_0,
            						"pt.opensoft.Xpath.PathExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_28); 

            			newLeafNode(otherlv_3, grammarAccess.getCountFunctionAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,32,FOLLOW_27); 

            			newLeafNode(otherlv_4, grammarAccess.getCountFunctionAccess().getEqualsSignKeyword_4());
            		
            // InternalXpath.g:2365:3: ( (lv_resultElement_5_0= rulePathExpr ) )
            // InternalXpath.g:2366:4: (lv_resultElement_5_0= rulePathExpr )
            {
            // InternalXpath.g:2366:4: (lv_resultElement_5_0= rulePathExpr )
            // InternalXpath.g:2367:5: lv_resultElement_5_0= rulePathExpr
            {

            					newCompositeNode(grammarAccess.getCountFunctionAccess().getResultElementPathExprParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_2);
            lv_resultElement_5_0=rulePathExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCountFunctionRule());
            					}
            					set(
            						current,
            						"resultElement",
            						lv_resultElement_5_0,
            						"pt.opensoft.Xpath.PathExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCountFunction"


    // $ANTLR start "entryRuleStringLiteral"
    // InternalXpath.g:2388:1: entryRuleStringLiteral returns [EObject current=null] : iv_ruleStringLiteral= ruleStringLiteral EOF ;
    public final EObject entryRuleStringLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringLiteral = null;


        try {
            // InternalXpath.g:2388:54: (iv_ruleStringLiteral= ruleStringLiteral EOF )
            // InternalXpath.g:2389:2: iv_ruleStringLiteral= ruleStringLiteral EOF
            {
             newCompositeNode(grammarAccess.getStringLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringLiteral=ruleStringLiteral();

            state._fsp--;

             current =iv_ruleStringLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringLiteral"


    // $ANTLR start "ruleStringLiteral"
    // InternalXpath.g:2395:1: ruleStringLiteral returns [EObject current=null] : ( (lv_name_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalXpath.g:2401:2: ( ( (lv_name_0_0= RULE_STRING ) ) )
            // InternalXpath.g:2402:2: ( (lv_name_0_0= RULE_STRING ) )
            {
            // InternalXpath.g:2402:2: ( (lv_name_0_0= RULE_STRING ) )
            // InternalXpath.g:2403:3: (lv_name_0_0= RULE_STRING )
            {
            // InternalXpath.g:2403:3: (lv_name_0_0= RULE_STRING )
            // InternalXpath.g:2404:4: lv_name_0_0= RULE_STRING
            {
            lv_name_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getStringLiteralAccess().getNameSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getStringLiteralRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringLiteral"


    // $ANTLR start "entryRuleNumericLiteral"
    // InternalXpath.g:2423:1: entryRuleNumericLiteral returns [EObject current=null] : iv_ruleNumericLiteral= ruleNumericLiteral EOF ;
    public final EObject entryRuleNumericLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumericLiteral = null;


        try {
            // InternalXpath.g:2423:55: (iv_ruleNumericLiteral= ruleNumericLiteral EOF )
            // InternalXpath.g:2424:2: iv_ruleNumericLiteral= ruleNumericLiteral EOF
            {
             newCompositeNode(grammarAccess.getNumericLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNumericLiteral=ruleNumericLiteral();

            state._fsp--;

             current =iv_ruleNumericLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumericLiteral"


    // $ANTLR start "ruleNumericLiteral"
    // InternalXpath.g:2430:1: ruleNumericLiteral returns [EObject current=null] : ( (lv_number_0_0= RULE_INT ) ) ;
    public final EObject ruleNumericLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_number_0_0=null;


        	enterRule();

        try {
            // InternalXpath.g:2436:2: ( ( (lv_number_0_0= RULE_INT ) ) )
            // InternalXpath.g:2437:2: ( (lv_number_0_0= RULE_INT ) )
            {
            // InternalXpath.g:2437:2: ( (lv_number_0_0= RULE_INT ) )
            // InternalXpath.g:2438:3: (lv_number_0_0= RULE_INT )
            {
            // InternalXpath.g:2438:3: (lv_number_0_0= RULE_INT )
            // InternalXpath.g:2439:4: lv_number_0_0= RULE_INT
            {
            lv_number_0_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            				newLeafNode(lv_number_0_0, grammarAccess.getNumericLiteralAccess().getNumberINTTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getNumericLiteralRule());
            				}
            				setWithLastConsumed(
            					current,
            					"number",
            					lv_number_0_0,
            					"org.eclipse.xtext.common.Terminals.INT");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumericLiteral"


    // $ANTLR start "entryRuleThenElse"
    // InternalXpath.g:2458:1: entryRuleThenElse returns [EObject current=null] : iv_ruleThenElse= ruleThenElse EOF ;
    public final EObject entryRuleThenElse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThenElse = null;


        try {
            // InternalXpath.g:2458:49: (iv_ruleThenElse= ruleThenElse EOF )
            // InternalXpath.g:2459:2: iv_ruleThenElse= ruleThenElse EOF
            {
             newCompositeNode(grammarAccess.getThenElseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleThenElse=ruleThenElse();

            state._fsp--;

             current =iv_ruleThenElse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThenElse"


    // $ANTLR start "ruleThenElse"
    // InternalXpath.g:2465:1: ruleThenElse returns [EObject current=null] : ( (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) ) | (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) ) ) ;
    public final EObject ruleThenElse() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_thenExprTrue_1_0 = null;

        EObject lv_elseExprFalse_3_0 = null;

        EObject lv_thenExprFalse_5_0 = null;

        AntlrDatatypeRuleToken lv_elseExprTrue_7_0 = null;



        	enterRule();

        try {
            // InternalXpath.g:2471:2: ( ( (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) ) | (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) ) ) )
            // InternalXpath.g:2472:2: ( (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) ) | (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) ) )
            {
            // InternalXpath.g:2472:2: ( (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) ) | (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) ) )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==52) ) {
                int LA30_1 = input.LA(2);

                if ( (LA30_1==RULE_STRING||LA30_1==55) ) {
                    alt30=2;
                }
                else if ( (LA30_1==54) ) {
                    alt30=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalXpath.g:2473:3: (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) )
                    {
                    // InternalXpath.g:2473:3: (otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) ) )
                    // InternalXpath.g:2474:4: otherlv_0= 'then' ( (lv_thenExprTrue_1_0= ruleTrue ) ) otherlv_2= 'else' ( (lv_elseExprFalse_3_0= ruleFalse ) )
                    {
                    otherlv_0=(Token)match(input,52,FOLLOW_29); 

                    				newLeafNode(otherlv_0, grammarAccess.getThenElseAccess().getThenKeyword_0_0());
                    			
                    // InternalXpath.g:2478:4: ( (lv_thenExprTrue_1_0= ruleTrue ) )
                    // InternalXpath.g:2479:5: (lv_thenExprTrue_1_0= ruleTrue )
                    {
                    // InternalXpath.g:2479:5: (lv_thenExprTrue_1_0= ruleTrue )
                    // InternalXpath.g:2480:6: lv_thenExprTrue_1_0= ruleTrue
                    {

                    						newCompositeNode(grammarAccess.getThenElseAccess().getThenExprTrueTrueParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_30);
                    lv_thenExprTrue_1_0=ruleTrue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getThenElseRule());
                    						}
                    						set(
                    							current,
                    							"thenExprTrue",
                    							lv_thenExprTrue_1_0,
                    							"pt.opensoft.Xpath.True");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,53,FOLLOW_31); 

                    				newLeafNode(otherlv_2, grammarAccess.getThenElseAccess().getElseKeyword_0_2());
                    			
                    // InternalXpath.g:2501:4: ( (lv_elseExprFalse_3_0= ruleFalse ) )
                    // InternalXpath.g:2502:5: (lv_elseExprFalse_3_0= ruleFalse )
                    {
                    // InternalXpath.g:2502:5: (lv_elseExprFalse_3_0= ruleFalse )
                    // InternalXpath.g:2503:6: lv_elseExprFalse_3_0= ruleFalse
                    {

                    						newCompositeNode(grammarAccess.getThenElseAccess().getElseExprFalseFalseParserRuleCall_0_3_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_elseExprFalse_3_0=ruleFalse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getThenElseRule());
                    						}
                    						set(
                    							current,
                    							"elseExprFalse",
                    							lv_elseExprFalse_3_0,
                    							"pt.opensoft.Xpath.False");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:2522:3: (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) )
                    {
                    // InternalXpath.g:2522:3: (otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) ) )
                    // InternalXpath.g:2523:4: otherlv_4= 'then' ( (lv_thenExprFalse_5_0= ruleFalse ) ) otherlv_6= 'else' ( (lv_elseExprTrue_7_0= ruleTrue ) )
                    {
                    otherlv_4=(Token)match(input,52,FOLLOW_31); 

                    				newLeafNode(otherlv_4, grammarAccess.getThenElseAccess().getThenKeyword_1_0());
                    			
                    // InternalXpath.g:2527:4: ( (lv_thenExprFalse_5_0= ruleFalse ) )
                    // InternalXpath.g:2528:5: (lv_thenExprFalse_5_0= ruleFalse )
                    {
                    // InternalXpath.g:2528:5: (lv_thenExprFalse_5_0= ruleFalse )
                    // InternalXpath.g:2529:6: lv_thenExprFalse_5_0= ruleFalse
                    {

                    						newCompositeNode(grammarAccess.getThenElseAccess().getThenExprFalseFalseParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_30);
                    lv_thenExprFalse_5_0=ruleFalse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getThenElseRule());
                    						}
                    						set(
                    							current,
                    							"thenExprFalse",
                    							lv_thenExprFalse_5_0,
                    							"pt.opensoft.Xpath.False");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,53,FOLLOW_29); 

                    				newLeafNode(otherlv_6, grammarAccess.getThenElseAccess().getElseKeyword_1_2());
                    			
                    // InternalXpath.g:2550:4: ( (lv_elseExprTrue_7_0= ruleTrue ) )
                    // InternalXpath.g:2551:5: (lv_elseExprTrue_7_0= ruleTrue )
                    {
                    // InternalXpath.g:2551:5: (lv_elseExprTrue_7_0= ruleTrue )
                    // InternalXpath.g:2552:6: lv_elseExprTrue_7_0= ruleTrue
                    {

                    						newCompositeNode(grammarAccess.getThenElseAccess().getElseExprTrueTrueParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_elseExprTrue_7_0=ruleTrue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getThenElseRule());
                    						}
                    						set(
                    							current,
                    							"elseExprTrue",
                    							lv_elseExprTrue_7_0,
                    							"pt.opensoft.Xpath.True");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThenElse"


    // $ANTLR start "entryRuleTrue"
    // InternalXpath.g:2574:1: entryRuleTrue returns [String current=null] : iv_ruleTrue= ruleTrue EOF ;
    public final String entryRuleTrue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTrue = null;


        try {
            // InternalXpath.g:2574:44: (iv_ruleTrue= ruleTrue EOF )
            // InternalXpath.g:2575:2: iv_ruleTrue= ruleTrue EOF
            {
             newCompositeNode(grammarAccess.getTrueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTrue=ruleTrue();

            state._fsp--;

             current =iv_ruleTrue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTrue"


    // $ANTLR start "ruleTrue"
    // InternalXpath.g:2581:1: ruleTrue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'true()' ;
    public final AntlrDatatypeRuleToken ruleTrue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXpath.g:2587:2: (kw= 'true()' )
            // InternalXpath.g:2588:2: kw= 'true()'
            {
            kw=(Token)match(input,54,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getTrueAccess().getTrueKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTrue"


    // $ANTLR start "entryRuleFalse"
    // InternalXpath.g:2596:1: entryRuleFalse returns [EObject current=null] : iv_ruleFalse= ruleFalse EOF ;
    public final EObject entryRuleFalse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFalse = null;


        try {
            // InternalXpath.g:2596:46: (iv_ruleFalse= ruleFalse EOF )
            // InternalXpath.g:2597:2: iv_ruleFalse= ruleFalse EOF
            {
             newCompositeNode(grammarAccess.getFalseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFalse=ruleFalse();

            state._fsp--;

             current =iv_ruleFalse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFalse"


    // $ANTLR start "ruleFalse"
    // InternalXpath.g:2603:1: ruleFalse returns [EObject current=null] : ( ( (lv_isFalse_0_0= 'false()' ) ) | ( (lv_msg_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleFalse() throws RecognitionException {
        EObject current = null;

        Token lv_isFalse_0_0=null;
        Token lv_msg_1_0=null;


        	enterRule();

        try {
            // InternalXpath.g:2609:2: ( ( ( (lv_isFalse_0_0= 'false()' ) ) | ( (lv_msg_1_0= RULE_STRING ) ) ) )
            // InternalXpath.g:2610:2: ( ( (lv_isFalse_0_0= 'false()' ) ) | ( (lv_msg_1_0= RULE_STRING ) ) )
            {
            // InternalXpath.g:2610:2: ( ( (lv_isFalse_0_0= 'false()' ) ) | ( (lv_msg_1_0= RULE_STRING ) ) )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==55) ) {
                alt31=1;
            }
            else if ( (LA31_0==RULE_STRING) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalXpath.g:2611:3: ( (lv_isFalse_0_0= 'false()' ) )
                    {
                    // InternalXpath.g:2611:3: ( (lv_isFalse_0_0= 'false()' ) )
                    // InternalXpath.g:2612:4: (lv_isFalse_0_0= 'false()' )
                    {
                    // InternalXpath.g:2612:4: (lv_isFalse_0_0= 'false()' )
                    // InternalXpath.g:2613:5: lv_isFalse_0_0= 'false()'
                    {
                    lv_isFalse_0_0=(Token)match(input,55,FOLLOW_2); 

                    					newLeafNode(lv_isFalse_0_0, grammarAccess.getFalseAccess().getIsFalseFalseKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFalseRule());
                    					}
                    					setWithLastConsumed(current, "isFalse", lv_isFalse_0_0, "false()");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:2626:3: ( (lv_msg_1_0= RULE_STRING ) )
                    {
                    // InternalXpath.g:2626:3: ( (lv_msg_1_0= RULE_STRING ) )
                    // InternalXpath.g:2627:4: (lv_msg_1_0= RULE_STRING )
                    {
                    // InternalXpath.g:2627:4: (lv_msg_1_0= RULE_STRING )
                    // InternalXpath.g:2628:5: lv_msg_1_0= RULE_STRING
                    {
                    lv_msg_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_msg_1_0, grammarAccess.getFalseAccess().getMsgSTRINGTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFalseRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"msg",
                    						lv_msg_1_0,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFalse"

    // Delegated rules


    protected DFA12 dfa12 = new DFA12(this);
    static final String dfa_1s = "\u00be\uffff";
    static final String dfa_2s = "\3\uffff\2\34\3\uffff\1\34\32\uffff\2\34\1\uffff\2\34\2\uffff\2\34\7\uffff\2\34\2\uffff\1\34\27\uffff\1\34\5\uffff\2\34\3\uffff\2\34\1\uffff\2\34\5\uffff\1\34\10\uffff\1\34\1\uffff\2\34\12\uffff\2\34\2\uffff\1\34\5\uffff\2\34\2\uffff\1\34\16\uffff\1\34\5\uffff\5\34\14\uffff\2\34\2\uffff\1\34\4\uffff\2\34\2\uffff\2\34";
    static final String dfa_3s = "\1\5\1\uffff\1\4\2\13\3\26\1\13\23\5\1\uffff\5\61\1\4\2\13\1\4\2\13\1\uffff\1\4\2\13\3\26\4\4\2\13\2\5\1\13\21\5\3\61\3\14\1\13\4\61\1\4\2\13\2\61\1\4\2\13\1\4\2\13\3\4\2\61\1\13\2\61\1\40\2\61\1\40\2\4\1\13\1\4\2\13\2\5\3\14\2\4\1\61\1\4\1\61\2\13\2\61\1\13\4\61\1\4\2\13\2\61\1\13\2\61\1\40\2\61\1\40\2\14\1\4\1\14\4\4\1\13\2\4\1\61\1\4\1\61\5\13\2\61\2\14\1\4\1\14\1\4\4\61\1\4\2\13\2\4\1\13\4\61\2\13\2\4\2\13";
    static final String dfa_4s = "\1\63\1\uffff\1\4\2\56\3\26\1\57\21\61\2\63\1\uffff\5\61\1\4\2\56\1\4\2\33\1\uffff\1\4\2\56\3\26\4\4\2\57\2\61\1\57\24\61\4\57\4\61\1\4\2\33\2\61\1\4\2\56\1\4\2\33\3\4\2\61\1\31\2\61\1\40\2\61\1\40\2\4\1\57\1\4\2\57\2\61\3\57\2\4\1\61\1\4\1\61\2\57\2\61\1\57\4\61\1\4\2\33\2\61\1\31\2\61\1\40\2\61\1\40\2\57\1\4\1\57\4\4\1\57\2\4\1\61\1\4\1\61\5\57\2\61\2\57\1\4\1\57\1\4\4\61\1\4\2\57\2\4\1\57\4\61\2\57\2\4\2\57";
    static final String dfa_5s = "\1\uffff\1\1\32\uffff\1\3\13\uffff\1\2\u0095\uffff";
    static final String dfa_6s = "\u00be\uffff}>";
    static final String[] dfa_7s = {
            "\1\3\1\4\17\uffff\1\1\31\uffff\1\5\1\2\1\6\1\7",
            "",
            "\1\10",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31",
            "\1\35",
            "\1\36",
            "\1\37",
            "\1\34\1\40\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\41",
            "\1\43\1\44\52\uffff\1\42",
            "\1\43\1\44\52\uffff\1\42",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\52\1\53\17\uffff\1\50\31\uffff\1\54\1\51\1\55\1\56",
            "\2\34\17\uffff\1\50\31\uffff\4\34",
            "",
            "\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\62",
            "\1\63",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31",
            "\1\64",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66",
            "",
            "\1\67",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110",
            "\1\111",
            "\1\112",
            "\1\113",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\117",
            "\1\34\1\120\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\121",
            "\1\34\1\122\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66\23\uffff\1\123",
            "\1\125\1\126\52\uffff\1\124",
            "\1\125\1\126\52\uffff\1\124",
            "\1\34\1\127\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\130",
            "\1\132\1\133\52\uffff\1\131",
            "\1\132\1\133\52\uffff\1\131",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142\12\uffff\1\144\27\uffff\1\143",
            "\1\145\12\uffff\1\147\27\uffff\1\146",
            "\1\150\12\uffff\1\152\27\uffff\1\151",
            "\1\34\1\40\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\41",
            "\1\153",
            "\1\153",
            "\1\154",
            "\1\154",
            "\1\155",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66",
            "\1\156",
            "\1\156",
            "\1\157",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110",
            "\1\160",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\166",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\167",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "\1\34\1\175\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66\23\uffff\1\176",
            "\1\177",
            "\1\34\1\u0080\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\u0081",
            "\1\34\1\u0082\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162\23\uffff\1\u0083",
            "\1\u0085\1\u0086\52\uffff\1\u0084",
            "\1\u0085\1\u0086\52\uffff\1\u0084",
            "\1\u0087\12\uffff\1\u0089\27\uffff\1\u0088",
            "\1\u008a\12\uffff\1\u008c\27\uffff\1\u008b",
            "\1\u008d\12\uffff\1\u008f\27\uffff\1\u008e",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\34\1\120\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\121",
            "\1\34\1\122\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66\23\uffff\1\123",
            "\1\u0095",
            "\1\u0095",
            "\1\34\1\127\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\130",
            "\1\u0096",
            "\1\u0096",
            "\1\u0097",
            "\1\u0097",
            "\1\u0098",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162",
            "\1\u0099",
            "\1\u0099",
            "\1\34\1\uffff\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u009a",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009c",
            "\1\u009d",
            "\1\142\12\uffff\1\144\27\uffff\1\143",
            "\1\145\12\uffff\1\147\27\uffff\1\146",
            "\1\u009e",
            "\1\150\12\uffff\1\152\27\uffff\1\151",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\34\1\u00a3\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162\23\uffff\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\34\1\u00aa\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00ab",
            "\1\34\1\u00ac\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00ad",
            "\1\34\1\175\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\65\1\66\23\uffff\1\176",
            "\1\34\1\u0080\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\u0081",
            "\1\34\1\u0082\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162\23\uffff\1\u0083",
            "\1\u00ae",
            "\1\u00ae",
            "\1\u0087\12\uffff\1\u0089\27\uffff\1\u0088",
            "\1\u008a\12\uffff\1\u008c\27\uffff\1\u008b",
            "\1\u00af",
            "\1\u008d\12\uffff\1\u008f\27\uffff\1\u008e",
            "\1\u00b0",
            "\1\u00b1",
            "\1\u00b1",
            "\1\u00b2",
            "\1\u00b2",
            "\1\u00b3",
            "\1\34\1\u00b4\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00b5",
            "\1\34\1\u00b6\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00b7",
            "\1\u00b8",
            "\1\u00b9",
            "\1\34\1\u00a3\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\1\161\1\162\23\uffff\1\u00a4",
            "\1\u00ba",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bb",
            "\1\34\1\u00aa\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00ab",
            "\1\34\1\u00ac\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00ad",
            "\1\u00bc",
            "\1\u00bd",
            "\1\34\1\u00b4\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00b5",
            "\1\34\1\u00b6\2\34\5\uffff\1\34\2\uffff\1\34\1\33\1\32\25\uffff\1\u00b7"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "813:2: ( (otherlv_0= '(' ( (lv_groupedCondition_1_0= ruleOrExpr ) ) otherlv_2= ')' ( ( ( (lv_operator_3_0= 'or' ) ) | ( (lv_operator_4_0= 'and' ) ) ) ( (lv_orExpr_5_0= ruleOrExpr ) ) )? ) | ( ( (lv_andExpr_6_0= ruleAndExpr ) ) ( ( (lv_operator_7_0= 'or' ) ) | ( (lv_operator_8_0= 'and' ) ) ) otherlv_9= '(' ( (lv_rightGroupedCondition_10_0= ruleOrExpr ) ) otherlv_11= ')' ) | ( ( (lv_andExpr_12_0= ruleAndExpr ) ) (otherlv_13= 'or' ( (lv_orExpr_list_14_0= ruleAndExpr ) ) )* ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000F0000006C8060L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000102000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000003000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x000F000000000060L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00007FFF00000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0002000000000060L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000030000002L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000000C0000002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000800000001002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0080000000000020L});

}