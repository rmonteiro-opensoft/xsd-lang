/**
 * generated by Xtext 2.22.0
 */
package pt.opensoft.xpath;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expr Single</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.opensoft.xpath.ExprSingle#getForExpr <em>For Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.ExprSingle#getQuantExpr <em>Quant Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.ExprSingle#getIfExpr <em>If Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.ExprSingle#getOrExpr <em>Or Expr</em>}</li>
 * </ul>
 *
 * @see pt.opensoft.xpath.XpathPackage#getExprSingle()
 * @model
 * @generated
 */
public interface ExprSingle extends EObject
{
  /**
   * Returns the value of the '<em><b>For Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>For Expr</em>' containment reference.
   * @see #setForExpr(ForExpr)
   * @see pt.opensoft.xpath.XpathPackage#getExprSingle_ForExpr()
   * @model containment="true"
   * @generated
   */
  ForExpr getForExpr();

  /**
   * Sets the value of the '{@link pt.opensoft.xpath.ExprSingle#getForExpr <em>For Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>For Expr</em>' containment reference.
   * @see #getForExpr()
   * @generated
   */
  void setForExpr(ForExpr value);

  /**
   * Returns the value of the '<em><b>Quant Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Quant Expr</em>' containment reference.
   * @see #setQuantExpr(QuantifiedExpr)
   * @see pt.opensoft.xpath.XpathPackage#getExprSingle_QuantExpr()
   * @model containment="true"
   * @generated
   */
  QuantifiedExpr getQuantExpr();

  /**
   * Sets the value of the '{@link pt.opensoft.xpath.ExprSingle#getQuantExpr <em>Quant Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Quant Expr</em>' containment reference.
   * @see #getQuantExpr()
   * @generated
   */
  void setQuantExpr(QuantifiedExpr value);

  /**
   * Returns the value of the '<em><b>If Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>If Expr</em>' containment reference.
   * @see #setIfExpr(IfExpr)
   * @see pt.opensoft.xpath.XpathPackage#getExprSingle_IfExpr()
   * @model containment="true"
   * @generated
   */
  IfExpr getIfExpr();

  /**
   * Sets the value of the '{@link pt.opensoft.xpath.ExprSingle#getIfExpr <em>If Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>If Expr</em>' containment reference.
   * @see #getIfExpr()
   * @generated
   */
  void setIfExpr(IfExpr value);

  /**
   * Returns the value of the '<em><b>Or Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Or Expr</em>' containment reference.
   * @see #setOrExpr(OrExpr)
   * @see pt.opensoft.xpath.XpathPackage#getExprSingle_OrExpr()
   * @model containment="true"
   * @generated
   */
  OrExpr getOrExpr();

  /**
   * Sets the value of the '{@link pt.opensoft.xpath.ExprSingle#getOrExpr <em>Or Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Or Expr</em>' containment reference.
   * @see #getOrExpr()
   * @generated
   */
  void setOrExpr(OrExpr value);

} // ExprSingle
