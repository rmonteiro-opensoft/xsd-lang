/**
 * generated by Xtext 2.22.0
 */
package pt.opensoft.xpath;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Additive Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.opensoft.xpath.AdditiveExpr#getValueExpr <em>Value Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.AdditiveExpr#getAdditive_list <em>Additive list</em>}</li>
 *   <li>{@link pt.opensoft.xpath.AdditiveExpr#getValueExpr_list <em>Value Expr list</em>}</li>
 * </ul>
 *
 * @see pt.opensoft.xpath.XpathPackage#getAdditiveExpr()
 * @model
 * @generated
 */
public interface AdditiveExpr extends EObject
{
  /**
   * Returns the value of the '<em><b>Value Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value Expr</em>' containment reference.
   * @see #setValueExpr(ValueExpr)
   * @see pt.opensoft.xpath.XpathPackage#getAdditiveExpr_ValueExpr()
   * @model containment="true"
   * @generated
   */
  ValueExpr getValueExpr();

  /**
   * Sets the value of the '{@link pt.opensoft.xpath.AdditiveExpr#getValueExpr <em>Value Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value Expr</em>' containment reference.
   * @see #getValueExpr()
   * @generated
   */
  void setValueExpr(ValueExpr value);

  /**
   * Returns the value of the '<em><b>Additive list</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Additive list</em>' attribute list.
   * @see pt.opensoft.xpath.XpathPackage#getAdditiveExpr_Additive_list()
   * @model unique="false"
   * @generated
   */
  EList<String> getAdditive_list();

  /**
   * Returns the value of the '<em><b>Value Expr list</b></em>' containment reference list.
   * The list contents are of type {@link pt.opensoft.xpath.ValueExpr}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value Expr list</em>' containment reference list.
   * @see pt.opensoft.xpath.XpathPackage#getAdditiveExpr_ValueExpr_list()
   * @model containment="true"
   * @generated
   */
  EList<ValueExpr> getValueExpr_list();

} // AdditiveExpr
