/**
 * generated by Xtext 2.22.0
 */
package pt.opensoft.xpath.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pt.opensoft.xpath.PathExpr;
import pt.opensoft.xpath.SumFunction;
import pt.opensoft.xpath.XpathPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sum Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.opensoft.xpath.impl.SumFunctionImpl#getSumElement <em>Sum Element</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.SumFunctionImpl#getResultElement <em>Result Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SumFunctionImpl extends MinimalEObjectImpl.Container implements SumFunction
{
  /**
   * The cached value of the '{@link #getSumElement() <em>Sum Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSumElement()
   * @generated
   * @ordered
   */
  protected PathExpr sumElement;

  /**
   * The cached value of the '{@link #getResultElement() <em>Result Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultElement()
   * @generated
   * @ordered
   */
  protected PathExpr resultElement;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SumFunctionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XpathPackage.Literals.SUM_FUNCTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public PathExpr getSumElement()
  {
    return sumElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSumElement(PathExpr newSumElement, NotificationChain msgs)
  {
    PathExpr oldSumElement = sumElement;
    sumElement = newSumElement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.SUM_FUNCTION__SUM_ELEMENT, oldSumElement, newSumElement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setSumElement(PathExpr newSumElement)
  {
    if (newSumElement != sumElement)
    {
      NotificationChain msgs = null;
      if (sumElement != null)
        msgs = ((InternalEObject)sumElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.SUM_FUNCTION__SUM_ELEMENT, null, msgs);
      if (newSumElement != null)
        msgs = ((InternalEObject)newSumElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.SUM_FUNCTION__SUM_ELEMENT, null, msgs);
      msgs = basicSetSumElement(newSumElement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.SUM_FUNCTION__SUM_ELEMENT, newSumElement, newSumElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public PathExpr getResultElement()
  {
    return resultElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResultElement(PathExpr newResultElement, NotificationChain msgs)
  {
    PathExpr oldResultElement = resultElement;
    resultElement = newResultElement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.SUM_FUNCTION__RESULT_ELEMENT, oldResultElement, newResultElement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setResultElement(PathExpr newResultElement)
  {
    if (newResultElement != resultElement)
    {
      NotificationChain msgs = null;
      if (resultElement != null)
        msgs = ((InternalEObject)resultElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.SUM_FUNCTION__RESULT_ELEMENT, null, msgs);
      if (newResultElement != null)
        msgs = ((InternalEObject)newResultElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.SUM_FUNCTION__RESULT_ELEMENT, null, msgs);
      msgs = basicSetResultElement(newResultElement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.SUM_FUNCTION__RESULT_ELEMENT, newResultElement, newResultElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XpathPackage.SUM_FUNCTION__SUM_ELEMENT:
        return basicSetSumElement(null, msgs);
      case XpathPackage.SUM_FUNCTION__RESULT_ELEMENT:
        return basicSetResultElement(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XpathPackage.SUM_FUNCTION__SUM_ELEMENT:
        return getSumElement();
      case XpathPackage.SUM_FUNCTION__RESULT_ELEMENT:
        return getResultElement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XpathPackage.SUM_FUNCTION__SUM_ELEMENT:
        setSumElement((PathExpr)newValue);
        return;
      case XpathPackage.SUM_FUNCTION__RESULT_ELEMENT:
        setResultElement((PathExpr)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.SUM_FUNCTION__SUM_ELEMENT:
        setSumElement((PathExpr)null);
        return;
      case XpathPackage.SUM_FUNCTION__RESULT_ELEMENT:
        setResultElement((PathExpr)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.SUM_FUNCTION__SUM_ELEMENT:
        return sumElement != null;
      case XpathPackage.SUM_FUNCTION__RESULT_ELEMENT:
        return resultElement != null;
    }
    return super.eIsSet(featureID);
  }

} //SumFunctionImpl
