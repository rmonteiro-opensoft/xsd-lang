/**
 * generated by Xtext 2.22.0
 */
package pt.opensoft.xpath.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import pt.opensoft.xpath.AndExpr;
import pt.opensoft.xpath.OrExpr;
import pt.opensoft.xpath.XpathPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Or Expr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getGroupedCondition <em>Grouped Condition</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getOrExpr <em>Or Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getAndExpr <em>And Expr</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getRightGroupedCondition <em>Right Grouped Condition</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.OrExprImpl#getOrExpr_list <em>Or Expr list</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrExprImpl extends MinimalEObjectImpl.Container implements OrExpr
{
  /**
   * The cached value of the '{@link #getGroupedCondition() <em>Grouped Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroupedCondition()
   * @generated
   * @ordered
   */
  protected OrExpr groupedCondition;

  /**
   * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected static final String OPERATOR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected String operator = OPERATOR_EDEFAULT;

  /**
   * The cached value of the '{@link #getOrExpr() <em>Or Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrExpr()
   * @generated
   * @ordered
   */
  protected OrExpr orExpr;

  /**
   * The cached value of the '{@link #getAndExpr() <em>And Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAndExpr()
   * @generated
   * @ordered
   */
  protected AndExpr andExpr;

  /**
   * The cached value of the '{@link #getRightGroupedCondition() <em>Right Grouped Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRightGroupedCondition()
   * @generated
   * @ordered
   */
  protected OrExpr rightGroupedCondition;

  /**
   * The cached value of the '{@link #getOrExpr_list() <em>Or Expr list</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrExpr_list()
   * @generated
   * @ordered
   */
  protected EList<AndExpr> orExpr_list;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OrExprImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XpathPackage.Literals.OR_EXPR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OrExpr getGroupedCondition()
  {
    return groupedCondition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroupedCondition(OrExpr newGroupedCondition, NotificationChain msgs)
  {
    OrExpr oldGroupedCondition = groupedCondition;
    groupedCondition = newGroupedCondition;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__GROUPED_CONDITION, oldGroupedCondition, newGroupedCondition);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setGroupedCondition(OrExpr newGroupedCondition)
  {
    if (newGroupedCondition != groupedCondition)
    {
      NotificationChain msgs = null;
      if (groupedCondition != null)
        msgs = ((InternalEObject)groupedCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__GROUPED_CONDITION, null, msgs);
      if (newGroupedCondition != null)
        msgs = ((InternalEObject)newGroupedCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__GROUPED_CONDITION, null, msgs);
      msgs = basicSetGroupedCondition(newGroupedCondition, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__GROUPED_CONDITION, newGroupedCondition, newGroupedCondition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getOperator()
  {
    return operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOperator(String newOperator)
  {
    String oldOperator = operator;
    operator = newOperator;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__OPERATOR, oldOperator, operator));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OrExpr getOrExpr()
  {
    return orExpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOrExpr(OrExpr newOrExpr, NotificationChain msgs)
  {
    OrExpr oldOrExpr = orExpr;
    orExpr = newOrExpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__OR_EXPR, oldOrExpr, newOrExpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOrExpr(OrExpr newOrExpr)
  {
    if (newOrExpr != orExpr)
    {
      NotificationChain msgs = null;
      if (orExpr != null)
        msgs = ((InternalEObject)orExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__OR_EXPR, null, msgs);
      if (newOrExpr != null)
        msgs = ((InternalEObject)newOrExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__OR_EXPR, null, msgs);
      msgs = basicSetOrExpr(newOrExpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__OR_EXPR, newOrExpr, newOrExpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AndExpr getAndExpr()
  {
    return andExpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAndExpr(AndExpr newAndExpr, NotificationChain msgs)
  {
    AndExpr oldAndExpr = andExpr;
    andExpr = newAndExpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__AND_EXPR, oldAndExpr, newAndExpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAndExpr(AndExpr newAndExpr)
  {
    if (newAndExpr != andExpr)
    {
      NotificationChain msgs = null;
      if (andExpr != null)
        msgs = ((InternalEObject)andExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__AND_EXPR, null, msgs);
      if (newAndExpr != null)
        msgs = ((InternalEObject)newAndExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__AND_EXPR, null, msgs);
      msgs = basicSetAndExpr(newAndExpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__AND_EXPR, newAndExpr, newAndExpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OrExpr getRightGroupedCondition()
  {
    return rightGroupedCondition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRightGroupedCondition(OrExpr newRightGroupedCondition, NotificationChain msgs)
  {
    OrExpr oldRightGroupedCondition = rightGroupedCondition;
    rightGroupedCondition = newRightGroupedCondition;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION, oldRightGroupedCondition, newRightGroupedCondition);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRightGroupedCondition(OrExpr newRightGroupedCondition)
  {
    if (newRightGroupedCondition != rightGroupedCondition)
    {
      NotificationChain msgs = null;
      if (rightGroupedCondition != null)
        msgs = ((InternalEObject)rightGroupedCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION, null, msgs);
      if (newRightGroupedCondition != null)
        msgs = ((InternalEObject)newRightGroupedCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION, null, msgs);
      msgs = basicSetRightGroupedCondition(newRightGroupedCondition, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION, newRightGroupedCondition, newRightGroupedCondition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<AndExpr> getOrExpr_list()
  {
    if (orExpr_list == null)
    {
      orExpr_list = new EObjectContainmentEList<AndExpr>(AndExpr.class, this, XpathPackage.OR_EXPR__OR_EXPR_LIST);
    }
    return orExpr_list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XpathPackage.OR_EXPR__GROUPED_CONDITION:
        return basicSetGroupedCondition(null, msgs);
      case XpathPackage.OR_EXPR__OR_EXPR:
        return basicSetOrExpr(null, msgs);
      case XpathPackage.OR_EXPR__AND_EXPR:
        return basicSetAndExpr(null, msgs);
      case XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION:
        return basicSetRightGroupedCondition(null, msgs);
      case XpathPackage.OR_EXPR__OR_EXPR_LIST:
        return ((InternalEList<?>)getOrExpr_list()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XpathPackage.OR_EXPR__GROUPED_CONDITION:
        return getGroupedCondition();
      case XpathPackage.OR_EXPR__OPERATOR:
        return getOperator();
      case XpathPackage.OR_EXPR__OR_EXPR:
        return getOrExpr();
      case XpathPackage.OR_EXPR__AND_EXPR:
        return getAndExpr();
      case XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION:
        return getRightGroupedCondition();
      case XpathPackage.OR_EXPR__OR_EXPR_LIST:
        return getOrExpr_list();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XpathPackage.OR_EXPR__GROUPED_CONDITION:
        setGroupedCondition((OrExpr)newValue);
        return;
      case XpathPackage.OR_EXPR__OPERATOR:
        setOperator((String)newValue);
        return;
      case XpathPackage.OR_EXPR__OR_EXPR:
        setOrExpr((OrExpr)newValue);
        return;
      case XpathPackage.OR_EXPR__AND_EXPR:
        setAndExpr((AndExpr)newValue);
        return;
      case XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION:
        setRightGroupedCondition((OrExpr)newValue);
        return;
      case XpathPackage.OR_EXPR__OR_EXPR_LIST:
        getOrExpr_list().clear();
        getOrExpr_list().addAll((Collection<? extends AndExpr>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.OR_EXPR__GROUPED_CONDITION:
        setGroupedCondition((OrExpr)null);
        return;
      case XpathPackage.OR_EXPR__OPERATOR:
        setOperator(OPERATOR_EDEFAULT);
        return;
      case XpathPackage.OR_EXPR__OR_EXPR:
        setOrExpr((OrExpr)null);
        return;
      case XpathPackage.OR_EXPR__AND_EXPR:
        setAndExpr((AndExpr)null);
        return;
      case XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION:
        setRightGroupedCondition((OrExpr)null);
        return;
      case XpathPackage.OR_EXPR__OR_EXPR_LIST:
        getOrExpr_list().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.OR_EXPR__GROUPED_CONDITION:
        return groupedCondition != null;
      case XpathPackage.OR_EXPR__OPERATOR:
        return OPERATOR_EDEFAULT == null ? operator != null : !OPERATOR_EDEFAULT.equals(operator);
      case XpathPackage.OR_EXPR__OR_EXPR:
        return orExpr != null;
      case XpathPackage.OR_EXPR__AND_EXPR:
        return andExpr != null;
      case XpathPackage.OR_EXPR__RIGHT_GROUPED_CONDITION:
        return rightGroupedCondition != null;
      case XpathPackage.OR_EXPR__OR_EXPR_LIST:
        return orExpr_list != null && !orExpr_list.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (operator: ");
    result.append(operator);
    result.append(')');
    return result.toString();
  }

} //OrExprImpl
