/**
 * generated by Xtext 2.22.0
 */
package pt.opensoft.xpath.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pt.opensoft.xpath.CountFunction;
import pt.opensoft.xpath.Function;
import pt.opensoft.xpath.SumFunction;
import pt.opensoft.xpath.XpathPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link pt.opensoft.xpath.impl.FunctionImpl#getSumFunction <em>Sum Function</em>}</li>
 *   <li>{@link pt.opensoft.xpath.impl.FunctionImpl#getCountFunction <em>Count Function</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionImpl extends MinimalEObjectImpl.Container implements Function
{
  /**
   * The cached value of the '{@link #getSumFunction() <em>Sum Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSumFunction()
   * @generated
   * @ordered
   */
  protected SumFunction sumFunction;

  /**
   * The cached value of the '{@link #getCountFunction() <em>Count Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCountFunction()
   * @generated
   * @ordered
   */
  protected CountFunction countFunction;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XpathPackage.Literals.FUNCTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SumFunction getSumFunction()
  {
    return sumFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSumFunction(SumFunction newSumFunction, NotificationChain msgs)
  {
    SumFunction oldSumFunction = sumFunction;
    sumFunction = newSumFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.FUNCTION__SUM_FUNCTION, oldSumFunction, newSumFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setSumFunction(SumFunction newSumFunction)
  {
    if (newSumFunction != sumFunction)
    {
      NotificationChain msgs = null;
      if (sumFunction != null)
        msgs = ((InternalEObject)sumFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.FUNCTION__SUM_FUNCTION, null, msgs);
      if (newSumFunction != null)
        msgs = ((InternalEObject)newSumFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.FUNCTION__SUM_FUNCTION, null, msgs);
      msgs = basicSetSumFunction(newSumFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.FUNCTION__SUM_FUNCTION, newSumFunction, newSumFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public CountFunction getCountFunction()
  {
    return countFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCountFunction(CountFunction newCountFunction, NotificationChain msgs)
  {
    CountFunction oldCountFunction = countFunction;
    countFunction = newCountFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XpathPackage.FUNCTION__COUNT_FUNCTION, oldCountFunction, newCountFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setCountFunction(CountFunction newCountFunction)
  {
    if (newCountFunction != countFunction)
    {
      NotificationChain msgs = null;
      if (countFunction != null)
        msgs = ((InternalEObject)countFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XpathPackage.FUNCTION__COUNT_FUNCTION, null, msgs);
      if (newCountFunction != null)
        msgs = ((InternalEObject)newCountFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XpathPackage.FUNCTION__COUNT_FUNCTION, null, msgs);
      msgs = basicSetCountFunction(newCountFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XpathPackage.FUNCTION__COUNT_FUNCTION, newCountFunction, newCountFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XpathPackage.FUNCTION__SUM_FUNCTION:
        return basicSetSumFunction(null, msgs);
      case XpathPackage.FUNCTION__COUNT_FUNCTION:
        return basicSetCountFunction(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XpathPackage.FUNCTION__SUM_FUNCTION:
        return getSumFunction();
      case XpathPackage.FUNCTION__COUNT_FUNCTION:
        return getCountFunction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XpathPackage.FUNCTION__SUM_FUNCTION:
        setSumFunction((SumFunction)newValue);
        return;
      case XpathPackage.FUNCTION__COUNT_FUNCTION:
        setCountFunction((CountFunction)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.FUNCTION__SUM_FUNCTION:
        setSumFunction((SumFunction)null);
        return;
      case XpathPackage.FUNCTION__COUNT_FUNCTION:
        setCountFunction((CountFunction)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XpathPackage.FUNCTION__SUM_FUNCTION:
        return sumFunction != null;
      case XpathPackage.FUNCTION__COUNT_FUNCTION:
        return countFunction != null;
    }
    return super.eIsSet(featureID);
  }

} //FunctionImpl
