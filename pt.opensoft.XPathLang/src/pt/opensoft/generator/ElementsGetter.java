package pt.opensoft.generator;

import java.util.LinkedList;
import java.util.List;

import pt.opensoft.xpath.AdditiveExpr;
import pt.opensoft.xpath.AndExpr;
import pt.opensoft.xpath.ComparisonExpr;
import pt.opensoft.xpath.CountFunction;
import pt.opensoft.xpath.Element;
import pt.opensoft.xpath.Function;
import pt.opensoft.xpath.NotExpr;
import pt.opensoft.xpath.OrExpr;
import pt.opensoft.xpath.PathExpr;
import pt.opensoft.xpath.SumFunction;
import pt.opensoft.xpath.ValueExpr;

public class ElementsGetter {


	static class Elements {
		List<String> elements;
		List<String> elementsToSum;
		List<String> elementsToCount;

		public Elements() {
			elements = new LinkedList<>();
			elementsToSum = new LinkedList<>();
			elementsToCount = new LinkedList<>();
		}

		void addAll(Elements otherElements){
			elements.addAll(otherElements.elements);
			elementsToSum.addAll(otherElements.elementsToSum);
			elementsToCount.addAll(otherElements.elementsToCount);
		}

	}

	private static Elements elements = new Elements();

	public static Elements getElements(OrExpr orExpr){
		OrExpr groupedCondition = orExpr.getGroupedCondition();
		OrExpr rightGroupedCondition = orExpr.getRightGroupedCondition();
		if (groupedCondition != null) {
			Elements elements = getElements(groupedCondition);
			OrExpr secondOrExpr = orExpr.getOrExpr();
			if (secondOrExpr != null) {
				elements.addAll(getElements(secondOrExpr));
			}
			return elements;
		}
		else if(rightGroupedCondition != null) {
			Elements elements = getElements(orExpr.getAndExpr());
			elements.addAll(getElements(rightGroupedCondition));

			return elements;
		}
		else {
			Elements elements = getElements(orExpr.getAndExpr());
			for(AndExpr andExpr: orExpr.getOrExpr_list()) {
				elements.addAll(getElements(andExpr));
			}
			return elements;
		}

	}

	private static Elements getElements(AndExpr andExpr){
		Elements elements = getElements(andExpr.getComparisonExpr());
		for(ComparisonExpr comparisonExpr: andExpr.getAndCondition_list()) {
			elements.addAll(getElements(comparisonExpr));
		}
		return elements;
	}

	private static Elements getElements(ComparisonExpr comparisonExpr){
		Function function = comparisonExpr.getFunction();
		if (function != null) {
			return getElements(function);
		}

		if(comparisonExpr.getNotExpr() != null) {
			Elements elements = new Elements();
			elements.elements.addAll(getElements(comparisonExpr.getNotExpr()));
			return elements;
		}
		else {
			Elements elements = getElements(comparisonExpr.getAdditiveExpr());
			if(comparisonExpr.getAdditiveExprOptional() != null) {
				elements.addAll(getElements(comparisonExpr.getAdditiveExprOptional()));
			}
			return elements;
		}

	}

	private static Elements getElements(AdditiveExpr additiveExpr){
		Elements elements = getElements(additiveExpr.getValueExpr());
		for(ValueExpr valueExpr: additiveExpr.getValueExpr_list()) {
			elements.addAll(getElements(valueExpr));
		}
		return elements;
	}

	private static Elements getElements(ValueExpr valueExpr){
		PathExpr pathExpr = valueExpr.getPathExpr();
		if (pathExpr != null) {
			Elements elements = new Elements();
			elements.elements.addAll(getElements(pathExpr));
			return elements;
		}

		return new Elements();

	}



	private static Elements getElements(Function function){
		SumFunction sumFunction = function.getSumFunction();
		if (sumFunction != null) {
			return getElements(sumFunction);
		}

		CountFunction countFunction = function.getCountFunction();
		if (countFunction != null) {
			return getElements(countFunction);
		}

		throw new RuntimeException("N�o existe outro tipo de Function");
	}

	private static Elements getElements(SumFunction sumFunction) {
		Elements elements = new Elements();
		elements.elementsToSum.add(getElement(sumFunction.getSumElement()));
		elements.elements.add(getElement(sumFunction.getResultElement()));
		return elements;
	}


	private static Elements getElements(CountFunction countFunction) {
		Elements elements = new Elements();
		elements.elementsToCount.add(getElement(countFunction.getCountElement()));
		elements.elements.add(getElement(countFunction.getResultElement()));
		return elements;
	}


	//Devolve lista para ser mais f�cil de tratar
	private static List<String> getElements(PathExpr pathExpr){
		List<String> elements = new LinkedList<>();
		String element = getElement(pathExpr);
		if (element != null) {
			elements.add(element);
		}
		return elements;
	}

	private static String getElement(PathExpr pathExpr){
		String elemPath = pathExpr.getElement().getName();
		if(pathExpr.getPathElement_list() != null) {
			for(Element element: pathExpr.getPathElement_list()) {
				elemPath += "/" + element.getName();
			}
		}

		if (elemPath != null && ! elemPath.isEmpty()) {
			return elemPath;
		}

		return null;
	}



	private static List<String> getElements(NotExpr notExpr){
		return getElements(notExpr.getPathExpr());
	}

}