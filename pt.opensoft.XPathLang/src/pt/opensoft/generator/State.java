package pt.opensoft.generator;

public class State {
	
	int i = 0;
	
	public void reset() {
		i = 0;
	}
	
	public int getValueAndIncrement() {
		return i++;
	}

}
