package pt.opensoft.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import pt.opensoft.services.XpathGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXpathParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true()'", "'every'", "'+'", "'-'", "'union'", "'|'", "'intersect'", "'except'", "'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'eq'", "'ne'", "'lt'", "'le'", "'gt'", "'ge'", "'is'", "'<<'", "'>>'", "'/'", "'//'", "';'", "','", "'return'", "'for'", "'$'", "'in'", "'satisfies'", "'if'", "'('", "')'", "'or'", "'and'", "'not'", "'ns:'", "'sum'", "'count'", "'then'", "'else'", "'some'", "'false()'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXpathParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXpathParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXpathParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXpath.g"; }


    	private XpathGrammarAccess grammarAccess;

    	public void setGrammarAccess(XpathGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDomainmodel"
    // InternalXpath.g:53:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // InternalXpath.g:54:1: ( ruleDomainmodel EOF )
            // InternalXpath.g:55:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalXpath.g:62:1: ruleDomainmodel : ( ( rule__Domainmodel__Group__0 )* ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:66:2: ( ( ( rule__Domainmodel__Group__0 )* ) )
            // InternalXpath.g:67:2: ( ( rule__Domainmodel__Group__0 )* )
            {
            // InternalXpath.g:67:2: ( ( rule__Domainmodel__Group__0 )* )
            // InternalXpath.g:68:3: ( rule__Domainmodel__Group__0 )*
            {
             before(grammarAccess.getDomainmodelAccess().getGroup()); 
            // InternalXpath.g:69:3: ( rule__Domainmodel__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXpath.g:69:4: rule__Domainmodel__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Domainmodel__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDomainmodelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleValidationPath"
    // InternalXpath.g:78:1: entryRuleValidationPath : ruleValidationPath EOF ;
    public final void entryRuleValidationPath() throws RecognitionException {
        try {
            // InternalXpath.g:79:1: ( ruleValidationPath EOF )
            // InternalXpath.g:80:1: ruleValidationPath EOF
            {
             before(grammarAccess.getValidationPathRule()); 
            pushFollow(FOLLOW_1);
            ruleValidationPath();

            state._fsp--;

             after(grammarAccess.getValidationPathRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValidationPath"


    // $ANTLR start "ruleValidationPath"
    // InternalXpath.g:87:1: ruleValidationPath : ( ( rule__ValidationPath__Group__0 ) ) ;
    public final void ruleValidationPath() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:91:2: ( ( ( rule__ValidationPath__Group__0 ) ) )
            // InternalXpath.g:92:2: ( ( rule__ValidationPath__Group__0 ) )
            {
            // InternalXpath.g:92:2: ( ( rule__ValidationPath__Group__0 ) )
            // InternalXpath.g:93:3: ( rule__ValidationPath__Group__0 )
            {
             before(grammarAccess.getValidationPathAccess().getGroup()); 
            // InternalXpath.g:94:3: ( rule__ValidationPath__Group__0 )
            // InternalXpath.g:94:4: rule__ValidationPath__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ValidationPath__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getValidationPathAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValidationPath"


    // $ANTLR start "entryRuleXPath"
    // InternalXpath.g:103:1: entryRuleXPath : ruleXPath EOF ;
    public final void entryRuleXPath() throws RecognitionException {
        try {
            // InternalXpath.g:104:1: ( ruleXPath EOF )
            // InternalXpath.g:105:1: ruleXPath EOF
            {
             before(grammarAccess.getXPathRule()); 
            pushFollow(FOLLOW_1);
            ruleXPath();

            state._fsp--;

             after(grammarAccess.getXPathRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXPath"


    // $ANTLR start "ruleXPath"
    // InternalXpath.g:112:1: ruleXPath : ( ( rule__XPath__ExprAssignment ) ) ;
    public final void ruleXPath() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:116:2: ( ( ( rule__XPath__ExprAssignment ) ) )
            // InternalXpath.g:117:2: ( ( rule__XPath__ExprAssignment ) )
            {
            // InternalXpath.g:117:2: ( ( rule__XPath__ExprAssignment ) )
            // InternalXpath.g:118:3: ( rule__XPath__ExprAssignment )
            {
             before(grammarAccess.getXPathAccess().getExprAssignment()); 
            // InternalXpath.g:119:3: ( rule__XPath__ExprAssignment )
            // InternalXpath.g:119:4: rule__XPath__ExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__XPath__ExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getXPathAccess().getExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXPath"


    // $ANTLR start "entryRuleExpr"
    // InternalXpath.g:128:1: entryRuleExpr : ruleExpr EOF ;
    public final void entryRuleExpr() throws RecognitionException {
        try {
            // InternalXpath.g:129:1: ( ruleExpr EOF )
            // InternalXpath.g:130:1: ruleExpr EOF
            {
             before(grammarAccess.getExprRule()); 
            pushFollow(FOLLOW_1);
            ruleExpr();

            state._fsp--;

             after(grammarAccess.getExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpr"


    // $ANTLR start "ruleExpr"
    // InternalXpath.g:137:1: ruleExpr : ( ( rule__Expr__Group__0 ) ) ;
    public final void ruleExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:141:2: ( ( ( rule__Expr__Group__0 ) ) )
            // InternalXpath.g:142:2: ( ( rule__Expr__Group__0 ) )
            {
            // InternalXpath.g:142:2: ( ( rule__Expr__Group__0 ) )
            // InternalXpath.g:143:3: ( rule__Expr__Group__0 )
            {
             before(grammarAccess.getExprAccess().getGroup()); 
            // InternalXpath.g:144:3: ( rule__Expr__Group__0 )
            // InternalXpath.g:144:4: rule__Expr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Expr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpr"


    // $ANTLR start "entryRuleExprSingle"
    // InternalXpath.g:153:1: entryRuleExprSingle : ruleExprSingle EOF ;
    public final void entryRuleExprSingle() throws RecognitionException {
        try {
            // InternalXpath.g:154:1: ( ruleExprSingle EOF )
            // InternalXpath.g:155:1: ruleExprSingle EOF
            {
             before(grammarAccess.getExprSingleRule()); 
            pushFollow(FOLLOW_1);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getExprSingleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExprSingle"


    // $ANTLR start "ruleExprSingle"
    // InternalXpath.g:162:1: ruleExprSingle : ( ( rule__ExprSingle__Alternatives ) ) ;
    public final void ruleExprSingle() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:166:2: ( ( ( rule__ExprSingle__Alternatives ) ) )
            // InternalXpath.g:167:2: ( ( rule__ExprSingle__Alternatives ) )
            {
            // InternalXpath.g:167:2: ( ( rule__ExprSingle__Alternatives ) )
            // InternalXpath.g:168:3: ( rule__ExprSingle__Alternatives )
            {
             before(grammarAccess.getExprSingleAccess().getAlternatives()); 
            // InternalXpath.g:169:3: ( rule__ExprSingle__Alternatives )
            // InternalXpath.g:169:4: rule__ExprSingle__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ExprSingle__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExprSingleAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExprSingle"


    // $ANTLR start "entryRuleForExpr"
    // InternalXpath.g:178:1: entryRuleForExpr : ruleForExpr EOF ;
    public final void entryRuleForExpr() throws RecognitionException {
        try {
            // InternalXpath.g:179:1: ( ruleForExpr EOF )
            // InternalXpath.g:180:1: ruleForExpr EOF
            {
             before(grammarAccess.getForExprRule()); 
            pushFollow(FOLLOW_1);
            ruleForExpr();

            state._fsp--;

             after(grammarAccess.getForExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForExpr"


    // $ANTLR start "ruleForExpr"
    // InternalXpath.g:187:1: ruleForExpr : ( ( rule__ForExpr__Group__0 ) ) ;
    public final void ruleForExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:191:2: ( ( ( rule__ForExpr__Group__0 ) ) )
            // InternalXpath.g:192:2: ( ( rule__ForExpr__Group__0 ) )
            {
            // InternalXpath.g:192:2: ( ( rule__ForExpr__Group__0 ) )
            // InternalXpath.g:193:3: ( rule__ForExpr__Group__0 )
            {
             before(grammarAccess.getForExprAccess().getGroup()); 
            // InternalXpath.g:194:3: ( rule__ForExpr__Group__0 )
            // InternalXpath.g:194:4: rule__ForExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ForExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForExpr"


    // $ANTLR start "entryRuleSimpleForClause"
    // InternalXpath.g:203:1: entryRuleSimpleForClause : ruleSimpleForClause EOF ;
    public final void entryRuleSimpleForClause() throws RecognitionException {
        try {
            // InternalXpath.g:204:1: ( ruleSimpleForClause EOF )
            // InternalXpath.g:205:1: ruleSimpleForClause EOF
            {
             before(grammarAccess.getSimpleForClauseRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleForClause();

            state._fsp--;

             after(grammarAccess.getSimpleForClauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleForClause"


    // $ANTLR start "ruleSimpleForClause"
    // InternalXpath.g:212:1: ruleSimpleForClause : ( ( rule__SimpleForClause__Group__0 ) ) ;
    public final void ruleSimpleForClause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:216:2: ( ( ( rule__SimpleForClause__Group__0 ) ) )
            // InternalXpath.g:217:2: ( ( rule__SimpleForClause__Group__0 ) )
            {
            // InternalXpath.g:217:2: ( ( rule__SimpleForClause__Group__0 ) )
            // InternalXpath.g:218:3: ( rule__SimpleForClause__Group__0 )
            {
             before(grammarAccess.getSimpleForClauseAccess().getGroup()); 
            // InternalXpath.g:219:3: ( rule__SimpleForClause__Group__0 )
            // InternalXpath.g:219:4: rule__SimpleForClause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleForClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleForClause"


    // $ANTLR start "entryRuleQuantifiedExpr"
    // InternalXpath.g:228:1: entryRuleQuantifiedExpr : ruleQuantifiedExpr EOF ;
    public final void entryRuleQuantifiedExpr() throws RecognitionException {
        try {
            // InternalXpath.g:229:1: ( ruleQuantifiedExpr EOF )
            // InternalXpath.g:230:1: ruleQuantifiedExpr EOF
            {
             before(grammarAccess.getQuantifiedExprRule()); 
            pushFollow(FOLLOW_1);
            ruleQuantifiedExpr();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuantifiedExpr"


    // $ANTLR start "ruleQuantifiedExpr"
    // InternalXpath.g:237:1: ruleQuantifiedExpr : ( ( rule__QuantifiedExpr__Group__0 ) ) ;
    public final void ruleQuantifiedExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:241:2: ( ( ( rule__QuantifiedExpr__Group__0 ) ) )
            // InternalXpath.g:242:2: ( ( rule__QuantifiedExpr__Group__0 ) )
            {
            // InternalXpath.g:242:2: ( ( rule__QuantifiedExpr__Group__0 ) )
            // InternalXpath.g:243:3: ( rule__QuantifiedExpr__Group__0 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getGroup()); 
            // InternalXpath.g:244:3: ( rule__QuantifiedExpr__Group__0 )
            // InternalXpath.g:244:4: rule__QuantifiedExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuantifiedExpr"


    // $ANTLR start "entryRuleIfExpr"
    // InternalXpath.g:253:1: entryRuleIfExpr : ruleIfExpr EOF ;
    public final void entryRuleIfExpr() throws RecognitionException {
        try {
            // InternalXpath.g:254:1: ( ruleIfExpr EOF )
            // InternalXpath.g:255:1: ruleIfExpr EOF
            {
             before(grammarAccess.getIfExprRule()); 
            pushFollow(FOLLOW_1);
            ruleIfExpr();

            state._fsp--;

             after(grammarAccess.getIfExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIfExpr"


    // $ANTLR start "ruleIfExpr"
    // InternalXpath.g:262:1: ruleIfExpr : ( ( rule__IfExpr__Group__0 ) ) ;
    public final void ruleIfExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:266:2: ( ( ( rule__IfExpr__Group__0 ) ) )
            // InternalXpath.g:267:2: ( ( rule__IfExpr__Group__0 ) )
            {
            // InternalXpath.g:267:2: ( ( rule__IfExpr__Group__0 ) )
            // InternalXpath.g:268:3: ( rule__IfExpr__Group__0 )
            {
             before(grammarAccess.getIfExprAccess().getGroup()); 
            // InternalXpath.g:269:3: ( rule__IfExpr__Group__0 )
            // InternalXpath.g:269:4: rule__IfExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIfExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIfExpr"


    // $ANTLR start "entryRuleOrExpr"
    // InternalXpath.g:278:1: entryRuleOrExpr : ruleOrExpr EOF ;
    public final void entryRuleOrExpr() throws RecognitionException {
        try {
            // InternalXpath.g:279:1: ( ruleOrExpr EOF )
            // InternalXpath.g:280:1: ruleOrExpr EOF
            {
             before(grammarAccess.getOrExprRule()); 
            pushFollow(FOLLOW_1);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getOrExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrExpr"


    // $ANTLR start "ruleOrExpr"
    // InternalXpath.g:287:1: ruleOrExpr : ( ( rule__OrExpr__Alternatives ) ) ;
    public final void ruleOrExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:291:2: ( ( ( rule__OrExpr__Alternatives ) ) )
            // InternalXpath.g:292:2: ( ( rule__OrExpr__Alternatives ) )
            {
            // InternalXpath.g:292:2: ( ( rule__OrExpr__Alternatives ) )
            // InternalXpath.g:293:3: ( rule__OrExpr__Alternatives )
            {
             before(grammarAccess.getOrExprAccess().getAlternatives()); 
            // InternalXpath.g:294:3: ( rule__OrExpr__Alternatives )
            // InternalXpath.g:294:4: rule__OrExpr__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrExpr"


    // $ANTLR start "entryRuleAndExpr"
    // InternalXpath.g:303:1: entryRuleAndExpr : ruleAndExpr EOF ;
    public final void entryRuleAndExpr() throws RecognitionException {
        try {
            // InternalXpath.g:304:1: ( ruleAndExpr EOF )
            // InternalXpath.g:305:1: ruleAndExpr EOF
            {
             before(grammarAccess.getAndExprRule()); 
            pushFollow(FOLLOW_1);
            ruleAndExpr();

            state._fsp--;

             after(grammarAccess.getAndExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAndExpr"


    // $ANTLR start "ruleAndExpr"
    // InternalXpath.g:312:1: ruleAndExpr : ( ( rule__AndExpr__Group__0 ) ) ;
    public final void ruleAndExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:316:2: ( ( ( rule__AndExpr__Group__0 ) ) )
            // InternalXpath.g:317:2: ( ( rule__AndExpr__Group__0 ) )
            {
            // InternalXpath.g:317:2: ( ( rule__AndExpr__Group__0 ) )
            // InternalXpath.g:318:3: ( rule__AndExpr__Group__0 )
            {
             before(grammarAccess.getAndExprAccess().getGroup()); 
            // InternalXpath.g:319:3: ( rule__AndExpr__Group__0 )
            // InternalXpath.g:319:4: rule__AndExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AndExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndExpr"


    // $ANTLR start "entryRuleComparisonExpr"
    // InternalXpath.g:328:1: entryRuleComparisonExpr : ruleComparisonExpr EOF ;
    public final void entryRuleComparisonExpr() throws RecognitionException {
        try {
            // InternalXpath.g:329:1: ( ruleComparisonExpr EOF )
            // InternalXpath.g:330:1: ruleComparisonExpr EOF
            {
             before(grammarAccess.getComparisonExprRule()); 
            pushFollow(FOLLOW_1);
            ruleComparisonExpr();

            state._fsp--;

             after(grammarAccess.getComparisonExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComparisonExpr"


    // $ANTLR start "ruleComparisonExpr"
    // InternalXpath.g:337:1: ruleComparisonExpr : ( ( rule__ComparisonExpr__Alternatives ) ) ;
    public final void ruleComparisonExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:341:2: ( ( ( rule__ComparisonExpr__Alternatives ) ) )
            // InternalXpath.g:342:2: ( ( rule__ComparisonExpr__Alternatives ) )
            {
            // InternalXpath.g:342:2: ( ( rule__ComparisonExpr__Alternatives ) )
            // InternalXpath.g:343:3: ( rule__ComparisonExpr__Alternatives )
            {
             before(grammarAccess.getComparisonExprAccess().getAlternatives()); 
            // InternalXpath.g:344:3: ( rule__ComparisonExpr__Alternatives )
            // InternalXpath.g:344:4: rule__ComparisonExpr__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparisonExpr"


    // $ANTLR start "entryRuleAdditiveExpr"
    // InternalXpath.g:353:1: entryRuleAdditiveExpr : ruleAdditiveExpr EOF ;
    public final void entryRuleAdditiveExpr() throws RecognitionException {
        try {
            // InternalXpath.g:354:1: ( ruleAdditiveExpr EOF )
            // InternalXpath.g:355:1: ruleAdditiveExpr EOF
            {
             before(grammarAccess.getAdditiveExprRule()); 
            pushFollow(FOLLOW_1);
            ruleAdditiveExpr();

            state._fsp--;

             after(grammarAccess.getAdditiveExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdditiveExpr"


    // $ANTLR start "ruleAdditiveExpr"
    // InternalXpath.g:362:1: ruleAdditiveExpr : ( ( rule__AdditiveExpr__Group__0 ) ) ;
    public final void ruleAdditiveExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:366:2: ( ( ( rule__AdditiveExpr__Group__0 ) ) )
            // InternalXpath.g:367:2: ( ( rule__AdditiveExpr__Group__0 ) )
            {
            // InternalXpath.g:367:2: ( ( rule__AdditiveExpr__Group__0 ) )
            // InternalXpath.g:368:3: ( rule__AdditiveExpr__Group__0 )
            {
             before(grammarAccess.getAdditiveExprAccess().getGroup()); 
            // InternalXpath.g:369:3: ( rule__AdditiveExpr__Group__0 )
            // InternalXpath.g:369:4: rule__AdditiveExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditiveExpr"


    // $ANTLR start "entryRuleUnionExpr"
    // InternalXpath.g:378:1: entryRuleUnionExpr : ruleUnionExpr EOF ;
    public final void entryRuleUnionExpr() throws RecognitionException {
        try {
            // InternalXpath.g:379:1: ( ruleUnionExpr EOF )
            // InternalXpath.g:380:1: ruleUnionExpr EOF
            {
             before(grammarAccess.getUnionExprRule()); 
            pushFollow(FOLLOW_1);
            ruleUnionExpr();

            state._fsp--;

             after(grammarAccess.getUnionExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnionExpr"


    // $ANTLR start "ruleUnionExpr"
    // InternalXpath.g:387:1: ruleUnionExpr : ( ( rule__UnionExpr__Group__0 ) ) ;
    public final void ruleUnionExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:391:2: ( ( ( rule__UnionExpr__Group__0 ) ) )
            // InternalXpath.g:392:2: ( ( rule__UnionExpr__Group__0 ) )
            {
            // InternalXpath.g:392:2: ( ( rule__UnionExpr__Group__0 ) )
            // InternalXpath.g:393:3: ( rule__UnionExpr__Group__0 )
            {
             before(grammarAccess.getUnionExprAccess().getGroup()); 
            // InternalXpath.g:394:3: ( rule__UnionExpr__Group__0 )
            // InternalXpath.g:394:4: rule__UnionExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnionExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnionExpr"


    // $ANTLR start "entryRuleIntersectExceptExpr"
    // InternalXpath.g:403:1: entryRuleIntersectExceptExpr : ruleIntersectExceptExpr EOF ;
    public final void entryRuleIntersectExceptExpr() throws RecognitionException {
        try {
            // InternalXpath.g:404:1: ( ruleIntersectExceptExpr EOF )
            // InternalXpath.g:405:1: ruleIntersectExceptExpr EOF
            {
             before(grammarAccess.getIntersectExceptExprRule()); 
            pushFollow(FOLLOW_1);
            ruleIntersectExceptExpr();

            state._fsp--;

             after(grammarAccess.getIntersectExceptExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntersectExceptExpr"


    // $ANTLR start "ruleIntersectExceptExpr"
    // InternalXpath.g:412:1: ruleIntersectExceptExpr : ( ( rule__IntersectExceptExpr__Group__0 ) ) ;
    public final void ruleIntersectExceptExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:416:2: ( ( ( rule__IntersectExceptExpr__Group__0 ) ) )
            // InternalXpath.g:417:2: ( ( rule__IntersectExceptExpr__Group__0 ) )
            {
            // InternalXpath.g:417:2: ( ( rule__IntersectExceptExpr__Group__0 ) )
            // InternalXpath.g:418:3: ( rule__IntersectExceptExpr__Group__0 )
            {
             before(grammarAccess.getIntersectExceptExprAccess().getGroup()); 
            // InternalXpath.g:419:3: ( rule__IntersectExceptExpr__Group__0 )
            // InternalXpath.g:419:4: rule__IntersectExceptExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntersectExceptExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntersectExceptExpr"


    // $ANTLR start "entryRuleInstanceofExpr"
    // InternalXpath.g:428:1: entryRuleInstanceofExpr : ruleInstanceofExpr EOF ;
    public final void entryRuleInstanceofExpr() throws RecognitionException {
        try {
            // InternalXpath.g:429:1: ( ruleInstanceofExpr EOF )
            // InternalXpath.g:430:1: ruleInstanceofExpr EOF
            {
             before(grammarAccess.getInstanceofExprRule()); 
            pushFollow(FOLLOW_1);
            ruleInstanceofExpr();

            state._fsp--;

             after(grammarAccess.getInstanceofExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstanceofExpr"


    // $ANTLR start "ruleInstanceofExpr"
    // InternalXpath.g:437:1: ruleInstanceofExpr : ( ( rule__InstanceofExpr__TreatExprAssignment ) ) ;
    public final void ruleInstanceofExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:441:2: ( ( ( rule__InstanceofExpr__TreatExprAssignment ) ) )
            // InternalXpath.g:442:2: ( ( rule__InstanceofExpr__TreatExprAssignment ) )
            {
            // InternalXpath.g:442:2: ( ( rule__InstanceofExpr__TreatExprAssignment ) )
            // InternalXpath.g:443:3: ( rule__InstanceofExpr__TreatExprAssignment )
            {
             before(grammarAccess.getInstanceofExprAccess().getTreatExprAssignment()); 
            // InternalXpath.g:444:3: ( rule__InstanceofExpr__TreatExprAssignment )
            // InternalXpath.g:444:4: rule__InstanceofExpr__TreatExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__InstanceofExpr__TreatExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getInstanceofExprAccess().getTreatExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstanceofExpr"


    // $ANTLR start "entryRuleTreatExpr"
    // InternalXpath.g:453:1: entryRuleTreatExpr : ruleTreatExpr EOF ;
    public final void entryRuleTreatExpr() throws RecognitionException {
        try {
            // InternalXpath.g:454:1: ( ruleTreatExpr EOF )
            // InternalXpath.g:455:1: ruleTreatExpr EOF
            {
             before(grammarAccess.getTreatExprRule()); 
            pushFollow(FOLLOW_1);
            ruleTreatExpr();

            state._fsp--;

             after(grammarAccess.getTreatExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTreatExpr"


    // $ANTLR start "ruleTreatExpr"
    // InternalXpath.g:462:1: ruleTreatExpr : ( ( rule__TreatExpr__CastableExprAssignment ) ) ;
    public final void ruleTreatExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:466:2: ( ( ( rule__TreatExpr__CastableExprAssignment ) ) )
            // InternalXpath.g:467:2: ( ( rule__TreatExpr__CastableExprAssignment ) )
            {
            // InternalXpath.g:467:2: ( ( rule__TreatExpr__CastableExprAssignment ) )
            // InternalXpath.g:468:3: ( rule__TreatExpr__CastableExprAssignment )
            {
             before(grammarAccess.getTreatExprAccess().getCastableExprAssignment()); 
            // InternalXpath.g:469:3: ( rule__TreatExpr__CastableExprAssignment )
            // InternalXpath.g:469:4: rule__TreatExpr__CastableExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__TreatExpr__CastableExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getTreatExprAccess().getCastableExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTreatExpr"


    // $ANTLR start "entryRuleCastableExpr"
    // InternalXpath.g:478:1: entryRuleCastableExpr : ruleCastableExpr EOF ;
    public final void entryRuleCastableExpr() throws RecognitionException {
        try {
            // InternalXpath.g:479:1: ( ruleCastableExpr EOF )
            // InternalXpath.g:480:1: ruleCastableExpr EOF
            {
             before(grammarAccess.getCastableExprRule()); 
            pushFollow(FOLLOW_1);
            ruleCastableExpr();

            state._fsp--;

             after(grammarAccess.getCastableExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCastableExpr"


    // $ANTLR start "ruleCastableExpr"
    // InternalXpath.g:487:1: ruleCastableExpr : ( ( rule__CastableExpr__CastExprAssignment ) ) ;
    public final void ruleCastableExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:491:2: ( ( ( rule__CastableExpr__CastExprAssignment ) ) )
            // InternalXpath.g:492:2: ( ( rule__CastableExpr__CastExprAssignment ) )
            {
            // InternalXpath.g:492:2: ( ( rule__CastableExpr__CastExprAssignment ) )
            // InternalXpath.g:493:3: ( rule__CastableExpr__CastExprAssignment )
            {
             before(grammarAccess.getCastableExprAccess().getCastExprAssignment()); 
            // InternalXpath.g:494:3: ( rule__CastableExpr__CastExprAssignment )
            // InternalXpath.g:494:4: rule__CastableExpr__CastExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__CastableExpr__CastExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getCastableExprAccess().getCastExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCastableExpr"


    // $ANTLR start "entryRuleCastExpr"
    // InternalXpath.g:503:1: entryRuleCastExpr : ruleCastExpr EOF ;
    public final void entryRuleCastExpr() throws RecognitionException {
        try {
            // InternalXpath.g:504:1: ( ruleCastExpr EOF )
            // InternalXpath.g:505:1: ruleCastExpr EOF
            {
             before(grammarAccess.getCastExprRule()); 
            pushFollow(FOLLOW_1);
            ruleCastExpr();

            state._fsp--;

             after(grammarAccess.getCastExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCastExpr"


    // $ANTLR start "ruleCastExpr"
    // InternalXpath.g:512:1: ruleCastExpr : ( ( rule__CastExpr__UnaryExprAssignment ) ) ;
    public final void ruleCastExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:516:2: ( ( ( rule__CastExpr__UnaryExprAssignment ) ) )
            // InternalXpath.g:517:2: ( ( rule__CastExpr__UnaryExprAssignment ) )
            {
            // InternalXpath.g:517:2: ( ( rule__CastExpr__UnaryExprAssignment ) )
            // InternalXpath.g:518:3: ( rule__CastExpr__UnaryExprAssignment )
            {
             before(grammarAccess.getCastExprAccess().getUnaryExprAssignment()); 
            // InternalXpath.g:519:3: ( rule__CastExpr__UnaryExprAssignment )
            // InternalXpath.g:519:4: rule__CastExpr__UnaryExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__CastExpr__UnaryExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getCastExprAccess().getUnaryExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCastExpr"


    // $ANTLR start "entryRuleUnaryExpr"
    // InternalXpath.g:528:1: entryRuleUnaryExpr : ruleUnaryExpr EOF ;
    public final void entryRuleUnaryExpr() throws RecognitionException {
        try {
            // InternalXpath.g:529:1: ( ruleUnaryExpr EOF )
            // InternalXpath.g:530:1: ruleUnaryExpr EOF
            {
             before(grammarAccess.getUnaryExprRule()); 
            pushFollow(FOLLOW_1);
            ruleUnaryExpr();

            state._fsp--;

             after(grammarAccess.getUnaryExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnaryExpr"


    // $ANTLR start "ruleUnaryExpr"
    // InternalXpath.g:537:1: ruleUnaryExpr : ( ( rule__UnaryExpr__ValueExprAssignment ) ) ;
    public final void ruleUnaryExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:541:2: ( ( ( rule__UnaryExpr__ValueExprAssignment ) ) )
            // InternalXpath.g:542:2: ( ( rule__UnaryExpr__ValueExprAssignment ) )
            {
            // InternalXpath.g:542:2: ( ( rule__UnaryExpr__ValueExprAssignment ) )
            // InternalXpath.g:543:3: ( rule__UnaryExpr__ValueExprAssignment )
            {
             before(grammarAccess.getUnaryExprAccess().getValueExprAssignment()); 
            // InternalXpath.g:544:3: ( rule__UnaryExpr__ValueExprAssignment )
            // InternalXpath.g:544:4: rule__UnaryExpr__ValueExprAssignment
            {
            pushFollow(FOLLOW_2);
            rule__UnaryExpr__ValueExprAssignment();

            state._fsp--;


            }

             after(grammarAccess.getUnaryExprAccess().getValueExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryExpr"


    // $ANTLR start "entryRuleValueExpr"
    // InternalXpath.g:553:1: entryRuleValueExpr : ruleValueExpr EOF ;
    public final void entryRuleValueExpr() throws RecognitionException {
        try {
            // InternalXpath.g:554:1: ( ruleValueExpr EOF )
            // InternalXpath.g:555:1: ruleValueExpr EOF
            {
             before(grammarAccess.getValueExprRule()); 
            pushFollow(FOLLOW_1);
            ruleValueExpr();

            state._fsp--;

             after(grammarAccess.getValueExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValueExpr"


    // $ANTLR start "ruleValueExpr"
    // InternalXpath.g:562:1: ruleValueExpr : ( ( rule__ValueExpr__Alternatives ) ) ;
    public final void ruleValueExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:566:2: ( ( ( rule__ValueExpr__Alternatives ) ) )
            // InternalXpath.g:567:2: ( ( rule__ValueExpr__Alternatives ) )
            {
            // InternalXpath.g:567:2: ( ( rule__ValueExpr__Alternatives ) )
            // InternalXpath.g:568:3: ( rule__ValueExpr__Alternatives )
            {
             before(grammarAccess.getValueExprAccess().getAlternatives()); 
            // InternalXpath.g:569:3: ( rule__ValueExpr__Alternatives )
            // InternalXpath.g:569:4: rule__ValueExpr__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ValueExpr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValueExpr"


    // $ANTLR start "entryRuleGeneralComp"
    // InternalXpath.g:578:1: entryRuleGeneralComp : ruleGeneralComp EOF ;
    public final void entryRuleGeneralComp() throws RecognitionException {
        try {
            // InternalXpath.g:579:1: ( ruleGeneralComp EOF )
            // InternalXpath.g:580:1: ruleGeneralComp EOF
            {
             before(grammarAccess.getGeneralCompRule()); 
            pushFollow(FOLLOW_1);
            ruleGeneralComp();

            state._fsp--;

             after(grammarAccess.getGeneralCompRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGeneralComp"


    // $ANTLR start "ruleGeneralComp"
    // InternalXpath.g:587:1: ruleGeneralComp : ( ( rule__GeneralComp__Alternatives ) ) ;
    public final void ruleGeneralComp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:591:2: ( ( ( rule__GeneralComp__Alternatives ) ) )
            // InternalXpath.g:592:2: ( ( rule__GeneralComp__Alternatives ) )
            {
            // InternalXpath.g:592:2: ( ( rule__GeneralComp__Alternatives ) )
            // InternalXpath.g:593:3: ( rule__GeneralComp__Alternatives )
            {
             before(grammarAccess.getGeneralCompAccess().getAlternatives()); 
            // InternalXpath.g:594:3: ( rule__GeneralComp__Alternatives )
            // InternalXpath.g:594:4: rule__GeneralComp__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GeneralComp__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getGeneralCompAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGeneralComp"


    // $ANTLR start "entryRuleValueComp"
    // InternalXpath.g:603:1: entryRuleValueComp : ruleValueComp EOF ;
    public final void entryRuleValueComp() throws RecognitionException {
        try {
            // InternalXpath.g:604:1: ( ruleValueComp EOF )
            // InternalXpath.g:605:1: ruleValueComp EOF
            {
             before(grammarAccess.getValueCompRule()); 
            pushFollow(FOLLOW_1);
            ruleValueComp();

            state._fsp--;

             after(grammarAccess.getValueCompRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValueComp"


    // $ANTLR start "ruleValueComp"
    // InternalXpath.g:612:1: ruleValueComp : ( ( rule__ValueComp__Alternatives ) ) ;
    public final void ruleValueComp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:616:2: ( ( ( rule__ValueComp__Alternatives ) ) )
            // InternalXpath.g:617:2: ( ( rule__ValueComp__Alternatives ) )
            {
            // InternalXpath.g:617:2: ( ( rule__ValueComp__Alternatives ) )
            // InternalXpath.g:618:3: ( rule__ValueComp__Alternatives )
            {
             before(grammarAccess.getValueCompAccess().getAlternatives()); 
            // InternalXpath.g:619:3: ( rule__ValueComp__Alternatives )
            // InternalXpath.g:619:4: rule__ValueComp__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ValueComp__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueCompAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValueComp"


    // $ANTLR start "entryRuleNodeComp"
    // InternalXpath.g:628:1: entryRuleNodeComp : ruleNodeComp EOF ;
    public final void entryRuleNodeComp() throws RecognitionException {
        try {
            // InternalXpath.g:629:1: ( ruleNodeComp EOF )
            // InternalXpath.g:630:1: ruleNodeComp EOF
            {
             before(grammarAccess.getNodeCompRule()); 
            pushFollow(FOLLOW_1);
            ruleNodeComp();

            state._fsp--;

             after(grammarAccess.getNodeCompRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNodeComp"


    // $ANTLR start "ruleNodeComp"
    // InternalXpath.g:637:1: ruleNodeComp : ( ( rule__NodeComp__Alternatives ) ) ;
    public final void ruleNodeComp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:641:2: ( ( ( rule__NodeComp__Alternatives ) ) )
            // InternalXpath.g:642:2: ( ( rule__NodeComp__Alternatives ) )
            {
            // InternalXpath.g:642:2: ( ( rule__NodeComp__Alternatives ) )
            // InternalXpath.g:643:3: ( rule__NodeComp__Alternatives )
            {
             before(grammarAccess.getNodeCompAccess().getAlternatives()); 
            // InternalXpath.g:644:3: ( rule__NodeComp__Alternatives )
            // InternalXpath.g:644:4: rule__NodeComp__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__NodeComp__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNodeCompAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNodeComp"


    // $ANTLR start "entryRulePathExpr"
    // InternalXpath.g:653:1: entryRulePathExpr : rulePathExpr EOF ;
    public final void entryRulePathExpr() throws RecognitionException {
        try {
            // InternalXpath.g:654:1: ( rulePathExpr EOF )
            // InternalXpath.g:655:1: rulePathExpr EOF
            {
             before(grammarAccess.getPathExprRule()); 
            pushFollow(FOLLOW_1);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getPathExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePathExpr"


    // $ANTLR start "rulePathExpr"
    // InternalXpath.g:662:1: rulePathExpr : ( ( rule__PathExpr__Group__0 ) ) ;
    public final void rulePathExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:666:2: ( ( ( rule__PathExpr__Group__0 ) ) )
            // InternalXpath.g:667:2: ( ( rule__PathExpr__Group__0 ) )
            {
            // InternalXpath.g:667:2: ( ( rule__PathExpr__Group__0 ) )
            // InternalXpath.g:668:3: ( rule__PathExpr__Group__0 )
            {
             before(grammarAccess.getPathExprAccess().getGroup()); 
            // InternalXpath.g:669:3: ( rule__PathExpr__Group__0 )
            // InternalXpath.g:669:4: rule__PathExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPathExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePathExpr"


    // $ANTLR start "entryRuleStepExpr"
    // InternalXpath.g:678:1: entryRuleStepExpr : ruleStepExpr EOF ;
    public final void entryRuleStepExpr() throws RecognitionException {
        try {
            // InternalXpath.g:679:1: ( ruleStepExpr EOF )
            // InternalXpath.g:680:1: ruleStepExpr EOF
            {
             before(grammarAccess.getStepExprRule()); 
            pushFollow(FOLLOW_1);
            ruleStepExpr();

            state._fsp--;

             after(grammarAccess.getStepExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStepExpr"


    // $ANTLR start "ruleStepExpr"
    // InternalXpath.g:687:1: ruleStepExpr : ( ( rule__StepExpr__ElementAssignment ) ) ;
    public final void ruleStepExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:691:2: ( ( ( rule__StepExpr__ElementAssignment ) ) )
            // InternalXpath.g:692:2: ( ( rule__StepExpr__ElementAssignment ) )
            {
            // InternalXpath.g:692:2: ( ( rule__StepExpr__ElementAssignment ) )
            // InternalXpath.g:693:3: ( rule__StepExpr__ElementAssignment )
            {
             before(grammarAccess.getStepExprAccess().getElementAssignment()); 
            // InternalXpath.g:694:3: ( rule__StepExpr__ElementAssignment )
            // InternalXpath.g:694:4: rule__StepExpr__ElementAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StepExpr__ElementAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStepExprAccess().getElementAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepExpr"


    // $ANTLR start "entryRuleNotExpr"
    // InternalXpath.g:703:1: entryRuleNotExpr : ruleNotExpr EOF ;
    public final void entryRuleNotExpr() throws RecognitionException {
        try {
            // InternalXpath.g:704:1: ( ruleNotExpr EOF )
            // InternalXpath.g:705:1: ruleNotExpr EOF
            {
             before(grammarAccess.getNotExprRule()); 
            pushFollow(FOLLOW_1);
            ruleNotExpr();

            state._fsp--;

             after(grammarAccess.getNotExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNotExpr"


    // $ANTLR start "ruleNotExpr"
    // InternalXpath.g:712:1: ruleNotExpr : ( ( rule__NotExpr__Group__0 ) ) ;
    public final void ruleNotExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:716:2: ( ( ( rule__NotExpr__Group__0 ) ) )
            // InternalXpath.g:717:2: ( ( rule__NotExpr__Group__0 ) )
            {
            // InternalXpath.g:717:2: ( ( rule__NotExpr__Group__0 ) )
            // InternalXpath.g:718:3: ( rule__NotExpr__Group__0 )
            {
             before(grammarAccess.getNotExprAccess().getGroup()); 
            // InternalXpath.g:719:3: ( rule__NotExpr__Group__0 )
            // InternalXpath.g:719:4: rule__NotExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NotExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotExpr"


    // $ANTLR start "entryRuleVarName"
    // InternalXpath.g:728:1: entryRuleVarName : ruleVarName EOF ;
    public final void entryRuleVarName() throws RecognitionException {
        try {
            // InternalXpath.g:729:1: ( ruleVarName EOF )
            // InternalXpath.g:730:1: ruleVarName EOF
            {
             before(grammarAccess.getVarNameRule()); 
            pushFollow(FOLLOW_1);
            ruleVarName();

            state._fsp--;

             after(grammarAccess.getVarNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVarName"


    // $ANTLR start "ruleVarName"
    // InternalXpath.g:737:1: ruleVarName : ( ruleQName ) ;
    public final void ruleVarName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:741:2: ( ( ruleQName ) )
            // InternalXpath.g:742:2: ( ruleQName )
            {
            // InternalXpath.g:742:2: ( ruleQName )
            // InternalXpath.g:743:3: ruleQName
            {
             before(grammarAccess.getVarNameAccess().getQNameParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getVarNameAccess().getQNameParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVarName"


    // $ANTLR start "entryRuleQName"
    // InternalXpath.g:753:1: entryRuleQName : ruleQName EOF ;
    public final void entryRuleQName() throws RecognitionException {
        try {
            // InternalXpath.g:754:1: ( ruleQName EOF )
            // InternalXpath.g:755:1: ruleQName EOF
            {
             before(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalXpath.g:762:1: ruleQName : ( ( rule__QName__NameAssignment ) ) ;
    public final void ruleQName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:766:2: ( ( ( rule__QName__NameAssignment ) ) )
            // InternalXpath.g:767:2: ( ( rule__QName__NameAssignment ) )
            {
            // InternalXpath.g:767:2: ( ( rule__QName__NameAssignment ) )
            // InternalXpath.g:768:3: ( rule__QName__NameAssignment )
            {
             before(grammarAccess.getQNameAccess().getNameAssignment()); 
            // InternalXpath.g:769:3: ( rule__QName__NameAssignment )
            // InternalXpath.g:769:4: rule__QName__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__QName__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getQNameAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleElement"
    // InternalXpath.g:778:1: entryRuleElement : ruleElement EOF ;
    public final void entryRuleElement() throws RecognitionException {
        try {
            // InternalXpath.g:779:1: ( ruleElement EOF )
            // InternalXpath.g:780:1: ruleElement EOF
            {
             before(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_1);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalXpath.g:787:1: ruleElement : ( ( rule__Element__Group__0 ) ) ;
    public final void ruleElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:791:2: ( ( ( rule__Element__Group__0 ) ) )
            // InternalXpath.g:792:2: ( ( rule__Element__Group__0 ) )
            {
            // InternalXpath.g:792:2: ( ( rule__Element__Group__0 ) )
            // InternalXpath.g:793:3: ( rule__Element__Group__0 )
            {
             before(grammarAccess.getElementAccess().getGroup()); 
            // InternalXpath.g:794:3: ( rule__Element__Group__0 )
            // InternalXpath.g:794:4: rule__Element__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Element__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getElementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleFunction"
    // InternalXpath.g:803:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // InternalXpath.g:804:1: ( ruleFunction EOF )
            // InternalXpath.g:805:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalXpath.g:812:1: ruleFunction : ( ( rule__Function__Alternatives ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:816:2: ( ( ( rule__Function__Alternatives ) ) )
            // InternalXpath.g:817:2: ( ( rule__Function__Alternatives ) )
            {
            // InternalXpath.g:817:2: ( ( rule__Function__Alternatives ) )
            // InternalXpath.g:818:3: ( rule__Function__Alternatives )
            {
             before(grammarAccess.getFunctionAccess().getAlternatives()); 
            // InternalXpath.g:819:3: ( rule__Function__Alternatives )
            // InternalXpath.g:819:4: rule__Function__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Function__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleSumFunction"
    // InternalXpath.g:828:1: entryRuleSumFunction : ruleSumFunction EOF ;
    public final void entryRuleSumFunction() throws RecognitionException {
        try {
            // InternalXpath.g:829:1: ( ruleSumFunction EOF )
            // InternalXpath.g:830:1: ruleSumFunction EOF
            {
             before(grammarAccess.getSumFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleSumFunction();

            state._fsp--;

             after(grammarAccess.getSumFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSumFunction"


    // $ANTLR start "ruleSumFunction"
    // InternalXpath.g:837:1: ruleSumFunction : ( ( rule__SumFunction__Group__0 ) ) ;
    public final void ruleSumFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:841:2: ( ( ( rule__SumFunction__Group__0 ) ) )
            // InternalXpath.g:842:2: ( ( rule__SumFunction__Group__0 ) )
            {
            // InternalXpath.g:842:2: ( ( rule__SumFunction__Group__0 ) )
            // InternalXpath.g:843:3: ( rule__SumFunction__Group__0 )
            {
             before(grammarAccess.getSumFunctionAccess().getGroup()); 
            // InternalXpath.g:844:3: ( rule__SumFunction__Group__0 )
            // InternalXpath.g:844:4: rule__SumFunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSumFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSumFunction"


    // $ANTLR start "entryRuleCountFunction"
    // InternalXpath.g:853:1: entryRuleCountFunction : ruleCountFunction EOF ;
    public final void entryRuleCountFunction() throws RecognitionException {
        try {
            // InternalXpath.g:854:1: ( ruleCountFunction EOF )
            // InternalXpath.g:855:1: ruleCountFunction EOF
            {
             before(grammarAccess.getCountFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleCountFunction();

            state._fsp--;

             after(grammarAccess.getCountFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCountFunction"


    // $ANTLR start "ruleCountFunction"
    // InternalXpath.g:862:1: ruleCountFunction : ( ( rule__CountFunction__Group__0 ) ) ;
    public final void ruleCountFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:866:2: ( ( ( rule__CountFunction__Group__0 ) ) )
            // InternalXpath.g:867:2: ( ( rule__CountFunction__Group__0 ) )
            {
            // InternalXpath.g:867:2: ( ( rule__CountFunction__Group__0 ) )
            // InternalXpath.g:868:3: ( rule__CountFunction__Group__0 )
            {
             before(grammarAccess.getCountFunctionAccess().getGroup()); 
            // InternalXpath.g:869:3: ( rule__CountFunction__Group__0 )
            // InternalXpath.g:869:4: rule__CountFunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCountFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCountFunction"


    // $ANTLR start "entryRuleStringLiteral"
    // InternalXpath.g:878:1: entryRuleStringLiteral : ruleStringLiteral EOF ;
    public final void entryRuleStringLiteral() throws RecognitionException {
        try {
            // InternalXpath.g:879:1: ( ruleStringLiteral EOF )
            // InternalXpath.g:880:1: ruleStringLiteral EOF
            {
             before(grammarAccess.getStringLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleStringLiteral();

            state._fsp--;

             after(grammarAccess.getStringLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringLiteral"


    // $ANTLR start "ruleStringLiteral"
    // InternalXpath.g:887:1: ruleStringLiteral : ( ( rule__StringLiteral__NameAssignment ) ) ;
    public final void ruleStringLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:891:2: ( ( ( rule__StringLiteral__NameAssignment ) ) )
            // InternalXpath.g:892:2: ( ( rule__StringLiteral__NameAssignment ) )
            {
            // InternalXpath.g:892:2: ( ( rule__StringLiteral__NameAssignment ) )
            // InternalXpath.g:893:3: ( rule__StringLiteral__NameAssignment )
            {
             before(grammarAccess.getStringLiteralAccess().getNameAssignment()); 
            // InternalXpath.g:894:3: ( rule__StringLiteral__NameAssignment )
            // InternalXpath.g:894:4: rule__StringLiteral__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StringLiteral__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringLiteralAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringLiteral"


    // $ANTLR start "entryRuleNumericLiteral"
    // InternalXpath.g:903:1: entryRuleNumericLiteral : ruleNumericLiteral EOF ;
    public final void entryRuleNumericLiteral() throws RecognitionException {
        try {
            // InternalXpath.g:904:1: ( ruleNumericLiteral EOF )
            // InternalXpath.g:905:1: ruleNumericLiteral EOF
            {
             before(grammarAccess.getNumericLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleNumericLiteral();

            state._fsp--;

             after(grammarAccess.getNumericLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumericLiteral"


    // $ANTLR start "ruleNumericLiteral"
    // InternalXpath.g:912:1: ruleNumericLiteral : ( ( rule__NumericLiteral__NumberAssignment ) ) ;
    public final void ruleNumericLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:916:2: ( ( ( rule__NumericLiteral__NumberAssignment ) ) )
            // InternalXpath.g:917:2: ( ( rule__NumericLiteral__NumberAssignment ) )
            {
            // InternalXpath.g:917:2: ( ( rule__NumericLiteral__NumberAssignment ) )
            // InternalXpath.g:918:3: ( rule__NumericLiteral__NumberAssignment )
            {
             before(grammarAccess.getNumericLiteralAccess().getNumberAssignment()); 
            // InternalXpath.g:919:3: ( rule__NumericLiteral__NumberAssignment )
            // InternalXpath.g:919:4: rule__NumericLiteral__NumberAssignment
            {
            pushFollow(FOLLOW_2);
            rule__NumericLiteral__NumberAssignment();

            state._fsp--;


            }

             after(grammarAccess.getNumericLiteralAccess().getNumberAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumericLiteral"


    // $ANTLR start "entryRuleThenElse"
    // InternalXpath.g:928:1: entryRuleThenElse : ruleThenElse EOF ;
    public final void entryRuleThenElse() throws RecognitionException {
        try {
            // InternalXpath.g:929:1: ( ruleThenElse EOF )
            // InternalXpath.g:930:1: ruleThenElse EOF
            {
             before(grammarAccess.getThenElseRule()); 
            pushFollow(FOLLOW_1);
            ruleThenElse();

            state._fsp--;

             after(grammarAccess.getThenElseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThenElse"


    // $ANTLR start "ruleThenElse"
    // InternalXpath.g:937:1: ruleThenElse : ( ( rule__ThenElse__Alternatives ) ) ;
    public final void ruleThenElse() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:941:2: ( ( ( rule__ThenElse__Alternatives ) ) )
            // InternalXpath.g:942:2: ( ( rule__ThenElse__Alternatives ) )
            {
            // InternalXpath.g:942:2: ( ( rule__ThenElse__Alternatives ) )
            // InternalXpath.g:943:3: ( rule__ThenElse__Alternatives )
            {
             before(grammarAccess.getThenElseAccess().getAlternatives()); 
            // InternalXpath.g:944:3: ( rule__ThenElse__Alternatives )
            // InternalXpath.g:944:4: rule__ThenElse__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getThenElseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThenElse"


    // $ANTLR start "entryRuleTrue"
    // InternalXpath.g:953:1: entryRuleTrue : ruleTrue EOF ;
    public final void entryRuleTrue() throws RecognitionException {
        try {
            // InternalXpath.g:954:1: ( ruleTrue EOF )
            // InternalXpath.g:955:1: ruleTrue EOF
            {
             before(grammarAccess.getTrueRule()); 
            pushFollow(FOLLOW_1);
            ruleTrue();

            state._fsp--;

             after(grammarAccess.getTrueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTrue"


    // $ANTLR start "ruleTrue"
    // InternalXpath.g:962:1: ruleTrue : ( 'true()' ) ;
    public final void ruleTrue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:966:2: ( ( 'true()' ) )
            // InternalXpath.g:967:2: ( 'true()' )
            {
            // InternalXpath.g:967:2: ( 'true()' )
            // InternalXpath.g:968:3: 'true()'
            {
             before(grammarAccess.getTrueAccess().getTrueKeyword()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getTrueAccess().getTrueKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTrue"


    // $ANTLR start "entryRuleFalse"
    // InternalXpath.g:978:1: entryRuleFalse : ruleFalse EOF ;
    public final void entryRuleFalse() throws RecognitionException {
        try {
            // InternalXpath.g:979:1: ( ruleFalse EOF )
            // InternalXpath.g:980:1: ruleFalse EOF
            {
             before(grammarAccess.getFalseRule()); 
            pushFollow(FOLLOW_1);
            ruleFalse();

            state._fsp--;

             after(grammarAccess.getFalseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFalse"


    // $ANTLR start "ruleFalse"
    // InternalXpath.g:987:1: ruleFalse : ( ( rule__False__Alternatives ) ) ;
    public final void ruleFalse() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:991:2: ( ( ( rule__False__Alternatives ) ) )
            // InternalXpath.g:992:2: ( ( rule__False__Alternatives ) )
            {
            // InternalXpath.g:992:2: ( ( rule__False__Alternatives ) )
            // InternalXpath.g:993:3: ( rule__False__Alternatives )
            {
             before(grammarAccess.getFalseAccess().getAlternatives()); 
            // InternalXpath.g:994:3: ( rule__False__Alternatives )
            // InternalXpath.g:994:4: rule__False__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__False__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFalseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFalse"


    // $ANTLR start "rule__ExprSingle__Alternatives"
    // InternalXpath.g:1002:1: rule__ExprSingle__Alternatives : ( ( ( rule__ExprSingle__ForExprAssignment_0 ) ) | ( ( rule__ExprSingle__QuantExprAssignment_1 ) ) | ( ( rule__ExprSingle__IfExprAssignment_2 ) ) | ( ( rule__ExprSingle__OrExprAssignment_3 ) ) );
    public final void rule__ExprSingle__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1006:1: ( ( ( rule__ExprSingle__ForExprAssignment_0 ) ) | ( ( rule__ExprSingle__QuantExprAssignment_1 ) ) | ( ( rule__ExprSingle__IfExprAssignment_2 ) ) | ( ( rule__ExprSingle__OrExprAssignment_3 ) ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt2=1;
                }
                break;
            case 12:
            case 54:
                {
                alt2=2;
                }
                break;
            case 43:
                {
                alt2=3;
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case 44:
            case 48:
            case 49:
            case 50:
            case 51:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalXpath.g:1007:2: ( ( rule__ExprSingle__ForExprAssignment_0 ) )
                    {
                    // InternalXpath.g:1007:2: ( ( rule__ExprSingle__ForExprAssignment_0 ) )
                    // InternalXpath.g:1008:3: ( rule__ExprSingle__ForExprAssignment_0 )
                    {
                     before(grammarAccess.getExprSingleAccess().getForExprAssignment_0()); 
                    // InternalXpath.g:1009:3: ( rule__ExprSingle__ForExprAssignment_0 )
                    // InternalXpath.g:1009:4: rule__ExprSingle__ForExprAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ExprSingle__ForExprAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExprSingleAccess().getForExprAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1013:2: ( ( rule__ExprSingle__QuantExprAssignment_1 ) )
                    {
                    // InternalXpath.g:1013:2: ( ( rule__ExprSingle__QuantExprAssignment_1 ) )
                    // InternalXpath.g:1014:3: ( rule__ExprSingle__QuantExprAssignment_1 )
                    {
                     before(grammarAccess.getExprSingleAccess().getQuantExprAssignment_1()); 
                    // InternalXpath.g:1015:3: ( rule__ExprSingle__QuantExprAssignment_1 )
                    // InternalXpath.g:1015:4: rule__ExprSingle__QuantExprAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ExprSingle__QuantExprAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getExprSingleAccess().getQuantExprAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1019:2: ( ( rule__ExprSingle__IfExprAssignment_2 ) )
                    {
                    // InternalXpath.g:1019:2: ( ( rule__ExprSingle__IfExprAssignment_2 ) )
                    // InternalXpath.g:1020:3: ( rule__ExprSingle__IfExprAssignment_2 )
                    {
                     before(grammarAccess.getExprSingleAccess().getIfExprAssignment_2()); 
                    // InternalXpath.g:1021:3: ( rule__ExprSingle__IfExprAssignment_2 )
                    // InternalXpath.g:1021:4: rule__ExprSingle__IfExprAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__ExprSingle__IfExprAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getExprSingleAccess().getIfExprAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXpath.g:1025:2: ( ( rule__ExprSingle__OrExprAssignment_3 ) )
                    {
                    // InternalXpath.g:1025:2: ( ( rule__ExprSingle__OrExprAssignment_3 ) )
                    // InternalXpath.g:1026:3: ( rule__ExprSingle__OrExprAssignment_3 )
                    {
                     before(grammarAccess.getExprSingleAccess().getOrExprAssignment_3()); 
                    // InternalXpath.g:1027:3: ( rule__ExprSingle__OrExprAssignment_3 )
                    // InternalXpath.g:1027:4: rule__ExprSingle__OrExprAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__ExprSingle__OrExprAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getExprSingleAccess().getOrExprAssignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExprSingle__Alternatives"


    // $ANTLR start "rule__QuantifiedExpr__Alternatives_0"
    // InternalXpath.g:1035:1: rule__QuantifiedExpr__Alternatives_0 : ( ( ( rule__QuantifiedExpr__QuantiAssignment_0_0 ) ) | ( 'every' ) );
    public final void rule__QuantifiedExpr__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1039:1: ( ( ( rule__QuantifiedExpr__QuantiAssignment_0_0 ) ) | ( 'every' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==54) ) {
                alt3=1;
            }
            else if ( (LA3_0==12) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalXpath.g:1040:2: ( ( rule__QuantifiedExpr__QuantiAssignment_0_0 ) )
                    {
                    // InternalXpath.g:1040:2: ( ( rule__QuantifiedExpr__QuantiAssignment_0_0 ) )
                    // InternalXpath.g:1041:3: ( rule__QuantifiedExpr__QuantiAssignment_0_0 )
                    {
                     before(grammarAccess.getQuantifiedExprAccess().getQuantiAssignment_0_0()); 
                    // InternalXpath.g:1042:3: ( rule__QuantifiedExpr__QuantiAssignment_0_0 )
                    // InternalXpath.g:1042:4: rule__QuantifiedExpr__QuantiAssignment_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QuantifiedExpr__QuantiAssignment_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getQuantifiedExprAccess().getQuantiAssignment_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1046:2: ( 'every' )
                    {
                    // InternalXpath.g:1046:2: ( 'every' )
                    // InternalXpath.g:1047:3: 'every'
                    {
                     before(grammarAccess.getQuantifiedExprAccess().getEveryKeyword_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getQuantifiedExprAccess().getEveryKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Alternatives_0"


    // $ANTLR start "rule__OrExpr__Alternatives"
    // InternalXpath.g:1056:1: rule__OrExpr__Alternatives : ( ( ( rule__OrExpr__Group_0__0 ) ) | ( ( rule__OrExpr__Group_1__0 ) ) | ( ( rule__OrExpr__Group_2__0 ) ) );
    public final void rule__OrExpr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1060:1: ( ( ( rule__OrExpr__Group_0__0 ) ) | ( ( rule__OrExpr__Group_1__0 ) ) | ( ( rule__OrExpr__Group_2__0 ) ) )
            int alt4=3;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalXpath.g:1061:2: ( ( rule__OrExpr__Group_0__0 ) )
                    {
                    // InternalXpath.g:1061:2: ( ( rule__OrExpr__Group_0__0 ) )
                    // InternalXpath.g:1062:3: ( rule__OrExpr__Group_0__0 )
                    {
                     before(grammarAccess.getOrExprAccess().getGroup_0()); 
                    // InternalXpath.g:1063:3: ( rule__OrExpr__Group_0__0 )
                    // InternalXpath.g:1063:4: rule__OrExpr__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1067:2: ( ( rule__OrExpr__Group_1__0 ) )
                    {
                    // InternalXpath.g:1067:2: ( ( rule__OrExpr__Group_1__0 ) )
                    // InternalXpath.g:1068:3: ( rule__OrExpr__Group_1__0 )
                    {
                     before(grammarAccess.getOrExprAccess().getGroup_1()); 
                    // InternalXpath.g:1069:3: ( rule__OrExpr__Group_1__0 )
                    // InternalXpath.g:1069:4: rule__OrExpr__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1073:2: ( ( rule__OrExpr__Group_2__0 ) )
                    {
                    // InternalXpath.g:1073:2: ( ( rule__OrExpr__Group_2__0 ) )
                    // InternalXpath.g:1074:3: ( rule__OrExpr__Group_2__0 )
                    {
                     before(grammarAccess.getOrExprAccess().getGroup_2()); 
                    // InternalXpath.g:1075:3: ( rule__OrExpr__Group_2__0 )
                    // InternalXpath.g:1075:4: rule__OrExpr__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Alternatives"


    // $ANTLR start "rule__OrExpr__Alternatives_0_3_0"
    // InternalXpath.g:1083:1: rule__OrExpr__Alternatives_0_3_0 : ( ( ( rule__OrExpr__OperatorAssignment_0_3_0_0 ) ) | ( ( rule__OrExpr__OperatorAssignment_0_3_0_1 ) ) );
    public final void rule__OrExpr__Alternatives_0_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1087:1: ( ( ( rule__OrExpr__OperatorAssignment_0_3_0_0 ) ) | ( ( rule__OrExpr__OperatorAssignment_0_3_0_1 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==46) ) {
                alt5=1;
            }
            else if ( (LA5_0==47) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalXpath.g:1088:2: ( ( rule__OrExpr__OperatorAssignment_0_3_0_0 ) )
                    {
                    // InternalXpath.g:1088:2: ( ( rule__OrExpr__OperatorAssignment_0_3_0_0 ) )
                    // InternalXpath.g:1089:3: ( rule__OrExpr__OperatorAssignment_0_3_0_0 )
                    {
                     before(grammarAccess.getOrExprAccess().getOperatorAssignment_0_3_0_0()); 
                    // InternalXpath.g:1090:3: ( rule__OrExpr__OperatorAssignment_0_3_0_0 )
                    // InternalXpath.g:1090:4: rule__OrExpr__OperatorAssignment_0_3_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__OperatorAssignment_0_3_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getOperatorAssignment_0_3_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1094:2: ( ( rule__OrExpr__OperatorAssignment_0_3_0_1 ) )
                    {
                    // InternalXpath.g:1094:2: ( ( rule__OrExpr__OperatorAssignment_0_3_0_1 ) )
                    // InternalXpath.g:1095:3: ( rule__OrExpr__OperatorAssignment_0_3_0_1 )
                    {
                     before(grammarAccess.getOrExprAccess().getOperatorAssignment_0_3_0_1()); 
                    // InternalXpath.g:1096:3: ( rule__OrExpr__OperatorAssignment_0_3_0_1 )
                    // InternalXpath.g:1096:4: rule__OrExpr__OperatorAssignment_0_3_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__OperatorAssignment_0_3_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getOperatorAssignment_0_3_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Alternatives_0_3_0"


    // $ANTLR start "rule__OrExpr__Alternatives_1_1"
    // InternalXpath.g:1104:1: rule__OrExpr__Alternatives_1_1 : ( ( ( rule__OrExpr__OperatorAssignment_1_1_0 ) ) | ( ( rule__OrExpr__OperatorAssignment_1_1_1 ) ) );
    public final void rule__OrExpr__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1108:1: ( ( ( rule__OrExpr__OperatorAssignment_1_1_0 ) ) | ( ( rule__OrExpr__OperatorAssignment_1_1_1 ) ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==46) ) {
                alt6=1;
            }
            else if ( (LA6_0==47) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalXpath.g:1109:2: ( ( rule__OrExpr__OperatorAssignment_1_1_0 ) )
                    {
                    // InternalXpath.g:1109:2: ( ( rule__OrExpr__OperatorAssignment_1_1_0 ) )
                    // InternalXpath.g:1110:3: ( rule__OrExpr__OperatorAssignment_1_1_0 )
                    {
                     before(grammarAccess.getOrExprAccess().getOperatorAssignment_1_1_0()); 
                    // InternalXpath.g:1111:3: ( rule__OrExpr__OperatorAssignment_1_1_0 )
                    // InternalXpath.g:1111:4: rule__OrExpr__OperatorAssignment_1_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__OperatorAssignment_1_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getOperatorAssignment_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1115:2: ( ( rule__OrExpr__OperatorAssignment_1_1_1 ) )
                    {
                    // InternalXpath.g:1115:2: ( ( rule__OrExpr__OperatorAssignment_1_1_1 ) )
                    // InternalXpath.g:1116:3: ( rule__OrExpr__OperatorAssignment_1_1_1 )
                    {
                     before(grammarAccess.getOrExprAccess().getOperatorAssignment_1_1_1()); 
                    // InternalXpath.g:1117:3: ( rule__OrExpr__OperatorAssignment_1_1_1 )
                    // InternalXpath.g:1117:4: rule__OrExpr__OperatorAssignment_1_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__OperatorAssignment_1_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getOrExprAccess().getOperatorAssignment_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Alternatives_1_1"


    // $ANTLR start "rule__ComparisonExpr__Alternatives"
    // InternalXpath.g:1125:1: rule__ComparisonExpr__Alternatives : ( ( ( rule__ComparisonExpr__Group_0__0 ) ) | ( ( rule__ComparisonExpr__NotExprAssignment_1 ) ) | ( ( rule__ComparisonExpr__FunctionAssignment_2 ) ) );
    public final void rule__ComparisonExpr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1129:1: ( ( ( rule__ComparisonExpr__Group_0__0 ) ) | ( ( rule__ComparisonExpr__NotExprAssignment_1 ) ) | ( ( rule__ComparisonExpr__FunctionAssignment_2 ) ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
            case 49:
                {
                alt7=1;
                }
                break;
            case 48:
                {
                alt7=2;
                }
                break;
            case 50:
            case 51:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalXpath.g:1130:2: ( ( rule__ComparisonExpr__Group_0__0 ) )
                    {
                    // InternalXpath.g:1130:2: ( ( rule__ComparisonExpr__Group_0__0 ) )
                    // InternalXpath.g:1131:3: ( rule__ComparisonExpr__Group_0__0 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getGroup_0()); 
                    // InternalXpath.g:1132:3: ( rule__ComparisonExpr__Group_0__0 )
                    // InternalXpath.g:1132:4: rule__ComparisonExpr__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1136:2: ( ( rule__ComparisonExpr__NotExprAssignment_1 ) )
                    {
                    // InternalXpath.g:1136:2: ( ( rule__ComparisonExpr__NotExprAssignment_1 ) )
                    // InternalXpath.g:1137:3: ( rule__ComparisonExpr__NotExprAssignment_1 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getNotExprAssignment_1()); 
                    // InternalXpath.g:1138:3: ( rule__ComparisonExpr__NotExprAssignment_1 )
                    // InternalXpath.g:1138:4: rule__ComparisonExpr__NotExprAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__NotExprAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getNotExprAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1142:2: ( ( rule__ComparisonExpr__FunctionAssignment_2 ) )
                    {
                    // InternalXpath.g:1142:2: ( ( rule__ComparisonExpr__FunctionAssignment_2 ) )
                    // InternalXpath.g:1143:3: ( rule__ComparisonExpr__FunctionAssignment_2 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getFunctionAssignment_2()); 
                    // InternalXpath.g:1144:3: ( rule__ComparisonExpr__FunctionAssignment_2 )
                    // InternalXpath.g:1144:4: rule__ComparisonExpr__FunctionAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__FunctionAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getFunctionAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Alternatives"


    // $ANTLR start "rule__ComparisonExpr__Alternatives_0_1_0"
    // InternalXpath.g:1152:1: rule__ComparisonExpr__Alternatives_0_1_0 : ( ( ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 ) ) | ( ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 ) ) | ( ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 ) ) );
    public final void rule__ComparisonExpr__Alternatives_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1156:1: ( ( ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 ) ) | ( ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 ) ) | ( ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 ) ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
                {
                alt8=1;
                }
                break;
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
                {
                alt8=2;
                }
                break;
            case 31:
            case 32:
            case 33:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalXpath.g:1157:2: ( ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 ) )
                    {
                    // InternalXpath.g:1157:2: ( ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 ) )
                    // InternalXpath.g:1158:3: ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getValueCompOptionalAssignment_0_1_0_0()); 
                    // InternalXpath.g:1159:3: ( rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 )
                    // InternalXpath.g:1159:4: rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getValueCompOptionalAssignment_0_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1163:2: ( ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 ) )
                    {
                    // InternalXpath.g:1163:2: ( ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 ) )
                    // InternalXpath.g:1164:3: ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getGeneralCompOptionalAssignment_0_1_0_1()); 
                    // InternalXpath.g:1165:3: ( rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 )
                    // InternalXpath.g:1165:4: rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getGeneralCompOptionalAssignment_0_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1169:2: ( ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 ) )
                    {
                    // InternalXpath.g:1169:2: ( ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 ) )
                    // InternalXpath.g:1170:3: ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 )
                    {
                     before(grammarAccess.getComparisonExprAccess().getNodeCompOptionalAssignment_0_1_0_2()); 
                    // InternalXpath.g:1171:3: ( rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 )
                    // InternalXpath.g:1171:4: rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonExprAccess().getNodeCompOptionalAssignment_0_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Alternatives_0_1_0"


    // $ANTLR start "rule__AdditiveExpr__Additive_listAlternatives_1_0_0"
    // InternalXpath.g:1179:1: rule__AdditiveExpr__Additive_listAlternatives_1_0_0 : ( ( '+' ) | ( '-' ) );
    public final void rule__AdditiveExpr__Additive_listAlternatives_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1183:1: ( ( '+' ) | ( '-' ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==13) ) {
                alt9=1;
            }
            else if ( (LA9_0==14) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalXpath.g:1184:2: ( '+' )
                    {
                    // InternalXpath.g:1184:2: ( '+' )
                    // InternalXpath.g:1185:3: '+'
                    {
                     before(grammarAccess.getAdditiveExprAccess().getAdditive_listPlusSignKeyword_1_0_0_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAdditiveExprAccess().getAdditive_listPlusSignKeyword_1_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1190:2: ( '-' )
                    {
                    // InternalXpath.g:1190:2: ( '-' )
                    // InternalXpath.g:1191:3: '-'
                    {
                     before(grammarAccess.getAdditiveExprAccess().getAdditive_listHyphenMinusKeyword_1_0_0_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAdditiveExprAccess().getAdditive_listHyphenMinusKeyword_1_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Additive_listAlternatives_1_0_0"


    // $ANTLR start "rule__UnionExpr__Intercection_listAlternatives_1_0_0"
    // InternalXpath.g:1200:1: rule__UnionExpr__Intercection_listAlternatives_1_0_0 : ( ( 'union' ) | ( '|' ) );
    public final void rule__UnionExpr__Intercection_listAlternatives_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1204:1: ( ( 'union' ) | ( '|' ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==15) ) {
                alt10=1;
            }
            else if ( (LA10_0==16) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalXpath.g:1205:2: ( 'union' )
                    {
                    // InternalXpath.g:1205:2: ( 'union' )
                    // InternalXpath.g:1206:3: 'union'
                    {
                     before(grammarAccess.getUnionExprAccess().getIntercection_listUnionKeyword_1_0_0_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getUnionExprAccess().getIntercection_listUnionKeyword_1_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1211:2: ( '|' )
                    {
                    // InternalXpath.g:1211:2: ( '|' )
                    // InternalXpath.g:1212:3: '|'
                    {
                     before(grammarAccess.getUnionExprAccess().getIntercection_listVerticalLineKeyword_1_0_0_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getUnionExprAccess().getIntercection_listVerticalLineKeyword_1_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Intercection_listAlternatives_1_0_0"


    // $ANTLR start "rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0"
    // InternalXpath.g:1221:1: rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 : ( ( 'intersect' ) | ( 'except' ) );
    public final void rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1225:1: ( ( 'intersect' ) | ( 'except' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            else if ( (LA11_0==18) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalXpath.g:1226:2: ( 'intersect' )
                    {
                    // InternalXpath.g:1226:2: ( 'intersect' )
                    // InternalXpath.g:1227:3: 'intersect'
                    {
                     before(grammarAccess.getIntersectExceptExprAccess().getInstance_listIntersectKeyword_1_0_0_0()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getIntersectExceptExprAccess().getInstance_listIntersectKeyword_1_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1232:2: ( 'except' )
                    {
                    // InternalXpath.g:1232:2: ( 'except' )
                    // InternalXpath.g:1233:3: 'except'
                    {
                     before(grammarAccess.getIntersectExceptExprAccess().getInstance_listExceptKeyword_1_0_0_1()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getIntersectExceptExprAccess().getInstance_listExceptKeyword_1_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0"


    // $ANTLR start "rule__ValueExpr__Alternatives"
    // InternalXpath.g:1242:1: rule__ValueExpr__Alternatives : ( ( ( rule__ValueExpr__PathExprAssignment_0 ) ) | ( ( rule__ValueExpr__StringliteralAssignment_1 ) ) | ( ( rule__ValueExpr__NumericLiteralAssignment_2 ) ) );
    public final void rule__ValueExpr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1246:1: ( ( ( rule__ValueExpr__PathExprAssignment_0 ) ) | ( ( rule__ValueExpr__StringliteralAssignment_1 ) ) | ( ( rule__ValueExpr__NumericLiteralAssignment_2 ) ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt12=1;
                }
                break;
            case RULE_STRING:
                {
                alt12=2;
                }
                break;
            case RULE_INT:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalXpath.g:1247:2: ( ( rule__ValueExpr__PathExprAssignment_0 ) )
                    {
                    // InternalXpath.g:1247:2: ( ( rule__ValueExpr__PathExprAssignment_0 ) )
                    // InternalXpath.g:1248:3: ( rule__ValueExpr__PathExprAssignment_0 )
                    {
                     before(grammarAccess.getValueExprAccess().getPathExprAssignment_0()); 
                    // InternalXpath.g:1249:3: ( rule__ValueExpr__PathExprAssignment_0 )
                    // InternalXpath.g:1249:4: rule__ValueExpr__PathExprAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ValueExpr__PathExprAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueExprAccess().getPathExprAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1253:2: ( ( rule__ValueExpr__StringliteralAssignment_1 ) )
                    {
                    // InternalXpath.g:1253:2: ( ( rule__ValueExpr__StringliteralAssignment_1 ) )
                    // InternalXpath.g:1254:3: ( rule__ValueExpr__StringliteralAssignment_1 )
                    {
                     before(grammarAccess.getValueExprAccess().getStringliteralAssignment_1()); 
                    // InternalXpath.g:1255:3: ( rule__ValueExpr__StringliteralAssignment_1 )
                    // InternalXpath.g:1255:4: rule__ValueExpr__StringliteralAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ValueExpr__StringliteralAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueExprAccess().getStringliteralAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1259:2: ( ( rule__ValueExpr__NumericLiteralAssignment_2 ) )
                    {
                    // InternalXpath.g:1259:2: ( ( rule__ValueExpr__NumericLiteralAssignment_2 ) )
                    // InternalXpath.g:1260:3: ( rule__ValueExpr__NumericLiteralAssignment_2 )
                    {
                     before(grammarAccess.getValueExprAccess().getNumericLiteralAssignment_2()); 
                    // InternalXpath.g:1261:3: ( rule__ValueExpr__NumericLiteralAssignment_2 )
                    // InternalXpath.g:1261:4: rule__ValueExpr__NumericLiteralAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__ValueExpr__NumericLiteralAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueExprAccess().getNumericLiteralAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueExpr__Alternatives"


    // $ANTLR start "rule__GeneralComp__Alternatives"
    // InternalXpath.g:1269:1: rule__GeneralComp__Alternatives : ( ( '=' ) | ( '!=' ) | ( '<' ) | ( '<=' ) | ( '>' ) | ( '>=' ) );
    public final void rule__GeneralComp__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1273:1: ( ( '=' ) | ( '!=' ) | ( '<' ) | ( '<=' ) | ( '>' ) | ( '>=' ) )
            int alt13=6;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt13=1;
                }
                break;
            case 20:
                {
                alt13=2;
                }
                break;
            case 21:
                {
                alt13=3;
                }
                break;
            case 22:
                {
                alt13=4;
                }
                break;
            case 23:
                {
                alt13=5;
                }
                break;
            case 24:
                {
                alt13=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalXpath.g:1274:2: ( '=' )
                    {
                    // InternalXpath.g:1274:2: ( '=' )
                    // InternalXpath.g:1275:3: '='
                    {
                     before(grammarAccess.getGeneralCompAccess().getEqualsSignKeyword_0()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getEqualsSignKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1280:2: ( '!=' )
                    {
                    // InternalXpath.g:1280:2: ( '!=' )
                    // InternalXpath.g:1281:3: '!='
                    {
                     before(grammarAccess.getGeneralCompAccess().getExclamationMarkEqualsSignKeyword_1()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getExclamationMarkEqualsSignKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1286:2: ( '<' )
                    {
                    // InternalXpath.g:1286:2: ( '<' )
                    // InternalXpath.g:1287:3: '<'
                    {
                     before(grammarAccess.getGeneralCompAccess().getLessThanSignKeyword_2()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getLessThanSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXpath.g:1292:2: ( '<=' )
                    {
                    // InternalXpath.g:1292:2: ( '<=' )
                    // InternalXpath.g:1293:3: '<='
                    {
                     before(grammarAccess.getGeneralCompAccess().getLessThanSignEqualsSignKeyword_3()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getLessThanSignEqualsSignKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXpath.g:1298:2: ( '>' )
                    {
                    // InternalXpath.g:1298:2: ( '>' )
                    // InternalXpath.g:1299:3: '>'
                    {
                     before(grammarAccess.getGeneralCompAccess().getGreaterThanSignKeyword_4()); 
                    match(input,23,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getGreaterThanSignKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalXpath.g:1304:2: ( '>=' )
                    {
                    // InternalXpath.g:1304:2: ( '>=' )
                    // InternalXpath.g:1305:3: '>='
                    {
                     before(grammarAccess.getGeneralCompAccess().getGreaterThanSignEqualsSignKeyword_5()); 
                    match(input,24,FOLLOW_2); 
                     after(grammarAccess.getGeneralCompAccess().getGreaterThanSignEqualsSignKeyword_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GeneralComp__Alternatives"


    // $ANTLR start "rule__ValueComp__Alternatives"
    // InternalXpath.g:1314:1: rule__ValueComp__Alternatives : ( ( 'eq' ) | ( 'ne' ) | ( 'lt' ) | ( 'le' ) | ( 'gt' ) | ( 'ge' ) );
    public final void rule__ValueComp__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1318:1: ( ( 'eq' ) | ( 'ne' ) | ( 'lt' ) | ( 'le' ) | ( 'gt' ) | ( 'ge' ) )
            int alt14=6;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt14=1;
                }
                break;
            case 26:
                {
                alt14=2;
                }
                break;
            case 27:
                {
                alt14=3;
                }
                break;
            case 28:
                {
                alt14=4;
                }
                break;
            case 29:
                {
                alt14=5;
                }
                break;
            case 30:
                {
                alt14=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalXpath.g:1319:2: ( 'eq' )
                    {
                    // InternalXpath.g:1319:2: ( 'eq' )
                    // InternalXpath.g:1320:3: 'eq'
                    {
                     before(grammarAccess.getValueCompAccess().getEqKeyword_0()); 
                    match(input,25,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getEqKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1325:2: ( 'ne' )
                    {
                    // InternalXpath.g:1325:2: ( 'ne' )
                    // InternalXpath.g:1326:3: 'ne'
                    {
                     before(grammarAccess.getValueCompAccess().getNeKeyword_1()); 
                    match(input,26,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getNeKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1331:2: ( 'lt' )
                    {
                    // InternalXpath.g:1331:2: ( 'lt' )
                    // InternalXpath.g:1332:3: 'lt'
                    {
                     before(grammarAccess.getValueCompAccess().getLtKeyword_2()); 
                    match(input,27,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getLtKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXpath.g:1337:2: ( 'le' )
                    {
                    // InternalXpath.g:1337:2: ( 'le' )
                    // InternalXpath.g:1338:3: 'le'
                    {
                     before(grammarAccess.getValueCompAccess().getLeKeyword_3()); 
                    match(input,28,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getLeKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXpath.g:1343:2: ( 'gt' )
                    {
                    // InternalXpath.g:1343:2: ( 'gt' )
                    // InternalXpath.g:1344:3: 'gt'
                    {
                     before(grammarAccess.getValueCompAccess().getGtKeyword_4()); 
                    match(input,29,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getGtKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalXpath.g:1349:2: ( 'ge' )
                    {
                    // InternalXpath.g:1349:2: ( 'ge' )
                    // InternalXpath.g:1350:3: 'ge'
                    {
                     before(grammarAccess.getValueCompAccess().getGeKeyword_5()); 
                    match(input,30,FOLLOW_2); 
                     after(grammarAccess.getValueCompAccess().getGeKeyword_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueComp__Alternatives"


    // $ANTLR start "rule__NodeComp__Alternatives"
    // InternalXpath.g:1359:1: rule__NodeComp__Alternatives : ( ( 'is' ) | ( '<<' ) | ( '>>' ) );
    public final void rule__NodeComp__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1363:1: ( ( 'is' ) | ( '<<' ) | ( '>>' ) )
            int alt15=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt15=1;
                }
                break;
            case 32:
                {
                alt15=2;
                }
                break;
            case 33:
                {
                alt15=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalXpath.g:1364:2: ( 'is' )
                    {
                    // InternalXpath.g:1364:2: ( 'is' )
                    // InternalXpath.g:1365:3: 'is'
                    {
                     before(grammarAccess.getNodeCompAccess().getIsKeyword_0()); 
                    match(input,31,FOLLOW_2); 
                     after(grammarAccess.getNodeCompAccess().getIsKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1370:2: ( '<<' )
                    {
                    // InternalXpath.g:1370:2: ( '<<' )
                    // InternalXpath.g:1371:3: '<<'
                    {
                     before(grammarAccess.getNodeCompAccess().getLessThanSignLessThanSignKeyword_1()); 
                    match(input,32,FOLLOW_2); 
                     after(grammarAccess.getNodeCompAccess().getLessThanSignLessThanSignKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXpath.g:1376:2: ( '>>' )
                    {
                    // InternalXpath.g:1376:2: ( '>>' )
                    // InternalXpath.g:1377:3: '>>'
                    {
                     before(grammarAccess.getNodeCompAccess().getGreaterThanSignGreaterThanSignKeyword_2()); 
                    match(input,33,FOLLOW_2); 
                     after(grammarAccess.getNodeCompAccess().getGreaterThanSignGreaterThanSignKeyword_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeComp__Alternatives"


    // $ANTLR start "rule__PathExpr__Path_listAlternatives_1_0_0"
    // InternalXpath.g:1386:1: rule__PathExpr__Path_listAlternatives_1_0_0 : ( ( '/' ) | ( '//' ) );
    public final void rule__PathExpr__Path_listAlternatives_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1390:1: ( ( '/' ) | ( '//' ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==34) ) {
                alt16=1;
            }
            else if ( (LA16_0==35) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalXpath.g:1391:2: ( '/' )
                    {
                    // InternalXpath.g:1391:2: ( '/' )
                    // InternalXpath.g:1392:3: '/'
                    {
                     before(grammarAccess.getPathExprAccess().getPath_listSolidusKeyword_1_0_0_0()); 
                    match(input,34,FOLLOW_2); 
                     after(grammarAccess.getPathExprAccess().getPath_listSolidusKeyword_1_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1397:2: ( '//' )
                    {
                    // InternalXpath.g:1397:2: ( '//' )
                    // InternalXpath.g:1398:3: '//'
                    {
                     before(grammarAccess.getPathExprAccess().getPath_listSolidusSolidusKeyword_1_0_0_1()); 
                    match(input,35,FOLLOW_2); 
                     after(grammarAccess.getPathExprAccess().getPath_listSolidusSolidusKeyword_1_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Path_listAlternatives_1_0_0"


    // $ANTLR start "rule__Function__Alternatives"
    // InternalXpath.g:1407:1: rule__Function__Alternatives : ( ( ( rule__Function__SumFunctionAssignment_0 ) ) | ( ( rule__Function__CountFunctionAssignment_1 ) ) );
    public final void rule__Function__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1411:1: ( ( ( rule__Function__SumFunctionAssignment_0 ) ) | ( ( rule__Function__CountFunctionAssignment_1 ) ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==50) ) {
                alt17=1;
            }
            else if ( (LA17_0==51) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalXpath.g:1412:2: ( ( rule__Function__SumFunctionAssignment_0 ) )
                    {
                    // InternalXpath.g:1412:2: ( ( rule__Function__SumFunctionAssignment_0 ) )
                    // InternalXpath.g:1413:3: ( rule__Function__SumFunctionAssignment_0 )
                    {
                     before(grammarAccess.getFunctionAccess().getSumFunctionAssignment_0()); 
                    // InternalXpath.g:1414:3: ( rule__Function__SumFunctionAssignment_0 )
                    // InternalXpath.g:1414:4: rule__Function__SumFunctionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__SumFunctionAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFunctionAccess().getSumFunctionAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1418:2: ( ( rule__Function__CountFunctionAssignment_1 ) )
                    {
                    // InternalXpath.g:1418:2: ( ( rule__Function__CountFunctionAssignment_1 ) )
                    // InternalXpath.g:1419:3: ( rule__Function__CountFunctionAssignment_1 )
                    {
                     before(grammarAccess.getFunctionAccess().getCountFunctionAssignment_1()); 
                    // InternalXpath.g:1420:3: ( rule__Function__CountFunctionAssignment_1 )
                    // InternalXpath.g:1420:4: rule__Function__CountFunctionAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__CountFunctionAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getFunctionAccess().getCountFunctionAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Alternatives"


    // $ANTLR start "rule__ThenElse__Alternatives"
    // InternalXpath.g:1428:1: rule__ThenElse__Alternatives : ( ( ( rule__ThenElse__Group_0__0 ) ) | ( ( rule__ThenElse__Group_1__0 ) ) );
    public final void rule__ThenElse__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1432:1: ( ( ( rule__ThenElse__Group_0__0 ) ) | ( ( rule__ThenElse__Group_1__0 ) ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==52) ) {
                int LA18_1 = input.LA(2);

                if ( (LA18_1==11) ) {
                    alt18=1;
                }
                else if ( (LA18_1==RULE_STRING||LA18_1==55) ) {
                    alt18=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 18, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalXpath.g:1433:2: ( ( rule__ThenElse__Group_0__0 ) )
                    {
                    // InternalXpath.g:1433:2: ( ( rule__ThenElse__Group_0__0 ) )
                    // InternalXpath.g:1434:3: ( rule__ThenElse__Group_0__0 )
                    {
                     before(grammarAccess.getThenElseAccess().getGroup_0()); 
                    // InternalXpath.g:1435:3: ( rule__ThenElse__Group_0__0 )
                    // InternalXpath.g:1435:4: rule__ThenElse__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ThenElse__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getThenElseAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1439:2: ( ( rule__ThenElse__Group_1__0 ) )
                    {
                    // InternalXpath.g:1439:2: ( ( rule__ThenElse__Group_1__0 ) )
                    // InternalXpath.g:1440:3: ( rule__ThenElse__Group_1__0 )
                    {
                     before(grammarAccess.getThenElseAccess().getGroup_1()); 
                    // InternalXpath.g:1441:3: ( rule__ThenElse__Group_1__0 )
                    // InternalXpath.g:1441:4: rule__ThenElse__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ThenElse__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getThenElseAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Alternatives"


    // $ANTLR start "rule__False__Alternatives"
    // InternalXpath.g:1449:1: rule__False__Alternatives : ( ( ( rule__False__IsFalseAssignment_0 ) ) | ( ( rule__False__MsgAssignment_1 ) ) );
    public final void rule__False__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1453:1: ( ( ( rule__False__IsFalseAssignment_0 ) ) | ( ( rule__False__MsgAssignment_1 ) ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==55) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_STRING) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalXpath.g:1454:2: ( ( rule__False__IsFalseAssignment_0 ) )
                    {
                    // InternalXpath.g:1454:2: ( ( rule__False__IsFalseAssignment_0 ) )
                    // InternalXpath.g:1455:3: ( rule__False__IsFalseAssignment_0 )
                    {
                     before(grammarAccess.getFalseAccess().getIsFalseAssignment_0()); 
                    // InternalXpath.g:1456:3: ( rule__False__IsFalseAssignment_0 )
                    // InternalXpath.g:1456:4: rule__False__IsFalseAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__False__IsFalseAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFalseAccess().getIsFalseAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXpath.g:1460:2: ( ( rule__False__MsgAssignment_1 ) )
                    {
                    // InternalXpath.g:1460:2: ( ( rule__False__MsgAssignment_1 ) )
                    // InternalXpath.g:1461:3: ( rule__False__MsgAssignment_1 )
                    {
                     before(grammarAccess.getFalseAccess().getMsgAssignment_1()); 
                    // InternalXpath.g:1462:3: ( rule__False__MsgAssignment_1 )
                    // InternalXpath.g:1462:4: rule__False__MsgAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__False__MsgAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getFalseAccess().getMsgAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__False__Alternatives"


    // $ANTLR start "rule__Domainmodel__Group__0"
    // InternalXpath.g:1470:1: rule__Domainmodel__Group__0 : rule__Domainmodel__Group__0__Impl rule__Domainmodel__Group__1 ;
    public final void rule__Domainmodel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1474:1: ( rule__Domainmodel__Group__0__Impl rule__Domainmodel__Group__1 )
            // InternalXpath.g:1475:2: rule__Domainmodel__Group__0__Impl rule__Domainmodel__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Domainmodel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domainmodel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__0"


    // $ANTLR start "rule__Domainmodel__Group__0__Impl"
    // InternalXpath.g:1482:1: rule__Domainmodel__Group__0__Impl : ( ( rule__Domainmodel__ValidationPathAssignment_0 ) ) ;
    public final void rule__Domainmodel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1486:1: ( ( ( rule__Domainmodel__ValidationPathAssignment_0 ) ) )
            // InternalXpath.g:1487:1: ( ( rule__Domainmodel__ValidationPathAssignment_0 ) )
            {
            // InternalXpath.g:1487:1: ( ( rule__Domainmodel__ValidationPathAssignment_0 ) )
            // InternalXpath.g:1488:2: ( rule__Domainmodel__ValidationPathAssignment_0 )
            {
             before(grammarAccess.getDomainmodelAccess().getValidationPathAssignment_0()); 
            // InternalXpath.g:1489:2: ( rule__Domainmodel__ValidationPathAssignment_0 )
            // InternalXpath.g:1489:3: rule__Domainmodel__ValidationPathAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Domainmodel__ValidationPathAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDomainmodelAccess().getValidationPathAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__0__Impl"


    // $ANTLR start "rule__Domainmodel__Group__1"
    // InternalXpath.g:1497:1: rule__Domainmodel__Group__1 : rule__Domainmodel__Group__1__Impl rule__Domainmodel__Group__2 ;
    public final void rule__Domainmodel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1501:1: ( rule__Domainmodel__Group__1__Impl rule__Domainmodel__Group__2 )
            // InternalXpath.g:1502:2: rule__Domainmodel__Group__1__Impl rule__Domainmodel__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Domainmodel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domainmodel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__1"


    // $ANTLR start "rule__Domainmodel__Group__1__Impl"
    // InternalXpath.g:1509:1: rule__Domainmodel__Group__1__Impl : ( ';' ) ;
    public final void rule__Domainmodel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1513:1: ( ( ';' ) )
            // InternalXpath.g:1514:1: ( ';' )
            {
            // InternalXpath.g:1514:1: ( ';' )
            // InternalXpath.g:1515:2: ';'
            {
             before(grammarAccess.getDomainmodelAccess().getSemicolonKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getDomainmodelAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__1__Impl"


    // $ANTLR start "rule__Domainmodel__Group__2"
    // InternalXpath.g:1524:1: rule__Domainmodel__Group__2 : rule__Domainmodel__Group__2__Impl rule__Domainmodel__Group__3 ;
    public final void rule__Domainmodel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1528:1: ( rule__Domainmodel__Group__2__Impl rule__Domainmodel__Group__3 )
            // InternalXpath.g:1529:2: rule__Domainmodel__Group__2__Impl rule__Domainmodel__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Domainmodel__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Domainmodel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__2"


    // $ANTLR start "rule__Domainmodel__Group__2__Impl"
    // InternalXpath.g:1536:1: rule__Domainmodel__Group__2__Impl : ( ( rule__Domainmodel__ElementsAssignment_2 ) ) ;
    public final void rule__Domainmodel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1540:1: ( ( ( rule__Domainmodel__ElementsAssignment_2 ) ) )
            // InternalXpath.g:1541:1: ( ( rule__Domainmodel__ElementsAssignment_2 ) )
            {
            // InternalXpath.g:1541:1: ( ( rule__Domainmodel__ElementsAssignment_2 ) )
            // InternalXpath.g:1542:2: ( rule__Domainmodel__ElementsAssignment_2 )
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAssignment_2()); 
            // InternalXpath.g:1543:2: ( rule__Domainmodel__ElementsAssignment_2 )
            // InternalXpath.g:1543:3: rule__Domainmodel__ElementsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Domainmodel__ElementsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDomainmodelAccess().getElementsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__2__Impl"


    // $ANTLR start "rule__Domainmodel__Group__3"
    // InternalXpath.g:1551:1: rule__Domainmodel__Group__3 : rule__Domainmodel__Group__3__Impl ;
    public final void rule__Domainmodel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1555:1: ( rule__Domainmodel__Group__3__Impl )
            // InternalXpath.g:1556:2: rule__Domainmodel__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Domainmodel__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__3"


    // $ANTLR start "rule__Domainmodel__Group__3__Impl"
    // InternalXpath.g:1562:1: rule__Domainmodel__Group__3__Impl : ( ';' ) ;
    public final void rule__Domainmodel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1566:1: ( ( ';' ) )
            // InternalXpath.g:1567:1: ( ';' )
            {
            // InternalXpath.g:1567:1: ( ';' )
            // InternalXpath.g:1568:2: ';'
            {
             before(grammarAccess.getDomainmodelAccess().getSemicolonKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getDomainmodelAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__Group__3__Impl"


    // $ANTLR start "rule__ValidationPath__Group__0"
    // InternalXpath.g:1578:1: rule__ValidationPath__Group__0 : rule__ValidationPath__Group__0__Impl rule__ValidationPath__Group__1 ;
    public final void rule__ValidationPath__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1582:1: ( rule__ValidationPath__Group__0__Impl rule__ValidationPath__Group__1 )
            // InternalXpath.g:1583:2: rule__ValidationPath__Group__0__Impl rule__ValidationPath__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__ValidationPath__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ValidationPath__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group__0"


    // $ANTLR start "rule__ValidationPath__Group__0__Impl"
    // InternalXpath.g:1590:1: rule__ValidationPath__Group__0__Impl : ( ( rule__ValidationPath__PathAssignment_0 ) ) ;
    public final void rule__ValidationPath__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1594:1: ( ( ( rule__ValidationPath__PathAssignment_0 ) ) )
            // InternalXpath.g:1595:1: ( ( rule__ValidationPath__PathAssignment_0 ) )
            {
            // InternalXpath.g:1595:1: ( ( rule__ValidationPath__PathAssignment_0 ) )
            // InternalXpath.g:1596:2: ( rule__ValidationPath__PathAssignment_0 )
            {
             before(grammarAccess.getValidationPathAccess().getPathAssignment_0()); 
            // InternalXpath.g:1597:2: ( rule__ValidationPath__PathAssignment_0 )
            // InternalXpath.g:1597:3: rule__ValidationPath__PathAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ValidationPath__PathAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getValidationPathAccess().getPathAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group__0__Impl"


    // $ANTLR start "rule__ValidationPath__Group__1"
    // InternalXpath.g:1605:1: rule__ValidationPath__Group__1 : rule__ValidationPath__Group__1__Impl ;
    public final void rule__ValidationPath__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1609:1: ( rule__ValidationPath__Group__1__Impl )
            // InternalXpath.g:1610:2: rule__ValidationPath__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ValidationPath__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group__1"


    // $ANTLR start "rule__ValidationPath__Group__1__Impl"
    // InternalXpath.g:1616:1: rule__ValidationPath__Group__1__Impl : ( ( rule__ValidationPath__Group_1__0 )* ) ;
    public final void rule__ValidationPath__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1620:1: ( ( ( rule__ValidationPath__Group_1__0 )* ) )
            // InternalXpath.g:1621:1: ( ( rule__ValidationPath__Group_1__0 )* )
            {
            // InternalXpath.g:1621:1: ( ( rule__ValidationPath__Group_1__0 )* )
            // InternalXpath.g:1622:2: ( rule__ValidationPath__Group_1__0 )*
            {
             before(grammarAccess.getValidationPathAccess().getGroup_1()); 
            // InternalXpath.g:1623:2: ( rule__ValidationPath__Group_1__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==34) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalXpath.g:1623:3: rule__ValidationPath__Group_1__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__ValidationPath__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getValidationPathAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group__1__Impl"


    // $ANTLR start "rule__ValidationPath__Group_1__0"
    // InternalXpath.g:1632:1: rule__ValidationPath__Group_1__0 : rule__ValidationPath__Group_1__0__Impl rule__ValidationPath__Group_1__1 ;
    public final void rule__ValidationPath__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1636:1: ( rule__ValidationPath__Group_1__0__Impl rule__ValidationPath__Group_1__1 )
            // InternalXpath.g:1637:2: rule__ValidationPath__Group_1__0__Impl rule__ValidationPath__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__ValidationPath__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ValidationPath__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group_1__0"


    // $ANTLR start "rule__ValidationPath__Group_1__0__Impl"
    // InternalXpath.g:1644:1: rule__ValidationPath__Group_1__0__Impl : ( '/' ) ;
    public final void rule__ValidationPath__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1648:1: ( ( '/' ) )
            // InternalXpath.g:1649:1: ( '/' )
            {
            // InternalXpath.g:1649:1: ( '/' )
            // InternalXpath.g:1650:2: '/'
            {
             before(grammarAccess.getValidationPathAccess().getSolidusKeyword_1_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getValidationPathAccess().getSolidusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group_1__0__Impl"


    // $ANTLR start "rule__ValidationPath__Group_1__1"
    // InternalXpath.g:1659:1: rule__ValidationPath__Group_1__1 : rule__ValidationPath__Group_1__1__Impl ;
    public final void rule__ValidationPath__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1663:1: ( rule__ValidationPath__Group_1__1__Impl )
            // InternalXpath.g:1664:2: rule__ValidationPath__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ValidationPath__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group_1__1"


    // $ANTLR start "rule__ValidationPath__Group_1__1__Impl"
    // InternalXpath.g:1670:1: rule__ValidationPath__Group_1__1__Impl : ( ( rule__ValidationPath__Path_listAssignment_1_1 ) ) ;
    public final void rule__ValidationPath__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1674:1: ( ( ( rule__ValidationPath__Path_listAssignment_1_1 ) ) )
            // InternalXpath.g:1675:1: ( ( rule__ValidationPath__Path_listAssignment_1_1 ) )
            {
            // InternalXpath.g:1675:1: ( ( rule__ValidationPath__Path_listAssignment_1_1 ) )
            // InternalXpath.g:1676:2: ( rule__ValidationPath__Path_listAssignment_1_1 )
            {
             before(grammarAccess.getValidationPathAccess().getPath_listAssignment_1_1()); 
            // InternalXpath.g:1677:2: ( rule__ValidationPath__Path_listAssignment_1_1 )
            // InternalXpath.g:1677:3: rule__ValidationPath__Path_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ValidationPath__Path_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getValidationPathAccess().getPath_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Group_1__1__Impl"


    // $ANTLR start "rule__Expr__Group__0"
    // InternalXpath.g:1686:1: rule__Expr__Group__0 : rule__Expr__Group__0__Impl rule__Expr__Group__1 ;
    public final void rule__Expr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1690:1: ( rule__Expr__Group__0__Impl rule__Expr__Group__1 )
            // InternalXpath.g:1691:2: rule__Expr__Group__0__Impl rule__Expr__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Expr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group__0"


    // $ANTLR start "rule__Expr__Group__0__Impl"
    // InternalXpath.g:1698:1: rule__Expr__Group__0__Impl : ( ( rule__Expr__ExprSingleAssignment_0 ) ) ;
    public final void rule__Expr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1702:1: ( ( ( rule__Expr__ExprSingleAssignment_0 ) ) )
            // InternalXpath.g:1703:1: ( ( rule__Expr__ExprSingleAssignment_0 ) )
            {
            // InternalXpath.g:1703:1: ( ( rule__Expr__ExprSingleAssignment_0 ) )
            // InternalXpath.g:1704:2: ( rule__Expr__ExprSingleAssignment_0 )
            {
             before(grammarAccess.getExprAccess().getExprSingleAssignment_0()); 
            // InternalXpath.g:1705:2: ( rule__Expr__ExprSingleAssignment_0 )
            // InternalXpath.g:1705:3: rule__Expr__ExprSingleAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Expr__ExprSingleAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getExprAccess().getExprSingleAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group__0__Impl"


    // $ANTLR start "rule__Expr__Group__1"
    // InternalXpath.g:1713:1: rule__Expr__Group__1 : rule__Expr__Group__1__Impl ;
    public final void rule__Expr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1717:1: ( rule__Expr__Group__1__Impl )
            // InternalXpath.g:1718:2: rule__Expr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group__1"


    // $ANTLR start "rule__Expr__Group__1__Impl"
    // InternalXpath.g:1724:1: rule__Expr__Group__1__Impl : ( ( rule__Expr__Group_1__0 )* ) ;
    public final void rule__Expr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1728:1: ( ( ( rule__Expr__Group_1__0 )* ) )
            // InternalXpath.g:1729:1: ( ( rule__Expr__Group_1__0 )* )
            {
            // InternalXpath.g:1729:1: ( ( rule__Expr__Group_1__0 )* )
            // InternalXpath.g:1730:2: ( rule__Expr__Group_1__0 )*
            {
             before(grammarAccess.getExprAccess().getGroup_1()); 
            // InternalXpath.g:1731:2: ( rule__Expr__Group_1__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==37) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalXpath.g:1731:3: rule__Expr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Expr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group__1__Impl"


    // $ANTLR start "rule__Expr__Group_1__0"
    // InternalXpath.g:1740:1: rule__Expr__Group_1__0 : rule__Expr__Group_1__0__Impl rule__Expr__Group_1__1 ;
    public final void rule__Expr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1744:1: ( rule__Expr__Group_1__0__Impl rule__Expr__Group_1__1 )
            // InternalXpath.g:1745:2: rule__Expr__Group_1__0__Impl rule__Expr__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__Expr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group_1__0"


    // $ANTLR start "rule__Expr__Group_1__0__Impl"
    // InternalXpath.g:1752:1: rule__Expr__Group_1__0__Impl : ( ',' ) ;
    public final void rule__Expr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1756:1: ( ( ',' ) )
            // InternalXpath.g:1757:1: ( ',' )
            {
            // InternalXpath.g:1757:1: ( ',' )
            // InternalXpath.g:1758:2: ','
            {
             before(grammarAccess.getExprAccess().getCommaKeyword_1_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getExprAccess().getCommaKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group_1__0__Impl"


    // $ANTLR start "rule__Expr__Group_1__1"
    // InternalXpath.g:1767:1: rule__Expr__Group_1__1 : rule__Expr__Group_1__1__Impl ;
    public final void rule__Expr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1771:1: ( rule__Expr__Group_1__1__Impl )
            // InternalXpath.g:1772:2: rule__Expr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group_1__1"


    // $ANTLR start "rule__Expr__Group_1__1__Impl"
    // InternalXpath.g:1778:1: rule__Expr__Group_1__1__Impl : ( ( rule__Expr__Multi_listAssignment_1_1 ) ) ;
    public final void rule__Expr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1782:1: ( ( ( rule__Expr__Multi_listAssignment_1_1 ) ) )
            // InternalXpath.g:1783:1: ( ( rule__Expr__Multi_listAssignment_1_1 ) )
            {
            // InternalXpath.g:1783:1: ( ( rule__Expr__Multi_listAssignment_1_1 ) )
            // InternalXpath.g:1784:2: ( rule__Expr__Multi_listAssignment_1_1 )
            {
             before(grammarAccess.getExprAccess().getMulti_listAssignment_1_1()); 
            // InternalXpath.g:1785:2: ( rule__Expr__Multi_listAssignment_1_1 )
            // InternalXpath.g:1785:3: rule__Expr__Multi_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Expr__Multi_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExprAccess().getMulti_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Group_1__1__Impl"


    // $ANTLR start "rule__ForExpr__Group__0"
    // InternalXpath.g:1794:1: rule__ForExpr__Group__0 : rule__ForExpr__Group__0__Impl rule__ForExpr__Group__1 ;
    public final void rule__ForExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1798:1: ( rule__ForExpr__Group__0__Impl rule__ForExpr__Group__1 )
            // InternalXpath.g:1799:2: rule__ForExpr__Group__0__Impl rule__ForExpr__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__ForExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__0"


    // $ANTLR start "rule__ForExpr__Group__0__Impl"
    // InternalXpath.g:1806:1: rule__ForExpr__Group__0__Impl : ( ( rule__ForExpr__SimpleForClauseAssignment_0 ) ) ;
    public final void rule__ForExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1810:1: ( ( ( rule__ForExpr__SimpleForClauseAssignment_0 ) ) )
            // InternalXpath.g:1811:1: ( ( rule__ForExpr__SimpleForClauseAssignment_0 ) )
            {
            // InternalXpath.g:1811:1: ( ( rule__ForExpr__SimpleForClauseAssignment_0 ) )
            // InternalXpath.g:1812:2: ( rule__ForExpr__SimpleForClauseAssignment_0 )
            {
             before(grammarAccess.getForExprAccess().getSimpleForClauseAssignment_0()); 
            // InternalXpath.g:1813:2: ( rule__ForExpr__SimpleForClauseAssignment_0 )
            // InternalXpath.g:1813:3: rule__ForExpr__SimpleForClauseAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ForExpr__SimpleForClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getForExprAccess().getSimpleForClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__0__Impl"


    // $ANTLR start "rule__ForExpr__Group__1"
    // InternalXpath.g:1821:1: rule__ForExpr__Group__1 : rule__ForExpr__Group__1__Impl rule__ForExpr__Group__2 ;
    public final void rule__ForExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1825:1: ( rule__ForExpr__Group__1__Impl rule__ForExpr__Group__2 )
            // InternalXpath.g:1826:2: rule__ForExpr__Group__1__Impl rule__ForExpr__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__ForExpr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForExpr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__1"


    // $ANTLR start "rule__ForExpr__Group__1__Impl"
    // InternalXpath.g:1833:1: rule__ForExpr__Group__1__Impl : ( 'return' ) ;
    public final void rule__ForExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1837:1: ( ( 'return' ) )
            // InternalXpath.g:1838:1: ( 'return' )
            {
            // InternalXpath.g:1838:1: ( 'return' )
            // InternalXpath.g:1839:2: 'return'
            {
             before(grammarAccess.getForExprAccess().getReturnKeyword_1()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getForExprAccess().getReturnKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__1__Impl"


    // $ANTLR start "rule__ForExpr__Group__2"
    // InternalXpath.g:1848:1: rule__ForExpr__Group__2 : rule__ForExpr__Group__2__Impl ;
    public final void rule__ForExpr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1852:1: ( rule__ForExpr__Group__2__Impl )
            // InternalXpath.g:1853:2: rule__ForExpr__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ForExpr__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__2"


    // $ANTLR start "rule__ForExpr__Group__2__Impl"
    // InternalXpath.g:1859:1: rule__ForExpr__Group__2__Impl : ( ( rule__ForExpr__ExprSingleAssignment_2 ) ) ;
    public final void rule__ForExpr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1863:1: ( ( ( rule__ForExpr__ExprSingleAssignment_2 ) ) )
            // InternalXpath.g:1864:1: ( ( rule__ForExpr__ExprSingleAssignment_2 ) )
            {
            // InternalXpath.g:1864:1: ( ( rule__ForExpr__ExprSingleAssignment_2 ) )
            // InternalXpath.g:1865:2: ( rule__ForExpr__ExprSingleAssignment_2 )
            {
             before(grammarAccess.getForExprAccess().getExprSingleAssignment_2()); 
            // InternalXpath.g:1866:2: ( rule__ForExpr__ExprSingleAssignment_2 )
            // InternalXpath.g:1866:3: rule__ForExpr__ExprSingleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ForExpr__ExprSingleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getForExprAccess().getExprSingleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__Group__2__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__0"
    // InternalXpath.g:1875:1: rule__SimpleForClause__Group__0 : rule__SimpleForClause__Group__0__Impl rule__SimpleForClause__Group__1 ;
    public final void rule__SimpleForClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1879:1: ( rule__SimpleForClause__Group__0__Impl rule__SimpleForClause__Group__1 )
            // InternalXpath.g:1880:2: rule__SimpleForClause__Group__0__Impl rule__SimpleForClause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__SimpleForClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__0"


    // $ANTLR start "rule__SimpleForClause__Group__0__Impl"
    // InternalXpath.g:1887:1: rule__SimpleForClause__Group__0__Impl : ( 'for' ) ;
    public final void rule__SimpleForClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1891:1: ( ( 'for' ) )
            // InternalXpath.g:1892:1: ( 'for' )
            {
            // InternalXpath.g:1892:1: ( 'for' )
            // InternalXpath.g:1893:2: 'for'
            {
             before(grammarAccess.getSimpleForClauseAccess().getForKeyword_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getForKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__0__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__1"
    // InternalXpath.g:1902:1: rule__SimpleForClause__Group__1 : rule__SimpleForClause__Group__1__Impl rule__SimpleForClause__Group__2 ;
    public final void rule__SimpleForClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1906:1: ( rule__SimpleForClause__Group__1__Impl rule__SimpleForClause__Group__2 )
            // InternalXpath.g:1907:2: rule__SimpleForClause__Group__1__Impl rule__SimpleForClause__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__SimpleForClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__1"


    // $ANTLR start "rule__SimpleForClause__Group__1__Impl"
    // InternalXpath.g:1914:1: rule__SimpleForClause__Group__1__Impl : ( '$' ) ;
    public final void rule__SimpleForClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1918:1: ( ( '$' ) )
            // InternalXpath.g:1919:1: ( '$' )
            {
            // InternalXpath.g:1919:1: ( '$' )
            // InternalXpath.g:1920:2: '$'
            {
             before(grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__1__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__2"
    // InternalXpath.g:1929:1: rule__SimpleForClause__Group__2 : rule__SimpleForClause__Group__2__Impl rule__SimpleForClause__Group__3 ;
    public final void rule__SimpleForClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1933:1: ( rule__SimpleForClause__Group__2__Impl rule__SimpleForClause__Group__3 )
            // InternalXpath.g:1934:2: rule__SimpleForClause__Group__2__Impl rule__SimpleForClause__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__SimpleForClause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__2"


    // $ANTLR start "rule__SimpleForClause__Group__2__Impl"
    // InternalXpath.g:1941:1: rule__SimpleForClause__Group__2__Impl : ( ( rule__SimpleForClause__VarnameAssignment_2 ) ) ;
    public final void rule__SimpleForClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1945:1: ( ( ( rule__SimpleForClause__VarnameAssignment_2 ) ) )
            // InternalXpath.g:1946:1: ( ( rule__SimpleForClause__VarnameAssignment_2 ) )
            {
            // InternalXpath.g:1946:1: ( ( rule__SimpleForClause__VarnameAssignment_2 ) )
            // InternalXpath.g:1947:2: ( rule__SimpleForClause__VarnameAssignment_2 )
            {
             before(grammarAccess.getSimpleForClauseAccess().getVarnameAssignment_2()); 
            // InternalXpath.g:1948:2: ( rule__SimpleForClause__VarnameAssignment_2 )
            // InternalXpath.g:1948:3: rule__SimpleForClause__VarnameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__VarnameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSimpleForClauseAccess().getVarnameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__2__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__3"
    // InternalXpath.g:1956:1: rule__SimpleForClause__Group__3 : rule__SimpleForClause__Group__3__Impl rule__SimpleForClause__Group__4 ;
    public final void rule__SimpleForClause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1960:1: ( rule__SimpleForClause__Group__3__Impl rule__SimpleForClause__Group__4 )
            // InternalXpath.g:1961:2: rule__SimpleForClause__Group__3__Impl rule__SimpleForClause__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__SimpleForClause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__3"


    // $ANTLR start "rule__SimpleForClause__Group__3__Impl"
    // InternalXpath.g:1968:1: rule__SimpleForClause__Group__3__Impl : ( 'in' ) ;
    public final void rule__SimpleForClause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1972:1: ( ( 'in' ) )
            // InternalXpath.g:1973:1: ( 'in' )
            {
            // InternalXpath.g:1973:1: ( 'in' )
            // InternalXpath.g:1974:2: 'in'
            {
             before(grammarAccess.getSimpleForClauseAccess().getInKeyword_3()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getInKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__3__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__4"
    // InternalXpath.g:1983:1: rule__SimpleForClause__Group__4 : rule__SimpleForClause__Group__4__Impl rule__SimpleForClause__Group__5 ;
    public final void rule__SimpleForClause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1987:1: ( rule__SimpleForClause__Group__4__Impl rule__SimpleForClause__Group__5 )
            // InternalXpath.g:1988:2: rule__SimpleForClause__Group__4__Impl rule__SimpleForClause__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__SimpleForClause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__4"


    // $ANTLR start "rule__SimpleForClause__Group__4__Impl"
    // InternalXpath.g:1995:1: rule__SimpleForClause__Group__4__Impl : ( ( rule__SimpleForClause__ExprSingleAssignment_4 ) ) ;
    public final void rule__SimpleForClause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:1999:1: ( ( ( rule__SimpleForClause__ExprSingleAssignment_4 ) ) )
            // InternalXpath.g:2000:1: ( ( rule__SimpleForClause__ExprSingleAssignment_4 ) )
            {
            // InternalXpath.g:2000:1: ( ( rule__SimpleForClause__ExprSingleAssignment_4 ) )
            // InternalXpath.g:2001:2: ( rule__SimpleForClause__ExprSingleAssignment_4 )
            {
             before(grammarAccess.getSimpleForClauseAccess().getExprSingleAssignment_4()); 
            // InternalXpath.g:2002:2: ( rule__SimpleForClause__ExprSingleAssignment_4 )
            // InternalXpath.g:2002:3: rule__SimpleForClause__ExprSingleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__ExprSingleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getSimpleForClauseAccess().getExprSingleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__4__Impl"


    // $ANTLR start "rule__SimpleForClause__Group__5"
    // InternalXpath.g:2010:1: rule__SimpleForClause__Group__5 : rule__SimpleForClause__Group__5__Impl ;
    public final void rule__SimpleForClause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2014:1: ( rule__SimpleForClause__Group__5__Impl )
            // InternalXpath.g:2015:2: rule__SimpleForClause__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__5"


    // $ANTLR start "rule__SimpleForClause__Group__5__Impl"
    // InternalXpath.g:2021:1: rule__SimpleForClause__Group__5__Impl : ( ( rule__SimpleForClause__Group_5__0 )* ) ;
    public final void rule__SimpleForClause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2025:1: ( ( ( rule__SimpleForClause__Group_5__0 )* ) )
            // InternalXpath.g:2026:1: ( ( rule__SimpleForClause__Group_5__0 )* )
            {
            // InternalXpath.g:2026:1: ( ( rule__SimpleForClause__Group_5__0 )* )
            // InternalXpath.g:2027:2: ( rule__SimpleForClause__Group_5__0 )*
            {
             before(grammarAccess.getSimpleForClauseAccess().getGroup_5()); 
            // InternalXpath.g:2028:2: ( rule__SimpleForClause__Group_5__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==37) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalXpath.g:2028:3: rule__SimpleForClause__Group_5__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__SimpleForClause__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getSimpleForClauseAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group__5__Impl"


    // $ANTLR start "rule__SimpleForClause__Group_5__0"
    // InternalXpath.g:2037:1: rule__SimpleForClause__Group_5__0 : rule__SimpleForClause__Group_5__0__Impl rule__SimpleForClause__Group_5__1 ;
    public final void rule__SimpleForClause__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2041:1: ( rule__SimpleForClause__Group_5__0__Impl rule__SimpleForClause__Group_5__1 )
            // InternalXpath.g:2042:2: rule__SimpleForClause__Group_5__0__Impl rule__SimpleForClause__Group_5__1
            {
            pushFollow(FOLLOW_12);
            rule__SimpleForClause__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__0"


    // $ANTLR start "rule__SimpleForClause__Group_5__0__Impl"
    // InternalXpath.g:2049:1: rule__SimpleForClause__Group_5__0__Impl : ( ',' ) ;
    public final void rule__SimpleForClause__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2053:1: ( ( ',' ) )
            // InternalXpath.g:2054:1: ( ',' )
            {
            // InternalXpath.g:2054:1: ( ',' )
            // InternalXpath.g:2055:2: ','
            {
             before(grammarAccess.getSimpleForClauseAccess().getCommaKeyword_5_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getCommaKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__0__Impl"


    // $ANTLR start "rule__SimpleForClause__Group_5__1"
    // InternalXpath.g:2064:1: rule__SimpleForClause__Group_5__1 : rule__SimpleForClause__Group_5__1__Impl rule__SimpleForClause__Group_5__2 ;
    public final void rule__SimpleForClause__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2068:1: ( rule__SimpleForClause__Group_5__1__Impl rule__SimpleForClause__Group_5__2 )
            // InternalXpath.g:2069:2: rule__SimpleForClause__Group_5__1__Impl rule__SimpleForClause__Group_5__2
            {
            pushFollow(FOLLOW_8);
            rule__SimpleForClause__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__1"


    // $ANTLR start "rule__SimpleForClause__Group_5__1__Impl"
    // InternalXpath.g:2076:1: rule__SimpleForClause__Group_5__1__Impl : ( '$' ) ;
    public final void rule__SimpleForClause__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2080:1: ( ( '$' ) )
            // InternalXpath.g:2081:1: ( '$' )
            {
            // InternalXpath.g:2081:1: ( '$' )
            // InternalXpath.g:2082:2: '$'
            {
             before(grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_5_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getDollarSignKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__1__Impl"


    // $ANTLR start "rule__SimpleForClause__Group_5__2"
    // InternalXpath.g:2091:1: rule__SimpleForClause__Group_5__2 : rule__SimpleForClause__Group_5__2__Impl rule__SimpleForClause__Group_5__3 ;
    public final void rule__SimpleForClause__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2095:1: ( rule__SimpleForClause__Group_5__2__Impl rule__SimpleForClause__Group_5__3 )
            // InternalXpath.g:2096:2: rule__SimpleForClause__Group_5__2__Impl rule__SimpleForClause__Group_5__3
            {
            pushFollow(FOLLOW_13);
            rule__SimpleForClause__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__2"


    // $ANTLR start "rule__SimpleForClause__Group_5__2__Impl"
    // InternalXpath.g:2103:1: rule__SimpleForClause__Group_5__2__Impl : ( ( rule__SimpleForClause__Varname_listAssignment_5_2 ) ) ;
    public final void rule__SimpleForClause__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2107:1: ( ( ( rule__SimpleForClause__Varname_listAssignment_5_2 ) ) )
            // InternalXpath.g:2108:1: ( ( rule__SimpleForClause__Varname_listAssignment_5_2 ) )
            {
            // InternalXpath.g:2108:1: ( ( rule__SimpleForClause__Varname_listAssignment_5_2 ) )
            // InternalXpath.g:2109:2: ( rule__SimpleForClause__Varname_listAssignment_5_2 )
            {
             before(grammarAccess.getSimpleForClauseAccess().getVarname_listAssignment_5_2()); 
            // InternalXpath.g:2110:2: ( rule__SimpleForClause__Varname_listAssignment_5_2 )
            // InternalXpath.g:2110:3: rule__SimpleForClause__Varname_listAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Varname_listAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getSimpleForClauseAccess().getVarname_listAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__2__Impl"


    // $ANTLR start "rule__SimpleForClause__Group_5__3"
    // InternalXpath.g:2118:1: rule__SimpleForClause__Group_5__3 : rule__SimpleForClause__Group_5__3__Impl rule__SimpleForClause__Group_5__4 ;
    public final void rule__SimpleForClause__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2122:1: ( rule__SimpleForClause__Group_5__3__Impl rule__SimpleForClause__Group_5__4 )
            // InternalXpath.g:2123:2: rule__SimpleForClause__Group_5__3__Impl rule__SimpleForClause__Group_5__4
            {
            pushFollow(FOLLOW_5);
            rule__SimpleForClause__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__3"


    // $ANTLR start "rule__SimpleForClause__Group_5__3__Impl"
    // InternalXpath.g:2130:1: rule__SimpleForClause__Group_5__3__Impl : ( 'in' ) ;
    public final void rule__SimpleForClause__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2134:1: ( ( 'in' ) )
            // InternalXpath.g:2135:1: ( 'in' )
            {
            // InternalXpath.g:2135:1: ( 'in' )
            // InternalXpath.g:2136:2: 'in'
            {
             before(grammarAccess.getSimpleForClauseAccess().getInKeyword_5_3()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getSimpleForClauseAccess().getInKeyword_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__3__Impl"


    // $ANTLR start "rule__SimpleForClause__Group_5__4"
    // InternalXpath.g:2145:1: rule__SimpleForClause__Group_5__4 : rule__SimpleForClause__Group_5__4__Impl ;
    public final void rule__SimpleForClause__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2149:1: ( rule__SimpleForClause__Group_5__4__Impl )
            // InternalXpath.g:2150:2: rule__SimpleForClause__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__4"


    // $ANTLR start "rule__SimpleForClause__Group_5__4__Impl"
    // InternalXpath.g:2156:1: rule__SimpleForClause__Group_5__4__Impl : ( ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 ) ) ;
    public final void rule__SimpleForClause__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2160:1: ( ( ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 ) ) )
            // InternalXpath.g:2161:1: ( ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 ) )
            {
            // InternalXpath.g:2161:1: ( ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 ) )
            // InternalXpath.g:2162:2: ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 )
            {
             before(grammarAccess.getSimpleForClauseAccess().getExprSingle_listAssignment_5_4()); 
            // InternalXpath.g:2163:2: ( rule__SimpleForClause__ExprSingle_listAssignment_5_4 )
            // InternalXpath.g:2163:3: rule__SimpleForClause__ExprSingle_listAssignment_5_4
            {
            pushFollow(FOLLOW_2);
            rule__SimpleForClause__ExprSingle_listAssignment_5_4();

            state._fsp--;


            }

             after(grammarAccess.getSimpleForClauseAccess().getExprSingle_listAssignment_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Group_5__4__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__0"
    // InternalXpath.g:2172:1: rule__QuantifiedExpr__Group__0 : rule__QuantifiedExpr__Group__0__Impl rule__QuantifiedExpr__Group__1 ;
    public final void rule__QuantifiedExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2176:1: ( rule__QuantifiedExpr__Group__0__Impl rule__QuantifiedExpr__Group__1 )
            // InternalXpath.g:2177:2: rule__QuantifiedExpr__Group__0__Impl rule__QuantifiedExpr__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__QuantifiedExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__0"


    // $ANTLR start "rule__QuantifiedExpr__Group__0__Impl"
    // InternalXpath.g:2184:1: rule__QuantifiedExpr__Group__0__Impl : ( ( rule__QuantifiedExpr__Alternatives_0 ) ) ;
    public final void rule__QuantifiedExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2188:1: ( ( ( rule__QuantifiedExpr__Alternatives_0 ) ) )
            // InternalXpath.g:2189:1: ( ( rule__QuantifiedExpr__Alternatives_0 ) )
            {
            // InternalXpath.g:2189:1: ( ( rule__QuantifiedExpr__Alternatives_0 ) )
            // InternalXpath.g:2190:2: ( rule__QuantifiedExpr__Alternatives_0 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getAlternatives_0()); 
            // InternalXpath.g:2191:2: ( rule__QuantifiedExpr__Alternatives_0 )
            // InternalXpath.g:2191:3: rule__QuantifiedExpr__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__0__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__1"
    // InternalXpath.g:2199:1: rule__QuantifiedExpr__Group__1 : rule__QuantifiedExpr__Group__1__Impl rule__QuantifiedExpr__Group__2 ;
    public final void rule__QuantifiedExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2203:1: ( rule__QuantifiedExpr__Group__1__Impl rule__QuantifiedExpr__Group__2 )
            // InternalXpath.g:2204:2: rule__QuantifiedExpr__Group__1__Impl rule__QuantifiedExpr__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__QuantifiedExpr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__1"


    // $ANTLR start "rule__QuantifiedExpr__Group__1__Impl"
    // InternalXpath.g:2211:1: rule__QuantifiedExpr__Group__1__Impl : ( '$' ) ;
    public final void rule__QuantifiedExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2215:1: ( ( '$' ) )
            // InternalXpath.g:2216:1: ( '$' )
            {
            // InternalXpath.g:2216:1: ( '$' )
            // InternalXpath.g:2217:2: '$'
            {
             before(grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__1__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__2"
    // InternalXpath.g:2226:1: rule__QuantifiedExpr__Group__2 : rule__QuantifiedExpr__Group__2__Impl rule__QuantifiedExpr__Group__3 ;
    public final void rule__QuantifiedExpr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2230:1: ( rule__QuantifiedExpr__Group__2__Impl rule__QuantifiedExpr__Group__3 )
            // InternalXpath.g:2231:2: rule__QuantifiedExpr__Group__2__Impl rule__QuantifiedExpr__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__QuantifiedExpr__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__2"


    // $ANTLR start "rule__QuantifiedExpr__Group__2__Impl"
    // InternalXpath.g:2238:1: rule__QuantifiedExpr__Group__2__Impl : ( ( rule__QuantifiedExpr__ElementAssignment_2 ) ) ;
    public final void rule__QuantifiedExpr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2242:1: ( ( ( rule__QuantifiedExpr__ElementAssignment_2 ) ) )
            // InternalXpath.g:2243:1: ( ( rule__QuantifiedExpr__ElementAssignment_2 ) )
            {
            // InternalXpath.g:2243:1: ( ( rule__QuantifiedExpr__ElementAssignment_2 ) )
            // InternalXpath.g:2244:2: ( rule__QuantifiedExpr__ElementAssignment_2 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getElementAssignment_2()); 
            // InternalXpath.g:2245:2: ( rule__QuantifiedExpr__ElementAssignment_2 )
            // InternalXpath.g:2245:3: rule__QuantifiedExpr__ElementAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__ElementAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getElementAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__2__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__3"
    // InternalXpath.g:2253:1: rule__QuantifiedExpr__Group__3 : rule__QuantifiedExpr__Group__3__Impl rule__QuantifiedExpr__Group__4 ;
    public final void rule__QuantifiedExpr__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2257:1: ( rule__QuantifiedExpr__Group__3__Impl rule__QuantifiedExpr__Group__4 )
            // InternalXpath.g:2258:2: rule__QuantifiedExpr__Group__3__Impl rule__QuantifiedExpr__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__QuantifiedExpr__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__3"


    // $ANTLR start "rule__QuantifiedExpr__Group__3__Impl"
    // InternalXpath.g:2265:1: rule__QuantifiedExpr__Group__3__Impl : ( 'in' ) ;
    public final void rule__QuantifiedExpr__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2269:1: ( ( 'in' ) )
            // InternalXpath.g:2270:1: ( 'in' )
            {
            // InternalXpath.g:2270:1: ( 'in' )
            // InternalXpath.g:2271:2: 'in'
            {
             before(grammarAccess.getQuantifiedExprAccess().getInKeyword_3()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getInKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__3__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__4"
    // InternalXpath.g:2280:1: rule__QuantifiedExpr__Group__4 : rule__QuantifiedExpr__Group__4__Impl rule__QuantifiedExpr__Group__5 ;
    public final void rule__QuantifiedExpr__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2284:1: ( rule__QuantifiedExpr__Group__4__Impl rule__QuantifiedExpr__Group__5 )
            // InternalXpath.g:2285:2: rule__QuantifiedExpr__Group__4__Impl rule__QuantifiedExpr__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__QuantifiedExpr__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__4"


    // $ANTLR start "rule__QuantifiedExpr__Group__4__Impl"
    // InternalXpath.g:2292:1: rule__QuantifiedExpr__Group__4__Impl : ( ( rule__QuantifiedExpr__CollectionAssignment_4 ) ) ;
    public final void rule__QuantifiedExpr__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2296:1: ( ( ( rule__QuantifiedExpr__CollectionAssignment_4 ) ) )
            // InternalXpath.g:2297:1: ( ( rule__QuantifiedExpr__CollectionAssignment_4 ) )
            {
            // InternalXpath.g:2297:1: ( ( rule__QuantifiedExpr__CollectionAssignment_4 ) )
            // InternalXpath.g:2298:2: ( rule__QuantifiedExpr__CollectionAssignment_4 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getCollectionAssignment_4()); 
            // InternalXpath.g:2299:2: ( rule__QuantifiedExpr__CollectionAssignment_4 )
            // InternalXpath.g:2299:3: rule__QuantifiedExpr__CollectionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__CollectionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getCollectionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__4__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__5"
    // InternalXpath.g:2307:1: rule__QuantifiedExpr__Group__5 : rule__QuantifiedExpr__Group__5__Impl rule__QuantifiedExpr__Group__6 ;
    public final void rule__QuantifiedExpr__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2311:1: ( rule__QuantifiedExpr__Group__5__Impl rule__QuantifiedExpr__Group__6 )
            // InternalXpath.g:2312:2: rule__QuantifiedExpr__Group__5__Impl rule__QuantifiedExpr__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__QuantifiedExpr__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__5"


    // $ANTLR start "rule__QuantifiedExpr__Group__5__Impl"
    // InternalXpath.g:2319:1: rule__QuantifiedExpr__Group__5__Impl : ( ( rule__QuantifiedExpr__Group_5__0 )* ) ;
    public final void rule__QuantifiedExpr__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2323:1: ( ( ( rule__QuantifiedExpr__Group_5__0 )* ) )
            // InternalXpath.g:2324:1: ( ( rule__QuantifiedExpr__Group_5__0 )* )
            {
            // InternalXpath.g:2324:1: ( ( rule__QuantifiedExpr__Group_5__0 )* )
            // InternalXpath.g:2325:2: ( rule__QuantifiedExpr__Group_5__0 )*
            {
             before(grammarAccess.getQuantifiedExprAccess().getGroup_5()); 
            // InternalXpath.g:2326:2: ( rule__QuantifiedExpr__Group_5__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==37) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalXpath.g:2326:3: rule__QuantifiedExpr__Group_5__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__QuantifiedExpr__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getQuantifiedExprAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__5__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__6"
    // InternalXpath.g:2334:1: rule__QuantifiedExpr__Group__6 : rule__QuantifiedExpr__Group__6__Impl rule__QuantifiedExpr__Group__7 ;
    public final void rule__QuantifiedExpr__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2338:1: ( rule__QuantifiedExpr__Group__6__Impl rule__QuantifiedExpr__Group__7 )
            // InternalXpath.g:2339:2: rule__QuantifiedExpr__Group__6__Impl rule__QuantifiedExpr__Group__7
            {
            pushFollow(FOLLOW_5);
            rule__QuantifiedExpr__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__6"


    // $ANTLR start "rule__QuantifiedExpr__Group__6__Impl"
    // InternalXpath.g:2346:1: rule__QuantifiedExpr__Group__6__Impl : ( 'satisfies' ) ;
    public final void rule__QuantifiedExpr__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2350:1: ( ( 'satisfies' ) )
            // InternalXpath.g:2351:1: ( 'satisfies' )
            {
            // InternalXpath.g:2351:1: ( 'satisfies' )
            // InternalXpath.g:2352:2: 'satisfies'
            {
             before(grammarAccess.getQuantifiedExprAccess().getSatisfiesKeyword_6()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getSatisfiesKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__6__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group__7"
    // InternalXpath.g:2361:1: rule__QuantifiedExpr__Group__7 : rule__QuantifiedExpr__Group__7__Impl ;
    public final void rule__QuantifiedExpr__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2365:1: ( rule__QuantifiedExpr__Group__7__Impl )
            // InternalXpath.g:2366:2: rule__QuantifiedExpr__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__7"


    // $ANTLR start "rule__QuantifiedExpr__Group__7__Impl"
    // InternalXpath.g:2372:1: rule__QuantifiedExpr__Group__7__Impl : ( ( rule__QuantifiedExpr__ConditionAssignment_7 ) ) ;
    public final void rule__QuantifiedExpr__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2376:1: ( ( ( rule__QuantifiedExpr__ConditionAssignment_7 ) ) )
            // InternalXpath.g:2377:1: ( ( rule__QuantifiedExpr__ConditionAssignment_7 ) )
            {
            // InternalXpath.g:2377:1: ( ( rule__QuantifiedExpr__ConditionAssignment_7 ) )
            // InternalXpath.g:2378:2: ( rule__QuantifiedExpr__ConditionAssignment_7 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getConditionAssignment_7()); 
            // InternalXpath.g:2379:2: ( rule__QuantifiedExpr__ConditionAssignment_7 )
            // InternalXpath.g:2379:3: rule__QuantifiedExpr__ConditionAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__ConditionAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getConditionAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group__7__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__0"
    // InternalXpath.g:2388:1: rule__QuantifiedExpr__Group_5__0 : rule__QuantifiedExpr__Group_5__0__Impl rule__QuantifiedExpr__Group_5__1 ;
    public final void rule__QuantifiedExpr__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2392:1: ( rule__QuantifiedExpr__Group_5__0__Impl rule__QuantifiedExpr__Group_5__1 )
            // InternalXpath.g:2393:2: rule__QuantifiedExpr__Group_5__0__Impl rule__QuantifiedExpr__Group_5__1
            {
            pushFollow(FOLLOW_12);
            rule__QuantifiedExpr__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__0"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__0__Impl"
    // InternalXpath.g:2400:1: rule__QuantifiedExpr__Group_5__0__Impl : ( ',' ) ;
    public final void rule__QuantifiedExpr__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2404:1: ( ( ',' ) )
            // InternalXpath.g:2405:1: ( ',' )
            {
            // InternalXpath.g:2405:1: ( ',' )
            // InternalXpath.g:2406:2: ','
            {
             before(grammarAccess.getQuantifiedExprAccess().getCommaKeyword_5_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getCommaKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__0__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__1"
    // InternalXpath.g:2415:1: rule__QuantifiedExpr__Group_5__1 : rule__QuantifiedExpr__Group_5__1__Impl rule__QuantifiedExpr__Group_5__2 ;
    public final void rule__QuantifiedExpr__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2419:1: ( rule__QuantifiedExpr__Group_5__1__Impl rule__QuantifiedExpr__Group_5__2 )
            // InternalXpath.g:2420:2: rule__QuantifiedExpr__Group_5__1__Impl rule__QuantifiedExpr__Group_5__2
            {
            pushFollow(FOLLOW_8);
            rule__QuantifiedExpr__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__1"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__1__Impl"
    // InternalXpath.g:2427:1: rule__QuantifiedExpr__Group_5__1__Impl : ( '$' ) ;
    public final void rule__QuantifiedExpr__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2431:1: ( ( '$' ) )
            // InternalXpath.g:2432:1: ( '$' )
            {
            // InternalXpath.g:2432:1: ( '$' )
            // InternalXpath.g:2433:2: '$'
            {
             before(grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_5_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getDollarSignKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__1__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__2"
    // InternalXpath.g:2442:1: rule__QuantifiedExpr__Group_5__2 : rule__QuantifiedExpr__Group_5__2__Impl rule__QuantifiedExpr__Group_5__3 ;
    public final void rule__QuantifiedExpr__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2446:1: ( rule__QuantifiedExpr__Group_5__2__Impl rule__QuantifiedExpr__Group_5__3 )
            // InternalXpath.g:2447:2: rule__QuantifiedExpr__Group_5__2__Impl rule__QuantifiedExpr__Group_5__3
            {
            pushFollow(FOLLOW_13);
            rule__QuantifiedExpr__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__2"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__2__Impl"
    // InternalXpath.g:2454:1: rule__QuantifiedExpr__Group_5__2__Impl : ( ( rule__QuantifiedExpr__Element_listAssignment_5_2 ) ) ;
    public final void rule__QuantifiedExpr__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2458:1: ( ( ( rule__QuantifiedExpr__Element_listAssignment_5_2 ) ) )
            // InternalXpath.g:2459:1: ( ( rule__QuantifiedExpr__Element_listAssignment_5_2 ) )
            {
            // InternalXpath.g:2459:1: ( ( rule__QuantifiedExpr__Element_listAssignment_5_2 ) )
            // InternalXpath.g:2460:2: ( rule__QuantifiedExpr__Element_listAssignment_5_2 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getElement_listAssignment_5_2()); 
            // InternalXpath.g:2461:2: ( rule__QuantifiedExpr__Element_listAssignment_5_2 )
            // InternalXpath.g:2461:3: rule__QuantifiedExpr__Element_listAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Element_listAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getElement_listAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__2__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__3"
    // InternalXpath.g:2469:1: rule__QuantifiedExpr__Group_5__3 : rule__QuantifiedExpr__Group_5__3__Impl rule__QuantifiedExpr__Group_5__4 ;
    public final void rule__QuantifiedExpr__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2473:1: ( rule__QuantifiedExpr__Group_5__3__Impl rule__QuantifiedExpr__Group_5__4 )
            // InternalXpath.g:2474:2: rule__QuantifiedExpr__Group_5__3__Impl rule__QuantifiedExpr__Group_5__4
            {
            pushFollow(FOLLOW_5);
            rule__QuantifiedExpr__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__3"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__3__Impl"
    // InternalXpath.g:2481:1: rule__QuantifiedExpr__Group_5__3__Impl : ( 'in' ) ;
    public final void rule__QuantifiedExpr__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2485:1: ( ( 'in' ) )
            // InternalXpath.g:2486:1: ( 'in' )
            {
            // InternalXpath.g:2486:1: ( 'in' )
            // InternalXpath.g:2487:2: 'in'
            {
             before(grammarAccess.getQuantifiedExprAccess().getInKeyword_5_3()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getInKeyword_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__3__Impl"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__4"
    // InternalXpath.g:2496:1: rule__QuantifiedExpr__Group_5__4 : rule__QuantifiedExpr__Group_5__4__Impl ;
    public final void rule__QuantifiedExpr__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2500:1: ( rule__QuantifiedExpr__Group_5__4__Impl )
            // InternalXpath.g:2501:2: rule__QuantifiedExpr__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__4"


    // $ANTLR start "rule__QuantifiedExpr__Group_5__4__Impl"
    // InternalXpath.g:2507:1: rule__QuantifiedExpr__Group_5__4__Impl : ( ( rule__QuantifiedExpr__Collection_listAssignment_5_4 ) ) ;
    public final void rule__QuantifiedExpr__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2511:1: ( ( ( rule__QuantifiedExpr__Collection_listAssignment_5_4 ) ) )
            // InternalXpath.g:2512:1: ( ( rule__QuantifiedExpr__Collection_listAssignment_5_4 ) )
            {
            // InternalXpath.g:2512:1: ( ( rule__QuantifiedExpr__Collection_listAssignment_5_4 ) )
            // InternalXpath.g:2513:2: ( rule__QuantifiedExpr__Collection_listAssignment_5_4 )
            {
             before(grammarAccess.getQuantifiedExprAccess().getCollection_listAssignment_5_4()); 
            // InternalXpath.g:2514:2: ( rule__QuantifiedExpr__Collection_listAssignment_5_4 )
            // InternalXpath.g:2514:3: rule__QuantifiedExpr__Collection_listAssignment_5_4
            {
            pushFollow(FOLLOW_2);
            rule__QuantifiedExpr__Collection_listAssignment_5_4();

            state._fsp--;


            }

             after(grammarAccess.getQuantifiedExprAccess().getCollection_listAssignment_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Group_5__4__Impl"


    // $ANTLR start "rule__IfExpr__Group__0"
    // InternalXpath.g:2523:1: rule__IfExpr__Group__0 : rule__IfExpr__Group__0__Impl rule__IfExpr__Group__1 ;
    public final void rule__IfExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2527:1: ( rule__IfExpr__Group__0__Impl rule__IfExpr__Group__1 )
            // InternalXpath.g:2528:2: rule__IfExpr__Group__0__Impl rule__IfExpr__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__IfExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__0"


    // $ANTLR start "rule__IfExpr__Group__0__Impl"
    // InternalXpath.g:2535:1: rule__IfExpr__Group__0__Impl : ( 'if' ) ;
    public final void rule__IfExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2539:1: ( ( 'if' ) )
            // InternalXpath.g:2540:1: ( 'if' )
            {
            // InternalXpath.g:2540:1: ( 'if' )
            // InternalXpath.g:2541:2: 'if'
            {
             before(grammarAccess.getIfExprAccess().getIfKeyword_0()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getIfExprAccess().getIfKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__0__Impl"


    // $ANTLR start "rule__IfExpr__Group__1"
    // InternalXpath.g:2550:1: rule__IfExpr__Group__1 : rule__IfExpr__Group__1__Impl rule__IfExpr__Group__2 ;
    public final void rule__IfExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2554:1: ( rule__IfExpr__Group__1__Impl rule__IfExpr__Group__2 )
            // InternalXpath.g:2555:2: rule__IfExpr__Group__1__Impl rule__IfExpr__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__IfExpr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__1"


    // $ANTLR start "rule__IfExpr__Group__1__Impl"
    // InternalXpath.g:2562:1: rule__IfExpr__Group__1__Impl : ( '(' ) ;
    public final void rule__IfExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2566:1: ( ( '(' ) )
            // InternalXpath.g:2567:1: ( '(' )
            {
            // InternalXpath.g:2567:1: ( '(' )
            // InternalXpath.g:2568:2: '('
            {
             before(grammarAccess.getIfExprAccess().getLeftParenthesisKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getIfExprAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__1__Impl"


    // $ANTLR start "rule__IfExpr__Group__2"
    // InternalXpath.g:2577:1: rule__IfExpr__Group__2 : rule__IfExpr__Group__2__Impl rule__IfExpr__Group__3 ;
    public final void rule__IfExpr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2581:1: ( rule__IfExpr__Group__2__Impl rule__IfExpr__Group__3 )
            // InternalXpath.g:2582:2: rule__IfExpr__Group__2__Impl rule__IfExpr__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__IfExpr__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__2"


    // $ANTLR start "rule__IfExpr__Group__2__Impl"
    // InternalXpath.g:2589:1: rule__IfExpr__Group__2__Impl : ( ( rule__IfExpr__ConditionExprAssignment_2 ) ) ;
    public final void rule__IfExpr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2593:1: ( ( ( rule__IfExpr__ConditionExprAssignment_2 ) ) )
            // InternalXpath.g:2594:1: ( ( rule__IfExpr__ConditionExprAssignment_2 ) )
            {
            // InternalXpath.g:2594:1: ( ( rule__IfExpr__ConditionExprAssignment_2 ) )
            // InternalXpath.g:2595:2: ( rule__IfExpr__ConditionExprAssignment_2 )
            {
             before(grammarAccess.getIfExprAccess().getConditionExprAssignment_2()); 
            // InternalXpath.g:2596:2: ( rule__IfExpr__ConditionExprAssignment_2 )
            // InternalXpath.g:2596:3: rule__IfExpr__ConditionExprAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__IfExpr__ConditionExprAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIfExprAccess().getConditionExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__2__Impl"


    // $ANTLR start "rule__IfExpr__Group__3"
    // InternalXpath.g:2604:1: rule__IfExpr__Group__3 : rule__IfExpr__Group__3__Impl rule__IfExpr__Group__4 ;
    public final void rule__IfExpr__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2608:1: ( rule__IfExpr__Group__3__Impl rule__IfExpr__Group__4 )
            // InternalXpath.g:2609:2: rule__IfExpr__Group__3__Impl rule__IfExpr__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__IfExpr__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__3"


    // $ANTLR start "rule__IfExpr__Group__3__Impl"
    // InternalXpath.g:2616:1: rule__IfExpr__Group__3__Impl : ( ')' ) ;
    public final void rule__IfExpr__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2620:1: ( ( ')' ) )
            // InternalXpath.g:2621:1: ( ')' )
            {
            // InternalXpath.g:2621:1: ( ')' )
            // InternalXpath.g:2622:2: ')'
            {
             before(grammarAccess.getIfExprAccess().getRightParenthesisKeyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getIfExprAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__3__Impl"


    // $ANTLR start "rule__IfExpr__Group__4"
    // InternalXpath.g:2631:1: rule__IfExpr__Group__4 : rule__IfExpr__Group__4__Impl ;
    public final void rule__IfExpr__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2635:1: ( rule__IfExpr__Group__4__Impl )
            // InternalXpath.g:2636:2: rule__IfExpr__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IfExpr__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__4"


    // $ANTLR start "rule__IfExpr__Group__4__Impl"
    // InternalXpath.g:2642:1: rule__IfExpr__Group__4__Impl : ( ( rule__IfExpr__ThenElseAssignment_4 ) ) ;
    public final void rule__IfExpr__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2646:1: ( ( ( rule__IfExpr__ThenElseAssignment_4 ) ) )
            // InternalXpath.g:2647:1: ( ( rule__IfExpr__ThenElseAssignment_4 ) )
            {
            // InternalXpath.g:2647:1: ( ( rule__IfExpr__ThenElseAssignment_4 ) )
            // InternalXpath.g:2648:2: ( rule__IfExpr__ThenElseAssignment_4 )
            {
             before(grammarAccess.getIfExprAccess().getThenElseAssignment_4()); 
            // InternalXpath.g:2649:2: ( rule__IfExpr__ThenElseAssignment_4 )
            // InternalXpath.g:2649:3: rule__IfExpr__ThenElseAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__IfExpr__ThenElseAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getIfExprAccess().getThenElseAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__Group__4__Impl"


    // $ANTLR start "rule__OrExpr__Group_0__0"
    // InternalXpath.g:2658:1: rule__OrExpr__Group_0__0 : rule__OrExpr__Group_0__0__Impl rule__OrExpr__Group_0__1 ;
    public final void rule__OrExpr__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2662:1: ( rule__OrExpr__Group_0__0__Impl rule__OrExpr__Group_0__1 )
            // InternalXpath.g:2663:2: rule__OrExpr__Group_0__0__Impl rule__OrExpr__Group_0__1
            {
            pushFollow(FOLLOW_5);
            rule__OrExpr__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__0"


    // $ANTLR start "rule__OrExpr__Group_0__0__Impl"
    // InternalXpath.g:2670:1: rule__OrExpr__Group_0__0__Impl : ( '(' ) ;
    public final void rule__OrExpr__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2674:1: ( ( '(' ) )
            // InternalXpath.g:2675:1: ( '(' )
            {
            // InternalXpath.g:2675:1: ( '(' )
            // InternalXpath.g:2676:2: '('
            {
             before(grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__0__Impl"


    // $ANTLR start "rule__OrExpr__Group_0__1"
    // InternalXpath.g:2685:1: rule__OrExpr__Group_0__1 : rule__OrExpr__Group_0__1__Impl rule__OrExpr__Group_0__2 ;
    public final void rule__OrExpr__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2689:1: ( rule__OrExpr__Group_0__1__Impl rule__OrExpr__Group_0__2 )
            // InternalXpath.g:2690:2: rule__OrExpr__Group_0__1__Impl rule__OrExpr__Group_0__2
            {
            pushFollow(FOLLOW_16);
            rule__OrExpr__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__1"


    // $ANTLR start "rule__OrExpr__Group_0__1__Impl"
    // InternalXpath.g:2697:1: rule__OrExpr__Group_0__1__Impl : ( ( rule__OrExpr__GroupedConditionAssignment_0_1 ) ) ;
    public final void rule__OrExpr__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2701:1: ( ( ( rule__OrExpr__GroupedConditionAssignment_0_1 ) ) )
            // InternalXpath.g:2702:1: ( ( rule__OrExpr__GroupedConditionAssignment_0_1 ) )
            {
            // InternalXpath.g:2702:1: ( ( rule__OrExpr__GroupedConditionAssignment_0_1 ) )
            // InternalXpath.g:2703:2: ( rule__OrExpr__GroupedConditionAssignment_0_1 )
            {
             before(grammarAccess.getOrExprAccess().getGroupedConditionAssignment_0_1()); 
            // InternalXpath.g:2704:2: ( rule__OrExpr__GroupedConditionAssignment_0_1 )
            // InternalXpath.g:2704:3: rule__OrExpr__GroupedConditionAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__GroupedConditionAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getGroupedConditionAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__1__Impl"


    // $ANTLR start "rule__OrExpr__Group_0__2"
    // InternalXpath.g:2712:1: rule__OrExpr__Group_0__2 : rule__OrExpr__Group_0__2__Impl rule__OrExpr__Group_0__3 ;
    public final void rule__OrExpr__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2716:1: ( rule__OrExpr__Group_0__2__Impl rule__OrExpr__Group_0__3 )
            // InternalXpath.g:2717:2: rule__OrExpr__Group_0__2__Impl rule__OrExpr__Group_0__3
            {
            pushFollow(FOLLOW_18);
            rule__OrExpr__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__2"


    // $ANTLR start "rule__OrExpr__Group_0__2__Impl"
    // InternalXpath.g:2724:1: rule__OrExpr__Group_0__2__Impl : ( ')' ) ;
    public final void rule__OrExpr__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2728:1: ( ( ')' ) )
            // InternalXpath.g:2729:1: ( ')' )
            {
            // InternalXpath.g:2729:1: ( ')' )
            // InternalXpath.g:2730:2: ')'
            {
             before(grammarAccess.getOrExprAccess().getRightParenthesisKeyword_0_2()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__2__Impl"


    // $ANTLR start "rule__OrExpr__Group_0__3"
    // InternalXpath.g:2739:1: rule__OrExpr__Group_0__3 : rule__OrExpr__Group_0__3__Impl ;
    public final void rule__OrExpr__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2743:1: ( rule__OrExpr__Group_0__3__Impl )
            // InternalXpath.g:2744:2: rule__OrExpr__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__3"


    // $ANTLR start "rule__OrExpr__Group_0__3__Impl"
    // InternalXpath.g:2750:1: rule__OrExpr__Group_0__3__Impl : ( ( rule__OrExpr__Group_0_3__0 )? ) ;
    public final void rule__OrExpr__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2754:1: ( ( ( rule__OrExpr__Group_0_3__0 )? ) )
            // InternalXpath.g:2755:1: ( ( rule__OrExpr__Group_0_3__0 )? )
            {
            // InternalXpath.g:2755:1: ( ( rule__OrExpr__Group_0_3__0 )? )
            // InternalXpath.g:2756:2: ( rule__OrExpr__Group_0_3__0 )?
            {
             before(grammarAccess.getOrExprAccess().getGroup_0_3()); 
            // InternalXpath.g:2757:2: ( rule__OrExpr__Group_0_3__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=46 && LA24_0<=47)) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalXpath.g:2757:3: rule__OrExpr__Group_0_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OrExpr__Group_0_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOrExprAccess().getGroup_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0__3__Impl"


    // $ANTLR start "rule__OrExpr__Group_0_3__0"
    // InternalXpath.g:2766:1: rule__OrExpr__Group_0_3__0 : rule__OrExpr__Group_0_3__0__Impl rule__OrExpr__Group_0_3__1 ;
    public final void rule__OrExpr__Group_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2770:1: ( rule__OrExpr__Group_0_3__0__Impl rule__OrExpr__Group_0_3__1 )
            // InternalXpath.g:2771:2: rule__OrExpr__Group_0_3__0__Impl rule__OrExpr__Group_0_3__1
            {
            pushFollow(FOLLOW_5);
            rule__OrExpr__Group_0_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0_3__0"


    // $ANTLR start "rule__OrExpr__Group_0_3__0__Impl"
    // InternalXpath.g:2778:1: rule__OrExpr__Group_0_3__0__Impl : ( ( rule__OrExpr__Alternatives_0_3_0 ) ) ;
    public final void rule__OrExpr__Group_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2782:1: ( ( ( rule__OrExpr__Alternatives_0_3_0 ) ) )
            // InternalXpath.g:2783:1: ( ( rule__OrExpr__Alternatives_0_3_0 ) )
            {
            // InternalXpath.g:2783:1: ( ( rule__OrExpr__Alternatives_0_3_0 ) )
            // InternalXpath.g:2784:2: ( rule__OrExpr__Alternatives_0_3_0 )
            {
             before(grammarAccess.getOrExprAccess().getAlternatives_0_3_0()); 
            // InternalXpath.g:2785:2: ( rule__OrExpr__Alternatives_0_3_0 )
            // InternalXpath.g:2785:3: rule__OrExpr__Alternatives_0_3_0
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Alternatives_0_3_0();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getAlternatives_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0_3__0__Impl"


    // $ANTLR start "rule__OrExpr__Group_0_3__1"
    // InternalXpath.g:2793:1: rule__OrExpr__Group_0_3__1 : rule__OrExpr__Group_0_3__1__Impl ;
    public final void rule__OrExpr__Group_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2797:1: ( rule__OrExpr__Group_0_3__1__Impl )
            // InternalXpath.g:2798:2: rule__OrExpr__Group_0_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_0_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0_3__1"


    // $ANTLR start "rule__OrExpr__Group_0_3__1__Impl"
    // InternalXpath.g:2804:1: rule__OrExpr__Group_0_3__1__Impl : ( ( rule__OrExpr__OrExprAssignment_0_3_1 ) ) ;
    public final void rule__OrExpr__Group_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2808:1: ( ( ( rule__OrExpr__OrExprAssignment_0_3_1 ) ) )
            // InternalXpath.g:2809:1: ( ( rule__OrExpr__OrExprAssignment_0_3_1 ) )
            {
            // InternalXpath.g:2809:1: ( ( rule__OrExpr__OrExprAssignment_0_3_1 ) )
            // InternalXpath.g:2810:2: ( rule__OrExpr__OrExprAssignment_0_3_1 )
            {
             before(grammarAccess.getOrExprAccess().getOrExprAssignment_0_3_1()); 
            // InternalXpath.g:2811:2: ( rule__OrExpr__OrExprAssignment_0_3_1 )
            // InternalXpath.g:2811:3: rule__OrExpr__OrExprAssignment_0_3_1
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__OrExprAssignment_0_3_1();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getOrExprAssignment_0_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_0_3__1__Impl"


    // $ANTLR start "rule__OrExpr__Group_1__0"
    // InternalXpath.g:2820:1: rule__OrExpr__Group_1__0 : rule__OrExpr__Group_1__0__Impl rule__OrExpr__Group_1__1 ;
    public final void rule__OrExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2824:1: ( rule__OrExpr__Group_1__0__Impl rule__OrExpr__Group_1__1 )
            // InternalXpath.g:2825:2: rule__OrExpr__Group_1__0__Impl rule__OrExpr__Group_1__1
            {
            pushFollow(FOLLOW_18);
            rule__OrExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__0"


    // $ANTLR start "rule__OrExpr__Group_1__0__Impl"
    // InternalXpath.g:2832:1: rule__OrExpr__Group_1__0__Impl : ( ( rule__OrExpr__AndExprAssignment_1_0 ) ) ;
    public final void rule__OrExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2836:1: ( ( ( rule__OrExpr__AndExprAssignment_1_0 ) ) )
            // InternalXpath.g:2837:1: ( ( rule__OrExpr__AndExprAssignment_1_0 ) )
            {
            // InternalXpath.g:2837:1: ( ( rule__OrExpr__AndExprAssignment_1_0 ) )
            // InternalXpath.g:2838:2: ( rule__OrExpr__AndExprAssignment_1_0 )
            {
             before(grammarAccess.getOrExprAccess().getAndExprAssignment_1_0()); 
            // InternalXpath.g:2839:2: ( rule__OrExpr__AndExprAssignment_1_0 )
            // InternalXpath.g:2839:3: rule__OrExpr__AndExprAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__AndExprAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getAndExprAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__0__Impl"


    // $ANTLR start "rule__OrExpr__Group_1__1"
    // InternalXpath.g:2847:1: rule__OrExpr__Group_1__1 : rule__OrExpr__Group_1__1__Impl rule__OrExpr__Group_1__2 ;
    public final void rule__OrExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2851:1: ( rule__OrExpr__Group_1__1__Impl rule__OrExpr__Group_1__2 )
            // InternalXpath.g:2852:2: rule__OrExpr__Group_1__1__Impl rule__OrExpr__Group_1__2
            {
            pushFollow(FOLLOW_15);
            rule__OrExpr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__1"


    // $ANTLR start "rule__OrExpr__Group_1__1__Impl"
    // InternalXpath.g:2859:1: rule__OrExpr__Group_1__1__Impl : ( ( rule__OrExpr__Alternatives_1_1 ) ) ;
    public final void rule__OrExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2863:1: ( ( ( rule__OrExpr__Alternatives_1_1 ) ) )
            // InternalXpath.g:2864:1: ( ( rule__OrExpr__Alternatives_1_1 ) )
            {
            // InternalXpath.g:2864:1: ( ( rule__OrExpr__Alternatives_1_1 ) )
            // InternalXpath.g:2865:2: ( rule__OrExpr__Alternatives_1_1 )
            {
             before(grammarAccess.getOrExprAccess().getAlternatives_1_1()); 
            // InternalXpath.g:2866:2: ( rule__OrExpr__Alternatives_1_1 )
            // InternalXpath.g:2866:3: rule__OrExpr__Alternatives_1_1
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__1__Impl"


    // $ANTLR start "rule__OrExpr__Group_1__2"
    // InternalXpath.g:2874:1: rule__OrExpr__Group_1__2 : rule__OrExpr__Group_1__2__Impl rule__OrExpr__Group_1__3 ;
    public final void rule__OrExpr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2878:1: ( rule__OrExpr__Group_1__2__Impl rule__OrExpr__Group_1__3 )
            // InternalXpath.g:2879:2: rule__OrExpr__Group_1__2__Impl rule__OrExpr__Group_1__3
            {
            pushFollow(FOLLOW_5);
            rule__OrExpr__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__2"


    // $ANTLR start "rule__OrExpr__Group_1__2__Impl"
    // InternalXpath.g:2886:1: rule__OrExpr__Group_1__2__Impl : ( '(' ) ;
    public final void rule__OrExpr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2890:1: ( ( '(' ) )
            // InternalXpath.g:2891:1: ( '(' )
            {
            // InternalXpath.g:2891:1: ( '(' )
            // InternalXpath.g:2892:2: '('
            {
             before(grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_1_2()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getLeftParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__2__Impl"


    // $ANTLR start "rule__OrExpr__Group_1__3"
    // InternalXpath.g:2901:1: rule__OrExpr__Group_1__3 : rule__OrExpr__Group_1__3__Impl rule__OrExpr__Group_1__4 ;
    public final void rule__OrExpr__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2905:1: ( rule__OrExpr__Group_1__3__Impl rule__OrExpr__Group_1__4 )
            // InternalXpath.g:2906:2: rule__OrExpr__Group_1__3__Impl rule__OrExpr__Group_1__4
            {
            pushFollow(FOLLOW_16);
            rule__OrExpr__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__3"


    // $ANTLR start "rule__OrExpr__Group_1__3__Impl"
    // InternalXpath.g:2913:1: rule__OrExpr__Group_1__3__Impl : ( ( rule__OrExpr__RightGroupedConditionAssignment_1_3 ) ) ;
    public final void rule__OrExpr__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2917:1: ( ( ( rule__OrExpr__RightGroupedConditionAssignment_1_3 ) ) )
            // InternalXpath.g:2918:1: ( ( rule__OrExpr__RightGroupedConditionAssignment_1_3 ) )
            {
            // InternalXpath.g:2918:1: ( ( rule__OrExpr__RightGroupedConditionAssignment_1_3 ) )
            // InternalXpath.g:2919:2: ( rule__OrExpr__RightGroupedConditionAssignment_1_3 )
            {
             before(grammarAccess.getOrExprAccess().getRightGroupedConditionAssignment_1_3()); 
            // InternalXpath.g:2920:2: ( rule__OrExpr__RightGroupedConditionAssignment_1_3 )
            // InternalXpath.g:2920:3: rule__OrExpr__RightGroupedConditionAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__RightGroupedConditionAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getRightGroupedConditionAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__3__Impl"


    // $ANTLR start "rule__OrExpr__Group_1__4"
    // InternalXpath.g:2928:1: rule__OrExpr__Group_1__4 : rule__OrExpr__Group_1__4__Impl ;
    public final void rule__OrExpr__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2932:1: ( rule__OrExpr__Group_1__4__Impl )
            // InternalXpath.g:2933:2: rule__OrExpr__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__4"


    // $ANTLR start "rule__OrExpr__Group_1__4__Impl"
    // InternalXpath.g:2939:1: rule__OrExpr__Group_1__4__Impl : ( ')' ) ;
    public final void rule__OrExpr__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2943:1: ( ( ')' ) )
            // InternalXpath.g:2944:1: ( ')' )
            {
            // InternalXpath.g:2944:1: ( ')' )
            // InternalXpath.g:2945:2: ')'
            {
             before(grammarAccess.getOrExprAccess().getRightParenthesisKeyword_1_4()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getRightParenthesisKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_1__4__Impl"


    // $ANTLR start "rule__OrExpr__Group_2__0"
    // InternalXpath.g:2955:1: rule__OrExpr__Group_2__0 : rule__OrExpr__Group_2__0__Impl rule__OrExpr__Group_2__1 ;
    public final void rule__OrExpr__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2959:1: ( rule__OrExpr__Group_2__0__Impl rule__OrExpr__Group_2__1 )
            // InternalXpath.g:2960:2: rule__OrExpr__Group_2__0__Impl rule__OrExpr__Group_2__1
            {
            pushFollow(FOLLOW_19);
            rule__OrExpr__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2__0"


    // $ANTLR start "rule__OrExpr__Group_2__0__Impl"
    // InternalXpath.g:2967:1: rule__OrExpr__Group_2__0__Impl : ( ( rule__OrExpr__AndExprAssignment_2_0 ) ) ;
    public final void rule__OrExpr__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2971:1: ( ( ( rule__OrExpr__AndExprAssignment_2_0 ) ) )
            // InternalXpath.g:2972:1: ( ( rule__OrExpr__AndExprAssignment_2_0 ) )
            {
            // InternalXpath.g:2972:1: ( ( rule__OrExpr__AndExprAssignment_2_0 ) )
            // InternalXpath.g:2973:2: ( rule__OrExpr__AndExprAssignment_2_0 )
            {
             before(grammarAccess.getOrExprAccess().getAndExprAssignment_2_0()); 
            // InternalXpath.g:2974:2: ( rule__OrExpr__AndExprAssignment_2_0 )
            // InternalXpath.g:2974:3: rule__OrExpr__AndExprAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__AndExprAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getAndExprAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2__0__Impl"


    // $ANTLR start "rule__OrExpr__Group_2__1"
    // InternalXpath.g:2982:1: rule__OrExpr__Group_2__1 : rule__OrExpr__Group_2__1__Impl ;
    public final void rule__OrExpr__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2986:1: ( rule__OrExpr__Group_2__1__Impl )
            // InternalXpath.g:2987:2: rule__OrExpr__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2__1"


    // $ANTLR start "rule__OrExpr__Group_2__1__Impl"
    // InternalXpath.g:2993:1: rule__OrExpr__Group_2__1__Impl : ( ( rule__OrExpr__Group_2_1__0 )* ) ;
    public final void rule__OrExpr__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:2997:1: ( ( ( rule__OrExpr__Group_2_1__0 )* ) )
            // InternalXpath.g:2998:1: ( ( rule__OrExpr__Group_2_1__0 )* )
            {
            // InternalXpath.g:2998:1: ( ( rule__OrExpr__Group_2_1__0 )* )
            // InternalXpath.g:2999:2: ( rule__OrExpr__Group_2_1__0 )*
            {
             before(grammarAccess.getOrExprAccess().getGroup_2_1()); 
            // InternalXpath.g:3000:2: ( rule__OrExpr__Group_2_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==46) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalXpath.g:3000:3: rule__OrExpr__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__OrExpr__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getOrExprAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2__1__Impl"


    // $ANTLR start "rule__OrExpr__Group_2_1__0"
    // InternalXpath.g:3009:1: rule__OrExpr__Group_2_1__0 : rule__OrExpr__Group_2_1__0__Impl rule__OrExpr__Group_2_1__1 ;
    public final void rule__OrExpr__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3013:1: ( rule__OrExpr__Group_2_1__0__Impl rule__OrExpr__Group_2_1__1 )
            // InternalXpath.g:3014:2: rule__OrExpr__Group_2_1__0__Impl rule__OrExpr__Group_2_1__1
            {
            pushFollow(FOLLOW_21);
            rule__OrExpr__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2_1__0"


    // $ANTLR start "rule__OrExpr__Group_2_1__0__Impl"
    // InternalXpath.g:3021:1: rule__OrExpr__Group_2_1__0__Impl : ( 'or' ) ;
    public final void rule__OrExpr__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3025:1: ( ( 'or' ) )
            // InternalXpath.g:3026:1: ( 'or' )
            {
            // InternalXpath.g:3026:1: ( 'or' )
            // InternalXpath.g:3027:2: 'or'
            {
             before(grammarAccess.getOrExprAccess().getOrKeyword_2_1_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getOrKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2_1__0__Impl"


    // $ANTLR start "rule__OrExpr__Group_2_1__1"
    // InternalXpath.g:3036:1: rule__OrExpr__Group_2_1__1 : rule__OrExpr__Group_2_1__1__Impl ;
    public final void rule__OrExpr__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3040:1: ( rule__OrExpr__Group_2_1__1__Impl )
            // InternalXpath.g:3041:2: rule__OrExpr__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2_1__1"


    // $ANTLR start "rule__OrExpr__Group_2_1__1__Impl"
    // InternalXpath.g:3047:1: rule__OrExpr__Group_2_1__1__Impl : ( ( rule__OrExpr__OrExpr_listAssignment_2_1_1 ) ) ;
    public final void rule__OrExpr__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3051:1: ( ( ( rule__OrExpr__OrExpr_listAssignment_2_1_1 ) ) )
            // InternalXpath.g:3052:1: ( ( rule__OrExpr__OrExpr_listAssignment_2_1_1 ) )
            {
            // InternalXpath.g:3052:1: ( ( rule__OrExpr__OrExpr_listAssignment_2_1_1 ) )
            // InternalXpath.g:3053:2: ( rule__OrExpr__OrExpr_listAssignment_2_1_1 )
            {
             before(grammarAccess.getOrExprAccess().getOrExpr_listAssignment_2_1_1()); 
            // InternalXpath.g:3054:2: ( rule__OrExpr__OrExpr_listAssignment_2_1_1 )
            // InternalXpath.g:3054:3: rule__OrExpr__OrExpr_listAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__OrExpr__OrExpr_listAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrExprAccess().getOrExpr_listAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__Group_2_1__1__Impl"


    // $ANTLR start "rule__AndExpr__Group__0"
    // InternalXpath.g:3063:1: rule__AndExpr__Group__0 : rule__AndExpr__Group__0__Impl rule__AndExpr__Group__1 ;
    public final void rule__AndExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3067:1: ( rule__AndExpr__Group__0__Impl rule__AndExpr__Group__1 )
            // InternalXpath.g:3068:2: rule__AndExpr__Group__0__Impl rule__AndExpr__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__AndExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group__0"


    // $ANTLR start "rule__AndExpr__Group__0__Impl"
    // InternalXpath.g:3075:1: rule__AndExpr__Group__0__Impl : ( ( rule__AndExpr__ComparisonExprAssignment_0 ) ) ;
    public final void rule__AndExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3079:1: ( ( ( rule__AndExpr__ComparisonExprAssignment_0 ) ) )
            // InternalXpath.g:3080:1: ( ( rule__AndExpr__ComparisonExprAssignment_0 ) )
            {
            // InternalXpath.g:3080:1: ( ( rule__AndExpr__ComparisonExprAssignment_0 ) )
            // InternalXpath.g:3081:2: ( rule__AndExpr__ComparisonExprAssignment_0 )
            {
             before(grammarAccess.getAndExprAccess().getComparisonExprAssignment_0()); 
            // InternalXpath.g:3082:2: ( rule__AndExpr__ComparisonExprAssignment_0 )
            // InternalXpath.g:3082:3: rule__AndExpr__ComparisonExprAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__AndExpr__ComparisonExprAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAndExprAccess().getComparisonExprAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group__0__Impl"


    // $ANTLR start "rule__AndExpr__Group__1"
    // InternalXpath.g:3090:1: rule__AndExpr__Group__1 : rule__AndExpr__Group__1__Impl ;
    public final void rule__AndExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3094:1: ( rule__AndExpr__Group__1__Impl )
            // InternalXpath.g:3095:2: rule__AndExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group__1"


    // $ANTLR start "rule__AndExpr__Group__1__Impl"
    // InternalXpath.g:3101:1: rule__AndExpr__Group__1__Impl : ( ( rule__AndExpr__Group_1__0 )* ) ;
    public final void rule__AndExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3105:1: ( ( ( rule__AndExpr__Group_1__0 )* ) )
            // InternalXpath.g:3106:1: ( ( rule__AndExpr__Group_1__0 )* )
            {
            // InternalXpath.g:3106:1: ( ( rule__AndExpr__Group_1__0 )* )
            // InternalXpath.g:3107:2: ( rule__AndExpr__Group_1__0 )*
            {
             before(grammarAccess.getAndExprAccess().getGroup_1()); 
            // InternalXpath.g:3108:2: ( rule__AndExpr__Group_1__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==47) ) {
                    int LA26_2 = input.LA(2);

                    if ( ((LA26_2>=RULE_STRING && LA26_2<=RULE_INT)||(LA26_2>=48 && LA26_2<=51)) ) {
                        alt26=1;
                    }


                }


                switch (alt26) {
            	case 1 :
            	    // InternalXpath.g:3108:3: rule__AndExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_23);
            	    rule__AndExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getAndExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group__1__Impl"


    // $ANTLR start "rule__AndExpr__Group_1__0"
    // InternalXpath.g:3117:1: rule__AndExpr__Group_1__0 : rule__AndExpr__Group_1__0__Impl rule__AndExpr__Group_1__1 ;
    public final void rule__AndExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3121:1: ( rule__AndExpr__Group_1__0__Impl rule__AndExpr__Group_1__1 )
            // InternalXpath.g:3122:2: rule__AndExpr__Group_1__0__Impl rule__AndExpr__Group_1__1
            {
            pushFollow(FOLLOW_21);
            rule__AndExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group_1__0"


    // $ANTLR start "rule__AndExpr__Group_1__0__Impl"
    // InternalXpath.g:3129:1: rule__AndExpr__Group_1__0__Impl : ( 'and' ) ;
    public final void rule__AndExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3133:1: ( ( 'and' ) )
            // InternalXpath.g:3134:1: ( 'and' )
            {
            // InternalXpath.g:3134:1: ( 'and' )
            // InternalXpath.g:3135:2: 'and'
            {
             before(grammarAccess.getAndExprAccess().getAndKeyword_1_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getAndExprAccess().getAndKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group_1__0__Impl"


    // $ANTLR start "rule__AndExpr__Group_1__1"
    // InternalXpath.g:3144:1: rule__AndExpr__Group_1__1 : rule__AndExpr__Group_1__1__Impl ;
    public final void rule__AndExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3148:1: ( rule__AndExpr__Group_1__1__Impl )
            // InternalXpath.g:3149:2: rule__AndExpr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndExpr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group_1__1"


    // $ANTLR start "rule__AndExpr__Group_1__1__Impl"
    // InternalXpath.g:3155:1: rule__AndExpr__Group_1__1__Impl : ( ( rule__AndExpr__AndCondition_listAssignment_1_1 ) ) ;
    public final void rule__AndExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3159:1: ( ( ( rule__AndExpr__AndCondition_listAssignment_1_1 ) ) )
            // InternalXpath.g:3160:1: ( ( rule__AndExpr__AndCondition_listAssignment_1_1 ) )
            {
            // InternalXpath.g:3160:1: ( ( rule__AndExpr__AndCondition_listAssignment_1_1 ) )
            // InternalXpath.g:3161:2: ( rule__AndExpr__AndCondition_listAssignment_1_1 )
            {
             before(grammarAccess.getAndExprAccess().getAndCondition_listAssignment_1_1()); 
            // InternalXpath.g:3162:2: ( rule__AndExpr__AndCondition_listAssignment_1_1 )
            // InternalXpath.g:3162:3: rule__AndExpr__AndCondition_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AndExpr__AndCondition_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndExprAccess().getAndCondition_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__Group_1__1__Impl"


    // $ANTLR start "rule__ComparisonExpr__Group_0__0"
    // InternalXpath.g:3171:1: rule__ComparisonExpr__Group_0__0 : rule__ComparisonExpr__Group_0__0__Impl rule__ComparisonExpr__Group_0__1 ;
    public final void rule__ComparisonExpr__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3175:1: ( rule__ComparisonExpr__Group_0__0__Impl rule__ComparisonExpr__Group_0__1 )
            // InternalXpath.g:3176:2: rule__ComparisonExpr__Group_0__0__Impl rule__ComparisonExpr__Group_0__1
            {
            pushFollow(FOLLOW_24);
            rule__ComparisonExpr__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0__0"


    // $ANTLR start "rule__ComparisonExpr__Group_0__0__Impl"
    // InternalXpath.g:3183:1: rule__ComparisonExpr__Group_0__0__Impl : ( ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 ) ) ;
    public final void rule__ComparisonExpr__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3187:1: ( ( ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 ) ) )
            // InternalXpath.g:3188:1: ( ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 ) )
            {
            // InternalXpath.g:3188:1: ( ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 ) )
            // InternalXpath.g:3189:2: ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 )
            {
             before(grammarAccess.getComparisonExprAccess().getAdditiveExprAssignment_0_0()); 
            // InternalXpath.g:3190:2: ( rule__ComparisonExpr__AdditiveExprAssignment_0_0 )
            // InternalXpath.g:3190:3: rule__ComparisonExpr__AdditiveExprAssignment_0_0
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__AdditiveExprAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getComparisonExprAccess().getAdditiveExprAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0__0__Impl"


    // $ANTLR start "rule__ComparisonExpr__Group_0__1"
    // InternalXpath.g:3198:1: rule__ComparisonExpr__Group_0__1 : rule__ComparisonExpr__Group_0__1__Impl ;
    public final void rule__ComparisonExpr__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3202:1: ( rule__ComparisonExpr__Group_0__1__Impl )
            // InternalXpath.g:3203:2: rule__ComparisonExpr__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0__1"


    // $ANTLR start "rule__ComparisonExpr__Group_0__1__Impl"
    // InternalXpath.g:3209:1: rule__ComparisonExpr__Group_0__1__Impl : ( ( rule__ComparisonExpr__Group_0_1__0 )? ) ;
    public final void rule__ComparisonExpr__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3213:1: ( ( ( rule__ComparisonExpr__Group_0_1__0 )? ) )
            // InternalXpath.g:3214:1: ( ( rule__ComparisonExpr__Group_0_1__0 )? )
            {
            // InternalXpath.g:3214:1: ( ( rule__ComparisonExpr__Group_0_1__0 )? )
            // InternalXpath.g:3215:2: ( rule__ComparisonExpr__Group_0_1__0 )?
            {
             before(grammarAccess.getComparisonExprAccess().getGroup_0_1()); 
            // InternalXpath.g:3216:2: ( rule__ComparisonExpr__Group_0_1__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( ((LA27_0>=19 && LA27_0<=33)) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalXpath.g:3216:3: rule__ComparisonExpr__Group_0_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComparisonExpr__Group_0_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getComparisonExprAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0__1__Impl"


    // $ANTLR start "rule__ComparisonExpr__Group_0_1__0"
    // InternalXpath.g:3225:1: rule__ComparisonExpr__Group_0_1__0 : rule__ComparisonExpr__Group_0_1__0__Impl rule__ComparisonExpr__Group_0_1__1 ;
    public final void rule__ComparisonExpr__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3229:1: ( rule__ComparisonExpr__Group_0_1__0__Impl rule__ComparisonExpr__Group_0_1__1 )
            // InternalXpath.g:3230:2: rule__ComparisonExpr__Group_0_1__0__Impl rule__ComparisonExpr__Group_0_1__1
            {
            pushFollow(FOLLOW_25);
            rule__ComparisonExpr__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0_1__0"


    // $ANTLR start "rule__ComparisonExpr__Group_0_1__0__Impl"
    // InternalXpath.g:3237:1: rule__ComparisonExpr__Group_0_1__0__Impl : ( ( rule__ComparisonExpr__Alternatives_0_1_0 ) ) ;
    public final void rule__ComparisonExpr__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3241:1: ( ( ( rule__ComparisonExpr__Alternatives_0_1_0 ) ) )
            // InternalXpath.g:3242:1: ( ( rule__ComparisonExpr__Alternatives_0_1_0 ) )
            {
            // InternalXpath.g:3242:1: ( ( rule__ComparisonExpr__Alternatives_0_1_0 ) )
            // InternalXpath.g:3243:2: ( rule__ComparisonExpr__Alternatives_0_1_0 )
            {
             before(grammarAccess.getComparisonExprAccess().getAlternatives_0_1_0()); 
            // InternalXpath.g:3244:2: ( rule__ComparisonExpr__Alternatives_0_1_0 )
            // InternalXpath.g:3244:3: rule__ComparisonExpr__Alternatives_0_1_0
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Alternatives_0_1_0();

            state._fsp--;


            }

             after(grammarAccess.getComparisonExprAccess().getAlternatives_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0_1__0__Impl"


    // $ANTLR start "rule__ComparisonExpr__Group_0_1__1"
    // InternalXpath.g:3252:1: rule__ComparisonExpr__Group_0_1__1 : rule__ComparisonExpr__Group_0_1__1__Impl ;
    public final void rule__ComparisonExpr__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3256:1: ( rule__ComparisonExpr__Group_0_1__1__Impl )
            // InternalXpath.g:3257:2: rule__ComparisonExpr__Group_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0_1__1"


    // $ANTLR start "rule__ComparisonExpr__Group_0_1__1__Impl"
    // InternalXpath.g:3263:1: rule__ComparisonExpr__Group_0_1__1__Impl : ( ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 ) ) ;
    public final void rule__ComparisonExpr__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3267:1: ( ( ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 ) ) )
            // InternalXpath.g:3268:1: ( ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 ) )
            {
            // InternalXpath.g:3268:1: ( ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 ) )
            // InternalXpath.g:3269:2: ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 )
            {
             before(grammarAccess.getComparisonExprAccess().getAdditiveExprOptionalAssignment_0_1_1()); 
            // InternalXpath.g:3270:2: ( rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 )
            // InternalXpath.g:3270:3: rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonExprAccess().getAdditiveExprOptionalAssignment_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__Group_0_1__1__Impl"


    // $ANTLR start "rule__AdditiveExpr__Group__0"
    // InternalXpath.g:3279:1: rule__AdditiveExpr__Group__0 : rule__AdditiveExpr__Group__0__Impl rule__AdditiveExpr__Group__1 ;
    public final void rule__AdditiveExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3283:1: ( rule__AdditiveExpr__Group__0__Impl rule__AdditiveExpr__Group__1 )
            // InternalXpath.g:3284:2: rule__AdditiveExpr__Group__0__Impl rule__AdditiveExpr__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__AdditiveExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group__0"


    // $ANTLR start "rule__AdditiveExpr__Group__0__Impl"
    // InternalXpath.g:3291:1: rule__AdditiveExpr__Group__0__Impl : ( ( rule__AdditiveExpr__ValueExprAssignment_0 ) ) ;
    public final void rule__AdditiveExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3295:1: ( ( ( rule__AdditiveExpr__ValueExprAssignment_0 ) ) )
            // InternalXpath.g:3296:1: ( ( rule__AdditiveExpr__ValueExprAssignment_0 ) )
            {
            // InternalXpath.g:3296:1: ( ( rule__AdditiveExpr__ValueExprAssignment_0 ) )
            // InternalXpath.g:3297:2: ( rule__AdditiveExpr__ValueExprAssignment_0 )
            {
             before(grammarAccess.getAdditiveExprAccess().getValueExprAssignment_0()); 
            // InternalXpath.g:3298:2: ( rule__AdditiveExpr__ValueExprAssignment_0 )
            // InternalXpath.g:3298:3: rule__AdditiveExpr__ValueExprAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__ValueExprAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExprAccess().getValueExprAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group__0__Impl"


    // $ANTLR start "rule__AdditiveExpr__Group__1"
    // InternalXpath.g:3306:1: rule__AdditiveExpr__Group__1 : rule__AdditiveExpr__Group__1__Impl ;
    public final void rule__AdditiveExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3310:1: ( rule__AdditiveExpr__Group__1__Impl )
            // InternalXpath.g:3311:2: rule__AdditiveExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group__1"


    // $ANTLR start "rule__AdditiveExpr__Group__1__Impl"
    // InternalXpath.g:3317:1: rule__AdditiveExpr__Group__1__Impl : ( ( rule__AdditiveExpr__Group_1__0 )* ) ;
    public final void rule__AdditiveExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3321:1: ( ( ( rule__AdditiveExpr__Group_1__0 )* ) )
            // InternalXpath.g:3322:1: ( ( rule__AdditiveExpr__Group_1__0 )* )
            {
            // InternalXpath.g:3322:1: ( ( rule__AdditiveExpr__Group_1__0 )* )
            // InternalXpath.g:3323:2: ( rule__AdditiveExpr__Group_1__0 )*
            {
             before(grammarAccess.getAdditiveExprAccess().getGroup_1()); 
            // InternalXpath.g:3324:2: ( rule__AdditiveExpr__Group_1__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( ((LA28_0>=13 && LA28_0<=14)) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalXpath.g:3324:3: rule__AdditiveExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_27);
            	    rule__AdditiveExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getAdditiveExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group__1__Impl"


    // $ANTLR start "rule__AdditiveExpr__Group_1__0"
    // InternalXpath.g:3333:1: rule__AdditiveExpr__Group_1__0 : rule__AdditiveExpr__Group_1__0__Impl rule__AdditiveExpr__Group_1__1 ;
    public final void rule__AdditiveExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3337:1: ( rule__AdditiveExpr__Group_1__0__Impl rule__AdditiveExpr__Group_1__1 )
            // InternalXpath.g:3338:2: rule__AdditiveExpr__Group_1__0__Impl rule__AdditiveExpr__Group_1__1
            {
            pushFollow(FOLLOW_25);
            rule__AdditiveExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group_1__0"


    // $ANTLR start "rule__AdditiveExpr__Group_1__0__Impl"
    // InternalXpath.g:3345:1: rule__AdditiveExpr__Group_1__0__Impl : ( ( rule__AdditiveExpr__Additive_listAssignment_1_0 ) ) ;
    public final void rule__AdditiveExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3349:1: ( ( ( rule__AdditiveExpr__Additive_listAssignment_1_0 ) ) )
            // InternalXpath.g:3350:1: ( ( rule__AdditiveExpr__Additive_listAssignment_1_0 ) )
            {
            // InternalXpath.g:3350:1: ( ( rule__AdditiveExpr__Additive_listAssignment_1_0 ) )
            // InternalXpath.g:3351:2: ( rule__AdditiveExpr__Additive_listAssignment_1_0 )
            {
             before(grammarAccess.getAdditiveExprAccess().getAdditive_listAssignment_1_0()); 
            // InternalXpath.g:3352:2: ( rule__AdditiveExpr__Additive_listAssignment_1_0 )
            // InternalXpath.g:3352:3: rule__AdditiveExpr__Additive_listAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Additive_listAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExprAccess().getAdditive_listAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group_1__0__Impl"


    // $ANTLR start "rule__AdditiveExpr__Group_1__1"
    // InternalXpath.g:3360:1: rule__AdditiveExpr__Group_1__1 : rule__AdditiveExpr__Group_1__1__Impl ;
    public final void rule__AdditiveExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3364:1: ( rule__AdditiveExpr__Group_1__1__Impl )
            // InternalXpath.g:3365:2: rule__AdditiveExpr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group_1__1"


    // $ANTLR start "rule__AdditiveExpr__Group_1__1__Impl"
    // InternalXpath.g:3371:1: rule__AdditiveExpr__Group_1__1__Impl : ( ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 ) ) ;
    public final void rule__AdditiveExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3375:1: ( ( ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 ) ) )
            // InternalXpath.g:3376:1: ( ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 ) )
            {
            // InternalXpath.g:3376:1: ( ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 ) )
            // InternalXpath.g:3377:2: ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 )
            {
             before(grammarAccess.getAdditiveExprAccess().getValueExpr_listAssignment_1_1()); 
            // InternalXpath.g:3378:2: ( rule__AdditiveExpr__ValueExpr_listAssignment_1_1 )
            // InternalXpath.g:3378:3: rule__AdditiveExpr__ValueExpr_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__ValueExpr_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExprAccess().getValueExpr_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Group_1__1__Impl"


    // $ANTLR start "rule__UnionExpr__Group__0"
    // InternalXpath.g:3387:1: rule__UnionExpr__Group__0 : rule__UnionExpr__Group__0__Impl rule__UnionExpr__Group__1 ;
    public final void rule__UnionExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3391:1: ( rule__UnionExpr__Group__0__Impl rule__UnionExpr__Group__1 )
            // InternalXpath.g:3392:2: rule__UnionExpr__Group__0__Impl rule__UnionExpr__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__UnionExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnionExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group__0"


    // $ANTLR start "rule__UnionExpr__Group__0__Impl"
    // InternalXpath.g:3399:1: rule__UnionExpr__Group__0__Impl : ( ( rule__UnionExpr__IntersectExceptExprAssignment_0 ) ) ;
    public final void rule__UnionExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3403:1: ( ( ( rule__UnionExpr__IntersectExceptExprAssignment_0 ) ) )
            // InternalXpath.g:3404:1: ( ( rule__UnionExpr__IntersectExceptExprAssignment_0 ) )
            {
            // InternalXpath.g:3404:1: ( ( rule__UnionExpr__IntersectExceptExprAssignment_0 ) )
            // InternalXpath.g:3405:2: ( rule__UnionExpr__IntersectExceptExprAssignment_0 )
            {
             before(grammarAccess.getUnionExprAccess().getIntersectExceptExprAssignment_0()); 
            // InternalXpath.g:3406:2: ( rule__UnionExpr__IntersectExceptExprAssignment_0 )
            // InternalXpath.g:3406:3: rule__UnionExpr__IntersectExceptExprAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__IntersectExceptExprAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getUnionExprAccess().getIntersectExceptExprAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group__0__Impl"


    // $ANTLR start "rule__UnionExpr__Group__1"
    // InternalXpath.g:3414:1: rule__UnionExpr__Group__1 : rule__UnionExpr__Group__1__Impl ;
    public final void rule__UnionExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3418:1: ( rule__UnionExpr__Group__1__Impl )
            // InternalXpath.g:3419:2: rule__UnionExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group__1"


    // $ANTLR start "rule__UnionExpr__Group__1__Impl"
    // InternalXpath.g:3425:1: rule__UnionExpr__Group__1__Impl : ( ( rule__UnionExpr__Group_1__0 )* ) ;
    public final void rule__UnionExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3429:1: ( ( ( rule__UnionExpr__Group_1__0 )* ) )
            // InternalXpath.g:3430:1: ( ( rule__UnionExpr__Group_1__0 )* )
            {
            // InternalXpath.g:3430:1: ( ( rule__UnionExpr__Group_1__0 )* )
            // InternalXpath.g:3431:2: ( rule__UnionExpr__Group_1__0 )*
            {
             before(grammarAccess.getUnionExprAccess().getGroup_1()); 
            // InternalXpath.g:3432:2: ( rule__UnionExpr__Group_1__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( ((LA29_0>=15 && LA29_0<=16)) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalXpath.g:3432:3: rule__UnionExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_29);
            	    rule__UnionExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getUnionExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group__1__Impl"


    // $ANTLR start "rule__UnionExpr__Group_1__0"
    // InternalXpath.g:3441:1: rule__UnionExpr__Group_1__0 : rule__UnionExpr__Group_1__0__Impl rule__UnionExpr__Group_1__1 ;
    public final void rule__UnionExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3445:1: ( rule__UnionExpr__Group_1__0__Impl rule__UnionExpr__Group_1__1 )
            // InternalXpath.g:3446:2: rule__UnionExpr__Group_1__0__Impl rule__UnionExpr__Group_1__1
            {
            pushFollow(FOLLOW_25);
            rule__UnionExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnionExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group_1__0"


    // $ANTLR start "rule__UnionExpr__Group_1__0__Impl"
    // InternalXpath.g:3453:1: rule__UnionExpr__Group_1__0__Impl : ( ( rule__UnionExpr__Intercection_listAssignment_1_0 ) ) ;
    public final void rule__UnionExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3457:1: ( ( ( rule__UnionExpr__Intercection_listAssignment_1_0 ) ) )
            // InternalXpath.g:3458:1: ( ( rule__UnionExpr__Intercection_listAssignment_1_0 ) )
            {
            // InternalXpath.g:3458:1: ( ( rule__UnionExpr__Intercection_listAssignment_1_0 ) )
            // InternalXpath.g:3459:2: ( rule__UnionExpr__Intercection_listAssignment_1_0 )
            {
             before(grammarAccess.getUnionExprAccess().getIntercection_listAssignment_1_0()); 
            // InternalXpath.g:3460:2: ( rule__UnionExpr__Intercection_listAssignment_1_0 )
            // InternalXpath.g:3460:3: rule__UnionExpr__Intercection_listAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__Intercection_listAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getUnionExprAccess().getIntercection_listAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group_1__0__Impl"


    // $ANTLR start "rule__UnionExpr__Group_1__1"
    // InternalXpath.g:3468:1: rule__UnionExpr__Group_1__1 : rule__UnionExpr__Group_1__1__Impl ;
    public final void rule__UnionExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3472:1: ( rule__UnionExpr__Group_1__1__Impl )
            // InternalXpath.g:3473:2: rule__UnionExpr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group_1__1"


    // $ANTLR start "rule__UnionExpr__Group_1__1__Impl"
    // InternalXpath.g:3479:1: rule__UnionExpr__Group_1__1__Impl : ( ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 ) ) ;
    public final void rule__UnionExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3483:1: ( ( ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 ) ) )
            // InternalXpath.g:3484:1: ( ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 ) )
            {
            // InternalXpath.g:3484:1: ( ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 ) )
            // InternalXpath.g:3485:2: ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 )
            {
             before(grammarAccess.getUnionExprAccess().getIntersectExceptExpr_listAssignment_1_1()); 
            // InternalXpath.g:3486:2: ( rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 )
            // InternalXpath.g:3486:3: rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getUnionExprAccess().getIntersectExceptExpr_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Group_1__1__Impl"


    // $ANTLR start "rule__IntersectExceptExpr__Group__0"
    // InternalXpath.g:3495:1: rule__IntersectExceptExpr__Group__0 : rule__IntersectExceptExpr__Group__0__Impl rule__IntersectExceptExpr__Group__1 ;
    public final void rule__IntersectExceptExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3499:1: ( rule__IntersectExceptExpr__Group__0__Impl rule__IntersectExceptExpr__Group__1 )
            // InternalXpath.g:3500:2: rule__IntersectExceptExpr__Group__0__Impl rule__IntersectExceptExpr__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__IntersectExceptExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group__0"


    // $ANTLR start "rule__IntersectExceptExpr__Group__0__Impl"
    // InternalXpath.g:3507:1: rule__IntersectExceptExpr__Group__0__Impl : ( ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 ) ) ;
    public final void rule__IntersectExceptExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3511:1: ( ( ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 ) ) )
            // InternalXpath.g:3512:1: ( ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 ) )
            {
            // InternalXpath.g:3512:1: ( ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 ) )
            // InternalXpath.g:3513:2: ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 )
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstanceofExprAssignment_0()); 
            // InternalXpath.g:3514:2: ( rule__IntersectExceptExpr__InstanceofExprAssignment_0 )
            // InternalXpath.g:3514:3: rule__IntersectExceptExpr__InstanceofExprAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__InstanceofExprAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntersectExceptExprAccess().getInstanceofExprAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group__0__Impl"


    // $ANTLR start "rule__IntersectExceptExpr__Group__1"
    // InternalXpath.g:3522:1: rule__IntersectExceptExpr__Group__1 : rule__IntersectExceptExpr__Group__1__Impl ;
    public final void rule__IntersectExceptExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3526:1: ( rule__IntersectExceptExpr__Group__1__Impl )
            // InternalXpath.g:3527:2: rule__IntersectExceptExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group__1"


    // $ANTLR start "rule__IntersectExceptExpr__Group__1__Impl"
    // InternalXpath.g:3533:1: rule__IntersectExceptExpr__Group__1__Impl : ( ( rule__IntersectExceptExpr__Group_1__0 )* ) ;
    public final void rule__IntersectExceptExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3537:1: ( ( ( rule__IntersectExceptExpr__Group_1__0 )* ) )
            // InternalXpath.g:3538:1: ( ( rule__IntersectExceptExpr__Group_1__0 )* )
            {
            // InternalXpath.g:3538:1: ( ( rule__IntersectExceptExpr__Group_1__0 )* )
            // InternalXpath.g:3539:2: ( rule__IntersectExceptExpr__Group_1__0 )*
            {
             before(grammarAccess.getIntersectExceptExprAccess().getGroup_1()); 
            // InternalXpath.g:3540:2: ( rule__IntersectExceptExpr__Group_1__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( ((LA30_0>=17 && LA30_0<=18)) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalXpath.g:3540:3: rule__IntersectExceptExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__IntersectExceptExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getIntersectExceptExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group__1__Impl"


    // $ANTLR start "rule__IntersectExceptExpr__Group_1__0"
    // InternalXpath.g:3549:1: rule__IntersectExceptExpr__Group_1__0 : rule__IntersectExceptExpr__Group_1__0__Impl rule__IntersectExceptExpr__Group_1__1 ;
    public final void rule__IntersectExceptExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3553:1: ( rule__IntersectExceptExpr__Group_1__0__Impl rule__IntersectExceptExpr__Group_1__1 )
            // InternalXpath.g:3554:2: rule__IntersectExceptExpr__Group_1__0__Impl rule__IntersectExceptExpr__Group_1__1
            {
            pushFollow(FOLLOW_25);
            rule__IntersectExceptExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group_1__0"


    // $ANTLR start "rule__IntersectExceptExpr__Group_1__0__Impl"
    // InternalXpath.g:3561:1: rule__IntersectExceptExpr__Group_1__0__Impl : ( ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 ) ) ;
    public final void rule__IntersectExceptExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3565:1: ( ( ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 ) ) )
            // InternalXpath.g:3566:1: ( ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 ) )
            {
            // InternalXpath.g:3566:1: ( ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 ) )
            // InternalXpath.g:3567:2: ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 )
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstance_listAssignment_1_0()); 
            // InternalXpath.g:3568:2: ( rule__IntersectExceptExpr__Instance_listAssignment_1_0 )
            // InternalXpath.g:3568:3: rule__IntersectExceptExpr__Instance_listAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Instance_listAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getIntersectExceptExprAccess().getInstance_listAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group_1__0__Impl"


    // $ANTLR start "rule__IntersectExceptExpr__Group_1__1"
    // InternalXpath.g:3576:1: rule__IntersectExceptExpr__Group_1__1 : rule__IntersectExceptExpr__Group_1__1__Impl ;
    public final void rule__IntersectExceptExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3580:1: ( rule__IntersectExceptExpr__Group_1__1__Impl )
            // InternalXpath.g:3581:2: rule__IntersectExceptExpr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group_1__1"


    // $ANTLR start "rule__IntersectExceptExpr__Group_1__1__Impl"
    // InternalXpath.g:3587:1: rule__IntersectExceptExpr__Group_1__1__Impl : ( ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 ) ) ;
    public final void rule__IntersectExceptExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3591:1: ( ( ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 ) ) )
            // InternalXpath.g:3592:1: ( ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 ) )
            {
            // InternalXpath.g:3592:1: ( ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 ) )
            // InternalXpath.g:3593:2: ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 )
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstanceofExpr_listAssignment_1_1()); 
            // InternalXpath.g:3594:2: ( rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 )
            // InternalXpath.g:3594:3: rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getIntersectExceptExprAccess().getInstanceofExpr_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Group_1__1__Impl"


    // $ANTLR start "rule__PathExpr__Group__0"
    // InternalXpath.g:3603:1: rule__PathExpr__Group__0 : rule__PathExpr__Group__0__Impl rule__PathExpr__Group__1 ;
    public final void rule__PathExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3607:1: ( rule__PathExpr__Group__0__Impl rule__PathExpr__Group__1 )
            // InternalXpath.g:3608:2: rule__PathExpr__Group__0__Impl rule__PathExpr__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__PathExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PathExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group__0"


    // $ANTLR start "rule__PathExpr__Group__0__Impl"
    // InternalXpath.g:3615:1: rule__PathExpr__Group__0__Impl : ( ( rule__PathExpr__ElementAssignment_0 ) ) ;
    public final void rule__PathExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3619:1: ( ( ( rule__PathExpr__ElementAssignment_0 ) ) )
            // InternalXpath.g:3620:1: ( ( rule__PathExpr__ElementAssignment_0 ) )
            {
            // InternalXpath.g:3620:1: ( ( rule__PathExpr__ElementAssignment_0 ) )
            // InternalXpath.g:3621:2: ( rule__PathExpr__ElementAssignment_0 )
            {
             before(grammarAccess.getPathExprAccess().getElementAssignment_0()); 
            // InternalXpath.g:3622:2: ( rule__PathExpr__ElementAssignment_0 )
            // InternalXpath.g:3622:3: rule__PathExpr__ElementAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__ElementAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPathExprAccess().getElementAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group__0__Impl"


    // $ANTLR start "rule__PathExpr__Group__1"
    // InternalXpath.g:3630:1: rule__PathExpr__Group__1 : rule__PathExpr__Group__1__Impl ;
    public final void rule__PathExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3634:1: ( rule__PathExpr__Group__1__Impl )
            // InternalXpath.g:3635:2: rule__PathExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group__1"


    // $ANTLR start "rule__PathExpr__Group__1__Impl"
    // InternalXpath.g:3641:1: rule__PathExpr__Group__1__Impl : ( ( rule__PathExpr__Group_1__0 )* ) ;
    public final void rule__PathExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3645:1: ( ( ( rule__PathExpr__Group_1__0 )* ) )
            // InternalXpath.g:3646:1: ( ( rule__PathExpr__Group_1__0 )* )
            {
            // InternalXpath.g:3646:1: ( ( rule__PathExpr__Group_1__0 )* )
            // InternalXpath.g:3647:2: ( rule__PathExpr__Group_1__0 )*
            {
             before(grammarAccess.getPathExprAccess().getGroup_1()); 
            // InternalXpath.g:3648:2: ( rule__PathExpr__Group_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( ((LA31_0>=34 && LA31_0<=35)) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalXpath.g:3648:3: rule__PathExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__PathExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getPathExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group__1__Impl"


    // $ANTLR start "rule__PathExpr__Group_1__0"
    // InternalXpath.g:3657:1: rule__PathExpr__Group_1__0 : rule__PathExpr__Group_1__0__Impl rule__PathExpr__Group_1__1 ;
    public final void rule__PathExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3661:1: ( rule__PathExpr__Group_1__0__Impl rule__PathExpr__Group_1__1 )
            // InternalXpath.g:3662:2: rule__PathExpr__Group_1__0__Impl rule__PathExpr__Group_1__1
            {
            pushFollow(FOLLOW_34);
            rule__PathExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PathExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group_1__0"


    // $ANTLR start "rule__PathExpr__Group_1__0__Impl"
    // InternalXpath.g:3669:1: rule__PathExpr__Group_1__0__Impl : ( ( rule__PathExpr__Path_listAssignment_1_0 ) ) ;
    public final void rule__PathExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3673:1: ( ( ( rule__PathExpr__Path_listAssignment_1_0 ) ) )
            // InternalXpath.g:3674:1: ( ( rule__PathExpr__Path_listAssignment_1_0 ) )
            {
            // InternalXpath.g:3674:1: ( ( rule__PathExpr__Path_listAssignment_1_0 ) )
            // InternalXpath.g:3675:2: ( rule__PathExpr__Path_listAssignment_1_0 )
            {
             before(grammarAccess.getPathExprAccess().getPath_listAssignment_1_0()); 
            // InternalXpath.g:3676:2: ( rule__PathExpr__Path_listAssignment_1_0 )
            // InternalXpath.g:3676:3: rule__PathExpr__Path_listAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__Path_listAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getPathExprAccess().getPath_listAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group_1__0__Impl"


    // $ANTLR start "rule__PathExpr__Group_1__1"
    // InternalXpath.g:3684:1: rule__PathExpr__Group_1__1 : rule__PathExpr__Group_1__1__Impl ;
    public final void rule__PathExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3688:1: ( rule__PathExpr__Group_1__1__Impl )
            // InternalXpath.g:3689:2: rule__PathExpr__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group_1__1"


    // $ANTLR start "rule__PathExpr__Group_1__1__Impl"
    // InternalXpath.g:3695:1: rule__PathExpr__Group_1__1__Impl : ( ( rule__PathExpr__PathElement_listAssignment_1_1 ) ) ;
    public final void rule__PathExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3699:1: ( ( ( rule__PathExpr__PathElement_listAssignment_1_1 ) ) )
            // InternalXpath.g:3700:1: ( ( rule__PathExpr__PathElement_listAssignment_1_1 ) )
            {
            // InternalXpath.g:3700:1: ( ( rule__PathExpr__PathElement_listAssignment_1_1 ) )
            // InternalXpath.g:3701:2: ( rule__PathExpr__PathElement_listAssignment_1_1 )
            {
             before(grammarAccess.getPathExprAccess().getPathElement_listAssignment_1_1()); 
            // InternalXpath.g:3702:2: ( rule__PathExpr__PathElement_listAssignment_1_1 )
            // InternalXpath.g:3702:3: rule__PathExpr__PathElement_listAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__PathElement_listAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPathExprAccess().getPathElement_listAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Group_1__1__Impl"


    // $ANTLR start "rule__NotExpr__Group__0"
    // InternalXpath.g:3711:1: rule__NotExpr__Group__0 : rule__NotExpr__Group__0__Impl rule__NotExpr__Group__1 ;
    public final void rule__NotExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3715:1: ( rule__NotExpr__Group__0__Impl rule__NotExpr__Group__1 )
            // InternalXpath.g:3716:2: rule__NotExpr__Group__0__Impl rule__NotExpr__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__NotExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__0"


    // $ANTLR start "rule__NotExpr__Group__0__Impl"
    // InternalXpath.g:3723:1: rule__NotExpr__Group__0__Impl : ( 'not' ) ;
    public final void rule__NotExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3727:1: ( ( 'not' ) )
            // InternalXpath.g:3728:1: ( 'not' )
            {
            // InternalXpath.g:3728:1: ( 'not' )
            // InternalXpath.g:3729:2: 'not'
            {
             before(grammarAccess.getNotExprAccess().getNotKeyword_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getNotExprAccess().getNotKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__0__Impl"


    // $ANTLR start "rule__NotExpr__Group__1"
    // InternalXpath.g:3738:1: rule__NotExpr__Group__1 : rule__NotExpr__Group__1__Impl rule__NotExpr__Group__2 ;
    public final void rule__NotExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3742:1: ( rule__NotExpr__Group__1__Impl rule__NotExpr__Group__2 )
            // InternalXpath.g:3743:2: rule__NotExpr__Group__1__Impl rule__NotExpr__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__NotExpr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotExpr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__1"


    // $ANTLR start "rule__NotExpr__Group__1__Impl"
    // InternalXpath.g:3750:1: rule__NotExpr__Group__1__Impl : ( '(' ) ;
    public final void rule__NotExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3754:1: ( ( '(' ) )
            // InternalXpath.g:3755:1: ( '(' )
            {
            // InternalXpath.g:3755:1: ( '(' )
            // InternalXpath.g:3756:2: '('
            {
             before(grammarAccess.getNotExprAccess().getLeftParenthesisKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getNotExprAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__1__Impl"


    // $ANTLR start "rule__NotExpr__Group__2"
    // InternalXpath.g:3765:1: rule__NotExpr__Group__2 : rule__NotExpr__Group__2__Impl rule__NotExpr__Group__3 ;
    public final void rule__NotExpr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3769:1: ( rule__NotExpr__Group__2__Impl rule__NotExpr__Group__3 )
            // InternalXpath.g:3770:2: rule__NotExpr__Group__2__Impl rule__NotExpr__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__NotExpr__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotExpr__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__2"


    // $ANTLR start "rule__NotExpr__Group__2__Impl"
    // InternalXpath.g:3777:1: rule__NotExpr__Group__2__Impl : ( ( rule__NotExpr__PathExprAssignment_2 ) ) ;
    public final void rule__NotExpr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3781:1: ( ( ( rule__NotExpr__PathExprAssignment_2 ) ) )
            // InternalXpath.g:3782:1: ( ( rule__NotExpr__PathExprAssignment_2 ) )
            {
            // InternalXpath.g:3782:1: ( ( rule__NotExpr__PathExprAssignment_2 ) )
            // InternalXpath.g:3783:2: ( rule__NotExpr__PathExprAssignment_2 )
            {
             before(grammarAccess.getNotExprAccess().getPathExprAssignment_2()); 
            // InternalXpath.g:3784:2: ( rule__NotExpr__PathExprAssignment_2 )
            // InternalXpath.g:3784:3: rule__NotExpr__PathExprAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NotExpr__PathExprAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNotExprAccess().getPathExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__2__Impl"


    // $ANTLR start "rule__NotExpr__Group__3"
    // InternalXpath.g:3792:1: rule__NotExpr__Group__3 : rule__NotExpr__Group__3__Impl ;
    public final void rule__NotExpr__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3796:1: ( rule__NotExpr__Group__3__Impl )
            // InternalXpath.g:3797:2: rule__NotExpr__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotExpr__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__3"


    // $ANTLR start "rule__NotExpr__Group__3__Impl"
    // InternalXpath.g:3803:1: rule__NotExpr__Group__3__Impl : ( ')' ) ;
    public final void rule__NotExpr__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3807:1: ( ( ')' ) )
            // InternalXpath.g:3808:1: ( ')' )
            {
            // InternalXpath.g:3808:1: ( ')' )
            // InternalXpath.g:3809:2: ')'
            {
             before(grammarAccess.getNotExprAccess().getRightParenthesisKeyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getNotExprAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__Group__3__Impl"


    // $ANTLR start "rule__Element__Group__0"
    // InternalXpath.g:3819:1: rule__Element__Group__0 : rule__Element__Group__0__Impl rule__Element__Group__1 ;
    public final void rule__Element__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3823:1: ( rule__Element__Group__0__Impl rule__Element__Group__1 )
            // InternalXpath.g:3824:2: rule__Element__Group__0__Impl rule__Element__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Element__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Element__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Group__0"


    // $ANTLR start "rule__Element__Group__0__Impl"
    // InternalXpath.g:3831:1: rule__Element__Group__0__Impl : ( 'ns:' ) ;
    public final void rule__Element__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3835:1: ( ( 'ns:' ) )
            // InternalXpath.g:3836:1: ( 'ns:' )
            {
            // InternalXpath.g:3836:1: ( 'ns:' )
            // InternalXpath.g:3837:2: 'ns:'
            {
             before(grammarAccess.getElementAccess().getNsKeyword_0()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getElementAccess().getNsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Group__0__Impl"


    // $ANTLR start "rule__Element__Group__1"
    // InternalXpath.g:3846:1: rule__Element__Group__1 : rule__Element__Group__1__Impl ;
    public final void rule__Element__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3850:1: ( rule__Element__Group__1__Impl )
            // InternalXpath.g:3851:2: rule__Element__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Element__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Group__1"


    // $ANTLR start "rule__Element__Group__1__Impl"
    // InternalXpath.g:3857:1: rule__Element__Group__1__Impl : ( ( rule__Element__NameAssignment_1 ) ) ;
    public final void rule__Element__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3861:1: ( ( ( rule__Element__NameAssignment_1 ) ) )
            // InternalXpath.g:3862:1: ( ( rule__Element__NameAssignment_1 ) )
            {
            // InternalXpath.g:3862:1: ( ( rule__Element__NameAssignment_1 ) )
            // InternalXpath.g:3863:2: ( rule__Element__NameAssignment_1 )
            {
             before(grammarAccess.getElementAccess().getNameAssignment_1()); 
            // InternalXpath.g:3864:2: ( rule__Element__NameAssignment_1 )
            // InternalXpath.g:3864:3: rule__Element__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Element__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getElementAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Group__1__Impl"


    // $ANTLR start "rule__SumFunction__Group__0"
    // InternalXpath.g:3873:1: rule__SumFunction__Group__0 : rule__SumFunction__Group__0__Impl rule__SumFunction__Group__1 ;
    public final void rule__SumFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3877:1: ( rule__SumFunction__Group__0__Impl rule__SumFunction__Group__1 )
            // InternalXpath.g:3878:2: rule__SumFunction__Group__0__Impl rule__SumFunction__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__SumFunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__0"


    // $ANTLR start "rule__SumFunction__Group__0__Impl"
    // InternalXpath.g:3885:1: rule__SumFunction__Group__0__Impl : ( 'sum' ) ;
    public final void rule__SumFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3889:1: ( ( 'sum' ) )
            // InternalXpath.g:3890:1: ( 'sum' )
            {
            // InternalXpath.g:3890:1: ( 'sum' )
            // InternalXpath.g:3891:2: 'sum'
            {
             before(grammarAccess.getSumFunctionAccess().getSumKeyword_0()); 
            match(input,50,FOLLOW_2); 
             after(grammarAccess.getSumFunctionAccess().getSumKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__0__Impl"


    // $ANTLR start "rule__SumFunction__Group__1"
    // InternalXpath.g:3900:1: rule__SumFunction__Group__1 : rule__SumFunction__Group__1__Impl rule__SumFunction__Group__2 ;
    public final void rule__SumFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3904:1: ( rule__SumFunction__Group__1__Impl rule__SumFunction__Group__2 )
            // InternalXpath.g:3905:2: rule__SumFunction__Group__1__Impl rule__SumFunction__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__SumFunction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__1"


    // $ANTLR start "rule__SumFunction__Group__1__Impl"
    // InternalXpath.g:3912:1: rule__SumFunction__Group__1__Impl : ( '(' ) ;
    public final void rule__SumFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3916:1: ( ( '(' ) )
            // InternalXpath.g:3917:1: ( '(' )
            {
            // InternalXpath.g:3917:1: ( '(' )
            // InternalXpath.g:3918:2: '('
            {
             before(grammarAccess.getSumFunctionAccess().getLeftParenthesisKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getSumFunctionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__1__Impl"


    // $ANTLR start "rule__SumFunction__Group__2"
    // InternalXpath.g:3927:1: rule__SumFunction__Group__2 : rule__SumFunction__Group__2__Impl rule__SumFunction__Group__3 ;
    public final void rule__SumFunction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3931:1: ( rule__SumFunction__Group__2__Impl rule__SumFunction__Group__3 )
            // InternalXpath.g:3932:2: rule__SumFunction__Group__2__Impl rule__SumFunction__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__SumFunction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__2"


    // $ANTLR start "rule__SumFunction__Group__2__Impl"
    // InternalXpath.g:3939:1: rule__SumFunction__Group__2__Impl : ( ( rule__SumFunction__SumElementAssignment_2 ) ) ;
    public final void rule__SumFunction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3943:1: ( ( ( rule__SumFunction__SumElementAssignment_2 ) ) )
            // InternalXpath.g:3944:1: ( ( rule__SumFunction__SumElementAssignment_2 ) )
            {
            // InternalXpath.g:3944:1: ( ( rule__SumFunction__SumElementAssignment_2 ) )
            // InternalXpath.g:3945:2: ( rule__SumFunction__SumElementAssignment_2 )
            {
             before(grammarAccess.getSumFunctionAccess().getSumElementAssignment_2()); 
            // InternalXpath.g:3946:2: ( rule__SumFunction__SumElementAssignment_2 )
            // InternalXpath.g:3946:3: rule__SumFunction__SumElementAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SumFunction__SumElementAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSumFunctionAccess().getSumElementAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__2__Impl"


    // $ANTLR start "rule__SumFunction__Group__3"
    // InternalXpath.g:3954:1: rule__SumFunction__Group__3 : rule__SumFunction__Group__3__Impl rule__SumFunction__Group__4 ;
    public final void rule__SumFunction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3958:1: ( rule__SumFunction__Group__3__Impl rule__SumFunction__Group__4 )
            // InternalXpath.g:3959:2: rule__SumFunction__Group__3__Impl rule__SumFunction__Group__4
            {
            pushFollow(FOLLOW_35);
            rule__SumFunction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__3"


    // $ANTLR start "rule__SumFunction__Group__3__Impl"
    // InternalXpath.g:3966:1: rule__SumFunction__Group__3__Impl : ( ')' ) ;
    public final void rule__SumFunction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3970:1: ( ( ')' ) )
            // InternalXpath.g:3971:1: ( ')' )
            {
            // InternalXpath.g:3971:1: ( ')' )
            // InternalXpath.g:3972:2: ')'
            {
             before(grammarAccess.getSumFunctionAccess().getRightParenthesisKeyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getSumFunctionAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__3__Impl"


    // $ANTLR start "rule__SumFunction__Group__4"
    // InternalXpath.g:3981:1: rule__SumFunction__Group__4 : rule__SumFunction__Group__4__Impl rule__SumFunction__Group__5 ;
    public final void rule__SumFunction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3985:1: ( rule__SumFunction__Group__4__Impl rule__SumFunction__Group__5 )
            // InternalXpath.g:3986:2: rule__SumFunction__Group__4__Impl rule__SumFunction__Group__5
            {
            pushFollow(FOLLOW_34);
            rule__SumFunction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__4"


    // $ANTLR start "rule__SumFunction__Group__4__Impl"
    // InternalXpath.g:3993:1: rule__SumFunction__Group__4__Impl : ( '=' ) ;
    public final void rule__SumFunction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:3997:1: ( ( '=' ) )
            // InternalXpath.g:3998:1: ( '=' )
            {
            // InternalXpath.g:3998:1: ( '=' )
            // InternalXpath.g:3999:2: '='
            {
             before(grammarAccess.getSumFunctionAccess().getEqualsSignKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSumFunctionAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__4__Impl"


    // $ANTLR start "rule__SumFunction__Group__5"
    // InternalXpath.g:4008:1: rule__SumFunction__Group__5 : rule__SumFunction__Group__5__Impl ;
    public final void rule__SumFunction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4012:1: ( rule__SumFunction__Group__5__Impl )
            // InternalXpath.g:4013:2: rule__SumFunction__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SumFunction__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__5"


    // $ANTLR start "rule__SumFunction__Group__5__Impl"
    // InternalXpath.g:4019:1: rule__SumFunction__Group__5__Impl : ( ( rule__SumFunction__ResultElementAssignment_5 ) ) ;
    public final void rule__SumFunction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4023:1: ( ( ( rule__SumFunction__ResultElementAssignment_5 ) ) )
            // InternalXpath.g:4024:1: ( ( rule__SumFunction__ResultElementAssignment_5 ) )
            {
            // InternalXpath.g:4024:1: ( ( rule__SumFunction__ResultElementAssignment_5 ) )
            // InternalXpath.g:4025:2: ( rule__SumFunction__ResultElementAssignment_5 )
            {
             before(grammarAccess.getSumFunctionAccess().getResultElementAssignment_5()); 
            // InternalXpath.g:4026:2: ( rule__SumFunction__ResultElementAssignment_5 )
            // InternalXpath.g:4026:3: rule__SumFunction__ResultElementAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__SumFunction__ResultElementAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getSumFunctionAccess().getResultElementAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__Group__5__Impl"


    // $ANTLR start "rule__CountFunction__Group__0"
    // InternalXpath.g:4035:1: rule__CountFunction__Group__0 : rule__CountFunction__Group__0__Impl rule__CountFunction__Group__1 ;
    public final void rule__CountFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4039:1: ( rule__CountFunction__Group__0__Impl rule__CountFunction__Group__1 )
            // InternalXpath.g:4040:2: rule__CountFunction__Group__0__Impl rule__CountFunction__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__CountFunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__0"


    // $ANTLR start "rule__CountFunction__Group__0__Impl"
    // InternalXpath.g:4047:1: rule__CountFunction__Group__0__Impl : ( 'count' ) ;
    public final void rule__CountFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4051:1: ( ( 'count' ) )
            // InternalXpath.g:4052:1: ( 'count' )
            {
            // InternalXpath.g:4052:1: ( 'count' )
            // InternalXpath.g:4053:2: 'count'
            {
             before(grammarAccess.getCountFunctionAccess().getCountKeyword_0()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getCountFunctionAccess().getCountKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__0__Impl"


    // $ANTLR start "rule__CountFunction__Group__1"
    // InternalXpath.g:4062:1: rule__CountFunction__Group__1 : rule__CountFunction__Group__1__Impl rule__CountFunction__Group__2 ;
    public final void rule__CountFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4066:1: ( rule__CountFunction__Group__1__Impl rule__CountFunction__Group__2 )
            // InternalXpath.g:4067:2: rule__CountFunction__Group__1__Impl rule__CountFunction__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__CountFunction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__1"


    // $ANTLR start "rule__CountFunction__Group__1__Impl"
    // InternalXpath.g:4074:1: rule__CountFunction__Group__1__Impl : ( '(' ) ;
    public final void rule__CountFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4078:1: ( ( '(' ) )
            // InternalXpath.g:4079:1: ( '(' )
            {
            // InternalXpath.g:4079:1: ( '(' )
            // InternalXpath.g:4080:2: '('
            {
             before(grammarAccess.getCountFunctionAccess().getLeftParenthesisKeyword_1()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getCountFunctionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__1__Impl"


    // $ANTLR start "rule__CountFunction__Group__2"
    // InternalXpath.g:4089:1: rule__CountFunction__Group__2 : rule__CountFunction__Group__2__Impl rule__CountFunction__Group__3 ;
    public final void rule__CountFunction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4093:1: ( rule__CountFunction__Group__2__Impl rule__CountFunction__Group__3 )
            // InternalXpath.g:4094:2: rule__CountFunction__Group__2__Impl rule__CountFunction__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__CountFunction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__2"


    // $ANTLR start "rule__CountFunction__Group__2__Impl"
    // InternalXpath.g:4101:1: rule__CountFunction__Group__2__Impl : ( ( rule__CountFunction__CountElementAssignment_2 ) ) ;
    public final void rule__CountFunction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4105:1: ( ( ( rule__CountFunction__CountElementAssignment_2 ) ) )
            // InternalXpath.g:4106:1: ( ( rule__CountFunction__CountElementAssignment_2 ) )
            {
            // InternalXpath.g:4106:1: ( ( rule__CountFunction__CountElementAssignment_2 ) )
            // InternalXpath.g:4107:2: ( rule__CountFunction__CountElementAssignment_2 )
            {
             before(grammarAccess.getCountFunctionAccess().getCountElementAssignment_2()); 
            // InternalXpath.g:4108:2: ( rule__CountFunction__CountElementAssignment_2 )
            // InternalXpath.g:4108:3: rule__CountFunction__CountElementAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CountFunction__CountElementAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCountFunctionAccess().getCountElementAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__2__Impl"


    // $ANTLR start "rule__CountFunction__Group__3"
    // InternalXpath.g:4116:1: rule__CountFunction__Group__3 : rule__CountFunction__Group__3__Impl rule__CountFunction__Group__4 ;
    public final void rule__CountFunction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4120:1: ( rule__CountFunction__Group__3__Impl rule__CountFunction__Group__4 )
            // InternalXpath.g:4121:2: rule__CountFunction__Group__3__Impl rule__CountFunction__Group__4
            {
            pushFollow(FOLLOW_35);
            rule__CountFunction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__3"


    // $ANTLR start "rule__CountFunction__Group__3__Impl"
    // InternalXpath.g:4128:1: rule__CountFunction__Group__3__Impl : ( ')' ) ;
    public final void rule__CountFunction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4132:1: ( ( ')' ) )
            // InternalXpath.g:4133:1: ( ')' )
            {
            // InternalXpath.g:4133:1: ( ')' )
            // InternalXpath.g:4134:2: ')'
            {
             before(grammarAccess.getCountFunctionAccess().getRightParenthesisKeyword_3()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getCountFunctionAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__3__Impl"


    // $ANTLR start "rule__CountFunction__Group__4"
    // InternalXpath.g:4143:1: rule__CountFunction__Group__4 : rule__CountFunction__Group__4__Impl rule__CountFunction__Group__5 ;
    public final void rule__CountFunction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4147:1: ( rule__CountFunction__Group__4__Impl rule__CountFunction__Group__5 )
            // InternalXpath.g:4148:2: rule__CountFunction__Group__4__Impl rule__CountFunction__Group__5
            {
            pushFollow(FOLLOW_34);
            rule__CountFunction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__4"


    // $ANTLR start "rule__CountFunction__Group__4__Impl"
    // InternalXpath.g:4155:1: rule__CountFunction__Group__4__Impl : ( '=' ) ;
    public final void rule__CountFunction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4159:1: ( ( '=' ) )
            // InternalXpath.g:4160:1: ( '=' )
            {
            // InternalXpath.g:4160:1: ( '=' )
            // InternalXpath.g:4161:2: '='
            {
             before(grammarAccess.getCountFunctionAccess().getEqualsSignKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCountFunctionAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__4__Impl"


    // $ANTLR start "rule__CountFunction__Group__5"
    // InternalXpath.g:4170:1: rule__CountFunction__Group__5 : rule__CountFunction__Group__5__Impl ;
    public final void rule__CountFunction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4174:1: ( rule__CountFunction__Group__5__Impl )
            // InternalXpath.g:4175:2: rule__CountFunction__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CountFunction__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__5"


    // $ANTLR start "rule__CountFunction__Group__5__Impl"
    // InternalXpath.g:4181:1: rule__CountFunction__Group__5__Impl : ( ( rule__CountFunction__ResultElementAssignment_5 ) ) ;
    public final void rule__CountFunction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4185:1: ( ( ( rule__CountFunction__ResultElementAssignment_5 ) ) )
            // InternalXpath.g:4186:1: ( ( rule__CountFunction__ResultElementAssignment_5 ) )
            {
            // InternalXpath.g:4186:1: ( ( rule__CountFunction__ResultElementAssignment_5 ) )
            // InternalXpath.g:4187:2: ( rule__CountFunction__ResultElementAssignment_5 )
            {
             before(grammarAccess.getCountFunctionAccess().getResultElementAssignment_5()); 
            // InternalXpath.g:4188:2: ( rule__CountFunction__ResultElementAssignment_5 )
            // InternalXpath.g:4188:3: rule__CountFunction__ResultElementAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__CountFunction__ResultElementAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getCountFunctionAccess().getResultElementAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__Group__5__Impl"


    // $ANTLR start "rule__ThenElse__Group_0__0"
    // InternalXpath.g:4197:1: rule__ThenElse__Group_0__0 : rule__ThenElse__Group_0__0__Impl rule__ThenElse__Group_0__1 ;
    public final void rule__ThenElse__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4201:1: ( rule__ThenElse__Group_0__0__Impl rule__ThenElse__Group_0__1 )
            // InternalXpath.g:4202:2: rule__ThenElse__Group_0__0__Impl rule__ThenElse__Group_0__1
            {
            pushFollow(FOLLOW_36);
            rule__ThenElse__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__0"


    // $ANTLR start "rule__ThenElse__Group_0__0__Impl"
    // InternalXpath.g:4209:1: rule__ThenElse__Group_0__0__Impl : ( 'then' ) ;
    public final void rule__ThenElse__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4213:1: ( ( 'then' ) )
            // InternalXpath.g:4214:1: ( 'then' )
            {
            // InternalXpath.g:4214:1: ( 'then' )
            // InternalXpath.g:4215:2: 'then'
            {
             before(grammarAccess.getThenElseAccess().getThenKeyword_0_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getThenElseAccess().getThenKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__0__Impl"


    // $ANTLR start "rule__ThenElse__Group_0__1"
    // InternalXpath.g:4224:1: rule__ThenElse__Group_0__1 : rule__ThenElse__Group_0__1__Impl rule__ThenElse__Group_0__2 ;
    public final void rule__ThenElse__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4228:1: ( rule__ThenElse__Group_0__1__Impl rule__ThenElse__Group_0__2 )
            // InternalXpath.g:4229:2: rule__ThenElse__Group_0__1__Impl rule__ThenElse__Group_0__2
            {
            pushFollow(FOLLOW_37);
            rule__ThenElse__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__1"


    // $ANTLR start "rule__ThenElse__Group_0__1__Impl"
    // InternalXpath.g:4236:1: rule__ThenElse__Group_0__1__Impl : ( ( rule__ThenElse__ThenExprTrueAssignment_0_1 ) ) ;
    public final void rule__ThenElse__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4240:1: ( ( ( rule__ThenElse__ThenExprTrueAssignment_0_1 ) ) )
            // InternalXpath.g:4241:1: ( ( rule__ThenElse__ThenExprTrueAssignment_0_1 ) )
            {
            // InternalXpath.g:4241:1: ( ( rule__ThenElse__ThenExprTrueAssignment_0_1 ) )
            // InternalXpath.g:4242:2: ( rule__ThenElse__ThenExprTrueAssignment_0_1 )
            {
             before(grammarAccess.getThenElseAccess().getThenExprTrueAssignment_0_1()); 
            // InternalXpath.g:4243:2: ( rule__ThenElse__ThenExprTrueAssignment_0_1 )
            // InternalXpath.g:4243:3: rule__ThenElse__ThenExprTrueAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__ThenExprTrueAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getThenElseAccess().getThenExprTrueAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__1__Impl"


    // $ANTLR start "rule__ThenElse__Group_0__2"
    // InternalXpath.g:4251:1: rule__ThenElse__Group_0__2 : rule__ThenElse__Group_0__2__Impl rule__ThenElse__Group_0__3 ;
    public final void rule__ThenElse__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4255:1: ( rule__ThenElse__Group_0__2__Impl rule__ThenElse__Group_0__3 )
            // InternalXpath.g:4256:2: rule__ThenElse__Group_0__2__Impl rule__ThenElse__Group_0__3
            {
            pushFollow(FOLLOW_38);
            rule__ThenElse__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__2"


    // $ANTLR start "rule__ThenElse__Group_0__2__Impl"
    // InternalXpath.g:4263:1: rule__ThenElse__Group_0__2__Impl : ( 'else' ) ;
    public final void rule__ThenElse__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4267:1: ( ( 'else' ) )
            // InternalXpath.g:4268:1: ( 'else' )
            {
            // InternalXpath.g:4268:1: ( 'else' )
            // InternalXpath.g:4269:2: 'else'
            {
             before(grammarAccess.getThenElseAccess().getElseKeyword_0_2()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getThenElseAccess().getElseKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__2__Impl"


    // $ANTLR start "rule__ThenElse__Group_0__3"
    // InternalXpath.g:4278:1: rule__ThenElse__Group_0__3 : rule__ThenElse__Group_0__3__Impl ;
    public final void rule__ThenElse__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4282:1: ( rule__ThenElse__Group_0__3__Impl )
            // InternalXpath.g:4283:2: rule__ThenElse__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__3"


    // $ANTLR start "rule__ThenElse__Group_0__3__Impl"
    // InternalXpath.g:4289:1: rule__ThenElse__Group_0__3__Impl : ( ( rule__ThenElse__ElseExprFalseAssignment_0_3 ) ) ;
    public final void rule__ThenElse__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4293:1: ( ( ( rule__ThenElse__ElseExprFalseAssignment_0_3 ) ) )
            // InternalXpath.g:4294:1: ( ( rule__ThenElse__ElseExprFalseAssignment_0_3 ) )
            {
            // InternalXpath.g:4294:1: ( ( rule__ThenElse__ElseExprFalseAssignment_0_3 ) )
            // InternalXpath.g:4295:2: ( rule__ThenElse__ElseExprFalseAssignment_0_3 )
            {
             before(grammarAccess.getThenElseAccess().getElseExprFalseAssignment_0_3()); 
            // InternalXpath.g:4296:2: ( rule__ThenElse__ElseExprFalseAssignment_0_3 )
            // InternalXpath.g:4296:3: rule__ThenElse__ElseExprFalseAssignment_0_3
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__ElseExprFalseAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getThenElseAccess().getElseExprFalseAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_0__3__Impl"


    // $ANTLR start "rule__ThenElse__Group_1__0"
    // InternalXpath.g:4305:1: rule__ThenElse__Group_1__0 : rule__ThenElse__Group_1__0__Impl rule__ThenElse__Group_1__1 ;
    public final void rule__ThenElse__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4309:1: ( rule__ThenElse__Group_1__0__Impl rule__ThenElse__Group_1__1 )
            // InternalXpath.g:4310:2: rule__ThenElse__Group_1__0__Impl rule__ThenElse__Group_1__1
            {
            pushFollow(FOLLOW_38);
            rule__ThenElse__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__0"


    // $ANTLR start "rule__ThenElse__Group_1__0__Impl"
    // InternalXpath.g:4317:1: rule__ThenElse__Group_1__0__Impl : ( 'then' ) ;
    public final void rule__ThenElse__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4321:1: ( ( 'then' ) )
            // InternalXpath.g:4322:1: ( 'then' )
            {
            // InternalXpath.g:4322:1: ( 'then' )
            // InternalXpath.g:4323:2: 'then'
            {
             before(grammarAccess.getThenElseAccess().getThenKeyword_1_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getThenElseAccess().getThenKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__0__Impl"


    // $ANTLR start "rule__ThenElse__Group_1__1"
    // InternalXpath.g:4332:1: rule__ThenElse__Group_1__1 : rule__ThenElse__Group_1__1__Impl rule__ThenElse__Group_1__2 ;
    public final void rule__ThenElse__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4336:1: ( rule__ThenElse__Group_1__1__Impl rule__ThenElse__Group_1__2 )
            // InternalXpath.g:4337:2: rule__ThenElse__Group_1__1__Impl rule__ThenElse__Group_1__2
            {
            pushFollow(FOLLOW_37);
            rule__ThenElse__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__1"


    // $ANTLR start "rule__ThenElse__Group_1__1__Impl"
    // InternalXpath.g:4344:1: rule__ThenElse__Group_1__1__Impl : ( ( rule__ThenElse__ThenExprFalseAssignment_1_1 ) ) ;
    public final void rule__ThenElse__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4348:1: ( ( ( rule__ThenElse__ThenExprFalseAssignment_1_1 ) ) )
            // InternalXpath.g:4349:1: ( ( rule__ThenElse__ThenExprFalseAssignment_1_1 ) )
            {
            // InternalXpath.g:4349:1: ( ( rule__ThenElse__ThenExprFalseAssignment_1_1 ) )
            // InternalXpath.g:4350:2: ( rule__ThenElse__ThenExprFalseAssignment_1_1 )
            {
             before(grammarAccess.getThenElseAccess().getThenExprFalseAssignment_1_1()); 
            // InternalXpath.g:4351:2: ( rule__ThenElse__ThenExprFalseAssignment_1_1 )
            // InternalXpath.g:4351:3: rule__ThenElse__ThenExprFalseAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__ThenExprFalseAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getThenElseAccess().getThenExprFalseAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__1__Impl"


    // $ANTLR start "rule__ThenElse__Group_1__2"
    // InternalXpath.g:4359:1: rule__ThenElse__Group_1__2 : rule__ThenElse__Group_1__2__Impl rule__ThenElse__Group_1__3 ;
    public final void rule__ThenElse__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4363:1: ( rule__ThenElse__Group_1__2__Impl rule__ThenElse__Group_1__3 )
            // InternalXpath.g:4364:2: rule__ThenElse__Group_1__2__Impl rule__ThenElse__Group_1__3
            {
            pushFollow(FOLLOW_36);
            rule__ThenElse__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__2"


    // $ANTLR start "rule__ThenElse__Group_1__2__Impl"
    // InternalXpath.g:4371:1: rule__ThenElse__Group_1__2__Impl : ( 'else' ) ;
    public final void rule__ThenElse__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4375:1: ( ( 'else' ) )
            // InternalXpath.g:4376:1: ( 'else' )
            {
            // InternalXpath.g:4376:1: ( 'else' )
            // InternalXpath.g:4377:2: 'else'
            {
             before(grammarAccess.getThenElseAccess().getElseKeyword_1_2()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getThenElseAccess().getElseKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__2__Impl"


    // $ANTLR start "rule__ThenElse__Group_1__3"
    // InternalXpath.g:4386:1: rule__ThenElse__Group_1__3 : rule__ThenElse__Group_1__3__Impl ;
    public final void rule__ThenElse__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4390:1: ( rule__ThenElse__Group_1__3__Impl )
            // InternalXpath.g:4391:2: rule__ThenElse__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__3"


    // $ANTLR start "rule__ThenElse__Group_1__3__Impl"
    // InternalXpath.g:4397:1: rule__ThenElse__Group_1__3__Impl : ( ( rule__ThenElse__ElseExprTrueAssignment_1_3 ) ) ;
    public final void rule__ThenElse__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4401:1: ( ( ( rule__ThenElse__ElseExprTrueAssignment_1_3 ) ) )
            // InternalXpath.g:4402:1: ( ( rule__ThenElse__ElseExprTrueAssignment_1_3 ) )
            {
            // InternalXpath.g:4402:1: ( ( rule__ThenElse__ElseExprTrueAssignment_1_3 ) )
            // InternalXpath.g:4403:2: ( rule__ThenElse__ElseExprTrueAssignment_1_3 )
            {
             before(grammarAccess.getThenElseAccess().getElseExprTrueAssignment_1_3()); 
            // InternalXpath.g:4404:2: ( rule__ThenElse__ElseExprTrueAssignment_1_3 )
            // InternalXpath.g:4404:3: rule__ThenElse__ElseExprTrueAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__ThenElse__ElseExprTrueAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getThenElseAccess().getElseExprTrueAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__Group_1__3__Impl"


    // $ANTLR start "rule__Domainmodel__ValidationPathAssignment_0"
    // InternalXpath.g:4413:1: rule__Domainmodel__ValidationPathAssignment_0 : ( ruleValidationPath ) ;
    public final void rule__Domainmodel__ValidationPathAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4417:1: ( ( ruleValidationPath ) )
            // InternalXpath.g:4418:2: ( ruleValidationPath )
            {
            // InternalXpath.g:4418:2: ( ruleValidationPath )
            // InternalXpath.g:4419:3: ruleValidationPath
            {
             before(grammarAccess.getDomainmodelAccess().getValidationPathValidationPathParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleValidationPath();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getValidationPathValidationPathParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ValidationPathAssignment_0"


    // $ANTLR start "rule__Domainmodel__ElementsAssignment_2"
    // InternalXpath.g:4428:1: rule__Domainmodel__ElementsAssignment_2 : ( ruleXPath ) ;
    public final void rule__Domainmodel__ElementsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4432:1: ( ( ruleXPath ) )
            // InternalXpath.g:4433:2: ( ruleXPath )
            {
            // InternalXpath.g:4433:2: ( ruleXPath )
            // InternalXpath.g:4434:3: ruleXPath
            {
             before(grammarAccess.getDomainmodelAccess().getElementsXPathParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleXPath();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getElementsXPathParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ElementsAssignment_2"


    // $ANTLR start "rule__ValidationPath__PathAssignment_0"
    // InternalXpath.g:4443:1: rule__ValidationPath__PathAssignment_0 : ( RULE_ID ) ;
    public final void rule__ValidationPath__PathAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4447:1: ( ( RULE_ID ) )
            // InternalXpath.g:4448:2: ( RULE_ID )
            {
            // InternalXpath.g:4448:2: ( RULE_ID )
            // InternalXpath.g:4449:3: RULE_ID
            {
             before(grammarAccess.getValidationPathAccess().getPathIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getValidationPathAccess().getPathIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__PathAssignment_0"


    // $ANTLR start "rule__ValidationPath__Path_listAssignment_1_1"
    // InternalXpath.g:4458:1: rule__ValidationPath__Path_listAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__ValidationPath__Path_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4462:1: ( ( RULE_ID ) )
            // InternalXpath.g:4463:2: ( RULE_ID )
            {
            // InternalXpath.g:4463:2: ( RULE_ID )
            // InternalXpath.g:4464:3: RULE_ID
            {
             before(grammarAccess.getValidationPathAccess().getPath_listIDTerminalRuleCall_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getValidationPathAccess().getPath_listIDTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValidationPath__Path_listAssignment_1_1"


    // $ANTLR start "rule__XPath__ExprAssignment"
    // InternalXpath.g:4473:1: rule__XPath__ExprAssignment : ( ruleExpr ) ;
    public final void rule__XPath__ExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4477:1: ( ( ruleExpr ) )
            // InternalXpath.g:4478:2: ( ruleExpr )
            {
            // InternalXpath.g:4478:2: ( ruleExpr )
            // InternalXpath.g:4479:3: ruleExpr
            {
             before(grammarAccess.getXPathAccess().getExprExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExpr();

            state._fsp--;

             after(grammarAccess.getXPathAccess().getExprExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XPath__ExprAssignment"


    // $ANTLR start "rule__Expr__ExprSingleAssignment_0"
    // InternalXpath.g:4488:1: rule__Expr__ExprSingleAssignment_0 : ( ruleExprSingle ) ;
    public final void rule__Expr__ExprSingleAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4492:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4493:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4493:2: ( ruleExprSingle )
            // InternalXpath.g:4494:3: ruleExprSingle
            {
             before(grammarAccess.getExprAccess().getExprSingleExprSingleParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getExprAccess().getExprSingleExprSingleParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__ExprSingleAssignment_0"


    // $ANTLR start "rule__Expr__Multi_listAssignment_1_1"
    // InternalXpath.g:4503:1: rule__Expr__Multi_listAssignment_1_1 : ( ruleExprSingle ) ;
    public final void rule__Expr__Multi_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4507:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4508:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4508:2: ( ruleExprSingle )
            // InternalXpath.g:4509:3: ruleExprSingle
            {
             before(grammarAccess.getExprAccess().getMulti_listExprSingleParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getExprAccess().getMulti_listExprSingleParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expr__Multi_listAssignment_1_1"


    // $ANTLR start "rule__ExprSingle__ForExprAssignment_0"
    // InternalXpath.g:4518:1: rule__ExprSingle__ForExprAssignment_0 : ( ruleForExpr ) ;
    public final void rule__ExprSingle__ForExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4522:1: ( ( ruleForExpr ) )
            // InternalXpath.g:4523:2: ( ruleForExpr )
            {
            // InternalXpath.g:4523:2: ( ruleForExpr )
            // InternalXpath.g:4524:3: ruleForExpr
            {
             before(grammarAccess.getExprSingleAccess().getForExprForExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleForExpr();

            state._fsp--;

             after(grammarAccess.getExprSingleAccess().getForExprForExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExprSingle__ForExprAssignment_0"


    // $ANTLR start "rule__ExprSingle__QuantExprAssignment_1"
    // InternalXpath.g:4533:1: rule__ExprSingle__QuantExprAssignment_1 : ( ruleQuantifiedExpr ) ;
    public final void rule__ExprSingle__QuantExprAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4537:1: ( ( ruleQuantifiedExpr ) )
            // InternalXpath.g:4538:2: ( ruleQuantifiedExpr )
            {
            // InternalXpath.g:4538:2: ( ruleQuantifiedExpr )
            // InternalXpath.g:4539:3: ruleQuantifiedExpr
            {
             before(grammarAccess.getExprSingleAccess().getQuantExprQuantifiedExprParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQuantifiedExpr();

            state._fsp--;

             after(grammarAccess.getExprSingleAccess().getQuantExprQuantifiedExprParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExprSingle__QuantExprAssignment_1"


    // $ANTLR start "rule__ExprSingle__IfExprAssignment_2"
    // InternalXpath.g:4548:1: rule__ExprSingle__IfExprAssignment_2 : ( ruleIfExpr ) ;
    public final void rule__ExprSingle__IfExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4552:1: ( ( ruleIfExpr ) )
            // InternalXpath.g:4553:2: ( ruleIfExpr )
            {
            // InternalXpath.g:4553:2: ( ruleIfExpr )
            // InternalXpath.g:4554:3: ruleIfExpr
            {
             before(grammarAccess.getExprSingleAccess().getIfExprIfExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleIfExpr();

            state._fsp--;

             after(grammarAccess.getExprSingleAccess().getIfExprIfExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExprSingle__IfExprAssignment_2"


    // $ANTLR start "rule__ExprSingle__OrExprAssignment_3"
    // InternalXpath.g:4563:1: rule__ExprSingle__OrExprAssignment_3 : ( ruleOrExpr ) ;
    public final void rule__ExprSingle__OrExprAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4567:1: ( ( ruleOrExpr ) )
            // InternalXpath.g:4568:2: ( ruleOrExpr )
            {
            // InternalXpath.g:4568:2: ( ruleOrExpr )
            // InternalXpath.g:4569:3: ruleOrExpr
            {
             before(grammarAccess.getExprSingleAccess().getOrExprOrExprParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getExprSingleAccess().getOrExprOrExprParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExprSingle__OrExprAssignment_3"


    // $ANTLR start "rule__ForExpr__SimpleForClauseAssignment_0"
    // InternalXpath.g:4578:1: rule__ForExpr__SimpleForClauseAssignment_0 : ( ruleSimpleForClause ) ;
    public final void rule__ForExpr__SimpleForClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4582:1: ( ( ruleSimpleForClause ) )
            // InternalXpath.g:4583:2: ( ruleSimpleForClause )
            {
            // InternalXpath.g:4583:2: ( ruleSimpleForClause )
            // InternalXpath.g:4584:3: ruleSimpleForClause
            {
             before(grammarAccess.getForExprAccess().getSimpleForClauseSimpleForClauseParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleSimpleForClause();

            state._fsp--;

             after(grammarAccess.getForExprAccess().getSimpleForClauseSimpleForClauseParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__SimpleForClauseAssignment_0"


    // $ANTLR start "rule__ForExpr__ExprSingleAssignment_2"
    // InternalXpath.g:4593:1: rule__ForExpr__ExprSingleAssignment_2 : ( ruleExprSingle ) ;
    public final void rule__ForExpr__ExprSingleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4597:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4598:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4598:2: ( ruleExprSingle )
            // InternalXpath.g:4599:3: ruleExprSingle
            {
             before(grammarAccess.getForExprAccess().getExprSingleExprSingleParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getForExprAccess().getExprSingleExprSingleParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForExpr__ExprSingleAssignment_2"


    // $ANTLR start "rule__SimpleForClause__VarnameAssignment_2"
    // InternalXpath.g:4608:1: rule__SimpleForClause__VarnameAssignment_2 : ( ruleVarName ) ;
    public final void rule__SimpleForClause__VarnameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4612:1: ( ( ruleVarName ) )
            // InternalXpath.g:4613:2: ( ruleVarName )
            {
            // InternalXpath.g:4613:2: ( ruleVarName )
            // InternalXpath.g:4614:3: ruleVarName
            {
             before(grammarAccess.getSimpleForClauseAccess().getVarnameVarNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVarName();

            state._fsp--;

             after(grammarAccess.getSimpleForClauseAccess().getVarnameVarNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__VarnameAssignment_2"


    // $ANTLR start "rule__SimpleForClause__ExprSingleAssignment_4"
    // InternalXpath.g:4623:1: rule__SimpleForClause__ExprSingleAssignment_4 : ( ruleExprSingle ) ;
    public final void rule__SimpleForClause__ExprSingleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4627:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4628:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4628:2: ( ruleExprSingle )
            // InternalXpath.g:4629:3: ruleExprSingle
            {
             before(grammarAccess.getSimpleForClauseAccess().getExprSingleExprSingleParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getSimpleForClauseAccess().getExprSingleExprSingleParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__ExprSingleAssignment_4"


    // $ANTLR start "rule__SimpleForClause__Varname_listAssignment_5_2"
    // InternalXpath.g:4638:1: rule__SimpleForClause__Varname_listAssignment_5_2 : ( ruleVarName ) ;
    public final void rule__SimpleForClause__Varname_listAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4642:1: ( ( ruleVarName ) )
            // InternalXpath.g:4643:2: ( ruleVarName )
            {
            // InternalXpath.g:4643:2: ( ruleVarName )
            // InternalXpath.g:4644:3: ruleVarName
            {
             before(grammarAccess.getSimpleForClauseAccess().getVarname_listVarNameParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVarName();

            state._fsp--;

             after(grammarAccess.getSimpleForClauseAccess().getVarname_listVarNameParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__Varname_listAssignment_5_2"


    // $ANTLR start "rule__SimpleForClause__ExprSingle_listAssignment_5_4"
    // InternalXpath.g:4653:1: rule__SimpleForClause__ExprSingle_listAssignment_5_4 : ( ruleExprSingle ) ;
    public final void rule__SimpleForClause__ExprSingle_listAssignment_5_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4657:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4658:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4658:2: ( ruleExprSingle )
            // InternalXpath.g:4659:3: ruleExprSingle
            {
             before(grammarAccess.getSimpleForClauseAccess().getExprSingle_listExprSingleParserRuleCall_5_4_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getSimpleForClauseAccess().getExprSingle_listExprSingleParserRuleCall_5_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleForClause__ExprSingle_listAssignment_5_4"


    // $ANTLR start "rule__QuantifiedExpr__QuantiAssignment_0_0"
    // InternalXpath.g:4668:1: rule__QuantifiedExpr__QuantiAssignment_0_0 : ( ( 'some' ) ) ;
    public final void rule__QuantifiedExpr__QuantiAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4672:1: ( ( ( 'some' ) ) )
            // InternalXpath.g:4673:2: ( ( 'some' ) )
            {
            // InternalXpath.g:4673:2: ( ( 'some' ) )
            // InternalXpath.g:4674:3: ( 'some' )
            {
             before(grammarAccess.getQuantifiedExprAccess().getQuantiSomeKeyword_0_0_0()); 
            // InternalXpath.g:4675:3: ( 'some' )
            // InternalXpath.g:4676:4: 'some'
            {
             before(grammarAccess.getQuantifiedExprAccess().getQuantiSomeKeyword_0_0_0()); 
            match(input,54,FOLLOW_2); 
             after(grammarAccess.getQuantifiedExprAccess().getQuantiSomeKeyword_0_0_0()); 

            }

             after(grammarAccess.getQuantifiedExprAccess().getQuantiSomeKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__QuantiAssignment_0_0"


    // $ANTLR start "rule__QuantifiedExpr__ElementAssignment_2"
    // InternalXpath.g:4687:1: rule__QuantifiedExpr__ElementAssignment_2 : ( ruleVarName ) ;
    public final void rule__QuantifiedExpr__ElementAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4691:1: ( ( ruleVarName ) )
            // InternalXpath.g:4692:2: ( ruleVarName )
            {
            // InternalXpath.g:4692:2: ( ruleVarName )
            // InternalXpath.g:4693:3: ruleVarName
            {
             before(grammarAccess.getQuantifiedExprAccess().getElementVarNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVarName();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprAccess().getElementVarNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__ElementAssignment_2"


    // $ANTLR start "rule__QuantifiedExpr__CollectionAssignment_4"
    // InternalXpath.g:4702:1: rule__QuantifiedExpr__CollectionAssignment_4 : ( ruleExprSingle ) ;
    public final void rule__QuantifiedExpr__CollectionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4706:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4707:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4707:2: ( ruleExprSingle )
            // InternalXpath.g:4708:3: ruleExprSingle
            {
             before(grammarAccess.getQuantifiedExprAccess().getCollectionExprSingleParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprAccess().getCollectionExprSingleParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__CollectionAssignment_4"


    // $ANTLR start "rule__QuantifiedExpr__Element_listAssignment_5_2"
    // InternalXpath.g:4717:1: rule__QuantifiedExpr__Element_listAssignment_5_2 : ( ruleVarName ) ;
    public final void rule__QuantifiedExpr__Element_listAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4721:1: ( ( ruleVarName ) )
            // InternalXpath.g:4722:2: ( ruleVarName )
            {
            // InternalXpath.g:4722:2: ( ruleVarName )
            // InternalXpath.g:4723:3: ruleVarName
            {
             before(grammarAccess.getQuantifiedExprAccess().getElement_listVarNameParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVarName();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprAccess().getElement_listVarNameParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Element_listAssignment_5_2"


    // $ANTLR start "rule__QuantifiedExpr__Collection_listAssignment_5_4"
    // InternalXpath.g:4732:1: rule__QuantifiedExpr__Collection_listAssignment_5_4 : ( ruleExprSingle ) ;
    public final void rule__QuantifiedExpr__Collection_listAssignment_5_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4736:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4737:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4737:2: ( ruleExprSingle )
            // InternalXpath.g:4738:3: ruleExprSingle
            {
             before(grammarAccess.getQuantifiedExprAccess().getCollection_listExprSingleParserRuleCall_5_4_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprAccess().getCollection_listExprSingleParserRuleCall_5_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__Collection_listAssignment_5_4"


    // $ANTLR start "rule__QuantifiedExpr__ConditionAssignment_7"
    // InternalXpath.g:4747:1: rule__QuantifiedExpr__ConditionAssignment_7 : ( ruleExprSingle ) ;
    public final void rule__QuantifiedExpr__ConditionAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4751:1: ( ( ruleExprSingle ) )
            // InternalXpath.g:4752:2: ( ruleExprSingle )
            {
            // InternalXpath.g:4752:2: ( ruleExprSingle )
            // InternalXpath.g:4753:3: ruleExprSingle
            {
             before(grammarAccess.getQuantifiedExprAccess().getConditionExprSingleParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleExprSingle();

            state._fsp--;

             after(grammarAccess.getQuantifiedExprAccess().getConditionExprSingleParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuantifiedExpr__ConditionAssignment_7"


    // $ANTLR start "rule__IfExpr__ConditionExprAssignment_2"
    // InternalXpath.g:4762:1: rule__IfExpr__ConditionExprAssignment_2 : ( ruleOrExpr ) ;
    public final void rule__IfExpr__ConditionExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4766:1: ( ( ruleOrExpr ) )
            // InternalXpath.g:4767:2: ( ruleOrExpr )
            {
            // InternalXpath.g:4767:2: ( ruleOrExpr )
            // InternalXpath.g:4768:3: ruleOrExpr
            {
             before(grammarAccess.getIfExprAccess().getConditionExprOrExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getIfExprAccess().getConditionExprOrExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__ConditionExprAssignment_2"


    // $ANTLR start "rule__IfExpr__ThenElseAssignment_4"
    // InternalXpath.g:4777:1: rule__IfExpr__ThenElseAssignment_4 : ( ruleThenElse ) ;
    public final void rule__IfExpr__ThenElseAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4781:1: ( ( ruleThenElse ) )
            // InternalXpath.g:4782:2: ( ruleThenElse )
            {
            // InternalXpath.g:4782:2: ( ruleThenElse )
            // InternalXpath.g:4783:3: ruleThenElse
            {
             before(grammarAccess.getIfExprAccess().getThenElseThenElseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleThenElse();

            state._fsp--;

             after(grammarAccess.getIfExprAccess().getThenElseThenElseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfExpr__ThenElseAssignment_4"


    // $ANTLR start "rule__OrExpr__GroupedConditionAssignment_0_1"
    // InternalXpath.g:4792:1: rule__OrExpr__GroupedConditionAssignment_0_1 : ( ruleOrExpr ) ;
    public final void rule__OrExpr__GroupedConditionAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4796:1: ( ( ruleOrExpr ) )
            // InternalXpath.g:4797:2: ( ruleOrExpr )
            {
            // InternalXpath.g:4797:2: ( ruleOrExpr )
            // InternalXpath.g:4798:3: ruleOrExpr
            {
             before(grammarAccess.getOrExprAccess().getGroupedConditionOrExprParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getGroupedConditionOrExprParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__GroupedConditionAssignment_0_1"


    // $ANTLR start "rule__OrExpr__OperatorAssignment_0_3_0_0"
    // InternalXpath.g:4807:1: rule__OrExpr__OperatorAssignment_0_3_0_0 : ( ( 'or' ) ) ;
    public final void rule__OrExpr__OperatorAssignment_0_3_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4811:1: ( ( ( 'or' ) ) )
            // InternalXpath.g:4812:2: ( ( 'or' ) )
            {
            // InternalXpath.g:4812:2: ( ( 'or' ) )
            // InternalXpath.g:4813:3: ( 'or' )
            {
             before(grammarAccess.getOrExprAccess().getOperatorOrKeyword_0_3_0_0_0()); 
            // InternalXpath.g:4814:3: ( 'or' )
            // InternalXpath.g:4815:4: 'or'
            {
             before(grammarAccess.getOrExprAccess().getOperatorOrKeyword_0_3_0_0_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getOperatorOrKeyword_0_3_0_0_0()); 

            }

             after(grammarAccess.getOrExprAccess().getOperatorOrKeyword_0_3_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OperatorAssignment_0_3_0_0"


    // $ANTLR start "rule__OrExpr__OperatorAssignment_0_3_0_1"
    // InternalXpath.g:4826:1: rule__OrExpr__OperatorAssignment_0_3_0_1 : ( ( 'and' ) ) ;
    public final void rule__OrExpr__OperatorAssignment_0_3_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4830:1: ( ( ( 'and' ) ) )
            // InternalXpath.g:4831:2: ( ( 'and' ) )
            {
            // InternalXpath.g:4831:2: ( ( 'and' ) )
            // InternalXpath.g:4832:3: ( 'and' )
            {
             before(grammarAccess.getOrExprAccess().getOperatorAndKeyword_0_3_0_1_0()); 
            // InternalXpath.g:4833:3: ( 'and' )
            // InternalXpath.g:4834:4: 'and'
            {
             before(grammarAccess.getOrExprAccess().getOperatorAndKeyword_0_3_0_1_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getOperatorAndKeyword_0_3_0_1_0()); 

            }

             after(grammarAccess.getOrExprAccess().getOperatorAndKeyword_0_3_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OperatorAssignment_0_3_0_1"


    // $ANTLR start "rule__OrExpr__OrExprAssignment_0_3_1"
    // InternalXpath.g:4845:1: rule__OrExpr__OrExprAssignment_0_3_1 : ( ruleOrExpr ) ;
    public final void rule__OrExpr__OrExprAssignment_0_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4849:1: ( ( ruleOrExpr ) )
            // InternalXpath.g:4850:2: ( ruleOrExpr )
            {
            // InternalXpath.g:4850:2: ( ruleOrExpr )
            // InternalXpath.g:4851:3: ruleOrExpr
            {
             before(grammarAccess.getOrExprAccess().getOrExprOrExprParserRuleCall_0_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getOrExprOrExprParserRuleCall_0_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OrExprAssignment_0_3_1"


    // $ANTLR start "rule__OrExpr__AndExprAssignment_1_0"
    // InternalXpath.g:4860:1: rule__OrExpr__AndExprAssignment_1_0 : ( ruleAndExpr ) ;
    public final void rule__OrExpr__AndExprAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4864:1: ( ( ruleAndExpr ) )
            // InternalXpath.g:4865:2: ( ruleAndExpr )
            {
            // InternalXpath.g:4865:2: ( ruleAndExpr )
            // InternalXpath.g:4866:3: ruleAndExpr
            {
             before(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAndExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__AndExprAssignment_1_0"


    // $ANTLR start "rule__OrExpr__OperatorAssignment_1_1_0"
    // InternalXpath.g:4875:1: rule__OrExpr__OperatorAssignment_1_1_0 : ( ( 'or' ) ) ;
    public final void rule__OrExpr__OperatorAssignment_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4879:1: ( ( ( 'or' ) ) )
            // InternalXpath.g:4880:2: ( ( 'or' ) )
            {
            // InternalXpath.g:4880:2: ( ( 'or' ) )
            // InternalXpath.g:4881:3: ( 'or' )
            {
             before(grammarAccess.getOrExprAccess().getOperatorOrKeyword_1_1_0_0()); 
            // InternalXpath.g:4882:3: ( 'or' )
            // InternalXpath.g:4883:4: 'or'
            {
             before(grammarAccess.getOrExprAccess().getOperatorOrKeyword_1_1_0_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getOperatorOrKeyword_1_1_0_0()); 

            }

             after(grammarAccess.getOrExprAccess().getOperatorOrKeyword_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OperatorAssignment_1_1_0"


    // $ANTLR start "rule__OrExpr__OperatorAssignment_1_1_1"
    // InternalXpath.g:4894:1: rule__OrExpr__OperatorAssignment_1_1_1 : ( ( 'and' ) ) ;
    public final void rule__OrExpr__OperatorAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4898:1: ( ( ( 'and' ) ) )
            // InternalXpath.g:4899:2: ( ( 'and' ) )
            {
            // InternalXpath.g:4899:2: ( ( 'and' ) )
            // InternalXpath.g:4900:3: ( 'and' )
            {
             before(grammarAccess.getOrExprAccess().getOperatorAndKeyword_1_1_1_0()); 
            // InternalXpath.g:4901:3: ( 'and' )
            // InternalXpath.g:4902:4: 'and'
            {
             before(grammarAccess.getOrExprAccess().getOperatorAndKeyword_1_1_1_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getOrExprAccess().getOperatorAndKeyword_1_1_1_0()); 

            }

             after(grammarAccess.getOrExprAccess().getOperatorAndKeyword_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OperatorAssignment_1_1_1"


    // $ANTLR start "rule__OrExpr__RightGroupedConditionAssignment_1_3"
    // InternalXpath.g:4913:1: rule__OrExpr__RightGroupedConditionAssignment_1_3 : ( ruleOrExpr ) ;
    public final void rule__OrExpr__RightGroupedConditionAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4917:1: ( ( ruleOrExpr ) )
            // InternalXpath.g:4918:2: ( ruleOrExpr )
            {
            // InternalXpath.g:4918:2: ( ruleOrExpr )
            // InternalXpath.g:4919:3: ruleOrExpr
            {
             before(grammarAccess.getOrExprAccess().getRightGroupedConditionOrExprParserRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOrExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getRightGroupedConditionOrExprParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__RightGroupedConditionAssignment_1_3"


    // $ANTLR start "rule__OrExpr__AndExprAssignment_2_0"
    // InternalXpath.g:4928:1: rule__OrExpr__AndExprAssignment_2_0 : ( ruleAndExpr ) ;
    public final void rule__OrExpr__AndExprAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4932:1: ( ( ruleAndExpr ) )
            // InternalXpath.g:4933:2: ( ruleAndExpr )
            {
            // InternalXpath.g:4933:2: ( ruleAndExpr )
            // InternalXpath.g:4934:3: ruleAndExpr
            {
             before(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAndExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getAndExprAndExprParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__AndExprAssignment_2_0"


    // $ANTLR start "rule__OrExpr__OrExpr_listAssignment_2_1_1"
    // InternalXpath.g:4943:1: rule__OrExpr__OrExpr_listAssignment_2_1_1 : ( ruleAndExpr ) ;
    public final void rule__OrExpr__OrExpr_listAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4947:1: ( ( ruleAndExpr ) )
            // InternalXpath.g:4948:2: ( ruleAndExpr )
            {
            // InternalXpath.g:4948:2: ( ruleAndExpr )
            // InternalXpath.g:4949:3: ruleAndExpr
            {
             before(grammarAccess.getOrExprAccess().getOrExpr_listAndExprParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAndExpr();

            state._fsp--;

             after(grammarAccess.getOrExprAccess().getOrExpr_listAndExprParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrExpr__OrExpr_listAssignment_2_1_1"


    // $ANTLR start "rule__AndExpr__ComparisonExprAssignment_0"
    // InternalXpath.g:4958:1: rule__AndExpr__ComparisonExprAssignment_0 : ( ruleComparisonExpr ) ;
    public final void rule__AndExpr__ComparisonExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4962:1: ( ( ruleComparisonExpr ) )
            // InternalXpath.g:4963:2: ( ruleComparisonExpr )
            {
            // InternalXpath.g:4963:2: ( ruleComparisonExpr )
            // InternalXpath.g:4964:3: ruleComparisonExpr
            {
             before(grammarAccess.getAndExprAccess().getComparisonExprComparisonExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleComparisonExpr();

            state._fsp--;

             after(grammarAccess.getAndExprAccess().getComparisonExprComparisonExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__ComparisonExprAssignment_0"


    // $ANTLR start "rule__AndExpr__AndCondition_listAssignment_1_1"
    // InternalXpath.g:4973:1: rule__AndExpr__AndCondition_listAssignment_1_1 : ( ruleComparisonExpr ) ;
    public final void rule__AndExpr__AndCondition_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4977:1: ( ( ruleComparisonExpr ) )
            // InternalXpath.g:4978:2: ( ruleComparisonExpr )
            {
            // InternalXpath.g:4978:2: ( ruleComparisonExpr )
            // InternalXpath.g:4979:3: ruleComparisonExpr
            {
             before(grammarAccess.getAndExprAccess().getAndCondition_listComparisonExprParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleComparisonExpr();

            state._fsp--;

             after(grammarAccess.getAndExprAccess().getAndCondition_listComparisonExprParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndExpr__AndCondition_listAssignment_1_1"


    // $ANTLR start "rule__ComparisonExpr__AdditiveExprAssignment_0_0"
    // InternalXpath.g:4988:1: rule__ComparisonExpr__AdditiveExprAssignment_0_0 : ( ruleAdditiveExpr ) ;
    public final void rule__ComparisonExpr__AdditiveExprAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:4992:1: ( ( ruleAdditiveExpr ) )
            // InternalXpath.g:4993:2: ( ruleAdditiveExpr )
            {
            // InternalXpath.g:4993:2: ( ruleAdditiveExpr )
            // InternalXpath.g:4994:3: ruleAdditiveExpr
            {
             before(grammarAccess.getComparisonExprAccess().getAdditiveExprAdditiveExprParserRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAdditiveExpr();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getAdditiveExprAdditiveExprParserRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__AdditiveExprAssignment_0_0"


    // $ANTLR start "rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0"
    // InternalXpath.g:5003:1: rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0 : ( ruleValueComp ) ;
    public final void rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5007:1: ( ( ruleValueComp ) )
            // InternalXpath.g:5008:2: ( ruleValueComp )
            {
            // InternalXpath.g:5008:2: ( ruleValueComp )
            // InternalXpath.g:5009:3: ruleValueComp
            {
             before(grammarAccess.getComparisonExprAccess().getValueCompOptionalValueCompParserRuleCall_0_1_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleValueComp();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getValueCompOptionalValueCompParserRuleCall_0_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__ValueCompOptionalAssignment_0_1_0_0"


    // $ANTLR start "rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1"
    // InternalXpath.g:5018:1: rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1 : ( ruleGeneralComp ) ;
    public final void rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5022:1: ( ( ruleGeneralComp ) )
            // InternalXpath.g:5023:2: ( ruleGeneralComp )
            {
            // InternalXpath.g:5023:2: ( ruleGeneralComp )
            // InternalXpath.g:5024:3: ruleGeneralComp
            {
             before(grammarAccess.getComparisonExprAccess().getGeneralCompOptionalGeneralCompParserRuleCall_0_1_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGeneralComp();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getGeneralCompOptionalGeneralCompParserRuleCall_0_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__GeneralCompOptionalAssignment_0_1_0_1"


    // $ANTLR start "rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2"
    // InternalXpath.g:5033:1: rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2 : ( ruleNodeComp ) ;
    public final void rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5037:1: ( ( ruleNodeComp ) )
            // InternalXpath.g:5038:2: ( ruleNodeComp )
            {
            // InternalXpath.g:5038:2: ( ruleNodeComp )
            // InternalXpath.g:5039:3: ruleNodeComp
            {
             before(grammarAccess.getComparisonExprAccess().getNodeCompOptionalNodeCompParserRuleCall_0_1_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNodeComp();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getNodeCompOptionalNodeCompParserRuleCall_0_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__NodeCompOptionalAssignment_0_1_0_2"


    // $ANTLR start "rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1"
    // InternalXpath.g:5048:1: rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1 : ( ruleAdditiveExpr ) ;
    public final void rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5052:1: ( ( ruleAdditiveExpr ) )
            // InternalXpath.g:5053:2: ( ruleAdditiveExpr )
            {
            // InternalXpath.g:5053:2: ( ruleAdditiveExpr )
            // InternalXpath.g:5054:3: ruleAdditiveExpr
            {
             before(grammarAccess.getComparisonExprAccess().getAdditiveExprOptionalAdditiveExprParserRuleCall_0_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAdditiveExpr();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getAdditiveExprOptionalAdditiveExprParserRuleCall_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__AdditiveExprOptionalAssignment_0_1_1"


    // $ANTLR start "rule__ComparisonExpr__NotExprAssignment_1"
    // InternalXpath.g:5063:1: rule__ComparisonExpr__NotExprAssignment_1 : ( ruleNotExpr ) ;
    public final void rule__ComparisonExpr__NotExprAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5067:1: ( ( ruleNotExpr ) )
            // InternalXpath.g:5068:2: ( ruleNotExpr )
            {
            // InternalXpath.g:5068:2: ( ruleNotExpr )
            // InternalXpath.g:5069:3: ruleNotExpr
            {
             before(grammarAccess.getComparisonExprAccess().getNotExprNotExprParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleNotExpr();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getNotExprNotExprParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__NotExprAssignment_1"


    // $ANTLR start "rule__ComparisonExpr__FunctionAssignment_2"
    // InternalXpath.g:5078:1: rule__ComparisonExpr__FunctionAssignment_2 : ( ruleFunction ) ;
    public final void rule__ComparisonExpr__FunctionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5082:1: ( ( ruleFunction ) )
            // InternalXpath.g:5083:2: ( ruleFunction )
            {
            // InternalXpath.g:5083:2: ( ruleFunction )
            // InternalXpath.g:5084:3: ruleFunction
            {
             before(grammarAccess.getComparisonExprAccess().getFunctionFunctionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getComparisonExprAccess().getFunctionFunctionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonExpr__FunctionAssignment_2"


    // $ANTLR start "rule__AdditiveExpr__ValueExprAssignment_0"
    // InternalXpath.g:5093:1: rule__AdditiveExpr__ValueExprAssignment_0 : ( ruleValueExpr ) ;
    public final void rule__AdditiveExpr__ValueExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5097:1: ( ( ruleValueExpr ) )
            // InternalXpath.g:5098:2: ( ruleValueExpr )
            {
            // InternalXpath.g:5098:2: ( ruleValueExpr )
            // InternalXpath.g:5099:3: ruleValueExpr
            {
             before(grammarAccess.getAdditiveExprAccess().getValueExprValueExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleValueExpr();

            state._fsp--;

             after(grammarAccess.getAdditiveExprAccess().getValueExprValueExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__ValueExprAssignment_0"


    // $ANTLR start "rule__AdditiveExpr__Additive_listAssignment_1_0"
    // InternalXpath.g:5108:1: rule__AdditiveExpr__Additive_listAssignment_1_0 : ( ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 ) ) ;
    public final void rule__AdditiveExpr__Additive_listAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5112:1: ( ( ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 ) ) )
            // InternalXpath.g:5113:2: ( ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 ) )
            {
            // InternalXpath.g:5113:2: ( ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 ) )
            // InternalXpath.g:5114:3: ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 )
            {
             before(grammarAccess.getAdditiveExprAccess().getAdditive_listAlternatives_1_0_0()); 
            // InternalXpath.g:5115:3: ( rule__AdditiveExpr__Additive_listAlternatives_1_0_0 )
            // InternalXpath.g:5115:4: rule__AdditiveExpr__Additive_listAlternatives_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpr__Additive_listAlternatives_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExprAccess().getAdditive_listAlternatives_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__Additive_listAssignment_1_0"


    // $ANTLR start "rule__AdditiveExpr__ValueExpr_listAssignment_1_1"
    // InternalXpath.g:5123:1: rule__AdditiveExpr__ValueExpr_listAssignment_1_1 : ( ruleValueExpr ) ;
    public final void rule__AdditiveExpr__ValueExpr_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5127:1: ( ( ruleValueExpr ) )
            // InternalXpath.g:5128:2: ( ruleValueExpr )
            {
            // InternalXpath.g:5128:2: ( ruleValueExpr )
            // InternalXpath.g:5129:3: ruleValueExpr
            {
             before(grammarAccess.getAdditiveExprAccess().getValueExpr_listValueExprParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValueExpr();

            state._fsp--;

             after(grammarAccess.getAdditiveExprAccess().getValueExpr_listValueExprParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpr__ValueExpr_listAssignment_1_1"


    // $ANTLR start "rule__UnionExpr__IntersectExceptExprAssignment_0"
    // InternalXpath.g:5138:1: rule__UnionExpr__IntersectExceptExprAssignment_0 : ( ruleIntersectExceptExpr ) ;
    public final void rule__UnionExpr__IntersectExceptExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5142:1: ( ( ruleIntersectExceptExpr ) )
            // InternalXpath.g:5143:2: ( ruleIntersectExceptExpr )
            {
            // InternalXpath.g:5143:2: ( ruleIntersectExceptExpr )
            // InternalXpath.g:5144:3: ruleIntersectExceptExpr
            {
             before(grammarAccess.getUnionExprAccess().getIntersectExceptExprIntersectExceptExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleIntersectExceptExpr();

            state._fsp--;

             after(grammarAccess.getUnionExprAccess().getIntersectExceptExprIntersectExceptExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__IntersectExceptExprAssignment_0"


    // $ANTLR start "rule__UnionExpr__Intercection_listAssignment_1_0"
    // InternalXpath.g:5153:1: rule__UnionExpr__Intercection_listAssignment_1_0 : ( ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 ) ) ;
    public final void rule__UnionExpr__Intercection_listAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5157:1: ( ( ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 ) ) )
            // InternalXpath.g:5158:2: ( ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 ) )
            {
            // InternalXpath.g:5158:2: ( ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 ) )
            // InternalXpath.g:5159:3: ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 )
            {
             before(grammarAccess.getUnionExprAccess().getIntercection_listAlternatives_1_0_0()); 
            // InternalXpath.g:5160:3: ( rule__UnionExpr__Intercection_listAlternatives_1_0_0 )
            // InternalXpath.g:5160:4: rule__UnionExpr__Intercection_listAlternatives_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__UnionExpr__Intercection_listAlternatives_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getUnionExprAccess().getIntercection_listAlternatives_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__Intercection_listAssignment_1_0"


    // $ANTLR start "rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1"
    // InternalXpath.g:5168:1: rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1 : ( ruleIntersectExceptExpr ) ;
    public final void rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5172:1: ( ( ruleIntersectExceptExpr ) )
            // InternalXpath.g:5173:2: ( ruleIntersectExceptExpr )
            {
            // InternalXpath.g:5173:2: ( ruleIntersectExceptExpr )
            // InternalXpath.g:5174:3: ruleIntersectExceptExpr
            {
             before(grammarAccess.getUnionExprAccess().getIntersectExceptExpr_listIntersectExceptExprParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIntersectExceptExpr();

            state._fsp--;

             after(grammarAccess.getUnionExprAccess().getIntersectExceptExpr_listIntersectExceptExprParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnionExpr__IntersectExceptExpr_listAssignment_1_1"


    // $ANTLR start "rule__IntersectExceptExpr__InstanceofExprAssignment_0"
    // InternalXpath.g:5183:1: rule__IntersectExceptExpr__InstanceofExprAssignment_0 : ( ruleInstanceofExpr ) ;
    public final void rule__IntersectExceptExpr__InstanceofExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5187:1: ( ( ruleInstanceofExpr ) )
            // InternalXpath.g:5188:2: ( ruleInstanceofExpr )
            {
            // InternalXpath.g:5188:2: ( ruleInstanceofExpr )
            // InternalXpath.g:5189:3: ruleInstanceofExpr
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstanceofExprInstanceofExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleInstanceofExpr();

            state._fsp--;

             after(grammarAccess.getIntersectExceptExprAccess().getInstanceofExprInstanceofExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__InstanceofExprAssignment_0"


    // $ANTLR start "rule__IntersectExceptExpr__Instance_listAssignment_1_0"
    // InternalXpath.g:5198:1: rule__IntersectExceptExpr__Instance_listAssignment_1_0 : ( ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 ) ) ;
    public final void rule__IntersectExceptExpr__Instance_listAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5202:1: ( ( ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 ) ) )
            // InternalXpath.g:5203:2: ( ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 ) )
            {
            // InternalXpath.g:5203:2: ( ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 ) )
            // InternalXpath.g:5204:3: ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 )
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstance_listAlternatives_1_0_0()); 
            // InternalXpath.g:5205:3: ( rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0 )
            // InternalXpath.g:5205:4: rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__IntersectExceptExpr__Instance_listAlternatives_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getIntersectExceptExprAccess().getInstance_listAlternatives_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__Instance_listAssignment_1_0"


    // $ANTLR start "rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1"
    // InternalXpath.g:5213:1: rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1 : ( ruleInstanceofExpr ) ;
    public final void rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5217:1: ( ( ruleInstanceofExpr ) )
            // InternalXpath.g:5218:2: ( ruleInstanceofExpr )
            {
            // InternalXpath.g:5218:2: ( ruleInstanceofExpr )
            // InternalXpath.g:5219:3: ruleInstanceofExpr
            {
             before(grammarAccess.getIntersectExceptExprAccess().getInstanceofExpr_listInstanceofExprParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInstanceofExpr();

            state._fsp--;

             after(grammarAccess.getIntersectExceptExprAccess().getInstanceofExpr_listInstanceofExprParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntersectExceptExpr__InstanceofExpr_listAssignment_1_1"


    // $ANTLR start "rule__InstanceofExpr__TreatExprAssignment"
    // InternalXpath.g:5228:1: rule__InstanceofExpr__TreatExprAssignment : ( ruleTreatExpr ) ;
    public final void rule__InstanceofExpr__TreatExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5232:1: ( ( ruleTreatExpr ) )
            // InternalXpath.g:5233:2: ( ruleTreatExpr )
            {
            // InternalXpath.g:5233:2: ( ruleTreatExpr )
            // InternalXpath.g:5234:3: ruleTreatExpr
            {
             before(grammarAccess.getInstanceofExprAccess().getTreatExprTreatExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleTreatExpr();

            state._fsp--;

             after(grammarAccess.getInstanceofExprAccess().getTreatExprTreatExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstanceofExpr__TreatExprAssignment"


    // $ANTLR start "rule__TreatExpr__CastableExprAssignment"
    // InternalXpath.g:5243:1: rule__TreatExpr__CastableExprAssignment : ( ruleCastableExpr ) ;
    public final void rule__TreatExpr__CastableExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5247:1: ( ( ruleCastableExpr ) )
            // InternalXpath.g:5248:2: ( ruleCastableExpr )
            {
            // InternalXpath.g:5248:2: ( ruleCastableExpr )
            // InternalXpath.g:5249:3: ruleCastableExpr
            {
             before(grammarAccess.getTreatExprAccess().getCastableExprCastableExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCastableExpr();

            state._fsp--;

             after(grammarAccess.getTreatExprAccess().getCastableExprCastableExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TreatExpr__CastableExprAssignment"


    // $ANTLR start "rule__CastableExpr__CastExprAssignment"
    // InternalXpath.g:5258:1: rule__CastableExpr__CastExprAssignment : ( ruleCastExpr ) ;
    public final void rule__CastableExpr__CastExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5262:1: ( ( ruleCastExpr ) )
            // InternalXpath.g:5263:2: ( ruleCastExpr )
            {
            // InternalXpath.g:5263:2: ( ruleCastExpr )
            // InternalXpath.g:5264:3: ruleCastExpr
            {
             before(grammarAccess.getCastableExprAccess().getCastExprCastExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCastExpr();

            state._fsp--;

             after(grammarAccess.getCastableExprAccess().getCastExprCastExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CastableExpr__CastExprAssignment"


    // $ANTLR start "rule__CastExpr__UnaryExprAssignment"
    // InternalXpath.g:5273:1: rule__CastExpr__UnaryExprAssignment : ( ruleUnaryExpr ) ;
    public final void rule__CastExpr__UnaryExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5277:1: ( ( ruleUnaryExpr ) )
            // InternalXpath.g:5278:2: ( ruleUnaryExpr )
            {
            // InternalXpath.g:5278:2: ( ruleUnaryExpr )
            // InternalXpath.g:5279:3: ruleUnaryExpr
            {
             before(grammarAccess.getCastExprAccess().getUnaryExprUnaryExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryExpr();

            state._fsp--;

             after(grammarAccess.getCastExprAccess().getUnaryExprUnaryExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CastExpr__UnaryExprAssignment"


    // $ANTLR start "rule__UnaryExpr__ValueExprAssignment"
    // InternalXpath.g:5288:1: rule__UnaryExpr__ValueExprAssignment : ( ruleValueExpr ) ;
    public final void rule__UnaryExpr__ValueExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5292:1: ( ( ruleValueExpr ) )
            // InternalXpath.g:5293:2: ( ruleValueExpr )
            {
            // InternalXpath.g:5293:2: ( ruleValueExpr )
            // InternalXpath.g:5294:3: ruleValueExpr
            {
             before(grammarAccess.getUnaryExprAccess().getValueExprValueExprParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleValueExpr();

            state._fsp--;

             after(grammarAccess.getUnaryExprAccess().getValueExprValueExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpr__ValueExprAssignment"


    // $ANTLR start "rule__ValueExpr__PathExprAssignment_0"
    // InternalXpath.g:5303:1: rule__ValueExpr__PathExprAssignment_0 : ( rulePathExpr ) ;
    public final void rule__ValueExpr__PathExprAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5307:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5308:2: ( rulePathExpr )
            {
            // InternalXpath.g:5308:2: ( rulePathExpr )
            // InternalXpath.g:5309:3: rulePathExpr
            {
             before(grammarAccess.getValueExprAccess().getPathExprPathExprParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getValueExprAccess().getPathExprPathExprParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueExpr__PathExprAssignment_0"


    // $ANTLR start "rule__ValueExpr__StringliteralAssignment_1"
    // InternalXpath.g:5318:1: rule__ValueExpr__StringliteralAssignment_1 : ( ruleStringLiteral ) ;
    public final void rule__ValueExpr__StringliteralAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5322:1: ( ( ruleStringLiteral ) )
            // InternalXpath.g:5323:2: ( ruleStringLiteral )
            {
            // InternalXpath.g:5323:2: ( ruleStringLiteral )
            // InternalXpath.g:5324:3: ruleStringLiteral
            {
             before(grammarAccess.getValueExprAccess().getStringliteralStringLiteralParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStringLiteral();

            state._fsp--;

             after(grammarAccess.getValueExprAccess().getStringliteralStringLiteralParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueExpr__StringliteralAssignment_1"


    // $ANTLR start "rule__ValueExpr__NumericLiteralAssignment_2"
    // InternalXpath.g:5333:1: rule__ValueExpr__NumericLiteralAssignment_2 : ( ruleNumericLiteral ) ;
    public final void rule__ValueExpr__NumericLiteralAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5337:1: ( ( ruleNumericLiteral ) )
            // InternalXpath.g:5338:2: ( ruleNumericLiteral )
            {
            // InternalXpath.g:5338:2: ( ruleNumericLiteral )
            // InternalXpath.g:5339:3: ruleNumericLiteral
            {
             before(grammarAccess.getValueExprAccess().getNumericLiteralNumericLiteralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNumericLiteral();

            state._fsp--;

             after(grammarAccess.getValueExprAccess().getNumericLiteralNumericLiteralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueExpr__NumericLiteralAssignment_2"


    // $ANTLR start "rule__PathExpr__ElementAssignment_0"
    // InternalXpath.g:5348:1: rule__PathExpr__ElementAssignment_0 : ( ruleElement ) ;
    public final void rule__PathExpr__ElementAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5352:1: ( ( ruleElement ) )
            // InternalXpath.g:5353:2: ( ruleElement )
            {
            // InternalXpath.g:5353:2: ( ruleElement )
            // InternalXpath.g:5354:3: ruleElement
            {
             before(grammarAccess.getPathExprAccess().getElementElementParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getPathExprAccess().getElementElementParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__ElementAssignment_0"


    // $ANTLR start "rule__PathExpr__Path_listAssignment_1_0"
    // InternalXpath.g:5363:1: rule__PathExpr__Path_listAssignment_1_0 : ( ( rule__PathExpr__Path_listAlternatives_1_0_0 ) ) ;
    public final void rule__PathExpr__Path_listAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5367:1: ( ( ( rule__PathExpr__Path_listAlternatives_1_0_0 ) ) )
            // InternalXpath.g:5368:2: ( ( rule__PathExpr__Path_listAlternatives_1_0_0 ) )
            {
            // InternalXpath.g:5368:2: ( ( rule__PathExpr__Path_listAlternatives_1_0_0 ) )
            // InternalXpath.g:5369:3: ( rule__PathExpr__Path_listAlternatives_1_0_0 )
            {
             before(grammarAccess.getPathExprAccess().getPath_listAlternatives_1_0_0()); 
            // InternalXpath.g:5370:3: ( rule__PathExpr__Path_listAlternatives_1_0_0 )
            // InternalXpath.g:5370:4: rule__PathExpr__Path_listAlternatives_1_0_0
            {
            pushFollow(FOLLOW_2);
            rule__PathExpr__Path_listAlternatives_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getPathExprAccess().getPath_listAlternatives_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__Path_listAssignment_1_0"


    // $ANTLR start "rule__PathExpr__PathElement_listAssignment_1_1"
    // InternalXpath.g:5378:1: rule__PathExpr__PathElement_listAssignment_1_1 : ( ruleElement ) ;
    public final void rule__PathExpr__PathElement_listAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5382:1: ( ( ruleElement ) )
            // InternalXpath.g:5383:2: ( ruleElement )
            {
            // InternalXpath.g:5383:2: ( ruleElement )
            // InternalXpath.g:5384:3: ruleElement
            {
             before(grammarAccess.getPathExprAccess().getPathElement_listElementParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getPathExprAccess().getPathElement_listElementParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PathExpr__PathElement_listAssignment_1_1"


    // $ANTLR start "rule__StepExpr__ElementAssignment"
    // InternalXpath.g:5393:1: rule__StepExpr__ElementAssignment : ( ruleElement ) ;
    public final void rule__StepExpr__ElementAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5397:1: ( ( ruleElement ) )
            // InternalXpath.g:5398:2: ( ruleElement )
            {
            // InternalXpath.g:5398:2: ( ruleElement )
            // InternalXpath.g:5399:3: ruleElement
            {
             before(grammarAccess.getStepExprAccess().getElementElementParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getStepExprAccess().getElementElementParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepExpr__ElementAssignment"


    // $ANTLR start "rule__NotExpr__PathExprAssignment_2"
    // InternalXpath.g:5408:1: rule__NotExpr__PathExprAssignment_2 : ( rulePathExpr ) ;
    public final void rule__NotExpr__PathExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5412:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5413:2: ( rulePathExpr )
            {
            // InternalXpath.g:5413:2: ( rulePathExpr )
            // InternalXpath.g:5414:3: rulePathExpr
            {
             before(grammarAccess.getNotExprAccess().getPathExprPathExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getNotExprAccess().getPathExprPathExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotExpr__PathExprAssignment_2"


    // $ANTLR start "rule__QName__NameAssignment"
    // InternalXpath.g:5423:1: rule__QName__NameAssignment : ( RULE_ID ) ;
    public final void rule__QName__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5427:1: ( ( RULE_ID ) )
            // InternalXpath.g:5428:2: ( RULE_ID )
            {
            // InternalXpath.g:5428:2: ( RULE_ID )
            // InternalXpath.g:5429:3: RULE_ID
            {
             before(grammarAccess.getQNameAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__NameAssignment"


    // $ANTLR start "rule__Element__NameAssignment_1"
    // InternalXpath.g:5438:1: rule__Element__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Element__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5442:1: ( ( RULE_ID ) )
            // InternalXpath.g:5443:2: ( RULE_ID )
            {
            // InternalXpath.g:5443:2: ( RULE_ID )
            // InternalXpath.g:5444:3: RULE_ID
            {
             before(grammarAccess.getElementAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getElementAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__NameAssignment_1"


    // $ANTLR start "rule__Function__SumFunctionAssignment_0"
    // InternalXpath.g:5453:1: rule__Function__SumFunctionAssignment_0 : ( ruleSumFunction ) ;
    public final void rule__Function__SumFunctionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5457:1: ( ( ruleSumFunction ) )
            // InternalXpath.g:5458:2: ( ruleSumFunction )
            {
            // InternalXpath.g:5458:2: ( ruleSumFunction )
            // InternalXpath.g:5459:3: ruleSumFunction
            {
             before(grammarAccess.getFunctionAccess().getSumFunctionSumFunctionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleSumFunction();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getSumFunctionSumFunctionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__SumFunctionAssignment_0"


    // $ANTLR start "rule__Function__CountFunctionAssignment_1"
    // InternalXpath.g:5468:1: rule__Function__CountFunctionAssignment_1 : ( ruleCountFunction ) ;
    public final void rule__Function__CountFunctionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5472:1: ( ( ruleCountFunction ) )
            // InternalXpath.g:5473:2: ( ruleCountFunction )
            {
            // InternalXpath.g:5473:2: ( ruleCountFunction )
            // InternalXpath.g:5474:3: ruleCountFunction
            {
             before(grammarAccess.getFunctionAccess().getCountFunctionCountFunctionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCountFunction();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getCountFunctionCountFunctionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__CountFunctionAssignment_1"


    // $ANTLR start "rule__SumFunction__SumElementAssignment_2"
    // InternalXpath.g:5483:1: rule__SumFunction__SumElementAssignment_2 : ( rulePathExpr ) ;
    public final void rule__SumFunction__SumElementAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5487:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5488:2: ( rulePathExpr )
            {
            // InternalXpath.g:5488:2: ( rulePathExpr )
            // InternalXpath.g:5489:3: rulePathExpr
            {
             before(grammarAccess.getSumFunctionAccess().getSumElementPathExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getSumFunctionAccess().getSumElementPathExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__SumElementAssignment_2"


    // $ANTLR start "rule__SumFunction__ResultElementAssignment_5"
    // InternalXpath.g:5498:1: rule__SumFunction__ResultElementAssignment_5 : ( rulePathExpr ) ;
    public final void rule__SumFunction__ResultElementAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5502:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5503:2: ( rulePathExpr )
            {
            // InternalXpath.g:5503:2: ( rulePathExpr )
            // InternalXpath.g:5504:3: rulePathExpr
            {
             before(grammarAccess.getSumFunctionAccess().getResultElementPathExprParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getSumFunctionAccess().getResultElementPathExprParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SumFunction__ResultElementAssignment_5"


    // $ANTLR start "rule__CountFunction__CountElementAssignment_2"
    // InternalXpath.g:5513:1: rule__CountFunction__CountElementAssignment_2 : ( rulePathExpr ) ;
    public final void rule__CountFunction__CountElementAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5517:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5518:2: ( rulePathExpr )
            {
            // InternalXpath.g:5518:2: ( rulePathExpr )
            // InternalXpath.g:5519:3: rulePathExpr
            {
             before(grammarAccess.getCountFunctionAccess().getCountElementPathExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getCountFunctionAccess().getCountElementPathExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__CountElementAssignment_2"


    // $ANTLR start "rule__CountFunction__ResultElementAssignment_5"
    // InternalXpath.g:5528:1: rule__CountFunction__ResultElementAssignment_5 : ( rulePathExpr ) ;
    public final void rule__CountFunction__ResultElementAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5532:1: ( ( rulePathExpr ) )
            // InternalXpath.g:5533:2: ( rulePathExpr )
            {
            // InternalXpath.g:5533:2: ( rulePathExpr )
            // InternalXpath.g:5534:3: rulePathExpr
            {
             before(grammarAccess.getCountFunctionAccess().getResultElementPathExprParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            rulePathExpr();

            state._fsp--;

             after(grammarAccess.getCountFunctionAccess().getResultElementPathExprParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CountFunction__ResultElementAssignment_5"


    // $ANTLR start "rule__StringLiteral__NameAssignment"
    // InternalXpath.g:5543:1: rule__StringLiteral__NameAssignment : ( RULE_STRING ) ;
    public final void rule__StringLiteral__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5547:1: ( ( RULE_STRING ) )
            // InternalXpath.g:5548:2: ( RULE_STRING )
            {
            // InternalXpath.g:5548:2: ( RULE_STRING )
            // InternalXpath.g:5549:3: RULE_STRING
            {
             before(grammarAccess.getStringLiteralAccess().getNameSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringLiteralAccess().getNameSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringLiteral__NameAssignment"


    // $ANTLR start "rule__NumericLiteral__NumberAssignment"
    // InternalXpath.g:5558:1: rule__NumericLiteral__NumberAssignment : ( RULE_INT ) ;
    public final void rule__NumericLiteral__NumberAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5562:1: ( ( RULE_INT ) )
            // InternalXpath.g:5563:2: ( RULE_INT )
            {
            // InternalXpath.g:5563:2: ( RULE_INT )
            // InternalXpath.g:5564:3: RULE_INT
            {
             before(grammarAccess.getNumericLiteralAccess().getNumberINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getNumericLiteralAccess().getNumberINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumericLiteral__NumberAssignment"


    // $ANTLR start "rule__ThenElse__ThenExprTrueAssignment_0_1"
    // InternalXpath.g:5573:1: rule__ThenElse__ThenExprTrueAssignment_0_1 : ( ruleTrue ) ;
    public final void rule__ThenElse__ThenExprTrueAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5577:1: ( ( ruleTrue ) )
            // InternalXpath.g:5578:2: ( ruleTrue )
            {
            // InternalXpath.g:5578:2: ( ruleTrue )
            // InternalXpath.g:5579:3: ruleTrue
            {
             before(grammarAccess.getThenElseAccess().getThenExprTrueTrueParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTrue();

            state._fsp--;

             after(grammarAccess.getThenElseAccess().getThenExprTrueTrueParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__ThenExprTrueAssignment_0_1"


    // $ANTLR start "rule__ThenElse__ElseExprFalseAssignment_0_3"
    // InternalXpath.g:5588:1: rule__ThenElse__ElseExprFalseAssignment_0_3 : ( ruleFalse ) ;
    public final void rule__ThenElse__ElseExprFalseAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5592:1: ( ( ruleFalse ) )
            // InternalXpath.g:5593:2: ( ruleFalse )
            {
            // InternalXpath.g:5593:2: ( ruleFalse )
            // InternalXpath.g:5594:3: ruleFalse
            {
             before(grammarAccess.getThenElseAccess().getElseExprFalseFalseParserRuleCall_0_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFalse();

            state._fsp--;

             after(grammarAccess.getThenElseAccess().getElseExprFalseFalseParserRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__ElseExprFalseAssignment_0_3"


    // $ANTLR start "rule__ThenElse__ThenExprFalseAssignment_1_1"
    // InternalXpath.g:5603:1: rule__ThenElse__ThenExprFalseAssignment_1_1 : ( ruleFalse ) ;
    public final void rule__ThenElse__ThenExprFalseAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5607:1: ( ( ruleFalse ) )
            // InternalXpath.g:5608:2: ( ruleFalse )
            {
            // InternalXpath.g:5608:2: ( ruleFalse )
            // InternalXpath.g:5609:3: ruleFalse
            {
             before(grammarAccess.getThenElseAccess().getThenExprFalseFalseParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFalse();

            state._fsp--;

             after(grammarAccess.getThenElseAccess().getThenExprFalseFalseParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__ThenExprFalseAssignment_1_1"


    // $ANTLR start "rule__ThenElse__ElseExprTrueAssignment_1_3"
    // InternalXpath.g:5618:1: rule__ThenElse__ElseExprTrueAssignment_1_3 : ( ruleTrue ) ;
    public final void rule__ThenElse__ElseExprTrueAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5622:1: ( ( ruleTrue ) )
            // InternalXpath.g:5623:2: ( ruleTrue )
            {
            // InternalXpath.g:5623:2: ( ruleTrue )
            // InternalXpath.g:5624:3: ruleTrue
            {
             before(grammarAccess.getThenElseAccess().getElseExprTrueTrueParserRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTrue();

            state._fsp--;

             after(grammarAccess.getThenElseAccess().getElseExprTrueTrueParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThenElse__ElseExprTrueAssignment_1_3"


    // $ANTLR start "rule__False__IsFalseAssignment_0"
    // InternalXpath.g:5633:1: rule__False__IsFalseAssignment_0 : ( ( 'false()' ) ) ;
    public final void rule__False__IsFalseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5637:1: ( ( ( 'false()' ) ) )
            // InternalXpath.g:5638:2: ( ( 'false()' ) )
            {
            // InternalXpath.g:5638:2: ( ( 'false()' ) )
            // InternalXpath.g:5639:3: ( 'false()' )
            {
             before(grammarAccess.getFalseAccess().getIsFalseFalseKeyword_0_0()); 
            // InternalXpath.g:5640:3: ( 'false()' )
            // InternalXpath.g:5641:4: 'false()'
            {
             before(grammarAccess.getFalseAccess().getIsFalseFalseKeyword_0_0()); 
            match(input,55,FOLLOW_2); 
             after(grammarAccess.getFalseAccess().getIsFalseFalseKeyword_0_0()); 

            }

             after(grammarAccess.getFalseAccess().getIsFalseFalseKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__False__IsFalseAssignment_0"


    // $ANTLR start "rule__False__MsgAssignment_1"
    // InternalXpath.g:5652:1: rule__False__MsgAssignment_1 : ( RULE_STRING ) ;
    public final void rule__False__MsgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXpath.g:5656:1: ( ( RULE_STRING ) )
            // InternalXpath.g:5657:2: ( RULE_STRING )
            {
            // InternalXpath.g:5657:2: ( RULE_STRING )
            // InternalXpath.g:5658:3: RULE_STRING
            {
             before(grammarAccess.getFalseAccess().getMsgSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getFalseAccess().getMsgSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__False__MsgAssignment_1"

    // Delegated rules


    protected DFA4 dfa4 = new DFA4(this);
    static final String dfa_1s = "\u00be\uffff";
    static final String dfa_2s = "\3\uffff\2\34\3\uffff\1\34\32\uffff\2\34\1\uffff\2\34\1\uffff\2\34\10\uffff\2\34\2\uffff\1\34\27\uffff\1\34\5\uffff\2\34\3\uffff\2\34\1\uffff\2\34\5\uffff\1\34\10\uffff\1\34\1\uffff\2\34\12\uffff\2\34\2\uffff\1\34\5\uffff\2\34\2\uffff\1\34\16\uffff\1\34\5\uffff\5\34\14\uffff\2\34\2\uffff\1\34\4\uffff\2\34\2\uffff\2\34";
    static final String dfa_3s = "\1\5\1\uffff\1\4\2\15\3\54\1\15\23\5\1\uffff\5\61\1\4\2\15\1\4\2\15\1\4\2\15\3\54\1\uffff\4\4\2\15\2\5\1\15\21\5\3\61\3\42\1\15\4\61\1\4\2\15\2\61\1\4\2\15\1\4\2\15\3\4\2\61\1\44\2\61\1\23\2\61\1\23\2\4\1\15\1\4\2\15\2\5\3\42\2\4\1\61\1\4\1\61\2\15\2\61\1\15\4\61\1\4\2\15\2\61\1\44\2\61\1\23\2\61\1\23\2\42\1\4\1\42\4\4\1\15\2\4\1\61\1\4\1\61\2\42\3\15\2\61\2\42\1\4\1\42\1\4\4\61\1\4\2\42\2\4\1\15\4\61\2\42\2\4\2\42";
    static final String dfa_4s = "\1\63\1\uffff\1\4\2\57\3\54\1\57\21\61\2\63\1\uffff\5\61\1\4\2\57\1\4\2\57\1\4\2\57\3\54\1\uffff\4\4\2\57\2\61\1\57\24\61\3\55\1\57\4\61\1\4\2\57\2\61\1\4\2\57\1\4\2\57\3\4\2\61\1\57\2\61\1\23\2\61\1\23\2\4\1\57\1\4\2\57\2\61\3\55\2\4\1\61\1\4\1\61\2\57\2\61\1\57\4\61\1\4\2\57\2\61\1\57\2\61\1\23\2\61\1\23\2\55\1\4\1\55\4\4\1\57\2\4\1\61\1\4\1\61\5\57\2\61\2\55\1\4\1\55\1\4\4\61\1\4\2\57\2\4\1\57\4\61\2\57\2\4\2\57";
    static final String dfa_5s = "\1\uffff\1\1\32\uffff\1\3\21\uffff\1\2\u008f\uffff";
    static final String dfa_6s = "\u00be\uffff}>";
    static final String[] dfa_7s = {
            "\1\3\1\4\45\uffff\1\1\3\uffff\1\5\1\2\1\6\1\7",
            "",
            "\1\10",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\35",
            "\1\36",
            "\1\37",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\40\1\41\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\43\1\44\52\uffff\1\42",
            "\1\43\1\44\52\uffff\1\42",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\46\1\47\52\uffff\1\45",
            "\1\51\1\52\45\uffff\1\56\3\uffff\1\53\1\50\1\54\1\55",
            "\2\34\45\uffff\1\56\3\uffff\4\34",
            "",
            "\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\62",
            "\1\63",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\64",
            "\1\65\1\66\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\65\1\66\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\67",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\111",
            "\1\112",
            "\1\113",
            "",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\117",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\120\1\121\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\65\1\66\23\uffff\1\122\1\123\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\125\1\126\52\uffff\1\124",
            "\1\125\1\126\52\uffff\1\124",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\127\1\130\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\132\1\133\52\uffff\1\131",
            "\1\132\1\133\52\uffff\1\131",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\135\1\136\52\uffff\1\134",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142\1\143\11\uffff\1\144",
            "\1\145\1\146\11\uffff\1\147",
            "\1\150\1\151\11\uffff\1\152",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\40\1\41\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\153",
            "\1\153",
            "\1\154",
            "\1\154",
            "\1\155",
            "\1\65\1\66\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\65\1\66\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\156",
            "\1\156",
            "\1\157",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\2\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\160",
            "\1\161\1\162\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\161\1\162\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\166",
            "\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\167",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "\1\65\1\66\23\uffff\1\175\1\176\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\177",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\u0080\1\u0081\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\161\1\162\23\uffff\1\u0082\1\u0083\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u0085\1\u0086\52\uffff\1\u0084",
            "\1\u0085\1\u0086\52\uffff\1\u0084",
            "\1\u0087\1\u0088\11\uffff\1\u0089",
            "\1\u008a\1\u008b\11\uffff\1\u008c",
            "\1\u008d\1\u008e\11\uffff\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\11\1\12\4\uffff\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14\1\15\1\16\1\17\1\20\1\27\1\30\1\31\1\120\1\121\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\65\1\66\23\uffff\1\122\1\123\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u0095",
            "\1\u0095",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\127\1\130\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u0096",
            "\1\u0096",
            "\1\u0097",
            "\1\u0097",
            "\1\u0098",
            "\1\161\1\162\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\161\1\162\25\uffff\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u0099",
            "\1\u0099",
            "\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u009a",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009c",
            "\1\u009d",
            "\1\142\1\143\11\uffff\1\144",
            "\1\145\1\146\11\uffff\1\147",
            "\1\u009e",
            "\1\150\1\151\11\uffff\1\152",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\161\1\162\23\uffff\1\u00a3\1\u00a4\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa\1\u00ab\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00ac\1\u00ad\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\65\1\66\23\uffff\1\175\1\176\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\70\1\71\4\uffff\1\100\1\101\1\102\1\103\1\104\1\105\1\72\1\73\1\74\1\75\1\76\1\77\1\106\1\107\1\110\1\u0080\1\u0081\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\161\1\162\23\uffff\1\u0082\1\u0083\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00ae",
            "\1\u00ae",
            "\1\u0087\1\u0088\11\uffff\1\u0089",
            "\1\u008a\1\u008b\11\uffff\1\u008c",
            "\1\u00af",
            "\1\u008d\1\u008e\11\uffff\1\u008f",
            "\1\u00b0",
            "\1\u00b1",
            "\1\u00b1",
            "\1\u00b2",
            "\1\u00b2",
            "\1\u00b3",
            "\1\u00b4\1\u00b5\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00b6\1\u00b7\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00b8",
            "\1\u00b9",
            "\1\161\1\162\23\uffff\1\u00a3\1\u00a4\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00ba",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bb",
            "\1\u00aa\1\u00ab\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00ac\1\u00ad\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00b4\1\u00b5\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32",
            "\1\u00b6\1\u00b7\3\34\3\uffff\1\34\2\uffff\1\34\1\33\1\32"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "1056:1: rule__OrExpr__Alternatives : ( ( ( rule__OrExpr__Group_0__0 ) ) | ( ( rule__OrExpr__Group_1__0 ) ) | ( ( rule__OrExpr__Group_2__0 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x004F188000001060L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000042000000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000C00000000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000F000000000060L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00000003FFF80000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0002000000000060L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000018002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000C00000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000C00000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0080000000000020L});

}